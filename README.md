# Vivern – A Virtual Environment for Multiscale Visualization and Modeling of DNA Nanostructures

This repository contains source code and other related resources as described in the research paper *Vivern – A Virtual Environment for Multiscale Visualization and Modeling of DNA Nanostructures* published in *IEEE Transactions on Visualization and Computer Graphics*.

<!-- **DOI:** [10.1109/TVCG.2021.3106328](https://ieeexplore.ieee.org/document/9523759) -->
<!-- **Screenshots:** To be added -->

**Contents of the repository:** 
- Unity project 
    - version 2019.4.29f1
    - To make it compile, you will need to provide TotalJSON Unity Asset (see README in Assets\_ThirdParty\TotalJSON) and Wispy Skybox Unity Asset (see README in Assets\Addons\Wispy Sky).

# Citation
In case of using Vivern (or parts of it) in your research, please cite: D. Kutak et al., "Vivern A Virtual Environment for Multiscale Visualization and Modeling of DNA Nanostructures," in IEEE Transactions on Visualization and Computer Graphics, doi: [10.1109/TVCG.2021.3106328](https://ieeexplore.ieee.org/document/9523759).  

Anyway, feel free to contact us at kutak[at]mail.muni.cz. We will be glad to hear from you. :-)


# UVC – [U]nity [V]isualization [C]ore 
  
UVC repository contains the basic framework to simplify (mainly molecular) visualization in Unity.  
It does not contain Unity project but rather a set of useful assets.  
Unity projects using UVC should be using one of the latest versions of the Unity to use up-to-date features.
  
# README

**If you want to use UVC:**

*  it is **HIGHLY RECOMMENDED** to [request invite to UVC Trello board](https://trello.com/invite/b/dOLmGKnW/2f43c4f706ee7683b5be257c8e58e363/uvc-unity-visualization-core) which serves as a bug/feature tracker.   

*  then, add UVC as a submodule into your repository, i.e., .gitmodules file might contain something like this afterwards:
  
*[submodule "UVC"]  
path = Assets/UVC  
url = https://gitlab.fi.muni.cz/xkutak/uvc   
branch = master*  

**If you want to add something to the repository:**

* make sure it is error-free and storage-effective (i.e., do not upload useless big files)

* choose appropriate directory or create a new one with a meaningful name. Use a good name also for the item you are adding.

* ensure that the item can be easily used by others (e.g, prefab or script). If not possible, provide sufficient documentation/how-to guide.

* in case of adding scripts, (try to) use UVC namespace.

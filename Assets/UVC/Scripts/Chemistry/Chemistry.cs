﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;
using UVC.Utils;

namespace UVC
{
    /// <summary>
    /// Represents set of known data about chemical elements, residues, chains and sites.
    /// The class is inspired by Chemistry from CAVER (release 2).
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public static class Chemistry
    {
        private readonly static string resourcesRelativePath = "Chemistry/";

        /// <summary>
        /// Equals to true if the data are correctly initialized and ready to use
        /// </summary>
        public static bool Initialized { get; private set; }

        /*
         * Atom types
         */

        private static Dictionary<string, AtomDataType> atomTypesByIdentifier
            = new Dictionary<string, AtomDataType>();

        private static Dictionary<int, AtomDataType> atomTypesByAtomicNumber
            = new Dictionary<int, AtomDataType>();

        /*
         * Residue types
         */

        private static Dictionary<string, ResidueDataType> residueTypesByIdentifier
            = new Dictionary<string, ResidueDataType>();

        private static Dictionary<int, ResidueDataType> residueTypesById
            = new Dictionary<int, ResidueDataType>();

        /*
         * Secondary structure types
         */

        private static Dictionary<string, SecondaryStructureDataType> secondaryStructureTypesByIdentifier
            = new Dictionary<string, SecondaryStructureDataType>();

        private static Dictionary<int, SecondaryStructureDataType> secondaryStructureTypesById
            = new Dictionary<int, SecondaryStructureDataType>();

        /// <summary>
        /// Elements' VDW radii
        /// Indices correspond to atomic numbers
        /// </summary>
        private static float[] atomElementVDWRadii;

        /// <summary>
        /// Elements' covalent radii
        /// Indices correspond to atomic numbers
        /// </summary>
        private static float[] atomElementCovalentRadii;

        /// <summary>
        /// Residue partial charges standard N-terminus (chain start)
        /// </summary>
        private static Dictionary<string, float> nTerminus
            = new Dictionary<string, float>();

        /// <summary>
        /// Residue partial charges standard C-terminus (chain end)
        /// </summary>
        private static Dictionary<string, float> cTerminus
             = new Dictionary<string, float>();

        /// <summary>
        /// Residue hydrogen bonds angles
        /// </summary>
        private static Dictionary<string, float> hydrogenBondAngles
            = new Dictionary<string, float>();

        /// <summary>
        /// Residue chi angle counts
        /// </summary>
        private static Dictionary<string, int> chiAngleCounts
            = new Dictionary<string, int>();

        /// <summary>
        /// Residue multiple bonds
        /// </summary>
        private static Dictionary<int, List<string[]>> multipleBonds
             = new Dictionary<int, List<string[]>>();

        // RuntimeInitializeLoadType.BeforeSceneLoad is used instead of RuntimeInitializeLoadType.BeforeSplashScreen
        // because the latter one does not work in editor. Also, since I would like to have the same behavior in editor and
        // build, no macros are used to have different load type for editor and build (which might provide the "best solution"
        // without Initialized flag but this solution would not be consistent which I would like to avoid).
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Initialize()
        {
            if (Initialized) { return; }

            try
            {
                LoadData();
                LoadAtomRadii();
                Initialized = true;

                UVCLogger.Instance.LogInfo("Chemistry initialized.");
            }
            catch (System.Exception e)
            {
                UVCLogger.Instance.LogError("Initialization of Chemistry failed: " + e.Message);
            }
        }

        /// <summary>
        /// Loads data from XML files into internal structures
        /// Precondition: if there are more XML files providing various information about the same structure (e.g. atoms)
        /// it is expected that i-th element of all of these files refers to the same atom/residue/...
        /// </summary>
        private static void LoadData()
        {
            LoadAtomDataXml("atomTypes", "elementCovalentRadii", "atomVDWRadii");
            LoadResidueDataXml("residueTypes", "residuePartialCharges");
            LoadAngleDataXml("forceFieldBondAngles", "dihedralChiAngles");
            LoadSecondaryStructureDataXml("secondaryStructureTypes");
            LoadBondsDataXml("aminoAcidBonds");
        }

        private static string GetXmlFileContent(string fileName)
        {
            return Resources.Load<TextAsset>(resourcesRelativePath + fileName).text;
        }

        private static void LoadAtomDataXml(string atomTypesFilename,
            string elementCovalentRadiiFilename, string atomVDWRadiiFilename)
        {
            var atomTypes = new XmlDocument();
            var elementCovalentRadii = new XmlDocument();
            var atomVDWRadii = new XmlDocument();

            atomTypes.LoadXml(GetXmlFileContent(atomTypesFilename));
            elementCovalentRadii.LoadXml(GetXmlFileContent(elementCovalentRadiiFilename));
            atomVDWRadii.LoadXml(GetXmlFileContent(atomVDWRadiiFilename));

            var atomTypesNodes = atomTypes?.DocumentElement?.GetElementsByTagName("atom");
            var elementCovalentRadiiNodes = elementCovalentRadii?.DocumentElement?.GetElementsByTagName("covalent");
            var atomVDWRadiiNodes = atomVDWRadii?.DocumentElement?.GetElementsByTagName("vdw");

            if (atomTypesNodes == null || elementCovalentRadiiNodes == null || atomVDWRadiiNodes == null ||
                atomTypesNodes.Count != elementCovalentRadiiNodes.Count || atomTypesNodes.Count != atomVDWRadiiNodes.Count)
            {
                throw new XmlException("Incorrect data in XML files with atom data.");
            }

            for (int i = 0; i < atomTypesNodes.Count; ++i)
            {
                var atNode = atomTypesNodes[i];
                var ecrNode = elementCovalentRadiiNodes[i];
                var avdwrNode = atomVDWRadiiNodes[i];

                AddAtomType(new AtomDataType(
                            atomicNumber: System.Convert.ToByte(atNode.Attributes.GetNamedItem("number").Value),
                            elementIdentifier: atNode.Attributes.GetNamedItem("identifier").Value,
                            elementName: atNode.Attributes.GetNamedItem("name").Value,
                            vdwRadius: System.Convert.ToSingle(avdwrNode.Attributes.GetNamedItem("radius").Value, System.Globalization.CultureInfo.InvariantCulture),
                            covalentRadius: System.Convert.ToSingle(ecrNode.Attributes.GetNamedItem("radius").Value, System.Globalization.CultureInfo.InvariantCulture),
                            electronegativity: System.Convert.ToSingle(atNode.Attributes.GetNamedItem("electronegativity").Value, System.Globalization.CultureInfo.InvariantCulture),
                            valenceElectrons: System.Convert.ToByte(atNode.Attributes.GetNamedItem("valenceElectrons").Value)
                            ));
            }
        }

        private static void LoadResidueDataXml(string residueTypesFilename, string residuePartialChargesFilename)
        {
            var residueTypes = new XmlDocument();
            var residuePartialCharges = new XmlDocument();

            residueTypes.LoadXml(GetXmlFileContent(residueTypesFilename));
            residuePartialCharges.LoadXml(GetXmlFileContent(residuePartialChargesFilename));

            var residueTypesNodes = residueTypes?.DocumentElement?.GetElementsByTagName("residue");
            var residuePartialChargesNodes = residuePartialCharges?
                .DocumentElement?.ChildNodes?.GetElementsWithAnyOfTagNames("residue", "nterminus", "cterminus");

            if (residueTypesNodes == null || residuePartialChargesNodes == null)
            {
                throw new XmlException("Incorrect data in XML files with residue types.");
            }

            // Process residue types as a first step

            for (int i = 0; i < residueTypesNodes.Count; ++i)
            {
                var resNode = residueTypesNodes[i];

                AddResidueType(new ResidueDataType(
                        id: System.Convert.ToByte(resNode.Attributes.GetNamedItem("id").Value),
                        type: ParseResidueType(resNode.Attributes.GetNamedItem("type").Value),
                        identifier: resNode.Attributes.GetNamedItem("identifier").Value,
                        shortcut: resNode.Attributes.GetNamedItem("shortcut").Value,
                        name: resNode.Attributes.GetNamedItem("name").Value,
                        hydropathy: System.Convert.ToSingle(resNode.Attributes.GetNamedItem("hydropathy").Value, System.Globalization.CultureInfo.InvariantCulture),
                        hydrophobicity: System.Convert.ToSingle(resNode.Attributes.GetNamedItem("hydrophobicity").Value, System.Globalization.CultureInfo.InvariantCulture),
                        donors: resNode.Attributes.GetNamedItem("donors").Value.Split(',').ToList(),
                        acceptors: resNode.Attributes.GetNamedItem("acceptors").Value.Split(',').ToList(),
                        charge: ParseResidueCharge(resNode.Attributes.GetNamedItem("charge")?.Value)
                        ));
            }

            // Process residue partial charges (this XML contains nested nodes, etc.)

            for (int i = 0; i < residuePartialChargesNodes.Count; ++i)
            {
                var resNode = residuePartialChargesNodes[i];
                Dictionary<string, float> partialCharges = new Dictionary<string, float>();

                var atomNodes = resNode?.ChildNodes?.GetElementsWithAnyOfTagNames("atom");

                if (atomNodes == null)
                {
                    throw new XmlException("Incorrect data in XML file with residue partial charges.");
                }

                for (int j = 0; j < atomNodes.Count; ++j)
                {
                    var atomNode = atomNodes[j];

                    var atomName = atomNode.Attributes.GetNamedItem("name").Value;
                    var atomCharge = System.Convert.ToSingle(atomNode.Attributes.GetNamedItem("charge").Value, System.Globalization.CultureInfo.InvariantCulture);

                    try
                    {
                        if (resNode.Name == "residue")
                        {
                            partialCharges.Add(atomName, atomCharge);

                        }
                        else if (resNode.Name == "nterminus")
                        {
                            nTerminus.Add(atomName, atomCharge);
                        }
                        else if (resNode.Name == "cterminus")
                        {
                            cTerminus.Add(atomName, atomCharge);
                        }
                    }
                    catch (System.ArgumentException exc)
                    {
                        UVCLogger.Instance.LogWarning("Duplicate key in " + residuePartialChargesFilename + ".xml: " + exc.Message);
                    }
                }

                if (resNode.Name == "residue")
                {
                    GetResidueType(resNode.Attributes.GetNamedItem("identifier").Value).SetPartialCharges(partialCharges);
                }
            }
        }

        private static void LoadAngleDataXml(string forceFieldBondAnglesFilename, string dihedralChiAnglesFilename)
        {
            var forceFieldBondAngles = new XmlDocument();
            var dihedralChiAngles = new XmlDocument();

            forceFieldBondAngles.LoadXml(GetXmlFileContent(forceFieldBondAnglesFilename));
            dihedralChiAngles.LoadXml(GetXmlFileContent(dihedralChiAnglesFilename));

            var forceFieldBondAnglesNodes = forceFieldBondAngles?.DocumentElement?.GetElementsByTagName("angle");
            var dihedralChiAnglesNodes = dihedralChiAngles?.DocumentElement?.GetElementsByTagName("chi");

            if (forceFieldBondAnglesNodes == null || dihedralChiAnglesNodes == null)
            {
                throw new XmlException("Incorrect data in XML files with angle information.");
            }

            for (int i = 0; i < forceFieldBondAnglesNodes.Count; ++i)
            {
                var ffAngle = forceFieldBondAnglesNodes[i];

                try
                {
                    hydrogenBondAngles.Add(
                            ffAngle.Attributes.GetNamedItem("location").Value,
                            System.Convert.ToSingle(ffAngle.Attributes.GetNamedItem("angle").Value, System.Globalization.CultureInfo.InvariantCulture)
                            );
                }
                catch (System.ArgumentException exc)
                {
                    UVCLogger.Instance.LogWarning("Duplicate key in " + forceFieldBondAnglesFilename + ".xml: " + exc.Message);
                }
            }

            for (int i = 0; i < dihedralChiAnglesNodes.Count; ++i)
            {
                var dchAngle = dihedralChiAnglesNodes[i];

                try
                {
                    chiAngleCounts.Add(
                            dchAngle.Attributes.GetNamedItem("residue").Value,
                            int.Parse(dchAngle.Attributes.GetNamedItem("count").Value)
                            );
                }
                catch (System.ArgumentException exc)
                {
                    UVCLogger.Instance.LogWarning("Duplicate key in " + dihedralChiAnglesFilename + ".xml: " + exc.Message);
                }
            }
        }

        private static void LoadSecondaryStructureDataXml(string secondaryStructureTypesFilename)
        {
            var secondaryStructures = new XmlDocument();

            secondaryStructures.LoadXml(GetXmlFileContent(secondaryStructureTypesFilename));

            var secondaryStructuresNodes = secondaryStructures?.DocumentElement?.GetElementsByTagName("ss");

            if (secondaryStructuresNodes == null)
            {
                throw new XmlException("Incorrect data in XML file with secondary structures.");
            }

            for (int i = 0; i < secondaryStructuresNodes.Count; ++i)
            {
                var ssNode = secondaryStructuresNodes[i];

                AddSecondaryStructureType(new SecondaryStructureDataType(
                        id: int.Parse(ssNode.Attributes.GetNamedItem("id").Value),
                        identifier: ssNode.Attributes.GetNamedItem("name").Value
                        ));
            }
        }

        private static void LoadBondsDataXml(string atomBondsFilename)
        {
            var bondsData = new XmlDocument();

            bondsData.LoadXml(GetXmlFileContent(atomBondsFilename));

            var bondsDataNodes = bondsData?.DocumentElement?.GetElementsByTagName("aminoAcid");

            if (bondsDataNodes == null)
            {
                throw new XmlException("Incorrect data in XML files with residue types.");
            }

            for (int i = 0; i < bondsDataNodes.Count; ++i)
            {
                var aminoAcidNode = bondsDataNodes[i];

                List<string[]> bonds = new List<string[]>();
                int aminoAcidId = int.Parse(aminoAcidNode.Attributes.GetNamedItem("id").Value);

                var bondNodes = aminoAcidNode.ChildNodes.GetElementsWithAnyOfTagNames("bond");
                for (int j = 0; j < bondNodes.Count; ++j)
                {
                    var bondNode = bondNodes[j];
                    var atomNodes = bondNode.ChildNodes.GetElementsWithAnyOfTagNames("atom");

                    if (atomNodes.Count == 2)
                    {
                        bonds.Add(new string[]{
                                    atomNodes[0].Attributes.GetNamedItem("name").Value,
                                    atomNodes[1].Attributes.GetNamedItem("name").Value,
                                    bondNode.Attributes.GetNamedItem("arity").Value
                                });
                    }
                    else
                    {
                        throw new XmlException("Invalid number of atoms in a bond record:" + atomBondsFilename + ".xml");
                    }
                }

                try
                {
                    multipleBonds.Add(aminoAcidId, bonds);
                }
                catch (System.ArgumentException exc)
                {
                    UVCLogger.Instance.LogWarning("Duplicate key in " + atomBondsFilename + ".xml: " + exc.Message);
                }
            }
        }

        private static void LoadAtomRadii()
        {
            // Index of the item in array corresponds to its atomic number
            // Index 0 is therefore unused
            atomElementVDWRadii = new float[atomTypesByAtomicNumber.Count + 1];
            atomElementCovalentRadii = new float[atomTypesByAtomicNumber.Count + 1];

            for (int an = 1; an < atomElementVDWRadii.Length; ++an)
            {
                var at = GetAtomType(an);

                atomElementVDWRadii[an] = at.VDWRadius;
                atomElementCovalentRadii[an] = at.CovalentRadius;
            }
        }

        private static void AddAtomType(AtomDataType atomData)
        {
            atomTypesByIdentifier.Add(atomData.ElementIdentifier, atomData);
            atomTypesByAtomicNumber.Add(atomData.AtomicNumber, atomData);
        }

        private static void AddResidueType(ResidueDataType residueData)
        {
            residueTypesByIdentifier.Add(residueData.ElementIdentifier, residueData);
            residueTypesById.Add(residueData.Id, residueData);
        }

        private static void AddSecondaryStructureType(SecondaryStructureDataType ssData)
        {
            secondaryStructureTypesByIdentifier.Add(ssData.ElementIdentifier, ssData);
            secondaryStructureTypesById.Add(ssData.Id, ssData);
        }

        private static ResidueDataType.ResType ParseResidueType(string type)
        {
            switch (type)
            {
                case "aa_20": return ResidueDataType.ResType.AminoAcidBase20;
                case "aa_ext": return ResidueDataType.ResType.AminoAcidExtra;
                case "nb": return ResidueDataType.ResType.Nucleobase;
                case "sol": return ResidueDataType.ResType.Solvent;
                case "lig": return ResidueDataType.ResType.Ligand;
            }

            UVCLogger.Instance.LogWarning("Found unknown residue type '" + type + "'. Residue will be considered as ligand.");
            return ResidueDataType.ResType.Ligand;
        }

        // If residue has no information about charge, "type" parameter should be null
        // and the residue will be considered as having neutral charge
        private static ResidueDataType.ResCharge ParseResidueCharge(string type)
        {
            if (type != null)
            {
                switch (type)
                {
                    case "+": return ResidueDataType.ResCharge.Positive;
                    case "-": return ResidueDataType.ResCharge.Negative;
                    default:
                        UVCLogger.Instance.LogWarning("Found unknown residue type '" + type +
                            "'. Residue will be considered as having neutral charge.");
                        break;
                }
            }

            return ResidueDataType.ResCharge.Neutral;
        }

        /// <summary>
        /// Gets atom's element VDW radius
        /// 
        /// NOTE: In CAVER, this function receives Atom structure as an input argument
        /// which allows to return correct VDW radius in case that the
        /// atom has its own custom radius instead of default one.
        /// This functionality was not preserved since I would like to keep
        /// this class separated from non-related structures and return
        /// only the "default" data from XML files; any modifications should be
        /// handled by higher-level classes.
        /// </summary>
        /// <param name="atomicNumber">Atomic number of element</param>
        /// <returns>VDW radius of the element</returns>
        public static float GetElementVDWRadius(int atomicNumber)
        {
            return atomElementVDWRadii[atomicNumber];
        }

        /// <summary>
        /// Returns atom's element covalent radius
        /// </summary>
        /// <param name="atomicNumber">Atomic number of element</param>
        /// <returns>Covalent radius of the element</returns>
        public static float GetElementCovalentRadius(int atomicNumber)
        {
            return atomElementCovalentRadii[atomicNumber];
        }

        /// <summary>
        /// Returns all known atom element identifiers
        /// </summary>
        /// <returns>Known atom element identifiers</returns>
        public static HashSet<string> GetElementIdentifiers()
        {
            return new HashSet<string>(atomTypesByIdentifier.Keys);
        }

        /// <summary>
        /// Gets atom element type based on given atom element identifier
        /// </summary>
        /// <param name="atomElementIdentifier">element identifier (1-2 letters)</param>
        /// <returns>atom type</returns>
        public static AtomDataType GetAtomType(string atomElementIdentifier)
        {
            return atomTypesByIdentifier.GetValueOrDefault(atomElementIdentifier);
        }

        /// <summary>
        /// Gets atom element type based on given atomic number
        /// </summary>
        /// <param name="atomElementIdentifier">atomic number (from periodic table of elements)</param>
        /// <returns>atom type</returns>
        public static AtomDataType GetAtomType(int atomicNumber)
        {
            return atomTypesByAtomicNumber.GetValueOrDefault(atomicNumber);
        }

        /// <summary>
        /// Returns known atom elements as a list
        /// </summary>
        /// <returns>List of know atom elements</returns>
        public static List<AtomDataType> GetAtomTypes()
        {
            return atomTypesByAtomicNumber.Values.ToList();
        }

        /// <summary>
        /// Verifies, if atom element of given identifier exists
        /// </summary>
        /// <param name="elementIdentifier">identifier of an atom element (1-2 characters)</param>
        /// <returns>true if such an atom element exists, false otherwise</returns>
        public static bool VerifyAtomElementExistence(string elementIdentifier)
        {
            return atomTypesByIdentifier.ContainsKey(elementIdentifier);
        }

        /// <summary>
        /// Gets a single residue type based on given residue element identifier
        /// </summary>
        /// <param name="residueIdentifier">residue element identifier, usually 1-3 letters</param>
        /// <returns>residue type</returns>
        public static ResidueDataType GetResidueType(string residueIdentifier)
        {
            return residueTypesByIdentifier.GetValueOrDefault(residueIdentifier);
        }

        /// <summary>
        /// Gets a single residue type based on given residue type id
        /// </summary>
        /// <param name="residueIdentifier">residue ids</param>
        /// <returns>residue type</returns>
        public static ResidueDataType GetResidueType(int id)
        {
            return residueTypesById.GetValueOrDefault(id);
        }

        /// <summary>
        /// Verifies, if residue of given identifier exists
        /// </summary>
        /// <param name="elementIdentifier">identifier of a residue (3 characters)</param>
        /// <returns>true if such a residue exists, false otherwise</returns>
        public static bool VerifyResidueExistence(string residueIdentifier)
        {
            return residueTypesByIdentifier.ContainsKey(residueIdentifier);
        }

        /// <summary>
        /// Returns all known residue types
        /// </summary>
        /// <returns>All known residue types</returns>
        public static List<ResidueDataType> GetResidueTypes()
        {
            return residueTypesById.Values.ToList();
        }

        /// <summary>
        /// Returns number of known residues
        /// </summary>
        /// <returns>Number of known residues</returns>
        public static int GetKnownResiduesCount()
        {
            return residueTypesByIdentifier.Count;
        }

        /// <summary>
        /// Returns dictionary of known secondary structure types
        /// </summary>
        /// <returns>Dictionary of known ss types where key = id, value = ss type</returns>
        public static Dictionary<int, SecondaryStructureDataType> GetSecondaryStructureTypes()
        {
            return secondaryStructureTypesById;
        }

        /// <summary>
        /// Gets a single secondary structure type based on given name
        /// </summary>
        /// <param name="secondaryStructureIdentifier">identifier of the secondary structure</param>
        /// <returns>secondary structure type</returns>
        public static SecondaryStructureDataType GetSecondaryStructureType(string secondaryStructureIdentifier)
        {
            return secondaryStructureTypesByIdentifier.GetValueOrDefault(secondaryStructureIdentifier);
        }

        /// <summary>
        /// Gets a single secondary structure type based on given id
        /// </summary>
        /// <param name="secondaryStructureId">id of the secondary structure</param>
        /// <returns>secondary structure type</returns>
        public static SecondaryStructureDataType GetSecondaryStructureType(int secondaryStructureId)
        {
            return secondaryStructureTypesById.GetValueOrDefault(secondaryStructureId);
        }

        /// <summary>
        /// Gets records of residue partial charges at N-terminus
        /// </summary>
        /// <returns>Records of residue partial charges at N-terminus</returns>
        public static Dictionary<string, float> GetNTerminus()
        {
            return nTerminus;
        }

        /// <summary>
        /// Gets records of residue partial charges at C-terminus
        /// </summary>
        /// <returns>Records of residue partial charges at C-terminus</returns>
        public static Dictionary<string, float> GetCTerminus()
        {
            return cTerminus;
        }

        /// <summary>
        /// Gets hydrogen bond angles
        /// </summary>
        /// <returns>Hydrogen bond angles</returns>
        public static Dictionary<string, float> GetHBondsAngles()
        {
            return hydrogenBondAngles;
        }

        /// <summary>
        /// Gets chi angle counts
        /// </summary>
        /// <returns>Chi angle counts</returns>
        public static Dictionary<string, int> GetChiAngleCounts()
        {
            return chiAngleCounts;
        }

        /// <summary>
        /// Returns dictionary of multiple bonds in known amino acid residues
        /// </summary>
        /// <returns>Dictionary of multiple bonds in known amino acid residues where key = residue id</returns>
        public static Dictionary<int, List<string[]>> GetMultipleBonds()
        {
            return multipleBonds;
        }

        /// <summary>
        /// Returns list of multiple bonds for given residue
        /// </summary>
        /// <param name="residueId">Residue id</param>
        /// <returns>Dictionary of multiple bonds in given residue</returns>
        public static List<string[]> GetMultipleBonds(int residueId)
        {
            return multipleBonds.GetValueOrDefault(residueId);
        }
    }
}

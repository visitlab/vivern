﻿using System;

namespace UVC
{
    /// <summary>
    /// Represents a data of single element from periodic table of elements.
    /// The class is inspired by AtomType from CAVER (release 2).
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public class AtomDataType : ChemistryDataType, IComparable<AtomDataType>
    {
        /// <summary>
        /// Full element name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Van der Waals radius of the element
        /// </summary>
        public float VDWRadius { get; set; }

        /// <summary>
        /// Element covalent radius
        /// </summary>
        public float CovalentRadius { get; set; }

        /// <summary>
        /// Electronegativity of the element
        /// </summary>
        public float Electronegativity { get; set; }

        /// <summary>
        /// Number of valence electrons
        /// </summary>
        public byte ValenceElectrons { get; set; }

        public int AtomicNumber
        {
            get
            {
                return Id;
            }
            set
            {
                Id = value;
            }
        }

        /// <summary>
        /// Creates an atom element type from given data
        /// </summary>
        /// <param name="atomicNumber">element atomic number</param>
        /// <param name="elementIdentifier">element name (1-2 characters usually)</param>
        /// <param name="elementName">full element name</param>
        /// <param name="vdwRadius">Van der Waals radius</param>
        /// <param name="covalentRadius">Covalent radius</param>
        /// <param name="electronegativity">Electronegativity of the element</param>
        /// <param name="valenceElectrons">Number of valence electrons</param>
        public AtomDataType(byte atomicNumber, string elementIdentifier,
            string elementName, float vdwRadius,
            float covalentRadius, float electronegativity,
            byte valenceElectrons)
            : base(atomicNumber, elementIdentifier)
        {
            Name = elementName;
            VDWRadius = vdwRadius;
            CovalentRadius = covalentRadius;
            Electronegativity = electronegativity;
            ValenceElectrons = valenceElectrons;
        }

        public override string ToString()
        {
            return AtomicNumber + " " + ElementIdentifier + " " + Name + " (" + VDWRadius.ToString() + "; " + CovalentRadius.ToString() + ")";
        }

        public int CompareTo(AtomDataType other)
        {
            return AtomicNumber - other.AtomicNumber;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UVC
{
    /// <summary>
    /// Immutable representation of a single known type of a residue.
    /// The class is inspired by ResidueType from CAVER (release 2).
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public class ResidueDataType : ChemistryDataType, IComparable<ResidueDataType>
    {
        public enum ResType
        {
            AminoAcidBase20, AminoAcidExtra, Nucleobase, Solvent, Ligand
        }

        public enum ResCharge
        {
            Negative, Neutral, Positive
        }

        /// <summary>
        /// Shortcut representation of the residue
        /// </summary>
        public string Shortcut { get; }

        /// <summary>
        /// Full name of the residue
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The residue hydropathy
        /// </summary>
        public float Hydropathy { get; }

        /// <summary>
        /// The residue hydrophobicity
        /// </summary>
        public float Hydrophobicity { get; }

        /// <summary>
        /// List containing residue donors - atoms (table values)
        /// </summary>
        private List<string> _donors;

        /// <summary>
        /// Collection containing residue donors - atoms (table values)
        /// </summary>
        public ReadOnlyCollection<string> Donors
        {
            get
            {
                return _donors.AsReadOnly();
            }
        }

        /// <summary>
        /// List containing residue acceptors - atoms (table values)
        /// </summary>
        private List<string> _acceptors;

        /// <summary>
        /// Collection containing residue acceptors - atoms (table values)
        /// </summary>
        public ReadOnlyCollection<string> Acceptors
        {
            get
            {
                return _acceptors.AsReadOnly();
            }
        }

        /// <summary>
        /// Map of partial charges of atoms taken from CHARMM22
        /// </summary>
        private Dictionary<string, float> mPartialCharges;

        /// <summary>
        /// Side chain charge
        /// </summary>
        public ResCharge Charge { get; }

        /// <summary>
        /// Biochemical type of residue
        /// </summary>
        public ResType Type { get; }

        /// <summary>
        /// Creates a residue data type from given data
        /// </summary>
        /// <param name="id">internal ID of the residue in the (residueTypes.)xml file</param>
        /// <param name="type">biochemical type of a residue</param>
        /// <param name="identifier">three-letter name of the residue</param>
        /// <param name="shortcut">one-letter ID of the residue</param>
        /// <param name="name">full residue name</param>
        /// <param name="hydropathy">The residue hydropathy</param>
        /// <param name="hydrophobicity">The residue hydrophobicity</param>
        /// <param name="donors">List containing residue donors</param>
        /// <param name="acceptors">List containing residue acceptors</param>
        /// <param name="charge">Side chain charge</param>
        public ResidueDataType(int id, ResType type, string identifier,
            string shortcut, string name, float hydropathy,
            float hydrophobicity, List<string> donors,
            List<string> acceptors, ResCharge charge)
            : base(id, identifier)
        {
            Type = type;
            Shortcut = shortcut;
            Name = name;
            Hydropathy = hydropathy;
            Hydrophobicity = hydrophobicity;
            _donors = donors;
            _acceptors = acceptors;
            Charge = charge;
        }

        /// <summary>
        /// Returns atom's partial charge based on its name
        /// </summary>
        /// <param name="atomName">Name of the atom</param>
        /// <returns>Atom's partial charge or float.MaxValue if atomName is not found</returns>
        public float GetPartialCharge(string atomName)
        {
            return mPartialCharges.GetValueOrDefault(atomName, float.MaxValue);
        }

        public void SetPartialCharges(Dictionary<string, float> partialCharges)
        {
            mPartialCharges = new Dictionary<string, float>(partialCharges);
        }

        /// <summary>
        /// Tests whether the residue is amino acid
        /// </summary>
        /// <returns>true if residue is amino acid, false otherwise</returns>
        public bool IsAminoAcid()
        {
            return Type == ResType.AminoAcidBase20 || Type == ResType.AminoAcidExtra;
        }

        /// <summary>
        /// Tests whether the residue is one of the base 20 amino acids
        /// </summary>
        /// <returns>true if residue is base amino acid, false otherwise</returns>
        public bool IsBase20AminoAcid()
        {
            return Type == ResType.AminoAcidBase20;
        }

        /// <summary>
        /// Tests whether residue is one of the explicitly protonated amino acids.
        /// </summary>
        /// <returns>true if residue is one of the explicitly protonated amino acids, false otherwise</returns>
        public bool IsExplicitlyProtonatedAminoAcid()
        {
            return Type == ResType.AminoAcidExtra;
        }

        /// <summary>
        /// Tests whether the residue is nucleobase
        /// </summary>
        /// <returns>True if residue is nucleobase, false otherwise</returns>
        public bool IsNucleobase()
        {
            return Type == ResType.Nucleobase;
        }

        /// <summary>
        /// Tests whether the residue is solvent
        /// </summary>
        /// <returns>True if residue is solvent, false otherwise</returns>
        public bool IsSolvent()
        {
            return Type == ResType.Solvent;
        }

        /// <summary>
        /// Tests whether the residue is ligand
        /// </summary>
        /// <returns>True if residue is ligand, false otherwise</returns>
        public bool IsLigand()
        {
            return Type == ResType.Ligand;
        }

        public override string ToString()
        {
            return ElementIdentifier + " (" + Shortcut + ") " + Name;
        }

        public int CompareTo(ResidueDataType other)
        {
            return ElementIdentifier.CompareTo(other.ElementIdentifier);
        }
    }
}

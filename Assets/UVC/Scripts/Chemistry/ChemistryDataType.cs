﻿namespace UVC
{
    /// <summary>
    /// Abstract class representing Chemistry data element.
    /// The class is inspired by ChemistryDataType from CAVER (release 2).
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public abstract class ChemistryDataType
    {
        /// <summary>
        /// Element unique number identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Element string identifier (not guaranted to be unique)
        /// </summary>
        public string ElementIdentifier { get; set; }

        public ChemistryDataType(int id, string elementIdentifier)
        {
            Id = id;
            ElementIdentifier = elementIdentifier;
        }
    }
}

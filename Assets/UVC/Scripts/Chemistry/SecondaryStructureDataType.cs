﻿using System;

namespace UVC
{
    /// <summary>
    /// Representation of a single known type of secondary structure.
    /// The class is inspired by SecondaryStructureType from CAVER (release 2).
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public class SecondaryStructureDataType : ChemistryDataType, IComparable<SecondaryStructureDataType>
    {
        /// <summary>
        /// Creates a new secondary structure type
        /// </summary>
        /// <param name="id">identifier of the secondary structure (1 - helix, 2 - sheet, 3 - coil)</param>
        /// <param name="identifier">name of the secondary structure part (helix, sheet, coil)</param>
        public SecondaryStructureDataType(int id, string identifier)
            : base(id, identifier)
        { }

        public override string ToString()
        {
            return ElementIdentifier + "; " + Id;
        }

        public int CompareTo(SecondaryStructureDataType other)
        {
            return Id - other.Id;
        }
    }
}

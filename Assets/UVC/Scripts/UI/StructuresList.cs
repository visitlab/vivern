﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UVC.UnityWrappers
{
    public class StructuresList : MonoBehaviour
    {
        [SerializeField]
        private Transform destinationParent;

        [SerializeField]
        private GameObject itemPrefab;

        private Dictionary<int, GameObject> sgosToItems
            = new Dictionary<int, GameObject>();

        public void AddStructureGo(GameObject structureGo)
        {
            var newItem = Instantiate(itemPrefab, destinationParent);
            newItem.SetActive(true);
            newItem.GetComponent<TMP_Text>().text = structureGo.name;

            sgosToItems.Add(structureGo.GetInstanceID(), newItem);
        }

        public void RemoveStructureGo(GameObject structureGo)
        {
            GameObject uiItem;

            if (sgosToItems.TryGetValue(structureGo.GetInstanceID(), out uiItem))
            {
                sgosToItems.Remove(structureGo.GetInstanceID());
                Destroy(uiItem);
            }
        }
    }
}

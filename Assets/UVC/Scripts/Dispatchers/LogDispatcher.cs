﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UVC.Utils;

namespace UVC.UnityWrappers
{
    public class LogDispatcher : MonoBehaviour
    {
        public static LogDispatcher Instance { get; private set; }

        private static readonly object loggingLock = new object();

        private Queue<UVCLogger.LogRecord> logsQueue = new Queue<UVCLogger.LogRecord>();
        private WaitForSeconds logsOutputDelay = new WaitForSeconds(0.1f);
        private const int maxLogsPerOutputIteration = 16;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            UVCLogger.Instance.NewLogAdded += Log;
            StartCoroutine(OutputLogs());
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        private IEnumerator OutputLogs()
        {
            while (true)
            {
                for (int maxLogsRemaining = maxLogsPerOutputIteration;
                    logsQueue.Count > 0 && maxLogsRemaining > 0; --maxLogsRemaining)
                {
                    var currLog = logsQueue.Dequeue();

                    switch (currLog.Type)
                    {
                        case UVCLogger.LogType.Error:
                            Debug.LogError(currLog.ToString());
                            break;
                        case UVCLogger.LogType.Warning:
                            Debug.LogWarning(currLog.ToString());
                            break;
                        default:
                            Debug.Log(currLog.ToString());
                            break;
                    }
                }

                yield return logsOutputDelay;
            }
        }

        private void Log(UVCLogger.LogRecord logRecord)
        {
            lock (loggingLock)
            {
                logsQueue.Enqueue(logRecord);
            }
        }

        public void Log(UVCLogger.LogType type, string message,
                        [CallerFilePath] string file = "",
                        [CallerMemberName] string member = "",
                        [CallerLineNumber] int line = 0)
        {
            Log(new UVCLogger.LogRecord
            {
                Time = System.DateTime.Now,
                Type = type,
                Message = message,
                FilePath = file,
                MemberName = member,
                LineNumber = line
            });
        }

        public void ChangeOutputDelay(WaitForSeconds newDelay)
        {
            lock (loggingLock)
            {
                logsOutputDelay = newDelay;
            }
        }
    }
}

﻿using UnityEngine;

namespace UVC.UnityWrappers
{
    public static class DispatcherSpawner
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void SpawnDispatchers()
        {
            GameObject dispManager = new GameObject("[AUTOADDED Dispatchers Spawner]");

            // Add dispatchers you want to automatically instantiate as components 
            // to game object above

            dispManager.AddComponent<LogDispatcher>();
            dispManager.AddComponent<StructureControllerDispatcher>();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    public class StructureControllerDispatcher : MonoBehaviour, IStructureController
    {
        public static StructureControllerDispatcher Instance { get; private set; }

        public bool ApplySymmetriesAutomatically
        {
            get
            {
                return StructureController.Instance.ApplySymmetriesAutomatically;
            }
            set
            {
                StructureController.Instance.ApplySymmetriesAutomatically = value;
            }
        }

        public bool DiscardWaterResidues
        {
            get
            {
                return StructureController.Instance.DiscardWaterResidues;
            }
            set
            {
                StructureController.Instance.DiscardWaterResidues = value;
            }
        }

        private static readonly object instanceLock = new object();
        private readonly object eventLock = new object();

        private WaitForSeconds stateCheckInterval = new WaitForSeconds(0.1f);

        private Queue<IStructuresContainer> newlyAddedContainers
            = new Queue<IStructuresContainer>();

        private Queue<IStructuresContainer> newlyRemovedContainers
            = new Queue<IStructuresContainer>();

        private Queue<IStructure> newlyAddedStructures
            = new Queue<IStructure>();

        private Queue<IStructure> newlyRemovedStructures
            = new Queue<IStructure>();

        private event Action<IStructuresContainer> _structureContainerAdded;
        public event Action<IStructuresContainer> _structuresContainerRemoved;
        public event Action<IStructure> _structureAdded;
        public event Action<IStructure> _structureRemoved;

        public event Action<IStructuresContainer> StructuresContainerAdded
        {
            add
            {
                lock (eventLock)
                {
                    _structureContainerAdded += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structureContainerAdded -= value;
                }
            }
        }

        public event Action<IStructuresContainer> StructuresContainerRemoved
        {
            add
            {
                lock (eventLock)
                {
                    _structuresContainerRemoved += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structuresContainerRemoved -= value;
                }
            }
        }

        public event Action<IStructure> StructureAdded
        {
            add
            {
                lock (eventLock)
                {
                    _structureAdded += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structureAdded -= value;
                }
            }
        }

        public event Action<IStructure> StructureRemoved
        {
            add
            {
                lock (eventLock)
                {
                    _structureRemoved += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structureRemoved -= value;
                }
            }
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            StructureController.Instance.StructuresContainerAdded += OnStructureContainerAdded;
            StructureController.Instance.StructuresContainerRemoved += OnStructureContainerRemoved;
            StructureController.Instance.StructureAdded += OnStructureAdded;
            StructureController.Instance.StructureRemoved += OnStructureRemoved;

            StartCoroutine(CheckState());
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        private IEnumerator CheckState()
        {
            while (true)
            {
                // Process at most one container from each group during one CheckState iteration
                if (newlyAddedContainers.Count > 0)
                {
                    _structureContainerAdded?.Invoke(newlyAddedContainers.Dequeue());
                }

                if (newlyRemovedContainers.Count > 0)
                {
                    _structuresContainerRemoved?.Invoke(newlyRemovedContainers.Dequeue());
                }

                if (newlyAddedStructures.Count > 0)
                {
                    _structureAdded?.Invoke(newlyAddedStructures.Dequeue());
                }

                if (newlyRemovedStructures.Count > 0)
                {
                    _structureRemoved?.Invoke(newlyRemovedStructures.Dequeue());
                }

                yield return stateCheckInterval;
            }
        }

        private void OnStructureContainerAdded(IStructuresContainer container)
        {
            lock (instanceLock)
            {
                newlyAddedContainers.Enqueue(container);
            }
        }

        private void OnStructureContainerRemoved(IStructuresContainer container)
        {
            lock (instanceLock)
            {
                newlyRemovedContainers.Enqueue(container);
            }
        }

        private void OnStructureAdded(IStructure structure)
        {
            lock (instanceLock)
            {
                newlyAddedStructures.Enqueue(structure);
            }
        }

        private void OnStructureRemoved(IStructure structure)
        {
            lock (instanceLock)
            {
                newlyRemovedStructures.Enqueue(structure);
            }
        }

        public void AddNewContainer(IStructuresContainer newContainer)
        {
            StructureController.Instance.AddNewContainer(newContainer);
        }

        public void RemoveContainer(IStructuresContainer container)
        {
            StructureController.Instance.RemoveContainer(container);
        }

        public List<IStructuresContainer> GetAllContainers()
        {
            return StructureController.Instance.GetAllContainers();
        }
    }
}

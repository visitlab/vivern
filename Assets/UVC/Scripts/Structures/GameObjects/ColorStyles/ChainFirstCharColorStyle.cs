﻿using UnityEngine;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    [CreateAssetMenu(fileName = "ChainFirstCharColorStyle", menuName = "Color Styles / By Chain's First Char", order = 2)]
    public class ChainFirstCharColorStyle : AbstractColorStyle
    {
        [SerializeField]
        private Color[] aToZchainColors;

        [SerializeField]
        private Color[] zeroToNineChainColors;

        public override Color GetColor(IAtom atom)
        {
            return GetChainColor(atom.Chain);
        }

        public override Color GetColor(IResidue residue)
        {
            return GetChainColor(residue.Chain);
        }

        public override Color GetColor(IChain chain)
        {
            return GetChainColor(chain);
        }

        private Color GetChainColor(IChain chain)
        {
            char firstChar = char.ToUpper(chain.Identifier[0]);

            if (char.IsDigit(firstChar))
            {
                return zeroToNineChainColors[firstChar - '0'];
            }
            else if (firstChar >= 'A' && firstChar <= 'Z')
            {
                return aToZchainColors[firstChar - 'A'];
            }

            return Color.white;
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    [CreateAssetMenu(fileName = "ResidueColorStyle", menuName = "Color Styles / By Residue", order = 1)]
    public class ResidueColorStyle : AbstractColorStyle
    {
        [System.Serializable]
        private class NameColorPair
        {
            public string residueIdentifier;
            public Color color;
        }

        [SerializeField]
        private NameColorPair[] nameColorPairs;

        [SerializeField]
        private Color defaultColor = Color.black;

        [System.NonSerialized]
        private Dictionary<string, Color> stringToColor;

        [System.NonSerialized]
        private bool isInitialized = false;

        public override Color GetColor(IAtom atom)
        {
            return GetColor(atom.Residue);
        }

        public override Color GetColor(IResidue residue)
        {
            if (!isInitialized)
            {
                Initialize();
                isInitialized = true;
            }

            Color col;
            if (stringToColor.TryGetValue(residue.Identifier.ToUpper(), out col))
            {
                return col;
            }

            return defaultColor;
        }

        public override Color GetColor(IChain chain)
        {
            return defaultColor;
        }

        private void Initialize()
        {
            stringToColor = new Dictionary<string, Color>();

            for (int i = 0; i < nameColorPairs.Length; ++i)
            {
                stringToColor.Add(nameColorPairs[i].residueIdentifier.ToUpper().Trim(), nameColorPairs[i].color);
            }
        }
    }
}

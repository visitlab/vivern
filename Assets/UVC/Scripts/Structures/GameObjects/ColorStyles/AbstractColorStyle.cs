﻿using UnityEngine;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    public abstract class AbstractColorStyle : ScriptableObject
    {
        public abstract Color GetColor(IAtom atom);

        public abstract Color GetColor(IResidue residue);

        public abstract Color GetColor(IChain chain);
    }
}

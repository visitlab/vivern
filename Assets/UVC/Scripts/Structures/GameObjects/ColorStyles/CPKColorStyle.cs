﻿using UnityEngine;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    [CreateAssetMenu(fileName = "CPKColorStyle", menuName = "Color Styles / CPK Color", order = 0)]
    public class CPKColorStyle : AbstractColorStyle
    {
        [SerializeField]
        // Index = atomic number, zero defines default color
        private Color[] elementsColors;

        public override Color GetColor(IAtom atom)
        {
            if (atom.Element < elementsColors.Length)
            {
                return elementsColors[atom.Element];
            }
            return elementsColors[0];
        }

        public override Color GetColor(IResidue residue)
        {
            return elementsColors[0];
        }

        public override Color GetColor(IChain chain)
        {
            return elementsColors[0];
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UVC.FileIO;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    public class StructureGameObjectsManager : MonoBehaviour
    {
        public static StructureGameObjectsManager Instance { get; private set; }

        [SerializeField]
        protected GameObject structureGameObjectPrefab;

        [SerializeField]
        protected string[] defaultStructuresToLoad = new string[] { "1bna" };

        [SerializeField]
        protected string[] defaultLocalFilesToLoad = new string[] { };

        [SerializeField]
        protected BoxCollider structureSpawnPosition;

        [SerializeField]
        protected bool forceFitToSpawnCollider = false;

        [SerializeField]
        protected float defaultAngstromsPerUnit = 256;

        [SerializeField]
        protected List<AbstractVisualRepresentation> visualRepresentations;

        [SerializeField]
        protected AbstractColorStyle[] colorStyles;

        [SerializeField]
        protected int defaultColorStyle = 0;

        [SerializeField]
        private UnityStructureGoEvent StructureGameObjectAdded;

        [SerializeField]
        private UnityStructureGoEvent StructureGameObjectRemoved;

        [Header("EXPERIMENTAL (needs verification)")]
        [SerializeField]
        protected bool applySymmetries = true;

        [SerializeField]
        protected bool discardWaterResidues = true;

        public float AngstromsPerUnit
        {
            get => defaultAngstromsPerUnit;
            set
            {
                defaultAngstromsPerUnit = value;

                for(int i = 0; i < existingStructures.Count; ++i)
                {
                    existingStructures[i].ScaleToMatchApu(value);
                }
            }
        }

        protected ChemFileParserRequest parserRequest;
        protected List<StructureGameObject> existingStructures = new List<StructureGameObject>();

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        protected virtual void Start()
        {
            StructureControllerDispatcher.Instance.ApplySymmetriesAutomatically = applySymmetries;
            StructureControllerDispatcher.Instance.DiscardWaterResidues = discardWaterResidues;
            StructureControllerDispatcher.Instance.StructureAdded += OnNewStructureAdded;

            for (int i = 0; i < defaultStructuresToLoad.Length; ++i)
            {
                if (defaultStructuresToLoad[i].Length > 0)
                {
                    InstantiateStructureFromPdb(defaultStructuresToLoad[i]);
                }
            }

            for (int i = 0; i < defaultLocalFilesToLoad.Length; ++i)
            {
                if (defaultLocalFilesToLoad[i].Length > 0)
                {
                    InstantiateStructureFromLocalFile(defaultLocalFilesToLoad[i]);
                }
            }
        }

        public virtual void InstantiateStructureFromPdb(string pdbId)
        {
            parserRequest = new ChemFileParserRequest(new TextFileLoader(new PDBFileProvider(pdbId), 1));
            parserRequest.StartParsing();
        }

        public virtual void InstantiateStructureFromLocalFile(string localPath)
        {
            parserRequest = new ChemFileParserRequest(new TextFileLoader(new LocalFileProvider(localPath), 1));
            parserRequest.StartParsing();
        }

        protected virtual void OnNewStructureAdded(IStructure structure)
        {
            var structureGo = Instantiate(structureGameObjectPrefab, transform);
            var structureGoComp = structureGo.GetComponent<StructureGameObject>();
            structureGoComp.CurrentColorStyle = colorStyles[defaultColorStyle];

            if (forceFitToSpawnCollider)
            {
                structureGoComp.SetData(this, structure, structureSpawnPosition, visualRepresentations);
            }
            else
            {
                structureGoComp.SetData(this, structure, structureSpawnPosition.transform.position,
                    defaultAngstromsPerUnit, visualRepresentations);
            }

            StructureGameObjectAdded?.Invoke(structureGoComp);
            existingStructures.Add(structureGoComp);
        }

        public virtual void RemoveStructure(StructureGameObject structureGo)
        {
            if (structureGo != null && structureGo.gameObject != null)
            {
                Destroy(structureGo);
                StructureGameObjectRemoved?.Invoke(structureGo);
                existingStructures.Remove(structureGo);
            }
        }

        public virtual void SetStructureColorStyle(StructureGameObject structureGo, int styleIndex)
        {
            structureGo.CurrentColorStyle = colorStyles[styleIndex];
        }
    }
}

﻿using UnityEngine;
using UVC.UnityWrappers;

namespace UVC.UnityWrappers
{
    public class SGOColliderGenerator : MonoBehaviour
    {
        [SerializeField]
        private StructureGameObject structureGameObject;

        [SerializeField]
        // When the collider was added  during runtime, XRIT had some issues with that
        private BoxCollider boxCollider;

        private void Start()
        {
            var bounds = structureGameObject.GetWorldBounds();

            boxCollider.center = bounds.center;
            boxCollider.size = bounds.size;
        }
    }
}

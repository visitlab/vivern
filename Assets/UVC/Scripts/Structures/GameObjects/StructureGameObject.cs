﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    public class StructureGameObject : MonoBehaviour
    {
        public StructureGameObjectsManager StructureGOManager { get; private set; }

        public IStructure StructureData { get; private set; }

        protected float defaultAngstromPerUnit = 1;

        public virtual float AngstromPerWorldUnit
        {
            get
            {
                return defaultAngstromPerUnit / transform.lossyScale.x;
            }
        }

        public virtual float Angstroms2Units
        {
            get
            {
                return 1 / AngstromPerWorldUnit;
            }
        }

        public virtual float Units2Angstroms
        {
            get
            {
                return AngstromPerWorldUnit;
            }
        }

        private AbstractColorStyle _currentColorStyle = null;
        public AbstractColorStyle CurrentColorStyle
        {
            get
            {
                return _currentColorStyle;
            }
            set
            {
                if (_currentColorStyle == value) { return; }

                var oldVal = _currentColorStyle;
                _currentColorStyle = value;

                if (oldVal != null)
                {
                    // Very performance ineffective solution, just temporary hopefully
                    StartCoroutine(SpawnRepresentations());
                }
            }
        }

        protected Vector3 initSpawnPosition;

        protected Vector3 initLocalScale;

        /// <summary>
        /// Position of the structure centroid (in the coordinate space of the structure)
        /// </summary>
        protected Vector3 centroidPosition;

        protected int activeVisualRepresentation = 0;

        public List<AbstractVisualRepresentation> VisRepresentationsObjects { get; set; } = new List<AbstractVisualRepresentation>();

        protected virtual IEnumerator Start()
        {
            if (StructureData == null || StructureData.ParentContainer == null)
            {
                Debug.LogError("StructureGameObject data not valid!");
            }

            // This object's position ([0, 0, 0] for child atoms) will be treated as a structure centroid
            // so the atoms should be positioned around this point
            transform.position = initSpawnPosition;

            yield return SpawnRepresentations();
        }

        protected virtual void OnDestroy()
        {
            StructureData.ParentContainer.Remove(StructureData);
            StructureGOManager.RemoveStructure(this);
        }

        public virtual void SetData(StructureGameObjectsManager sgoManager, IStructure structure,
            BoxCollider spawnPosition, List<AbstractVisualRepresentation> visualRepresentations)
        {
            SetData(sgoManager, structure,
                spawnPosition.bounds.center,
                GetApuToFitIntoBounds(structure, spawnPosition.bounds, structure.GetAtoms().GetCentroid()),
                visualRepresentations);
        }

        public virtual void SetData(StructureGameObjectsManager sgoManager, IStructure structure,
            Vector3 worldSpawnPosition, float angstromPerUnit, List<AbstractVisualRepresentation> visualRepresentations)
        {
            StructureGOManager = sgoManager;
            StructureData = structure;
            initSpawnPosition = worldSpawnPosition; // Object spawn position in world == centroid of the molecule
            centroidPosition = structure.GetAtoms().GetCentroid();
            defaultAngstromPerUnit = angstromPerUnit;
            gameObject.name = StructureData.Name;

            for (int i = 0; i < visualRepresentations.Count; ++i)
            {
                var visGo = Instantiate(visualRepresentations[i].gameObject);
                var visRepComp = visGo.GetComponent<AbstractVisualRepresentation>();

                if (visRepComp == null)
                {
                    Debug.LogError("Invalid visual representation game object found!");
                }
                else
                {
                    visRepComp.SetData(this, TransformPositionToLocalSpace);
                }

                VisRepresentationsObjects.Add(visRepComp);
            }
        }

        protected virtual float GetApuToFitIntoBounds(IStructure structure, Bounds spawnPositionAABB, Vector3 centroid)
        {
            Bounds currentBounds = GetWorldBounds(structure, centroid);

            float newAngstromPerUnit = Mathf.Max(currentBounds.size.x / spawnPositionAABB.size.x,
                                        Mathf.Max(currentBounds.size.y / spawnPositionAABB.size.y,
                                                  currentBounds.size.z / spawnPositionAABB.size.z));

            return newAngstromPerUnit;
        }

        public virtual Bounds GetWorldBounds()
        {
            return GetWorldBounds(StructureData, centroidPosition);
        }

        protected virtual Bounds GetWorldBounds(IStructure structure, Vector3 centroid)
        {
            Matrix4x4 transformation =
                Matrix4x4.Scale(new Vector3(Angstroms2Units, Angstroms2Units, Angstroms2Units)) *
                Matrix4x4.Translate(centroid) *
                Matrix4x4.Scale(new Vector3(-1.0f, -1.0f, -1.0f));

            var atoms = structure.GetAtoms();
            Vector3[] positions = new Vector3[atoms.Count];
            for (int i = 0; i < atoms.Count; ++i)
            {
                positions[i] = atoms[i].GetPosition();
            }

            return GeometryUtility.CalculateBounds(positions, transformation);
        }

        protected virtual Vector3 TransformPositionToLocalSpace(Vector3 oldPosition)
        {
            return (centroidPosition - oldPosition) * Angstroms2Units;
        }

        public void ScaleToMatchApu(float newApu)
        {
            float ratio = defaultAngstromPerUnit / newApu;

            transform.localScale = new Vector3(ratio, ratio, ratio);
        }

        protected virtual IEnumerator SpawnRepresentations()
        {
            for (int i = 0; i < VisRepresentationsObjects.Count; ++i)
            {
                VisRepresentationsObjects[i].gameObject.SetActive(i == activeVisualRepresentation);
                yield return StartCoroutine(VisRepresentationsObjects[i].SpawnRepresentation());
            }
        }

        public void SetVisualRepresentation(int index)
        {
            activeVisualRepresentation = index;

            for (int i = 0; i < VisRepresentationsObjects.Count; ++i)
            {
                VisRepresentationsObjects[i].gameObject.SetActive(i == activeVisualRepresentation);
            }
        }
    }
}

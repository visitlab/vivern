﻿using System;
using System.Collections;
using UnityEngine;
using UVC.Structures;

namespace UVC.UnityWrappers
{
    public class NucleotideVisualRepresentation : AbstractVisualRepresentation
    {
        [SerializeField]
        protected GameObject nucleotidePrefab;

        [SerializeField]
        protected GameObject nucleotideBondPrefab;

        [SerializeField]
        protected float bondTubeRadiusAngstrom = 2.0f;

        [SerializeField]
        protected float nucleotideRadiusAngstrom = 5.0f;

        public override void SetData(StructureGameObject structureGameObjectCaller, Func<Vector3, Vector3> structurePosToLocalPos)
        {
            base.SetData(structureGameObjectCaller, structurePosToLocalPos);
            gameObject.name = "Nucleotides";
        }

        public override IEnumerator SpawnRepresentation()
        {
            ClearRepresentationData();

            yield return UnitySpecificDefinitions.waitForEndOfFrame;

            var chains = StructureGo.StructureData.GetChains();

            var scaleVal = nucleotideRadiusAngstrom * StructureGo.Angstroms2Units;
            Vector3 nuclObjLocalScale = new Vector3(scaleVal, scaleVal, scaleVal) * 2.0f;
            float nuclBondXzScale = bondTubeRadiusAngstrom * StructureGo.Angstroms2Units * 2.0f;

            for (int i = 0; i < chains.Count; ++i)
            {
                var residues = chains[i].GetResidues();
                Vector3? lastCentroidPos = null;

                for (int j = 0; j < residues.Count; ++j)
                {
                    if (residues[j].IsNucleobase)
                    {
                        Vector3 centroidPos = residues[j].GetAtoms().GetCentroid();

                        InstantiateNucleotidePrefab(residues[j], centroidPos, nuclObjLocalScale);
                        if (lastCentroidPos.HasValue)
                        {
                            InstantiateNucleotideBond(lastCentroidPos.Value, centroidPos, nuclBondXzScale);
                        }

                        lastCentroidPos = centroidPos;
                    }
                }

                yield return UnitySpecificDefinitions.waitForEndOfFrame;
            }
        }

        protected void InstantiateNucleotidePrefab(IResidue residue, Vector3 centroid, Vector3 localScale)
        {
            var nuclObj = Instantiate(nucleotidePrefab, transform);
            nuclObj.name = residue.Identifier;

            nuclObj.GetComponent<MeshRenderer>().material.SetColor("_BaseColor",
                StructureGo.CurrentColorStyle.GetColor(residue));

            nuclObj.transform.localScale = localScale;
            nuclObj.transform.localPosition = TransformStructurePosToLocalPos(centroid);
        }

        protected void InstantiateNucleotideBond(Vector3 lastCentroidPos, Vector3 currCentroidPos, float xzScale)
        {
            var nuclBond = Instantiate(nucleotideBondPrefab, transform);
            nuclBond.name = "Bond";

            var lastToCurrDir = TransformStructurePosToLocalPos(currCentroidPos) - TransformStructurePosToLocalPos(lastCentroidPos);

            nuclBond.transform.localPosition = TransformStructurePosToLocalPos((lastCentroidPos + currCentroidPos) * 0.5f);
            nuclBond.transform.localScale = new Vector3(xzScale, lastToCurrDir.magnitude * 0.5f, xzScale);
            nuclBond.transform.rotation = Quaternion.FromToRotation(Vector3.up, transform.parent.TransformDirection(lastToCurrDir));
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UVC.UnityWrappers
{
    public class BackboneVisualRepresentation : AbstractVisualRepresentation
    {
        [SerializeField]
        private Material material;

        [SerializeField]
        private float tubeRadiusAngstrom = 5.0f;

        [SerializeField]
        private int verticesPerRing = 10;

        [SerializeField]
        private int interpolationSubdivisionRings = 1;

        public override void SetData(StructureGameObject structureGameObjectCaller, Func<Vector3, Vector3> structurePosToLocalPos)
        {
            base.SetData(structureGameObjectCaller, structurePosToLocalPos);
            gameObject.name = "Backbone";
        }

        public override IEnumerator SpawnRepresentation()
        {
            ClearRepresentationData();

            var chains = StructureGo.StructureData.GetChains();

            for (int i = 0; i < chains.Count; ++i)
            {
                var residues = chains[i].GetResidues();
                List<Vector3> backboneOnlyPoints = new List<Vector3>();
                List<Color> backboneOnlyColors = new List<Color>();
                List<Vector3> alphaCarbonsPoints = new List<Vector3>();
                List<Color> alphaCarbonsColors = new List<Color>();

                for (int j = 0; j < residues.Count; ++j)
                {
                    if (residues[j].IsSplitToBackboneAndBase)
                    {
                        backboneOnlyPoints.Add(TransformStructurePosToLocalPos(residues[j].GetBackboneAtoms().GetCentroid()));
                        backboneOnlyColors.Add(StructureGo.CurrentColorStyle.GetColor(residues[j]));
                    }
                    else
                    {
                        var resAtoms = residues[j].GetAtoms();
                        for (int k = 0; k < resAtoms.Count; ++k)
                        {
                            if (resAtoms[k].IsAlphaCarbon)
                            {
                                alphaCarbonsPoints.Add(TransformStructurePosToLocalPos(resAtoms[k].GetPosition()));
                                alphaCarbonsColors.Add(StructureGo.CurrentColorStyle.GetColor(residues[j]));
                                break;
                            }
                        }
                    }
                }

                backboneOnlyPoints = Utils.MathFunctions.HermitInterpolatePoints(backboneOnlyPoints, interpolationSubdivisionRings, ref backboneOnlyColors);
                alphaCarbonsPoints = Utils.MathFunctions.HermitInterpolatePoints(alphaCarbonsPoints, interpolationSubdivisionRings, ref alphaCarbonsColors);

                CreateBackboneObject(chains[i].Identifier + "_backbone_atoms_centroids", backboneOnlyPoints, backboneOnlyColors);
                CreateBackboneObject(chains[i].Identifier + "_alpha_carbons", alphaCarbonsPoints, alphaCarbonsColors);

                yield return UnitySpecificDefinitions.waitForEndOfFrame;
            }
        }

        private void CreateBackboneObject(string name, List<Vector3> points, List<Color> colors)
        {
            if (points.Count <= 1) { return; }

            var tubePartGo = new GameObject(name);
            tubePartGo.transform.SetParent(transform);
            tubePartGo.transform.localRotation = Quaternion.identity;
            tubePartGo.transform.localPosition = Vector3.zero;
            var mr = tubePartGo.AddComponent<MeshRenderer>();
            var mf = tubePartGo.AddComponent<MeshFilter>();

            mr.material = material;
            mf.mesh = MeshGenerator.GenerateBackboneTube(points, colors, tubeRadiusAngstrom * StructureGo.Angstroms2Units, verticesPerRing);
        }

        public override void ClearRepresentationData()
        {
            base.ClearRepresentationData();
        }
    }
}

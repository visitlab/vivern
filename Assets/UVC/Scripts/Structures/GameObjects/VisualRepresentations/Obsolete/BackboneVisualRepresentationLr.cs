﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UVC.UnityWrappers
{
    [Obsolete("This representation using line renderer is obsolete due to the rendering artifacts.", true)]
    public class BackboneVisualRepresentationLr : AbstractVisualRepresentation
    {
        [SerializeField]
        protected GameObject backbonePrefab;

        private List<LineRenderer> lineRenderers = new List<LineRenderer>();

        private void Update()
        {
            // TODO Ineffective (should check whether the Sgo.apwu changed first)
            for (int i = 0; i < lineRenderers.Count; ++i)
            {
                // Line renderer does not scale locally correctly so it is necessary to do it manually
                lineRenderers[i].widthMultiplier = StructureGo.Angstroms2Units;
            }
        }

        public override void SetData(StructureGameObject structureGameObjectCaller, Func<Vector3, Vector3> structurePosToLocalPos)
        {
            base.SetData(structureGameObjectCaller, structurePosToLocalPos);
            gameObject.name = "Backbone";
        }

        public override IEnumerator SpawnRepresentation()
        {
            var chains = StructureGo.StructureData.GetChains();

            for (int i = 0; i < chains.Count; ++i)
            {
                var residues = chains[i].GetResidues();
                List<Vector3> backboneOnlyPoints = new List<Vector3>();
                List<Vector3> alphaCarbonsPoints = new List<Vector3>();

                for (int j = 0; j < residues.Count; ++j)
                {
                    if (residues[j].IsSplitToBackboneAndBase)
                    {
                        backboneOnlyPoints.Add(TransformStructurePosToLocalPos(residues[j].GetBackboneAtoms().GetCentroid()));
                    }
                    else
                    {
                        var resAtoms = residues[j].GetAtoms();
                        for (int k = 0; k < resAtoms.Count; ++k)
                        {
                            if (resAtoms[k].IsAlphaCarbon)
                            {
                                alphaCarbonsPoints.Add(TransformStructurePosToLocalPos(resAtoms[k].GetPosition()));
                                break;
                            }
                        }
                    }
                }

                CreateBackboneObject(chains[i].Identifier + "_backbone_atoms_centroids", backboneOnlyPoints);
                CreateBackboneObject(chains[i].Identifier + "_alpha_carbons", alphaCarbonsPoints);

                yield return UnitySpecificDefinitions.waitForEndOfFrame;
            }

            void CreateBackboneObject(string name, List<Vector3> points)
            {
                if (points.Count > 1)
                {
                    var backboneGo = Instantiate(backbonePrefab, transform);
                    backboneGo.name = name;
                    backboneGo.transform.localPosition = Vector3.zero;

                    var backboneLr = backboneGo.GetComponent<LineRenderer>();
                    lineRenderers.Add(backboneLr);

                    backboneLr.widthMultiplier = StructureGo.Angstroms2Units;
                    backboneLr.positionCount = points.Count;
                    backboneLr.SetPositions(points.ToArray());
                }
            }
        }

        public override void ClearRepresentationData()
        {
            base.ClearRepresentationData();
            lineRenderers.Clear();
        }
    }
}

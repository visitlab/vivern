﻿using System;
using System.Collections;
using UnityEngine;

namespace UVC.UnityWrappers
{
    [Obsolete("This representation using particle system is obsolete due to the rendering artifacts.", true)]
    public class SpacefillParticleVisualRepresentation : AbstractVisualRepresentation
    {
        [SerializeField]
        protected GameObject particleSystemPrefab;

        [SerializeField]
        protected int nrOfAtomsPerFrameLoaded = 200000;

        private ParticleSystem ps;
        private ParticleSystem.Particle[] atomParticles;

        private void OnEnable()
        {
            if (atomParticles != null)
            {
                ps?.SetParticles(atomParticles, atomParticles.Length);
            }
        }

        public override void SetData(StructureGameObject structureGameObjectCaller, Func<Vector3, Vector3> structurePosToLocalPos)
        {
            base.SetData(structureGameObjectCaller, structurePosToLocalPos);
            gameObject.name = "Spacefill";
        }

        public override IEnumerator SpawnRepresentation()
        {
            ClearRepresentationData();

            var psObj = Instantiate(particleSystemPrefab, transform);
            ps = psObj.GetComponent<ParticleSystem>();
            psObj.name = gameObject.name + " particles";

            var atoms = StructureGo.StructureData.GetAtoms();
            atomParticles = new ParticleSystem.Particle[atoms.Count];
            ps.GetParticles(atomParticles);

            yield return UnitySpecificDefinitions.waitForEndOfFrame;

            for (int i = 0; i < atomParticles.Length; ++i)
            {
                var particle = atomParticles[i];

                particle.position = TransformStructurePosToLocalPos(atoms[i].GetPosition());
                particle.velocity = Vector3.zero;
                particle.startSize = atoms[i].Type.VDWRadius * 2.0f * StructureGo.Angstroms2Units;
                particle.startLifetime = float.PositiveInfinity;
                particle.startColor = StructureGo.CurrentColorStyle.GetColor(atoms[i]);

                atomParticles[i] = particle;

                if ((i + 1) % nrOfAtomsPerFrameLoaded == 0)
                {
                    yield return UnitySpecificDefinitions.waitForEndOfFrame;
                }
            }

            ps.SetParticles(atomParticles, atomParticles.Length);
        }
    }
}

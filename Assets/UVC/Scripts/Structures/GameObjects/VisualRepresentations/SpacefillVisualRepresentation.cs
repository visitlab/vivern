﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UVC.UnityWrappers
{
    public class SpacefillVisualRepresentation : AbstractVisualRepresentation
    {
        [SerializeField]
        protected GameObject atomPrefab;

        [SerializeField]
        protected bool bakeMeshesAfterSpawn = false;

        [SerializeField]
        protected int nrOfAtomsPerFrameLoaded = 5000;

        [SerializeField]
        protected string shaderColorPropertyName = "_BaseColor";

        public override void SetData(StructureGameObject structureGameObjectCaller, Func<Vector3, Vector3> structurePosToLocalPos)
        {
            base.SetData(structureGameObjectCaller, structurePosToLocalPos);
            gameObject.name = "Spacefill";
        }

        public override IEnumerator SpawnRepresentation()
        {
            ClearRepresentationData();

            yield return UnitySpecificDefinitions.waitForEndOfFrame;

            var atoms = StructureGo.StructureData.GetAtoms();

            Dictionary<Color, List<GameObject>> bakeGroups = new Dictionary<Color, List<GameObject>>();
            MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();

            for (int i = 0; i < atoms.Count; ++i)
            {
                var atObj = Instantiate(atomPrefab, transform);
                atObj.name = atoms[i].Name;

                var vdw = atoms[i].Type.VDWRadius;
                var color = StructureGo.CurrentColorStyle.GetColor(atoms[i]);

                materialPropertyBlock.SetColor(shaderColorPropertyName, color);
                atObj.GetComponent<MeshRenderer>().SetPropertyBlock(materialPropertyBlock);

                // Scale of a sphere determines its diameter, therefore the multiplication by 2
                atObj.transform.localScale = new Vector3(vdw, vdw, vdw) * 2.0f * StructureGo.Angstroms2Units;
                atObj.transform.localPosition = TransformStructurePosToLocalPos(atoms[i].GetPosition());

                if (!bakeGroups.ContainsKey(color))
                {
                    bakeGroups[color] = new List<GameObject>();
                }

                bakeGroups[color].Add(atObj);

                if ((i + 1) % nrOfAtomsPerFrameLoaded == 0)
                {
                    yield return UnitySpecificDefinitions.waitForEndOfFrame;
                }
            }

            if (bakeMeshesAfterSpawn)
            {
                foreach (var bGroup in bakeGroups)
                {
                    CombineInstance[] combine = new CombineInstance[bGroup.Value.Count];

                    for (int i = 0; i < bGroup.Value.Count; ++i)
                    {
                        combine[i].mesh = bGroup.Value[i].GetComponent<MeshFilter>().sharedMesh;
                        combine[i].transform = bGroup.Value[i].transform.localToWorldMatrix;
                    }

                    var combinedMeshesGo = new GameObject("Spacefill_combined_" + bGroup.Key.ToString());

                    combinedMeshesGo.transform.SetParent(transform);
                    combinedMeshesGo.transform.localScale = Vector3.one;
                    combinedMeshesGo.transform.localRotation = Quaternion.identity;

                    var mf = combinedMeshesGo.AddComponent<MeshFilter>();
                    mf.mesh = new Mesh();
                    mf.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
                    mf.mesh.CombineMeshes(combine, true, true, false);

                    var mr = combinedMeshesGo.AddComponent<MeshRenderer>();

                    materialPropertyBlock.SetColor(shaderColorPropertyName, bGroup.Key);
                    mr.material = bGroup.Value[0].GetComponent<MeshRenderer>().material;
                    mr.SetPropertyBlock(materialPropertyBlock);

                    for (int i = 0; i < bGroup.Value.Count; ++i)
                    {
                        Destroy(bGroup.Value[i]);
                    }
                }
            }
        }
    }
}

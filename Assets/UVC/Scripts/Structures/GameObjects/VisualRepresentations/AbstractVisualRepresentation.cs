﻿using System.Collections;
using UnityEngine;

namespace UVC.UnityWrappers
{
    public abstract class AbstractVisualRepresentation : MonoBehaviour
    {
        public virtual StructureGameObject StructureGo { get; protected set; }

        public virtual System.Func<Vector3, Vector3> TransformStructurePosToLocalPos { get; set; }

        protected void InitData()
        {
            if (StructureGo == null || TransformStructurePosToLocalPos == null)
            {
                Debug.LogError("Invalid data passed to visual representation.");
            }

            transform.SetParent(StructureGo.transform);
            transform.localPosition = Vector3.zero;
            transform.localScale = Vector3.one;
            transform.localRotation = Quaternion.identity;
        }

        public virtual void SetData(StructureGameObject structureGameObjectCaller,
            System.Func<Vector3, Vector3> structurePosToLocalPos)
        {
            StructureGo = structureGameObjectCaller;
            TransformStructurePosToLocalPos = structurePosToLocalPos;

            InitData();
        }

        public abstract IEnumerator SpawnRepresentation();

        public virtual void ClearRepresentationData()
        {
            foreach (Transform t in transform)
            {
                Destroy(t.gameObject);
            }
        }
    }
}

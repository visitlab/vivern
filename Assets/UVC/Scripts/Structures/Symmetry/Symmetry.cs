﻿
using Mat4 = UnityEngine.Matrix4x4;

namespace UVC.Structures
{
    public class Symmetry
    {
        public string Id { get; set; }

        public Mat4 Matrix { get; set; }

        public Symmetry(string id, Mat4 matrix)
        {
            Id = id;
            Matrix = matrix;
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace UVC.Structures
{
    public sealed class StructureController : IStructureController
    {
        private static readonly object instanceLock = new object();
        private readonly object eventLock = new object();

        private static StructureController _instance;
        public static StructureController Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (instanceLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new StructureController();
                        }
                    }
                }
                return _instance;
            }
        }

        public bool ApplySymmetriesAutomatically { get; set; }

        public bool DiscardWaterResidues { get; set; }

        private readonly List<IStructuresContainer> containers;

        private event Action<IStructuresContainer> _structuresContainerAdded;
        public event Action<IStructuresContainer> _structuresContainerRemoved;
        public event Action<IStructure> _structureAdded;
        public event Action<IStructure> _structureRemoved;

        public event Action<IStructuresContainer> StructuresContainerAdded
        {
            add
            {
                lock (eventLock)
                {
                    _structuresContainerAdded += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structuresContainerAdded -= value;
                }
            }
        }

        public event Action<IStructuresContainer> StructuresContainerRemoved
        {
            add
            {
                lock (eventLock)
                {
                    _structuresContainerRemoved += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structuresContainerRemoved -= value;
                }
            }
        }

        public event Action<IStructure> StructureAdded
        {
            add
            {
                lock (eventLock)
                {
                    _structureAdded += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structureAdded -= value;
                }
            }
        }

        public event Action<IStructure> StructureRemoved
        {
            add
            {
                lock (eventLock)
                {
                    _structureRemoved += value;
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _structureRemoved -= value;
                }
            }
        }

        private StructureController()
        {
            containers = new List<IStructuresContainer>();
        }

        public List<IStructuresContainer> GetAllContainers()
        {
            lock (instanceLock)
            {
                return containers;
            }
        }

        public void AddNewContainer(IStructuresContainer newContainer)
        {
            lock (instanceLock)
            {
                var contStructures = newContainer.GetStructures();
                for (int i = 0; i < contStructures.Count; ++i)
                {
                    OnStructureAdded(contStructures[i]);
                }

                newContainer.StructureAdded += OnStructureAdded;
                newContainer.StructureRemoved += OnStructureRemoved;
                containers.Add(newContainer);
                _structuresContainerAdded?.Invoke(newContainer);
            }
        }

        public void RemoveContainer(IStructuresContainer container)
        {
            lock (instanceLock)
            {
                if (containers.Contains(container))
                {
                    containers.Remove(container);
                    container.Clear();
                    _structuresContainerRemoved?.Invoke(container);
                }
            }
        }

        private void ProcessStructureBeforeAddition(IStructure structure)
        {
            if (DiscardWaterResidues)
            {
                var residues = structure.GetResidues();

                for (int j = residues.Count - 1; j >= 0; --j)
                {
                    if (residues[j] != null && (residues[j].IsSolvent || residues[j].Identifier.ToUpper() == "HOH"))
                    {
                        residues[j].Remove();
                    }
                }
            }

            if (ApplySymmetriesAutomatically)
            {
                structure.ApplySymmetries();
            }
        }

        private void OnStructureAdded(IStructure structure)
        {
            ProcessStructureBeforeAddition(structure);
            _structureAdded?.Invoke(structure);
        }

        private void OnStructureRemoved(IStructure structure)
        {
            _structureRemoved?.Invoke(structure);

            if (structure?.ParentContainer?.Count == 0)
            {
                RemoveContainer(structure?.ParentContainer);
            }
        }
    }
}

﻿namespace UVC.Structures
{
    public class Remark
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public Remark(int id, string text)
        {
            Id = id;
            Text = text;
        }
    }
}

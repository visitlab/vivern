﻿using System.Collections.Generic;
using System.Linq;

namespace UVC.Structures
{
    public abstract class AbstractChain<TResDataStruct> : AbstractStructureElement, IChain where TResDataStruct : ICollection<IResidue>, new()
    {
        public virtual string Identifier { get; set; }

        protected IStructure structure;
        public override IStructure Structure
        {
            get
            {
                return structure;
            }
        }

        public virtual bool IsEmpty
        {
            get
            {
                return GetAtomsCount() == 0 && GetResiduesCount() == 0;
            }
        }

        public event System.Action<IAtom, UnityEngine.Vector3?> AtomAddedToAnyResidue;
        public event System.Action<IAtom> AtomRemovedFromAnyResidue;
        public event System.Action<IResidue> ResidueAdded;
        public event System.Action<IResidue> ResidueRemoved;

        protected readonly TResDataStruct residues = new TResDataStruct();

        protected virtual Dictionary<int, IAtom> Atoms { get; set; } = new Dictionary<int, IAtom>();

        private Dictionary<IAtom, HashSet<Bond>> atomBonds = new Dictionary<IAtom, HashSet<Bond>>();

        public AbstractChain()
        { }

        protected AbstractChain(AbstractChain<TResDataStruct> other, IStructure newStructure = null)
            : base(other)
        {
            Identifier = other.Identifier;
            structure = newStructure ?? other.structure;

            var originalAtoms = new List<IAtom>();
            var clonnedAtoms = new List<IAtom>();

            foreach (var otherRes in other.residues)
            {
                var resClone = otherRes.Clone(this);
                AddResidue(resClone);

                originalAtoms.AddRange(otherRes.GetAtoms());
                originalAtoms.AddRange(otherRes.GetBackboneAtoms());
                originalAtoms.AddRange(otherRes.GetBaseAtoms());

                clonnedAtoms.AddRange(resClone.GetAtoms());
                clonnedAtoms.AddRange(resClone.GetBackboneAtoms());
                clonnedAtoms.AddRange(resClone.GetBaseAtoms());
            }

            for (int i = 0; i < originalAtoms.Count; ++i)
            {
                if (other.atomBonds.TryGetValue(originalAtoms[i], out HashSet<Bond> bonds))
                {
                    HashSet<Bond> newBonds = new HashSet<Bond>();

                    foreach (var bond in bonds)
                    {
                        newBonds.Add(new Bond(
                            clonnedAtoms[originalAtoms.IndexOf(bond.FirstAtom)],
                            clonnedAtoms[originalAtoms.IndexOf(bond.SecondAtom)]
                            ));
                    }

                    atomBonds.Add(clonnedAtoms[i], newBonds);
                }
            }
        }

        public virtual void SetStructureReference(IStructure newStructure)
        {
            structure = newStructure;
        }

        // NOTE This function is rather slow & can easily consume few megabytes of memory
        //      For example, allocation in case of GetAtoms() takes time since it happens
        //      also when no atoms are present
        public virtual void AddResidue(IResidue residue)
        {
            // If residue already has Id, do not generate a new one for it
            // since it would broke possibly already existing records
            // dependent on GetHashCode()
            // NOTE: Possible problem: residue with the same id already existing
            if (residue.Id < 0)
            {
                residue.Id = Structure.GenerateResidueId();
            }
            residue.SetChainReference(this);
            residue.AtomAdded += OnAtomAddedToResidue;
            residue.AtomRemoved += OnAtomRemovedFromResidue;

            var resAtoms = residue.GetAtoms();
            for (int i = 0; i < resAtoms.Count; ++i)
            {
                AddAtomToInternalCache(resAtoms[i]);
            }

            AddNewResidueToInternalList(residue);
            ResidueAdded?.Invoke(residue);
        }

        protected virtual void AddNewResidueToInternalList(IResidue residue)
        {
            residues.Add(residue);
        }

        protected virtual bool RemoveResidueFromInternalList(IResidue residue)
        {
            return residues.Remove(residue);
        }

        public virtual void RemoveResidue(IResidue residue)
        {
            if (RemoveResidueFromInternalList(residue))
            {
                var resAtoms = residue.GetAtoms();
                for (int i = 0; i < resAtoms.Count; ++i)
                {
                    RemoveAtomFromInternalCache(resAtoms[i]);
                }

                residue.AtomAdded -= OnAtomAddedToResidue;
                residue.AtomRemoved -= OnAtomRemovedFromResidue;
                ResidueRemoved?.Invoke(residue);
            }
        }

        public virtual void RemoveAllResidues()
        {
            foreach (var residue in residues)
            {
                RemoveResidue(residue);
            }
        }

        public virtual IResidue GetResidue(string sequenceIdef)
        {
            return residues.FirstOrDefault(x => x.SequenceIdef == sequenceIdef);
        }

        public virtual List<IResidue> GetResidues()
        {
            return new List<IResidue>(residues);
        }

        public virtual int GetResiduesCount()
        {
            return residues.Count;
        }

        protected virtual void OnAtomAddedToResidue(IAtom atom, UnityEngine.Vector3? position)
        {
            AddAtomToInternalCache(atom);
            AtomAddedToAnyResidue?.Invoke(atom, position);
        }

        protected virtual void OnAtomRemovedFromResidue(IAtom atom)
        {
            RemoveAtomFromInternalCache(atom);
            AtomRemovedFromAnyResidue?.Invoke(atom);
        }

        protected virtual void AddAtomToInternalCache(IAtom atom)
        {
            Atoms.Add(atom.SerialNumber, atom);
        }

        protected virtual bool RemoveAtomFromInternalCache(IAtom atom)
        {
            if (Atoms.Remove(atom.SerialNumber))
            {
                atomBonds.Remove(atom);
                foreach (var bonds in atomBonds)
                {
                    bonds.Value.RemoveWhere(x => x.FirstAtom == atom || x.SecondAtom == atom);
                }

                return true;
            }
            return false;
        }

        public virtual IAtom GetAtom(int serialNumber)
        {
            return Atoms.GetValueOrDefault(serialNumber);
        }

        public virtual List<IAtom> GetAtoms()
        {
            return new List<IAtom>(Atoms.Values);
        }

        public virtual int GetAtomsCount()
        {
            return Atoms.Count;
        }

        public virtual Bond GetBond(IAtom firstAtom, IAtom secondAtom)
        {
            HashSet<Bond> bonds;

            if (atomBonds.TryGetValue(firstAtom, out bonds))
            {
                foreach (var bond in bonds)
                {
                    if (bond.IsBetweenAtoms(firstAtom, secondAtom))
                    {
                        return bond;
                    }
                }
            }

            return null;
        }

        public virtual HashSet<Bond> GetBonds()
        {
            HashSet<Bond> result = new HashSet<Bond>();

            foreach (var atomRecord in atomBonds)
            {
                result.UnionWith(atomRecord.Value);
            }

            return result;
        }

        public virtual HashSet<Bond> GetBonds(IAtom atom)
        {
            return atomBonds.GetValueOrDefault(atom);
        }

        public virtual void AddBond(Bond bond)
        {
            // Reducing code duplication with local function
            void AddBondToAtomRecord(IAtom atom)
            {
                if (!atomBonds.ContainsKey(atom))
                {
                    atomBonds.Add(atom, new HashSet<Bond> { bond });
                }
                else
                {
                    atomBonds[atom].Add(bond);
                }
            }

            AddBondToAtomRecord(bond.FirstAtom);
            AddBondToAtomRecord(bond.SecondAtom);
        }

        public virtual int GetBondsCount(IAtom atom)
        {
            return atomBonds.GetValueOrDefault(atom)?.Count ?? 0;
        }

        public virtual void Remove()
        {
            Structure.RemoveChain(this);
        }

        public virtual IChain Clone(Symmetry symmetry)
        {
            var newChain = (IChain)Clone();
            newChain.Identifier = StructureUtils.GenerateChainIdentifier(newChain.Structure.GetChains());
            newChain.Structure.AddChain(newChain);

            var oldChainAtoms = GetAtoms();
            var newChainAtoms = newChain.GetAtoms();

            for (int i = 0; i < newChainAtoms.Count; ++i)
            {
                newChain.Structure.SetAtomPosition(newChainAtoms[i], symmetry.Matrix.MultiplyPoint3x4(oldChainAtoms[i].GetPosition()));
            }

            return newChain;
        }

        public abstract object Clone();

        public abstract IChain Clone(IStructure newStructure);

        public override int GetHashCode()
        {
            return 371 + Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return obj is IChain chain && chain.Id == Id;
        }
    }
}

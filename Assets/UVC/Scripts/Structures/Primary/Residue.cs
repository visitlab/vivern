﻿#define ENABLE_RES_SPLIT_WARNING
#undef ENABLE_RES_SPLIT_WARNING

using System.Collections.Generic;

namespace UVC.Structures
{
    /// <summary>
    /// Residue can act as a "traditional" protein residue, i.e.,
    /// provide access to all its atoms OR it can act as a nucleotide
    /// providing separate access to backbone and base atoms.
    /// 
    /// The latter behavior happens when IsSplitToBackboneAndBase equals to true
    /// </summary>
    public class Residue : AbstractStructureElement, IResidue
    {
        public virtual string SequenceIdef { get; set; }

        public virtual int SequenceNumber { get; set; }

        public virtual char InsertionCode { get; set; }

        public virtual ResidueDataType Type { get; protected set; }

        private string _identifier;
        public virtual string Identifier
        {
            get
            {
                return _identifier;
            }
            set
            {
                _identifier = value;
                Type = Chemistry.GetResidueType(_identifier);

                if (Type == null)
                {
                    Utils.UVCLogger.Instance.LogWarning("Unknown residue type: " + _identifier);
                }
            }
        }

        public override IStructure Structure
        {
            get
            {
                return Chain?.Structure;
            }
        }

        public virtual IChain Chain { get; protected set; }

        public virtual bool IsAminoAcid
        {
            get
            {
                return Type?.IsAminoAcid() ?? false;
            }
        }

        public virtual bool IsBase20AminoAcid
        {
            get
            {
                return Type?.IsBase20AminoAcid() ?? false;
            }
        }

        public virtual bool IsExplicitlyProtonatedAminoAcid
        {
            get
            {
                return Type?.IsExplicitlyProtonatedAminoAcid() ?? false;
            }
        }

        public virtual bool IsNucleobase
        {
            get
            {
                return Type?.IsNucleobase() ?? false;
            }
        }

        public virtual bool IsSolvent
        {
            get
            {
                return Type?.IsSolvent() ?? false;
            }
        }

        public virtual bool IsLigand
        {
            get
            {
                return Type?.IsLigand() ?? false;
            }
        }

        public virtual bool IsSplitToBackboneAndBase
        {
            get
            {
                return Type != null && IsNucleobase;
            }
        }

        public event System.Action<IAtom, UnityEngine.Vector3?> AtomAdded;
        public event System.Action<IAtom> AtomRemoved;

        private readonly List<IAtom> atoms = new List<IAtom>();         // TODO In case of more frequent modifications of the residue atoms, this code should be updated
        private readonly List<IAtom> backboneAtoms = new List<IAtom>(); //      to use faster data structure than list.
        private readonly List<IAtom> baseAtoms = new List<IAtom>();

        public Residue()
        { }

        protected Residue(Residue other, IChain newChain = null)
            : base(other)
        {
            SequenceIdef = other.SequenceIdef;
            SequenceNumber = other.SequenceNumber;
            InsertionCode = other.InsertionCode;
            Type = other.Type;
            _identifier = other._identifier;
            Chain = newChain ?? other.Chain;

            for (int i = 0; i < other.atoms.Count; ++i)
            {
                AddAtom(other.atoms[i].Clone(this));
            }

            for (int i = 0; i < other.backboneAtoms.Count; ++i)
            {
                AddBackboneAtom(other.backboneAtoms[i].Clone(this));
            }

            for (int i = 0; i < other.baseAtoms.Count; ++i)
            {
                AddBaseAtom(other.baseAtoms[i].Clone(this));
            }
        }

        public virtual void AddAtom(IAtom atom, UnityEngine.Vector3? position = null)
        {
#if (ENABLE_RES_SPLIT_WARNING)
            if(IsSplitToBackboneAndBase)
            {
                Utils.UVCLogger.Instance.LogWarning("Trying to add \"general\" atom to residue splitted to backbone & base.");
            }
#endif
            AddAtom(atom, position, atoms);
        }

        protected virtual void AddAtom(IAtom atom, UnityEngine.Vector3? position, List<IAtom> internalListToAddTo)
        {
            atom.Id = Structure.GenerateAtomId();
            atom.SetResidueReference(this);

            internalListToAddTo.Add(atom);

            // HACK Change element number for WATERS
            if (IsSolvent)
            {
                switch (atom.Element)
                {
                    case 1:
                        atom.Element = 113;
                        break;
                    case 8:
                        atom.Element = 114;
                        break;
                }
            }

            AtomAdded?.Invoke(atom, position);
        }

        public virtual void AddBackboneAtom(IAtom atom, UnityEngine.Vector3? position = null)
        {
#if (ENABLE_RES_SPLIT_WARNING)
            if (!IsSplitToBackboneAndBase)
            {
                Utils.UVCLogger.Instance.LogWarning("Trying to add backbone atoms to non-splitted residue.");
            }
#endif
            AddAtom(atom, position, backboneAtoms);
        }

        public virtual void AddBaseAtom(IAtom atom, UnityEngine.Vector3? position = null)
        {
#if (ENABLE_RES_SPLIT_WARNING)
            if (!IsSplitToBackboneAndBase)
            {
                Utils.UVCLogger.Instance.LogWarning("Trying to add base atoms to non-splitted residue.");
            }
#endif
            AddAtom(atom, position, baseAtoms);
        }

        public virtual void RemoveAtom(IAtom atom)
        {
            atoms.Remove(atom);
            backboneAtoms.Remove(atom);
            baseAtoms.Remove(atom);

            AtomRemoved?.Invoke(atom);
        }

        /// <summary>
        /// If the residue is split to backbone and base, 
        /// it is recommended to use appropriate getters 
        /// othwerise this call has O(n) complexity where n = number of atoms
        /// </summary>
        /// <returns>list of all atoms of the residue</returns>
        public virtual List<IAtom> GetAtoms()
        {
            if (IsSplitToBackboneAndBase)
            {
                var result = new List<IAtom>(backboneAtoms.Count + baseAtoms.Count);
                result.AddRange(backboneAtoms);
                result.AddRange(baseAtoms);
                result.AddRange(atoms); // Atoms should be empty but leaving it there just to be sure

                return result;
            }
            return atoms;
        }

        /// <summary>
        /// Returns list of backbone atoms
        /// 
        /// Works only when the residue is split into backbone and base
        /// </summary>
        public virtual List<IAtom> GetBackboneAtoms()
        {
#if (ENABLE_RES_SPLIT_WARNING)
            if(!IsSplitToBackboneAndBase)
            {
                Utils.UVCLogger.Instance.LogWarning("Trying to retrieve backbone atoms from non-splitted residue.");
            }
#endif
            return backboneAtoms;
        }

        /// <summary>
        /// Returns list of base atoms
        /// 
        /// Works only when the residue is split into backbone and base
        /// </summary>
        public virtual List<IAtom> GetBaseAtoms()
        {
#if (ENABLE_RES_SPLIT_WARNING)
            if (!IsSplitToBackboneAndBase)
            {
                Utils.UVCLogger.Instance.LogWarning("Trying to retrieve base atoms from non-separated residue.");
            }
#endif
            return baseAtoms;
        }

        public virtual void SetChainReference(IChain newChainRef)
        {
            Chain = newChainRef;
        }

        public virtual void Remove()
        {
            Chain.RemoveResidue(this);
        }

        public override int GetHashCode()
        {
            return 265 + Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return obj is IResidue residue && residue.Id == Id;
        }

        public object Clone()
        {
            return new Residue(this);
        }

        public IResidue Clone(IChain newChain)
        {
            return new Residue(this, newChain);
        }
    }
}

﻿using System.Collections.Generic;

using Vec3 = UnityEngine.Vector3;

namespace UVC.Structures
{
    public class Atom : AbstractStructureElement, IAtom
    {
        public virtual int SerialNumber { get; set; }

        public virtual byte Element { get; set; }

        public virtual bool IsHetero { get; set; }

        public virtual bool IsHydrogen
        {
            get
            {
                return Element == 1;
            }
        }

        public virtual bool IsCarbon
        {
            get
            {
                return Element == 6;
            }
        }

        public virtual bool IsOxygen
        {
            get
            {
                return Element == 8;
            }
        }

        private bool _isAlphaCarbon;
        public virtual bool IsAlphaCarbon
        {
            get
            {
                return _isAlphaCarbon;
            }
            set
            {
                if (IsCarbon)
                {
                    _isAlphaCarbon = true;
                }
                else
                {
                    throw new System.InvalidOperationException("Cannot set as aplha carbon, atom is not a carbon!");
                }
            }
        }

        public virtual bool IsDonor
        {
            get
            {
                return Residue?.Type?.Donors?.Contains(Name) ?? false;
            }
        }

        public virtual bool IsAcceptor
        {
            get
            {
                return Residue?.Type?.Acceptors?.Contains(Name) ?? false;
            }
        }

        public virtual AtomDataType Type
        {
            get
            {
                return Chemistry.GetAtomType(Element);
            }
        }

        public virtual string Name { get; set; }

        // TODO Alternate locations data are loaded from file but 
        //      the related functionality is not implemented yet
        //      as it is not of high importance.
        //      If it is needed, see CAVER for example implementation.
        //      It will be necessary to slightly modify PDBProcessor, 
        //      Residue and possibly also other code.
        public virtual char AlternateLocation { get; set; } = (char)0;

        public virtual float Occupancy { get; set; }

        public virtual float TemperatureFactor { get; set; }

        public virtual string SegmentId { get; set; }

        public virtual string Charge { get; set; }

        public override IStructure Structure
        {
            get
            {
                return Chain.Structure;
            }
        }

        public virtual IResidue Residue { get; protected set; }

        public virtual IChain Chain
        {
            get
            {
                return Residue.Chain;
            }
        }

        public Atom()
        { }

        protected Atom(Atom other, IResidue replacementResidue = null)
            : base(other)
        {
            SerialNumber = other.SerialNumber;
            Element = other.Element;
            IsHetero = other.IsHetero;
            _isAlphaCarbon = other._isAlphaCarbon;
            Name = other.Name;
            AlternateLocation = other.AlternateLocation;
            Occupancy = other.Occupancy;
            TemperatureFactor = other.TemperatureFactor;
            SegmentId = other.SegmentId;
            Charge = other.Charge;
            Residue = replacementResidue ?? other.Residue;
        }

        public virtual Vec3 GetPosition()
        {
            return Structure.GetAtomPosition(this);
        }

        public virtual void SetPosition(Vec3 newPosition)
        {
            Structure.SetAtomPosition(this, newPosition);
        }

        public virtual bool HasBondTo(IAtom secondAtom)
        {
            return Chain.GetBond(this, secondAtom) != null;
        }

        public virtual HashSet<Bond> GetBonds()
        {
            return Chain.GetBonds(this);
        }

        public virtual int GetBondsCount()
        {
            return Chain.GetBondsCount(this);
        }

        public virtual void SetResidueReference(IResidue residue)
        {
            Residue = residue;
        }

        public override int GetHashCode()
        {
            return 29 * (29 * 5 + SerialNumber) + Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }

            var at = obj as Atom;
            return at != null && at.Id == Id;
        }

        public virtual object Clone()
        {
            return new Atom(this);
        }

        public virtual IAtom Clone(IResidue newResidue)
        {
            return new Atom(this, newResidue);
        }
    }
}

﻿namespace UVC.Structures
{
    public class Bond : System.ICloneable
    {
        public virtual IAtom FirstAtom { get; set; }

        public virtual IAtom SecondAtom { get; set; }

        public virtual int Arity { get; set; }

        public virtual bool IsPredefined { get; set; }

        public Bond(IAtom firstAtom, IAtom secondAtom)
        {
            FirstAtom = firstAtom;
            SecondAtom = secondAtom;
            IsPredefined = false;
        }

        protected Bond(Bond other)
        {
            FirstAtom = other.FirstAtom;
            SecondAtom = other.SecondAtom;
            Arity = other.Arity;
            IsPredefined = other.IsPredefined;
        }

        public virtual IAtom GetOtherAtom(IAtom atom)
        {
            return atom.Equals(FirstAtom) ? SecondAtom : FirstAtom;
        }

        /// <summary>
        /// Verifies, if a bond between given two atoms exists.
        /// In CAVER, this function is called bondExists but 
        /// I have found the current name more suitable.
        /// 
        /// </summary>
        /// <param name="fst">first atom</param>
        /// <param name="snd">second atom</param>
        /// <returns>true if a bond exists, false otherwise</returns>
        public virtual bool IsBetweenAtoms(IAtom fst, IAtom snd)
        {
            return (fst.Equals(FirstAtom) && snd.Equals(SecondAtom)) ||
                (fst.Equals(SecondAtom) && snd.Equals(FirstAtom));
        }

        public override int GetHashCode()
        {
            int hash = 5;
            hash = 59 * hash + (FirstAtom?.GetHashCode() ?? 0);
            hash = 59 * hash + (SecondAtom?.GetHashCode() ?? 0);
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }

            Bond bond = obj as Bond;
            return bond != null && (
                (FirstAtom.Equals(bond.FirstAtom) && SecondAtom.Equals(bond.SecondAtom)) ||
                (FirstAtom.Equals(bond.SecondAtom) && SecondAtom.Equals(bond.FirstAtom))
                );
        }

        // Bond performs a shallow copy!
        public virtual object Clone()
        {
            return new Bond(this);
        }
    }
}

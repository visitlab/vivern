﻿using System.Collections.Generic;

namespace UVC.Structures
{
    public class Chain : AbstractChain<List<IResidue>>
    {
        public Chain()
            : base()
        { }

        public Chain(Chain other, IStructure newStructure = null)
            : base(other, newStructure)
        { }

        public override object Clone()
        {
            return new Chain(this);
        }

        public override IChain Clone(IStructure newStructure)
        {
            return new Chain(this, newStructure);
        }
    }
}

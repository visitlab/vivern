﻿namespace UVC.Structures
{
    public class SecondaryStructureElement
    {
        public int StartIndex { get; set; }

        public int EndIndex { get; set; }

        public SecondaryStructureElementType Type { get; set; }

        public int Length
        {
            get
            {
                return EndIndex - StartIndex;
            }
        }

        public SecondaryStructureTypeFamily TypeFamily { get; set; }

        public SecondaryStructureElement(int startIndex, int endIndex)
        {
            StartIndex = startIndex;
            EndIndex = endIndex;
        }
    }
}

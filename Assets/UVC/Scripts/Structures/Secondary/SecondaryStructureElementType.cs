﻿namespace UVC.Structures
{
    public enum SecondaryStructureElementType
    {
        AlphaHelix,
        BetaStrand,
        ExtendedStrand,
        Helix3,
        HelixPi,
        Turn,
        Bend,
        Coil
    }
}

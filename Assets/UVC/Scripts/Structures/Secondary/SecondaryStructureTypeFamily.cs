﻿namespace UVC.Structures
{
    public enum SecondaryStructureTypeFamily
    {
        Helix,
        Strand,
        Coil,
        Undefined
    }
}

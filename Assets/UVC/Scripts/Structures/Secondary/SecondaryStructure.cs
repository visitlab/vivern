﻿using System.Collections.Generic;

namespace UVC.Structures
{
    public class SecondaryStructure
    {
        public List<SecondaryStructureElement> Elements { get; private set; }
            = new List<SecondaryStructureElement>();

        public SecondaryStructure()
        { }

        public SecondaryStructureElement GetSecondaryStructureElement(IResidue res)
        {
            int index = res.Chain.GetResidues().IndexOf(res);

            for (int i = 0; i < Elements.Count; ++i)
            {
                if (Elements[i].StartIndex <= index && index <= Elements[i].EndIndex)
                {
                    return Elements[i];
                }
            }

            return null;
        }
    }
}

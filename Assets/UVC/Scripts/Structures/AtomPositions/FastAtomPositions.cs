﻿using System.Collections.Generic;
using System.Linq;
using Vec3 = UnityEngine.Vector3;

namespace UVC.Structures
{
    /// <summary>
    /// Fast atom possitions uses array-based underlying data structure
    /// which ensures amortised constant access time but can potentially waste a lot
    /// of space since when removing elements, they are not actually removed due to the
    /// way indexing works
    /// </summary>
    public class FastAtomPositions : IAtomPositions
    {
        /// <summary>
        /// Array of snapshots which contain same-length array of atoms positions
        /// </summary>
        private readonly List<List<Vec3>> positions;

        public FastAtomPositions()
        {
            positions = new List<List<Vec3>>();
        }

        public FastAtomPositions(IAtomPositions atomPositions)
        {
            positions = new List<List<Vec3>>(atomPositions.GetSnapshotCount());

            for (int snapshot = 0; snapshot < positions.Count; ++snapshot)
            {
                var positionsSnapshot = new List<Vec3>();
                var apSnapshot = atomPositions.GetSnapshot(snapshot);
                for (int i = 0; i < apSnapshot.Count; ++i)
                {
                    positionsSnapshot.Add(apSnapshot[i]);
                }
                positions.Add(positionsSnapshot);
            }
        }

        public void SetPosition(int internalAtomId, int snapshotId, Vec3 position)
        {
            if (internalAtomId >= positions[snapshotId].Count)
            {
                var numToAdd = positions[snapshotId].Count - internalAtomId + 1;
                positions[snapshotId].AddRange(Enumerable.Repeat(Vec3.zero, numToAdd));
            }
            positions[snapshotId][internalAtomId] = position;
        }

        public Vec3 GetPosition(int internalAtomId, int snapshotId)
        {
            return positions[snapshotId][internalAtomId];
        }

        public void AddPosition(int snapshotId, Vec3 position)
        {
            positions[snapshotId].Add(position);
        }

        public void RemovePosition(int internalAtomId)
        {
            for (int i = 0; i < positions.Count; ++i)
            {
                if (internalAtomId < positions[i].Count)
                {
                    positions[i][internalAtomId] = Vec3.positiveInfinity;
                }
            }
        }

        public void RemoveSnapshot(int index)
        {
            if (index >= 0 && index < positions.Count)
            {
                positions.RemoveAt(index);
            }
        }

        public void RemovePositions(List<int> internalAtomIds)
        {
            for (int i = internalAtomIds.Count - 1; i >= 0; --i)
            {
                RemovePosition(internalAtomIds[i]);
            }
        }

        public void AddSnapshot(List<Vec3> snapshot)
        {
            positions.Add(snapshot);
        }

        public List<Vec3> GetSnapshot(int snapshotId)
        {
            return positions[snapshotId];
        }

        public int GetSnapshotCount()
        {
            return positions.Count;
        }

        public Vec3 GetCenter(int snapshotId)
        {
            double x = 0;
            double y = 0;
            double z = 0;

            List<Vec3> snapshot = GetSnapshot(snapshotId);
            for (int i = 0; i < snapshot.Count; ++i)
            {
                x += snapshot[i].x;
                y += snapshot[i].y;
                z += snapshot[i].z;
            }

            return new Vec3(
                (float)(x / snapshot.Count),
                (float)(y / snapshot.Count),
                (float)(z / snapshot.Count));
        }

        public void Dispose()
        {
            // Nothing to dispose in this case
        }

        public void UpdatePositions(List<int> oldInternalAtomIds, List<int> newInternalAtomsIds)
        {
            if (oldInternalAtomIds.Count != newInternalAtomsIds.Count)
            {
                Utils.UVCLogger.Instance.LogError("Size mismatch when trying to update positions!");
            }

            for (int snapshot = 0; snapshot < positions.Count; ++snapshot)
            {
                List<Vec3> oldValues = new List<Vec3>();
                for (int i = 0; i < oldInternalAtomIds.Count; ++i)
                {
                    oldValues.Add(GetPosition(oldInternalAtomIds[i], snapshot));
                }

                for (int i = 0; i < newInternalAtomsIds.Count; ++i)
                {
                    SetPosition(newInternalAtomsIds[i], snapshot, oldValues[i]);
                }
            }
        }

        public void ClearData()
        {
            for (int i = 0; i < positions.Count; ++i)
            {
                positions[i].Clear();
            }
            positions.Clear();
        }
    }
}

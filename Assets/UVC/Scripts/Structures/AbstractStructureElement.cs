﻿namespace UVC.Structures
{
    public abstract class AbstractStructureElement : IStructureElement
    {
        public int Id { get; set; } = -1;

        public abstract IStructure Structure { get; }

        public AbstractStructureElement() { }

        protected AbstractStructureElement(AbstractStructureElement other)
        {
            Id = other.Id;
        }
    }
}

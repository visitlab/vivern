﻿using System.Collections.Generic;
using Mat4 = UnityEngine.Matrix4x4;
using Vec3 = UnityEngine.Vector3;

namespace UVC.Structures
{
    public class Structure : IStructure
    {
        public virtual int Id { get; set; }

        public virtual string PdbId { get; set; }

        public virtual string UniprotId { get; set; }

        public virtual int ModelId { get; set; } = 1;

        public virtual string Name { get; set; }

        public virtual string Classification { get; set; }

        public virtual IStructuresContainer ParentContainer { get; set; }

        public virtual List<Symmetry> Symmetries { get; set; } = new List<Symmetry>();

        public event System.Action<IChain> ChainAdded;
        public event System.Action<IChain> ChainRemoved;

        protected int atomIdCounter = 0;
        protected int residueIdCounter = 0;
        protected int chainIdCounter = 0;

        protected readonly IAtomPositions atomPositions = new FastAtomPositions();

        protected virtual List<IAtom> Atoms { get; set; } = new List<IAtom>(); // TODO If atoms were frequently modified, it would be wise to consider replacing the List
        protected virtual HashSet<IResidue> Residues { get; set; } = new HashSet<IResidue>();

        protected Dictionary<string, IChain> chains = new Dictionary<string, IChain>();

        protected readonly Dictionary<int, List<Remark>> remarks = new Dictionary<int, List<Remark>>();

        protected bool refreshChainIndicesOnRemoval = true;

        public Structure()
        { }

        public virtual IAtom GetAtom(int index)
        {
            return Atoms[index];
        }

        public virtual List<IAtom> GetAtoms()
        {
            return Atoms;
        }

        public virtual IAtom GetAtomBySerialNumber(int serialNumber)
        {
            foreach (var chainRecord in chains)
            {
                var atom = chainRecord.Value.GetAtom(serialNumber);
                if (atom != null)
                {
                    return atom;
                }
            }
            return null;
        }

        public virtual int GetAtomCount()
        {
            return Atoms.Count;
        }

        public virtual Vec3 GetAtomPosition(IAtom atom, int snapshot)
        {
            return atomPositions.GetPosition(atom.Id, snapshot);
        }

        public virtual Vec3 GetAtomPosition(IAtom atom)
        {
            // TODO Molecular dynamics are not yet supported (from the perspective 
            // of interpolation between snapshots etc.)
            //
            // If the support is going to be added, it will be probably best to have some 
            // DynamicsManager, storing all MD-related data and reference to structure,
            // which will ensure correct dynamics playback. 
            return atomPositions.GetPosition(atom.Id, 0);
        }

        public virtual void SetAtomPosition(IAtom atom, Vec3 position, int snapshot = 0)
        {
            atomPositions.SetPosition(atom.Id, snapshot, position);
        }

        public virtual void AddAtomPosition(Vec3 position, bool addToAllSnapshots = false)
        {
            if (addToAllSnapshots)
            {
                for (int i = 0; i < atomPositions.GetSnapshotCount(); ++i)
                {
                    atomPositions.AddPosition(i, position);
                }
            }
            else
            {
                atomPositions.AddPosition(0, position);
            }
        }

        public virtual int GetChainCount()
        {
            return chains.Count;
        }

        public virtual bool ChainExists(string chainIdentifier)
        {
            return chains.ContainsKey(chainIdentifier);
        }

        public virtual IChain GetChain(string chainIdentifier)
        {
            return chains.GetValueOrDefault(chainIdentifier);
        }

        public virtual void AddChain(IChain chain)
        {
            chain.Id = GenerateChainId();
            chain.SetStructureReference(this);
            chain.ResidueAdded += OnResidueAddedToChain;
            chain.ResidueRemoved += OnResidueRemovedFromChain;
            chain.AtomAddedToAnyResidue += OnAtomAddedToResidue;
            chain.AtomRemovedFromAnyResidue += OnAtomRemovedFromResidue;

            if (chain.Identifier == " " || chain.Identifier == null)
            {
                chain.Identifier = StructureUtils.GenerateChainIdentifier(GetChains());
            }

            var chainResidues = chain.GetResidues();
            for (int i = 0; i < chainResidues.Count; ++i)
            {
                AddResidueToInternalCache(chainResidues[i]);
            }

            chains.Add(chain.Identifier, chain);
            ChainAdded?.Invoke(chain);
        }

        public virtual void RemoveChain(IChain chain)
        {
            if (chains.Remove(chain.Identifier))
            {
                if (refreshChainIndicesOnRemoval)
                {
                    RefreshChainIndices();
                }

                var chainResidues = chain.GetResidues();
                for (int i = 0; i < chainResidues.Count; ++i)
                {
                    RemoveResidueFromInternalCache(chainResidues[i]);
                }

                chain.RemoveAllResidues();

                chain.ResidueAdded -= OnResidueAddedToChain;
                chain.ResidueRemoved -= OnResidueRemovedFromChain;
                chain.AtomAddedToAnyResidue -= OnAtomAddedToResidue;
                chain.AtomRemovedFromAnyResidue -= OnAtomRemovedFromResidue;

                ChainRemoved?.Invoke(chain);
            }
        }

        protected virtual void RefreshChainIndices()
        {
            chainIdCounter = 1;

            foreach (var ch in chains)
            {
                ch.Value.Id = GenerateChainId();
            }
        }

        public virtual List<IChain> GetChains()
        {
            return new List<IChain>(chains.Values);
        }

        public virtual List<IResidue> GetResidues()
        {
            return new List<IResidue>(Residues);
        }

        public virtual int GetResidueCount()
        {
            return Residues.Count;
        }

        public virtual void AddRemark(Remark remark)
        {
            if (!remarks.ContainsKey(remark.Id))
            {
                remarks.Add(remark.Id, new List<Remark>());
            }

            remarks[remark.Id].Add(remark);
        }

        public virtual Dictionary<int, List<Remark>> GetRemarks()
        {
            return remarks;
        }

        public virtual List<Remark> GetRemark(int remarkId)
        {
            return remarks.GetValueOrDefault(remarkId);
        }

        public virtual int GenerateAtomId()
        {
            return atomIdCounter++;
        }

        public virtual int GenerateResidueId()
        {
            return residueIdCounter++;
        }

        public virtual int GenerateChainId()
        {
            return chainIdCounter++;
        }

        protected virtual void OnAtomAddedToResidue(IAtom atom, Vec3? position)
        {
            AddAtomToInternalCache(atom);
            if (position.HasValue)
            {
                SetAtomPosition(atom, position.Value);
            }
        }

        protected virtual void OnAtomRemovedFromResidue(IAtom atom)
        {
            RemoveAtomFromInternalCache(atom);
            atomPositions.RemovePosition(atom.Id);
        }

        protected virtual void OnResidueAddedToChain(IResidue residue)
        {
            AddResidueToInternalCache(residue);
        }

        protected virtual void OnResidueRemovedFromChain(IResidue residue)
        {
            RemoveResidueFromInternalCache(residue);
        }

        protected virtual void AddAtomToInternalCache(IAtom atom)
        {
            Atoms.Add(atom);
        }

        protected virtual bool RemoveAtomFromInternalCache(IAtom atom)
        {
            return Atoms.Remove(atom);
        }

        protected virtual void AddResidueToInternalCache(IResidue residue)
        {
            Atoms.AddRange(residue.GetAtoms());
            Residues.Add(residue);
        }

        protected virtual bool RemoveResidueFromInternalCache(IResidue residue)
        {
            if (Residues.Remove(residue))
            {
                var resAtoms = residue.GetAtoms();
                Atoms.RemoveAll(x => resAtoms.Contains(x));

                if (residue.Chain.GetResiduesCount() == 0)
                {
                    RemoveChain(residue.Chain);
                }

                return true;
            }
            return false;
        }

        public virtual void AddSnapshot(List<Vec3> snapshot)
        {
            atomPositions.AddSnapshot(snapshot);
        }

        /// <summary>
        /// Extends this structure according to the
        /// stored symmetry data/matrices
        /// </summary>
        public virtual void ApplySymmetries()
        {
            List<IChain> oldChains = GetChains();

            for (int s = 0; s < Symmetries.Count; ++s)
            {
                if (!IsSymIdentityMatrix(Symmetries[s].Matrix))
                {
                    for (int i = 0; i < oldChains.Count; ++i)
                    {
                        AddChain(oldChains[i].Clone(Symmetries[s]));
                    }
                }
            }

            bool IsSymIdentityMatrix(Mat4 mat)
            {
                for (int r = 0; r < 4; ++r)
                {
                    for (int c = 0; c < 4; ++c)
                    {
                        if (r < 3 && r == c)
                        {
                            if (UnityEngine.Mathf.Abs(mat[r, c] - 1.0f) > float.Epsilon)
                            {
                                return false;
                            }
                        }
                        else if (UnityEngine.Mathf.Abs(mat[r, c]) > float.Epsilon)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Releases some of the data held by the structure object
        /// 
        /// Should be only used when it is known that this structure
        /// is not going to be used anymore
        /// </summary>
        public virtual void CleanUp()
        {
            atomPositions.Dispose();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return obj is IStructure structure && structure.Id == Id;
        }

        public override int GetHashCode()
        {
            return 469 + Id;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UVC.Structures
{
    public static class StructureUtils
    {
        private const int chainIdentifierIndexBase = 64;
        private const int chainIdentifierSymbolIndexShift = 1;
        private const int chainIdentifierIndexUpperOffset = '9' - '0' + 1;
        private const int chainIdentifierIndexLowerOffset = chainIdentifierIndexUpperOffset + 'Z' - 'A' + 1;

        // NOTE This function is rather slow & can easily consume few megabytes of memory
        public static string GenerateChainIdentifier(ICollection<IChain> chains)
        {
            HashSet<string> used = new HashSet<string>(chains.Select(x => x.Identifier));

            const int alphCharsNum = 'Z' - 'A' + 1;
            int idx = 0;
            string identifier;

            do
            {
                // map [0-N] to base64 [0-9A-Za-z] encoding
                int idx26 = idx;
                int idx64 = 0;
                int base64 = 1;
                do
                {
                    int symbol = idx26 % alphCharsNum;

                    idx64 += (10 + symbol + chainIdentifierSymbolIndexShift) * base64;
                    base64 *= chainIdentifierIndexBase;

                    idx26 /= alphCharsNum;
                }
                while (idx26 > 0);

                identifier = ToChainIdentifier(idx64);
                ++idx;
            }
            while (used.Contains(identifier));

            return identifier;
        }

        // NOTE This function is rather slow & can easily consume few megabytes of memory
        public static string ToChainIdentifier(int index)
        {
            StringBuilder idef = new StringBuilder();

            int symbolIdx;
            while (index > 0)
            {
                symbolIdx = (index % chainIdentifierIndexBase);
                symbolIdx -= chainIdentifierSymbolIndexShift;

                char letter;
                if (symbolIdx < 10)
                {
                    letter = (char)('0' + symbolIdx);
                }
                else if (symbolIdx < 36)
                {
                    letter = (char)('A' + symbolIdx - chainIdentifierIndexUpperOffset);
                }
                else
                {
                    letter = (char)('a' + symbolIdx - chainIdentifierIndexLowerOffset);
                }

                idef.Append(letter);
                index /= chainIdentifierIndexBase;
            }

            char[] arr = idef.ToString().ToCharArray();
            System.Array.Reverse(arr);
            return new string(arr);
        }
    }
}

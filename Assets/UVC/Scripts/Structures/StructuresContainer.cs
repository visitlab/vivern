﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UVC.Structures
{
    public class StructuresContainer : IStructuresContainer
    {
        private List<IStructure> structures;

        public event Action<IStructure> StructureAdded;

        public event Action<IStructure> StructureRemoved;

        public int Count
        {
            get
            {
                return structures.Count;
            }
        }

        // ICollection interface requirement
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public string SourceName { get; }

        public IStructure this[int index]
        {
            get => structures[index];
            set => structures[index] = value;
        }

        public StructuresContainer()
        {
            structures = new List<IStructure>();
        }

        public StructuresContainer(List<IStructure> structures)
        {
            this.structures = structures;

            for (int i = 0; i < this.structures.Count; ++i)
            {
                this.structures[i].ParentContainer = this;
            }
        }

        public StructuresContainer(List<IStructure> structures, string sourceName)
            : this(structures)
        {
            SourceName = sourceName;
        }

        public IList<IStructure> GetStructures()
        {
            return structures.AsReadOnly();
        }

        public void Add(IStructure item)
        {
            structures.Add(item);
            item.ParentContainer = this;
            StructureAdded?.Invoke(item);
        }

        public void Clear()
        {
            for (int i = structures.Count - 1; i >= 0; --i)
            {
                Remove(structures[i]);
            }
        }

        public bool Contains(IStructure item)
        {
            return structures.Contains(item);
        }

        public void CopyTo(IStructure[] array, int arrayIndex)
        {
            structures.CopyTo(array, arrayIndex);
        }

        public IEnumerator<IStructure> GetEnumerator()
        {
            return structures.GetEnumerator();
        }

        public bool Remove(IStructure item)
        {
            if (structures.Remove(item))
            {
                item?.CleanUp();
                StructureRemoved?.Invoke(item);

                return true;
            }
            return false;
        }

        public bool RemoveAt(int i)
        {
            return Remove(structures[i]);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return structures.GetEnumerator();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UVC.Structures
{
    public interface IChain : System.ICloneable, IStructureElement
    {
        string Identifier { get; set; }
        bool IsEmpty { get; }

        event Action<IAtom, Vector3?> AtomAddedToAnyResidue;
        event Action<IAtom> AtomRemovedFromAnyResidue;
        event Action<IResidue> ResidueAdded;
        event Action<IResidue> ResidueRemoved;

        void AddBond(Bond bond);
        void AddResidue(IResidue residue);
        IChain Clone(IStructure newStructure);
        IChain Clone(Symmetry symmetry);
        bool Equals(object obj);
        IAtom GetAtom(int serialNumber);
        List<IAtom> GetAtoms();
        int GetAtomsCount();
        Bond GetBond(IAtom firstAtom, IAtom secondAtom);
        HashSet<Bond> GetBonds();
        HashSet<Bond> GetBonds(IAtom atom);
        int GetBondsCount(IAtom atom);
        int GetHashCode();
        IResidue GetResidue(string sequenceIdef);
        List<IResidue> GetResidues();
        int GetResiduesCount();
        void Remove();
        void RemoveResidue(IResidue residue);
        void RemoveAllResidues();
        void SetStructureReference(IStructure newStructure);
    }
}
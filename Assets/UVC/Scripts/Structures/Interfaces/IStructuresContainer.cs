﻿using System.Collections.Generic;

namespace UVC.Structures
{
    public interface IStructuresContainer : ICollection<IStructure>
    {
        /// <summary>
        /// Name of the source file, PDB structure etc.
        /// </summary>
        string SourceName { get; }

        IStructure this[int index]
        {
            get;
            set;
        }

        event System.Action<IStructure> StructureAdded;

        event System.Action<IStructure> StructureRemoved;

        bool RemoveAt(int i);

        IList<IStructure> GetStructures();
    }
}

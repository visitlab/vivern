﻿namespace UVC.Structures
{
    public interface IStructureElement
    {
        int Id { get; set; }

        IStructure Structure { get; }
    }
}

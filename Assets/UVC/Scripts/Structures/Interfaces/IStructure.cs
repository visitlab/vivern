﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UVC.Structures
{
    public interface IStructure
    {
        string Classification { get; set; }
        int Id { get; set; }
        int ModelId { get; set; }
        string Name { get; set; }
        IStructuresContainer ParentContainer { get; set; }
        string PdbId { get; set; }
        List<Symmetry> Symmetries { get; set; }
        string UniprotId { get; set; }

        event Action<IChain> ChainAdded;
        event Action<IChain> ChainRemoved;

        void AddAtomPosition(Vector3 position, bool addToAllSnapshots = false);
        void AddChain(IChain chain);
        void AddRemark(Remark remark);
        void AddSnapshot(List<Vector3> snapshot);
        void ApplySymmetries();
        bool ChainExists(string chainIdentifier);
        void CleanUp();
        bool Equals(object obj);
        int GenerateAtomId();
        int GenerateChainId();
        int GenerateResidueId();
        IAtom GetAtom(int index);
        IAtom GetAtomBySerialNumber(int serialNumber);
        int GetAtomCount();
        Vector3 GetAtomPosition(IAtom atom);
        Vector3 GetAtomPosition(IAtom atom, int snapshot);
        List<IAtom> GetAtoms();
        IChain GetChain(string chainIdentifier);
        int GetChainCount();
        List<IChain> GetChains();
        int GetHashCode();
        List<Remark> GetRemark(int remarkId);
        Dictionary<int, List<Remark>> GetRemarks();
        int GetResidueCount();
        List<IResidue> GetResidues();
        void RemoveChain(IChain chain);
        void SetAtomPosition(IAtom atom, Vector3 position, int snapshot = 0);
    }
}
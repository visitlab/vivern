﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UVC.Structures
{
    public interface IResidue : System.ICloneable, IStructureElement
    {
        IChain Chain { get; }
        string Identifier { get; set; }
        char InsertionCode { get; set; }
        bool IsAminoAcid { get; }
        bool IsBase20AminoAcid { get; }
        bool IsExplicitlyProtonatedAminoAcid { get; }
        bool IsLigand { get; }
        bool IsNucleobase { get; }
        bool IsSolvent { get; }
        bool IsSplitToBackboneAndBase { get; }
        string SequenceIdef { get; set; }
        int SequenceNumber { get; set; }
        ResidueDataType Type { get; }

        event Action<IAtom, Vector3?> AtomAdded;
        event Action<IAtom> AtomRemoved;

        void AddAtom(IAtom atom, Vector3? position = null);
        void AddBackboneAtom(IAtom atom, Vector3? position = null);
        void AddBaseAtom(IAtom atom, Vector3? position = null);
        IResidue Clone(IChain newChain);
        bool Equals(object obj);
        List<IAtom> GetAtoms();
        List<IAtom> GetBackboneAtoms();
        List<IAtom> GetBaseAtoms();
        int GetHashCode();
        void Remove();
        void RemoveAtom(IAtom atom);
        void SetChainReference(IChain newChainRef);
    }
}
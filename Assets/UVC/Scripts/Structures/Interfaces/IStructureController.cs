﻿using System;

namespace UVC.Structures
{
    public interface IStructureController
    {
        bool ApplySymmetriesAutomatically { get; set; }

        bool DiscardWaterResidues { get; set; }

        /// <summary>
        /// Allows to add callbacks to be executed when new container is added
        /// </summary>
        event Action<IStructuresContainer> StructuresContainerAdded;

        /// <summary>
        /// Allows to add callbacks to be executed when container is removed
        /// </summary>
        event Action<IStructuresContainer> StructuresContainerRemoved;

        /// <summary>
        /// Allows to add callbacks to be executed when new structure is added to any container
        /// </summary>
        event Action<IStructure> StructureAdded;

        /// <summary>
        /// Allows to add callbacks to be executed when structure is removed from any container
        /// </summary>
        event Action<IStructure> StructureRemoved;

        void AddNewContainer(IStructuresContainer newContainer);

        void RemoveContainer(IStructuresContainer container);

        System.Collections.Generic.List<IStructuresContainer> GetAllContainers();
    }
}

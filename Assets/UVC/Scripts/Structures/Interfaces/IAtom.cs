﻿using System.Collections.Generic;
using UnityEngine;

namespace UVC.Structures
{
    public interface IAtom : System.ICloneable, IStructureElement
    {
        char AlternateLocation { get; set; }
        byte Element { get; set; }
        IChain Chain { get; }
        string Charge { get; set; }
        bool IsAcceptor { get; }
        bool IsAlphaCarbon { get; set; }
        bool IsCarbon { get; }
        bool IsDonor { get; }
        bool IsHetero { get; set; }
        bool IsHydrogen { get; }
        bool IsOxygen { get; }
        string Name { get; set; }
        float Occupancy { get; set; }
        IResidue Residue { get; }
        string SegmentId { get; set; }
        int SerialNumber { get; set; }
        float TemperatureFactor { get; set; }
        AtomDataType Type { get; }

        IAtom Clone(IResidue newResidue);
        bool Equals(object obj);
        HashSet<Bond> GetBonds();
        int GetBondsCount();
        int GetHashCode();
        Vector3 GetPosition();
        bool HasBondTo(IAtom secondAtom);
        void SetPosition(Vector3 newPosition);
        void SetResidueReference(IResidue residue);
    }
}
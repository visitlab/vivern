﻿using System.Collections.Generic;

using Vec3 = UnityEngine.Vector3;

namespace UVC.Structures
{
    public interface IAtomPositions : System.IDisposable
    {
        Vec3 GetPosition(int internalAtomId, int snapshotId);

        void SetPosition(int internalAtomId, int snapshotId, Vec3 position);

        void AddPosition(int snapshotId, Vec3 position);

        void RemovePosition(int internalAtomId);

        void RemovePositions(List<int> internalAtomIds);

        void UpdatePositions(List<int> oldInternalAtomIds, List<int> newInternalAtomsIds);

        void AddSnapshot(List<Vec3> snapshot);

        void RemoveSnapshot(int index);

        List<Vec3> GetSnapshot(int snapshotId);

        int GetSnapshotCount();

        Vec3 GetCenter(int snapshotId);

        void ClearData();
    }
}

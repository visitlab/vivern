﻿using UnityEngine;

namespace UVC.UnityWrappers
{
    public partial class AnyTypeToGameObjectEventForwarder : MonoBehaviour
    {
        [SerializeField]
        private UnityGameObjectEvent CallbacksToCall;

        public void OnSourceEventFired(StructureGameObject structureGameObject)
        {
            CallbacksToCall?.Invoke(structureGameObject.gameObject);
        }
    }
}

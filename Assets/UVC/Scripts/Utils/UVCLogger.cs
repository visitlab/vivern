﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UVC.Utils
{
    /// <summary>
    /// This class serves as an internal logger for UVC
    /// 
    /// Its only purpose is to capture log requests and then transfer
    /// them to higher-level, i.e., application-specific, handlers (registered via callbacks).
    /// It means that it does not store neither provides log records
    /// but serves only as middleman.
    /// Exception to rule above are logs which were recorded when no callback was registered.
    /// Such logs are stored in a queue and then forwarded to the first callback. The queue is then emptied.
    /// 
    /// Although the class is in separate namespace, UVCLogger name is still used instead of simple Logger
    /// because Logger class exists in UnityEngine and I want to make the distinction really clear.
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public class UVCLogger
    {
        // Internal structures

        public enum LogType
        {
            Info,
            Warning,
            Error
        }

        public class LogRecord
        {
            public System.DateTime Time { get; set; }
            public LogType Type { get; set; }
            public string Message { get; set; }

            public string FilePath { get; set; }
            public string MemberName { get; set; }
            public int LineNumber { get; set; }

            public override string ToString()
            {
                return string.Format("[Log record {0} at {1} in {2}_{3}({4})] {5}",
                    GetLogTypeString(Type), Time.ToLongTimeString(),
                    FilePath, MemberName, LineNumber, Message);
            }

            private string GetLogTypeString(LogType t)
            {
                switch (t)
                {
                    case LogType.Info:
                        return "INFO";
                    case LogType.Warning:
                        return "WARNING";
                    case LogType.Error:
                        return "ERROR";
                }
                return "UNDEFINED";
            }
        }

        // Static variables

        private static readonly object instanceLock = new object();

        private static UVCLogger _instance;
        public static UVCLogger Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (instanceLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new UVCLogger();
                        }
                    }
                }
                return _instance;
            }
        }

        // Class content

        protected Queue<LogRecord> logsAddedBeforeCallbacks;
        private readonly object eventLock = new object();

        // To avoid storing lots of logs in case that no callback is going to be attached
        // there is a limit after which no more logs will be added
        protected const int logsAddedBeforeCallbacksCapacityLimit = 32;

        private event System.Action<LogRecord> _newLogAdded;
        public event System.Action<LogRecord> NewLogAdded
        {
            add
            {
                lock (eventLock)
                {
                    _newLogAdded += value;

                    if (_newLogAdded?.GetInvocationList().Length == 1)
                    {
                        while (logsAddedBeforeCallbacks.Count > 0)
                        {
                            _newLogAdded?.Invoke(logsAddedBeforeCallbacks.Dequeue());
                        }
                    }
                }
            }
            remove
            {
                lock (eventLock)
                {
                    _newLogAdded -= value;
                }
            }
        }

        public UVCLogger()
        {
            logsAddedBeforeCallbacks = new Queue<LogRecord>();
        }

        public void Log(LogType type, string message,
                        [CallerFilePath] string file = "",
                        [CallerMemberName] string member = "",
                        [CallerLineNumber] int line = 0)
        {
            var newLogRecord = new LogRecord
            {
                Time = System.DateTime.Now,
                Type = type,
                Message = message,
                FilePath = file,
                MemberName = member,
                LineNumber = line
            };

            lock (instanceLock)
            {
                if (_newLogAdded?.GetInvocationList().Length == 0)
                {
                    if (logsAddedBeforeCallbacks.Count < logsAddedBeforeCallbacksCapacityLimit)
                    {
                        logsAddedBeforeCallbacks.Enqueue(newLogRecord);
                    }
                    // If the capacity limit was reached, enqueue an message informing about this
                    else if (logsAddedBeforeCallbacks.Count == logsAddedBeforeCallbacksCapacityLimit)
                    {
                        logsAddedBeforeCallbacks.Enqueue(new LogRecord
                        {
                            Time = System.DateTime.Now,
                            Type = LogType.Warning,
                            Message = "UVCLogger: the number of logs before adding the first callback" +
                            " reached the capacity limit. Some logs were probably discarded.",
                        });
                    }
                }
                else
                {
                    _newLogAdded?.Invoke(newLogRecord);
                }
            }
        }

        public void LogInfo(string message,
                        [CallerFilePath] string file = "",
                        [CallerMemberName] string member = "",
                        [CallerLineNumber] int line = 0)
        {
            Log(LogType.Info, message, file, member, line);
        }

        public void LogWarning(string message,
                        [CallerFilePath] string file = "",
                        [CallerMemberName] string member = "",
                        [CallerLineNumber] int line = 0)
        {
            Log(LogType.Warning, message, file, member, line);
        }

        public void LogError(string message,
                        [CallerFilePath] string file = "",
                        [CallerMemberName] string member = "",
                        [CallerLineNumber] int line = 0)
        {
            Log(LogType.Error, message, file, member, line);
        }
    }
}

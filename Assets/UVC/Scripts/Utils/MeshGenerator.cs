﻿using System.Collections.Generic;
using UnityEngine;

namespace UVC.UnityWrappers
{
    public static class MeshGenerator
    {
        /// <summary>
        /// Generates tube which could be used, for example, for backbone rendering
        /// </summary>
        /// <param name="points">Points which determine the center positions of the tube rings</param>
        /// <param name="colors">Point colors (will be set as vertex colors) or null if none needed</param>
        /// <param name="radiusInWorldUnits">Radius of the tube in world units</param>
        /// <param name="verticesPerRing">Of how many vertices should one ring of the tube consist</param>
        /// <param name="arrowedEndingRingCount">How many last tube rings will be included in creation of arrow-like ending.</param>
        /// <param name="endingRatio">The radius of the last ring of the tube / rest of the rings. Ratio smaller than 1.0 will create an arrow-like ending.</param>
        /// <returns></returns>
        public static Mesh GenerateBackboneTube(List<Vector3> points, List<Color> colors, float radiusInWorldUnits, int verticesPerRing = 8,
            int arrowedEndingRingCount = 0, float endingRatio = 0.25f)
        {
            if (points.Count <= 1 || verticesPerRing <= 2) { return null; }

            Vector3[] startPoints = new Vector3[verticesPerRing];
            Vector3[] endPoints = new Vector3[verticesPerRing];

            Vector3[] vertices = new Vector3[points.Count * verticesPerRing];
            Color[] meshColors = new Color[vertices.Length];
            int[] triangles = new int[((points.Count - 1) * verticesPerRing + verticesPerRing - 2) * 6];

            int verticesIdx = 0;
            int meshColorsIdx = 0;
            int trianglesIdx = 0;

            bool setMeshColors = colors != null && points.Count == colors.Count;

            // Generate pipe geometry
            // First, one ring of points is computed and then transformed to the new positions
            // The reason why the ring is transformed is that when it was generated again, 
            // it was hard (or to be more precise, I wasn't able to find a solution) to find
            // "matching" vertices (= belonging to the same face) at the beginning and end of each segment
            for (int i = 0; i < points.Count - 1; ++i)
            {
                Vector3 tubeDirection = points[i + 1] - points[i];

                if (i == 0)
                {
                    GetTubeRingPoints(points[i], tubeDirection, GetNormalForTubeDirection(tubeDirection), startPoints, radiusInWorldUnits);
                    System.Array.Copy(startPoints, 0, vertices, verticesIdx, startPoints.Length);
                    verticesIdx += startPoints.Length;

                    if (setMeshColors)
                    {
                        for (int mc = 0; mc < startPoints.Length; ++mc)
                        {
                            meshColors[meshColorsIdx++] = colors[i];
                        }
                    }
                }

                TransformTubeRingPoints(points[i + 1], tubeDirection,
                    i < points.Count - 2 ? points[i + 2] - points[i + 1] : tubeDirection, startPoints, endPoints);

                if(arrowedEndingRingCount > 0)
                {
                    float currRatio = Mathf.Lerp(endingRatio, 1.0f, (points.Count - 2 - i) / (float)arrowedEndingRingCount);

                    for (int j = 0; j < endPoints.Length; ++j)
                    {
                        endPoints[j] = points[i + 1] + (endPoints[j] - points[i + 1]) * (currRatio / 1.0f);
                    }
                }

                int startPosition = verticesIdx - startPoints.Length;
                int endPointIndex, vi1, vi2, vi3, vi4;
                for (int j = 0; j < startPoints.Length; ++j)
                {
                    endPointIndex = j;

                    vi1 = startPosition + (j + 1) % startPoints.Length;
                    vi2 = startPosition + endPointIndex % endPoints.Length + endPoints.Length;
                    vi3 = startPosition + j;
                    vi4 = startPosition + endPoints.Length + (endPointIndex + 1) % endPoints.Length;

                    triangles[trianglesIdx++] = vi1;
                    triangles[trianglesIdx++] = vi2;
                    triangles[trianglesIdx++] = vi3;

                    triangles[trianglesIdx++] = vi4;
                    triangles[trianglesIdx++] = vi2;
                    triangles[trianglesIdx++] = vi1;
                }

                System.Array.Copy(endPoints, 0, vertices, verticesIdx, endPoints.Length);
                verticesIdx += endPoints.Length;

                if (setMeshColors)
                {
                    for (int mc = 0; mc < endPoints.Length; ++mc)
                    {
                        meshColors[meshColorsIdx++] = colors[i + 1];
                    }
                }
                startPoints = endPoints;
            }

            // Create triangle-fan endings to "close the pipes"
            for (int i = 1; i < verticesPerRing - 1; ++i)
            {
                triangles[trianglesIdx++] = i;
                triangles[trianglesIdx++] = 0;
                triangles[trianglesIdx++] = i + 1;

                triangles[trianglesIdx++] = vertices.Length - 1 - i;
                triangles[trianglesIdx++] = vertices.Length - 1;
                triangles[trianglesIdx++] = vertices.Length - 2 - i;
            }

            Mesh tubeMesh = new Mesh();

            tubeMesh.vertices = vertices;
            tubeMesh.triangles = triangles;
            if (setMeshColors)
            {
                tubeMesh.colors = meshColors;
            }
            tubeMesh.RecalculateNormals();

            return tubeMesh;
        }

        private static Vector3 GetNormalForTubeDirection(Vector3 dir)
        {
            if (Mathf.Abs(dir.x) > Mathf.Epsilon)
            {
                return new Vector3((-dir.y - dir.z) / dir.x, 1, 1).normalized;
            }
            else if (Mathf.Abs(dir.y) > Mathf.Epsilon)
            {
                return new Vector3(1, (-dir.x - dir.z) / dir.y, 1).normalized;
            }
            return new Vector3(1, 1, (-dir.x - dir.y) / dir.z).normalized;
        }

        private static void GetTubeRingPoints(Vector3 centerPoint, Vector3 direction, Vector3 normalVector, Vector3[] resultArray, float radiusInWorldUnits)
        {
            float degStep = 360.0f / resultArray.Length;

            for (int i = 0; i < resultArray.Length; ++i)
            {
                resultArray[i] = centerPoint + (Quaternion.AngleAxis(i * degStep, direction) * normalVector).normalized * radiusInWorldUnits;
            }
        }

        private static void TransformTubeRingPoints(Vector3 newCenterPoint, Vector3 currDirection, Vector3 newDirection, Vector3[] currentPosition, Vector3[] resultArray)
        {
            if (currDirection == newDirection)
            {
                for (int i = 0; i < resultArray.Length; ++i)
                {
                    resultArray[i] = currentPosition[i] + currDirection;
                }
            }
            else
            {
                Quaternion rotationAngle = Quaternion.FromToRotation(currDirection, newDirection);
                Vector3 ncpToCdDir = currDirection - newCenterPoint;

                for (int i = 0; i < resultArray.Length; ++i)
                {
                    resultArray[i] = rotationAngle * (currentPosition[i] + ncpToCdDir) + newCenterPoint;
                }
            }
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace UVC.Utils
{
    public static class MathFunctions
    {
        public static Vector3 HermitInterpolation(Vector3 startPoint, Vector3 startTangent, Vector3 endPoint, Vector3 endTangent, float t)
        {
            var t2 = t * t;
            var t3 = t2 * t;

            return (2 * t3 - 3 * t2 + 1) * startPoint + (t3 - 2 * t2 + t) * startTangent + (3 * t2 - 2 * t3) * endPoint + (t3 - t2) * endTangent;
        }

        public static Vector3 ComputeTangent(Vector3 pointKminusOne, Vector3 pointKplusOne, float tension = 0.5f)
        {
            return (pointKplusOne - pointKminusOne) * (1.0f - tension);
        }

        public static Vector3 ComputeEndpointTangent(Vector3 pointK, Vector3 pointKplusOne)
        {
            return pointKplusOne - pointK;
        }

        public static List<Vector3> HermitInterpolatePoints(List<Vector3> points, int interpolationSubdivisionRings)
        {
            List<int> tmpRef = new List<int>();
            return HermitInterpolatePoints(points, interpolationSubdivisionRings, ref tmpRef);
        }

        /// <summary>
        /// Uses Hermit interpolation to interpolate given set of points
        /// </summary>
        /// <param name="points">Input points</param>
        /// <param name="interpolationSubdivisionRings">Number of subdivisions to add (for value X, X new points will be added between two existing points)</param>
        /// <param name="inputPerPointData">In case every point had some data related to it (e.g. color), this list will be resized/refilled to cover newly generated points</param>
        public static List<Vector3> HermitInterpolatePoints<T>(List<Vector3> points, int interpolationSubdivisions, ref List<T> inputPerPointData, float tension = 0.5f)
        {
            if (interpolationSubdivisions <= 0 || points.Count <= 1)
            {
                return points;
            }

            bool skipPerPointData = inputPerPointData.Count != points.Count;

            List<Vector3> result = new List<Vector3>(points.Count * interpolationSubdivisions);
            List<T> resultPerPointData = new List<T>();
            Vector3[] pointsTangents = new Vector3[points.Count];
            int pointsPerSegment = interpolationSubdivisions + 2; // 2 -> start and end points of segment

            pointsTangents[0] = ComputeEndpointTangent(points[0], points[1]);
            for (int i = 1; i < points.Count - 1; ++i)
            {
                pointsTangents[i] = ComputeTangent(points[i - 1], points[i + 1], tension);
            }
            pointsTangents[points.Count - 1] = ComputeEndpointTangent(points[points.Count - 2], points[points.Count - 1]);

            for (int i = 0; i < points.Count - 1; ++i)
            {
                Vector3 startTangent = pointsTangents[i];
                Vector3 endTangent = pointsTangents[i + 1];
                float t;

                for (int j = i == 0 ? 0 : 1; j < pointsPerSegment; ++j)
                {
                    t = (float)j / (pointsPerSegment - 1);
                    result.Add(HermitInterpolation(points[i], startTangent, points[i + 1], endTangent, t));

                    if (!skipPerPointData)
                    {
                        resultPerPointData.Add(t >= 0.5f ? inputPerPointData[i + 1] : inputPerPointData[i]);
                    }
                }
            }

            if (!skipPerPointData)
            {
                inputPerPointData = resultPerPointData;
            }

            return result;
        }
    }
}

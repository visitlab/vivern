﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace UVC
{
    public static class ExtensionMethods
    {
        // XmlNodeList unfortunately does not offer GetElementsWithTagName method by default (although XmlDocument/Element does)
        // so it is necessary to make own (slightly upgraded) implementation
        public static List<XmlNode> GetElementsWithAnyOfTagNames(this XmlNodeList nodeList, params string[] tagNames)
        {
            var result = new List<XmlNode>();

            for (int i = 0; i < nodeList.Count; ++i)
            {
                if (nodeList[i].NodeType == XmlNodeType.Element && tagNames.Contains(nodeList[i].Name))
                {
                    result.Add(nodeList[i]);
                }
            }

            return result;
        }

        public static bool StartsWithIgnoreCase(this string str, params string[] acceptablePatterns)
        {
            for (int i = 0; i < acceptablePatterns.Length; ++i)
            {
                if (str.StartsWith(acceptablePatterns[i], System.StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        public static string SubstringSafe(this string str, int beginIndex)
        {
            return beginIndex >= str.Length ? string.Empty : str.Substring(beginIndex);
        }

        public static string SubstringSafe(this string str, int beginIndex, int length)
        {
            if (beginIndex >= str.Length)
            {
                return string.Empty;
            }

            return str.Substring(beginIndex, System.Math.Min(length, str.Length - beginIndex));
        }

        private static int ParseFromAnyBase(string inputString, string baseCharacters)
        {
            int result = 0;
            int baseVal = baseCharacters.Length;

            for (int i = inputString.Length - 1; i >= 0; --i)
            {
                var charPos = baseCharacters.IndexOf(inputString[i]);
                if (charPos < 0)
                {
                    throw new System.ArgumentOutOfRangeException("Input string is not valid base36 number.");
                }
                result += (int)(charPos * System.Math.Pow(baseVal, inputString.Length - 1 - i));
            }

            return result;
        }

        public static int ToBase36Int(this string inputString)
        {
            return ParseFromAnyBase(inputString.ToUpper(), "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }

        // Extension method taken from here: https://stackoverflow.com/a/33223183
        public static TV GetValueOrDefault<TK, TV>(this IDictionary<TK, TV> dict, TK key, TV defaultValue = default)
        {
            TV value;
            return dict.TryGetValue(key, out value) ? value : defaultValue;
        }

        public static List<T> Clone<T>(this IList<T> clonnedList) where T : System.ICloneable
        {
            List<T> result = new List<T>(clonnedList.Count);
            for (int i = 0; i < clonnedList.Count; ++i)
            {
                result.Add((T)clonnedList[i].Clone());
            }
            return result;
        }

        public static UnityEngine.Vector3 GetCentroid(this IList<Structures.IAtom> atoms)
        {
            UnityEngine.Vector3 sum = UnityEngine.Vector3.zero;

            for (int i = 0; i < atoms.Count; ++i)
            {
                sum += atoms[i].GetPosition();
            }

            return sum / atoms.Count;
        }

        public static void AddRepeated<T>(this List<T> list, T item, int count)
        {
            list.Capacity = list.Capacity + count;

            for (int i = 0; i < count; ++i)
            {
                list.Add(item);
            }
        }
    }
}

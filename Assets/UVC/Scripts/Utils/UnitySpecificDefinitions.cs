﻿using UnityEngine;
using UnityEngine.Events;

namespace UVC.UnityWrappers
{
    public static class UnitySpecificDefinitions
    {
        public static readonly WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
    }

    [System.Serializable]
    public class UnityStructureGoEvent : UnityEvent<StructureGameObject>
    { }

    [System.Serializable]
    public class UnityGameObjectEvent : UnityEvent<GameObject>
    { }
}

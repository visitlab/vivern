﻿using System;
using System.Threading;
using UVC.Structures;

namespace UVC.FileIO
{
    public class ChemFileParserRequest : IChemFileParserRequest
    {
        public virtual IFileLoaderRequest LoaderRequest { get; protected set; }

        public virtual IChemFileFormatProcessor Processor { get; protected set; }

        public virtual FileRequestStatus Status { get; protected set; } = FileRequestStatus.WaitingForStart;

        protected Thread parsingThread = null;

        protected IStructuresContainer parsedDataContainer = null;

        public event Action<IChemFileParserRequest> RequestFinished;

        public ChemFileParserRequest(IFileLoader loader)
        {
            LoaderRequest = new FileLoaderRequest(loader);
        }

        public ChemFileParserRequest(IFileLoaderRequest loaderRequest)
        {
            LoaderRequest = loaderRequest;
        }

        public ChemFileParserRequest(IFileLoader loader, IChemFileFormatProcessor overrideProcessor)
            : this(loader)
        {
            Processor = overrideProcessor;
        }

        public ChemFileParserRequest(IFileLoaderRequest loaderRequest, IChemFileFormatProcessor overrideProcessor)
            : this(loaderRequest)
        {
            Processor = overrideProcessor;
        }

        public virtual void StartParsing()
        {
            if (parsingThread != null)
            {
                return;
            }

            Status = FileRequestStatus.Processing;
            parsingThread = new Thread(new ThreadStart(HandleFileParsing));
            parsingThread.Start();
        }

        public virtual void Abort()
        {
            parsingThread?.Abort();
            Status = FileRequestStatus.Failed;
            CallRequestFinishedCallbacks();
        }

        public virtual IStructuresContainer GetParsedData()
        {
            return parsedDataContainer;
        }

        protected virtual void HandleFileParsing()
        {
            try
            {
                if (Processor == null)
                {
                    Processor = ChemFileFormatProcessorRegistry
                        .GetProcessor(LoaderRequest.Loader.FileProvider.GetFileLocalPath());
                }

                if (Processor == null ||
                    Processor.GetDesiredFileType() != LoaderRequest.Loader.GetLoaderType())
                {
                    throw new InvalidOperationException("Cannot find suitable processor for given file (type): " +
                        LoaderRequest.Loader.FileProvider.GetFileLocalPath());
                }

                LoaderRequest.ChunkLoadingFinished += LoaderChunkLoadingFinished;
                LoaderRequest.RequestFinished += LoaderRequestFinished;
                LoaderRequest.StartLoadingInChunks();

            }
            catch (Exception)
            {
                Status = FileRequestStatus.Failed;
                throw;
            }
        }

        protected virtual void LoaderChunkLoadingFinished(IFileLoaderRequest request, byte[] data)
        {
            Processor.ParseAppend(data);
        }

        protected virtual void LoaderRequestFinished(IFileLoaderRequest request)
        {
            if (request.Status != FileRequestStatus.Success)
            {
                throw new ArgumentException("IFileLoaderRequest status is not success: " + request.Status.ToString());
            }

            parsedDataContainer = new StructuresContainer(Processor.GetParsedStructures(),
                request.Loader.FileProvider.GetFileLocalPath());
            StructureController.Instance.AddNewContainer(parsedDataContainer);

            Status = FileRequestStatus.Success;
            CallRequestFinishedCallbacks();
        }

        protected virtual void CallRequestFinishedCallbacks()
        {
            RequestFinished?.Invoke(this);
        }
    }
}

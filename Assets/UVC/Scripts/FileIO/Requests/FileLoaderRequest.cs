﻿using System;
using System.Threading;

namespace UVC.FileIO
{
    public class FileLoaderRequest : IFileLoaderRequest
    {
        public virtual IFileLoader Loader { get; protected set; }

        public virtual FileRequestStatus Status { get; protected set; } = FileRequestStatus.WaitingForStart;

        protected Thread loadingThread = null;

        public event Action<IFileLoaderRequest, byte[]> ChunkLoadingFinished;
        public event Action<IFileLoaderRequest> RequestFinished;

        public FileLoaderRequest(IFileLoader loader)
        {
            Loader = loader;
        }

        public virtual void StartLoadingInChunks()
        {
            if (loadingThread != null)
            {
                return;
            }

            Status = FileRequestStatus.Processing;
            loadingThread = new Thread(new ThreadStart(HandleFileLoading));
            loadingThread.Start();
        }

        public virtual void Abort()
        {
            loadingThread?.Abort();
            Status = FileRequestStatus.Failed;
            CallRequestFinishedCallbacks();
        }

        protected virtual void HandleFileLoading()
        {
            try
            {
                while (!Loader.IsFileReadingFinished())
                {
                    var chunkData = Loader.LoadChunk();

                    ChunkLoadingFinished?.Invoke(this, chunkData);
                }

                Status = FileRequestStatus.Success;
                CallRequestFinishedCallbacks();
            }
            catch (Exception)
            {
                Status = FileRequestStatus.Failed;
                throw;
            }
        }

        protected virtual void CallRequestFinishedCallbacks()
        {
            RequestFinished?.Invoke(this);
        }
    }
}

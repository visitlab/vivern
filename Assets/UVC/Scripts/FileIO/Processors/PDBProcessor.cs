﻿using System.Collections.Generic;
using UVC.Structures;
using UVC.Utils;
using Mat4 = UnityEngine.Matrix4x4;
/// <summary>
/// Since this class will be most of the time executed on a non-main thread,
/// it is good to note that usage of Unity API in non-main thread is generally not possible
/// but it should work in case of simple data structures like Vector3.
/// It is, however, necessary to make sure that no interaction with gameobjects,
/// transforms, etc. happens from this thread.
/// </summary>
using Vec3 = UnityEngine.Vector3;
using Vec4 = UnityEngine.Vector4;

namespace UVC.FileIO
{
    public class PDBProcessor : IChemFileFormatProcessor
    {
        protected List<IStructure> parsedStructures
            = new List<IStructure>();

        protected List<Vec3> atomPositions
            = new List<Vec3>();

        protected HashSet<SecondaryStructureElement> secStructElems
            = new HashSet<SecondaryStructureElement>();

        protected HashSet<int> modelIds
            = new HashSet<int>();

        protected List<string> unknownAtomElements
            = new List<string>();

        protected string code;
        protected string classification;
        protected int modelId;
        protected int serialNumber;

        protected IStructure structure;
        protected IChain chain;
        protected IResidue residue;

        protected string chainIdentifier;
        protected string lastChainIdentifier;
        protected string residueSequenceIdef;

        protected string residueIdentifier;
        protected int residueSequenceNumber;
        protected int lastResidueSequenceNumber;
        protected char residueInsertionCode;
        protected char lastResidueInsertionCode;

        protected bool isParsingFinished;
        protected bool pendingTermination;

        public PDBProcessor()
        {
            ResetInternalData();
        }

        public virtual bool ParseAppend(byte[] inputData)
        {
            var inputString = FileUtils.ConvertFromBinaryToText(inputData);
            bool result = true;

            // Input argument contains an unspecified number of lines so let's split them and parse one-by-one
            using (System.IO.StringReader sr = new System.IO.StringReader(inputString))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    result &= ProcessLine(line);
                }
            }

            return result;
        }

        protected virtual bool ProcessLine(string line)
        {
            // Process header
            if (line.StartsWithIgnoreCase("HEADER"))
            {
                if (line.Length >= 50)
                {
                    // Classification of a structure
                    classification = line.Substring(10, 40).Trim();

                    if (line.Length >= 66)
                    {
                        // Structure PDB ID
                        code = line.Substring(62, 4).Trim().ToLower();
                    }
                }
            }
            else if (line.StartsWithIgnoreCase("DBREF"))
            {
                // Uniprot database ID is acquired
                if (line.SubstringSafe(26, 5).Trim().Equals("UNP"))
                {
                    structure.UniprotId = line.SubstringSafe(33, 7).Trim();
                }
            }
            else if (line.StartsWithIgnoreCase("MODEL"))
            {
                ResetCurrentStructureValues();
                if (code != null)
                {
                    structure.PdbId = code;
                }

                // Structure name is acquired

                // The null parameter for separator is another way of saying that 
                // white-space characters should be the delimiters
                // (this behavior is consistent with the documentation)
                string[] data = line.Split((char[])null, System.StringSplitOptions.RemoveEmptyEntries);
                int? parsedModelId = null;
                if (data.Length >= 2)
                {
                    try
                    {
                        parsedModelId = int.Parse(data[1]);

                        // Ensure unique model id
                        if (modelIds.Contains(parsedModelId.Value))
                        {
                            UVCLogger.Instance.LogWarning("The PDB file contains multiple MODELs with same ID " + modelId.ToString());
                            parsedModelId = null;
                        }
                    }
                    catch (System.FormatException e)
                    {
                        UVCLogger.Instance.LogWarning("Could not parse MODEL ID: " + e.Message);
                    }
                }

                // Use model id loaded from PDB or generate new one
                modelId = parsedModelId.HasValue ? parsedModelId.Value : GenerateModelId(modelIds);

                structure.ModelId = modelId;
                structure.Classification = classification;
            }
            else if (line.StartsWithIgnoreCase("ENDMDL"))
            {
                ReportUnknownAtomElements();

                // If no atoms were loaded, warn the user about it and return false
                // to inform caller that there was a problem with current chunk of data
                if (structure.GetAtomCount() == 0)
                {
                    UVCLogger.Instance.LogWarning("No atoms found in model " + modelId);
                    return false;
                }

                structure.Name = structure.PdbId.ToString() + "(" + structure.ModelId.ToString() + ")";

                // First frame of atom positions is added to the structure
                structure.AddSnapshot(atomPositions);
                parsedStructures.Add(structure);
            }
            else if (line.StartsWithIgnoreCase("HELIX"))
            {
                ProcessHelixLine(line);
            }
            else if (line.StartsWithIgnoreCase("SHEET"))
            {
                ProcessSheetLine(line);
            }
            else if (line.StartsWithIgnoreCase("ATOM", "HETATM"))
            {
                ProcessAtomLine(line);
            }
            else if (line.StartsWithIgnoreCase("TER"))
            {
                pendingTermination = true;
            }
            else if (line.StartsWithIgnoreCase("CONECT"))
            {
                ProcessConectLine(line);
            }
            else if (line.StartsWithIgnoreCase("REMARK"))
            {
                ProcessRemarkLine(line);
            }

            return true;
        }

        protected virtual void FinishParsing()
        {
            isParsingFinished = true;

            // If there was only one structure in file, no ENDMDL
            // section is present and it is thus necessary to do some of the work now
            if (parsedStructures.Count == 0)
            {
                ReportUnknownAtomElements();

                if (code != null)
                {
                    structure.PdbId = code;
                }

                structure.Classification = classification == null ? "UNKNOWN" : classification;
                structure.Name = structure.PdbId?.ToString() + "(" + structure.ModelId.ToString() + ")";

                if (structure.GetAtomCount() == 0)
                {
                    UVCLogger.Instance.LogWarning("No atoms found in file");
                    return;
                }

                structure.AddSnapshot(atomPositions);
                ParseSymmetries(structure);
                parsedStructures.Add(structure);
            }
        }

        public virtual bool Accepts(string localFilePath)
        {
            return FileUtils.GetFileExtension(localFilePath).ToLower().Trim() == "pdb";
        }

        public virtual void ResetInternalData()
        {
            parsedStructures.Clear();
            secStructElems.Clear();
            code = null;
            classification = null;
            modelIds.Clear();
            unknownAtomElements.Clear();
            isParsingFinished = false;
            pendingTermination = false;

            ResetCurrentStructureValues();
        }

        protected virtual void ResetCurrentStructureValues()
        {
            lastChainIdentifier = string.Empty;
            lastResidueSequenceNumber = int.MinValue;
            lastResidueInsertionCode = ' ';
            structure = new Structure();
            atomPositions.Clear();
        }

        public virtual List<IStructure> GetParsedStructures()
        {
            if (!isParsingFinished)
            {
                FinishParsing();
            }

            return parsedStructures;
        }

        public virtual FileType GetDesiredFileType()
        {
            return FileType.Text;
        }

        protected virtual int GenerateModelId(HashSet<int> modelIds)
        {
            int id;
            for (id = modelIds.Count + 1; modelIds.Contains(id); ++id) ;

            modelIds.Add(id);
            return id;
        }

        protected virtual void ReportUnknownAtomElements()
        {
            const int maxReportSize = 5;
            int overTheTop = unknownAtomElements.Count - maxReportSize;
            int loopIterations = overTheTop > 0 ? maxReportSize : unknownAtomElements.Count;

            for (int i = 0; i < loopIterations; ++i)
            {
                UVCLogger.Instance.LogInfo("Uknown atom element. Extracting atom element from atom name " + unknownAtomElements[i]);
            }

            if (overTheTop > 0)
            {
                UVCLogger.Instance.LogInfo("There were additional (" + overTheTop + ") uknown atom elements");
            }

            unknownAtomElements.Clear();
        }

        protected virtual void ProcessAtomLine(string line)
        {
            string atomElement = "0";

            IAtom atom = new Atom();
            atom.IsHetero = line.StartsWithIgnoreCase("HETATM");

            var serialNumberText = line.SubstringSafe(6, 5).Trim();
            if (!int.TryParse(serialNumberText, out serialNumber))
            {
                serialNumber = serialNumberText.ToBase36Int();
            }

            atom.SerialNumber = serialNumber;
            atom.Name = line.SubstringSafe(12, 4).Trim();

            var altLoc = line.SubstringSafe(16, 1)?[0];
            if (!altLoc.HasValue)
            {
                throw new System.FormatException("Invalid atom line. Missing altLoc record.");
            }
            atom.AlternateLocation = altLoc.Value == ' ' ? (char)0 : altLoc.Value;

            chainIdentifier = line.SubstringSafe(21, 1);
            residueSequenceIdef = line.SubstringSafe(22, 4).Trim();
            if (!int.TryParse(residueSequenceIdef, out residueSequenceNumber))
            {
                ++residueSequenceNumber;
            }

            residueInsertionCode = line.SubstringSafe(26, 1)[0];

            const int colsPerElement = 8;
            Vec3 position = new Vec3();
            for (int i = 0; i < 3; ++i)
            {
                position[i] = System.Convert.ToSingle(
                    line.SubstringSafe(30 + i * colsPerElement, colsPerElement).Trim(), System.Globalization.CultureInfo.InvariantCulture);
            }
            atomPositions.Add(position);

            // If you are thinking "WTF", the answer is "PDB"
            if (line.Length >= 60)
            {
                atom.Occupancy = line.Substring(54, 6).Trim().Length > 0 ?
                        System.Convert.ToSingle(line.Substring(54, 6).Trim(), System.Globalization.CultureInfo.InvariantCulture) : 1.0f;

                if (line.Length >= 66)
                {
                    float bFactor = System.Convert.ToSingle(line.Substring(60, 6).Trim(), System.Globalization.CultureInfo.InvariantCulture);
                    atom.TemperatureFactor = bFactor;

                    if (line.Length >= 76)
                    {
                        atom.SegmentId = line.Substring(72, 4);

                        if (line.Length >= 78)
                        {
                            atomElement = line.Substring(76, 2).Trim();

                            if (line.Length >= 80)
                            {
                                atom.Charge = line.Substring(78, 2).Trim();
                            }
                        }
                    }
                }
            }

            if (chainIdentifier.Trim().Length == 0)
            {
                chainIdentifier = EstimateChainIdentifier(residueSequenceNumber);
                if (chainIdentifier.Trim().Length == 0)
                {
                    chainIdentifier = "A";
                }
            }

            if (chainIdentifier != lastChainIdentifier ||
                residueSequenceNumber < lastResidueSequenceNumber ||
                pendingTermination)
            {
                if (structure.ChainExists(chainIdentifier))
                {
                    chain = structure.GetChain(chainIdentifier);
                }
                else
                {
                    chain = new Chain();
                    chain.Identifier = chainIdentifier;
                    structure.AddChain(chain);
                }

                pendingTermination = false;
            }

            if (residueSequenceNumber != lastResidueSequenceNumber ||
               residueInsertionCode != lastResidueInsertionCode)
            {
                residue = chain.GetResidue(residueSequenceIdef);

                if (residue == null)
                {
                    residue = new Residue();
                    residue.SequenceNumber = residueSequenceNumber;
                    residue.SequenceIdef = residueSequenceIdef;
                    residue.InsertionCode = residueInsertionCode;

                    residueIdentifier = line.SubstringSafe(17, 3).Trim();
                    residue.Identifier = residueIdentifier;

                    chain.AddResidue(residue);
                }
            }

            bool valid = true;

            if (atomElement != "0")
            {
                if (atomElement.Length == 2)
                {
                    atomElement = atomElement[0].ToString() + atomElement.ToLower()[1].ToString();
                }

                if (Chemistry.VerifyAtomElementExistence(atomElement))
                {
                    atom.Element = (byte)Chemistry.GetAtomType(atomElement).AtomicNumber;
                }
            }

            if (atom.Element == 0)
            {
                unknownAtomElements.Add(atom.Name.Trim());

                if (atom.Name[0] != ' ')
                {
                    bool hydrogen = atom.Name[0] == 'H';

                    if (Chemistry.VerifyAtomElementExistence(atom.Name.Substring(0, 2)))
                    {
                        atom.Element = (byte)Chemistry.GetAtomType(atom.Name.Substring(0, 2)).AtomicNumber;
                    }
                    else if (!hydrogen && Chemistry.VerifyAtomElementExistence(atom.Name.Substring(1, 1)))
                    {
                        atom.Element = (byte)Chemistry.GetAtomType(atom.Name.Substring(1, 1)).AtomicNumber;
                    }
                    else if (Chemistry.VerifyAtomElementExistence(atom.Name.Substring(0, 1)))
                    {
                        atom.Element = (byte)Chemistry.GetAtomType(atom.Name.Substring(0, 1)).AtomicNumber;
                    }
                    else
                    {
                        valid = false;
                        UVCLogger.Instance.LogError("Atom " + serialNumber + " " + atom.Name + " has unknown element type and will not be added to the structure.");
                    }
                }
                else
                {
                    if (Chemistry.VerifyAtomElementExistence(atom.Name.Substring(1, 1)))
                    {
                        atom.Element = (byte)Chemistry.GetAtomType(atom.Name.Substring(1, 1)).AtomicNumber;
                    }
                    else if (Chemistry.VerifyAtomElementExistence(atom.Name.Substring(2, 1)))
                    {
                        atom.Element = (byte)Chemistry.GetAtomType(atom.Name.Substring(2, 1)).AtomicNumber;
                    }
                    else if (Chemistry.VerifyAtomElementExistence(atom.Name.Substring(3, 1)))
                    {
                        atom.Element = (byte)Chemistry.GetAtomType(atom.Name.Substring(3, 1)).AtomicNumber;
                    }
                    else
                    {
                        valid = false;
                        UVCLogger.Instance.LogError("Atom " + serialNumber + " " + atom.Name + " has unknown element type and will not be added to the structure.");
                    }
                }
            }

            if (valid)
            {
                if (atom.Element == Chemistry.GetAtomType("C").AtomicNumber)
                {
                    atom.IsAlphaCarbon = atom.Name.Contains("CA");
                }

                if (residue.IsSplitToBackboneAndBase)
                {
                    // Naming of appropriate parts of (R)(D)NA should be ideally consistent with
                    // the convention that (deoxy)ribose atoms should contain prime (') symbol
                    // and phosphate atoms should contain P in name
                    // See: http://www.bmrb.wisc.edu/referenc/nomenclature/
                    if (atom.Name.EndsWith("'") || atom.Name.Contains("P"))
                    {
                        residue.AddBackboneAtom(atom);
                    }
                    else
                    {
                        residue.AddBaseAtom(atom);
                    }
                }
                else
                {
                    residue.AddAtom(atom);
                }
            }

            lastResidueSequenceNumber = residueSequenceNumber;
            lastResidueInsertionCode = residueInsertionCode;
            lastChainIdentifier = chainIdentifier;
        }

        protected virtual void ProcessHelixLine(string line)
        {
            try
            {
                int startIndex = int.Parse(line.SubstringSafe(21, 4).Trim());
                int endIndex = int.Parse(line.SubstringSafe(33, 4).Trim());
                SecondaryStructureElement ssElem = new SecondaryStructureElement(startIndex, endIndex);
                ssElem.Type = SecondaryStructureElementType.AlphaHelix;
                ssElem.TypeFamily = SecondaryStructureTypeFamily.Helix;
                secStructElems.Add(ssElem);
            }
            catch (System.Exception e)
            {
                UVCLogger.Instance.LogError("Error parsing helix line: " + e.Message);
            }
        }

        protected virtual void ProcessSheetLine(string line)
        {
            try
            {
                int startIndex = int.Parse(line.SubstringSafe(22, 4).Trim());
                int endIndex = int.Parse(line.SubstringSafe(33, 4).Trim());
                SecondaryStructureElement ssElem = new SecondaryStructureElement(startIndex, endIndex);
                ssElem.Type = SecondaryStructureElementType.BetaStrand;
                secStructElems.Add(ssElem);
            }
            catch (System.Exception e)
            {
                UVCLogger.Instance.LogError("Error parsing sheet line: " + e.Message);
            }
        }

        protected virtual void ProcessConectLine(string line)
        {
            void ParseAtomSerialNumber(out int serialNumber, int start, int length = 5)
            {
                if (!int.TryParse(line.SubstringSafe(start, length).Trim(), out serialNumber))
                {
                    try
                    {
                        serialNumber = line.SubstringSafe(start, length).Trim().ToBase36Int();
                    }
                    catch (System.Exception e)
                    {
                        UVCLogger.Instance.LogError("Error parsing conect line: " + e.Message);
                    }
                }
            }

            int firstAtomSerialNumber;
            ParseAtomSerialNumber(out firstAtomSerialNumber, 6);

            IAtom firstAtom = structure.GetAtomBySerialNumber(firstAtomSerialNumber);
            if (firstAtom == null)
            {
                return;
            }

            for (int i = 11; i < line.Length; i += 5)
            {
                int secondAtomSerialNumber;
                ParseAtomSerialNumber(out secondAtomSerialNumber, i);

                IAtom secondAtom = structure.GetAtomBySerialNumber(secondAtomSerialNumber);
                if (!firstAtom.Chain.Equals(secondAtom?.Chain))
                {
                    return;
                }

                if (!firstAtom.HasBondTo(secondAtom))
                {
                    Bond bond = new Bond(firstAtom, secondAtom);
                    bond.IsPredefined = true;

                    firstAtom.Chain.AddBond(bond);
                    if (firstAtom.Chain != secondAtom.Chain)
                    {
                        secondAtom.Chain.AddBond(bond);
                    }
                }
            }
        }

        protected virtual void ProcessRemarkLine(string line)
        {
            try
            {
                if (line.Length < 10)
                {
                    return;
                }

                int remarkId = int.Parse(line.SubstringSafe(7, 3).Trim());
                string text = line.SubstringSafe(10);
                structure.AddRemark(new Remark(remarkId, text));
            }
            catch (System.Exception e)
            {
                UVCLogger.Instance.LogError("Error parsing remark line: " + e.Message);
            }
        }

        protected virtual void ParseSymmetries(IStructure structure)
        {
            List<Remark> symRemarks = structure.GetRemark(350);

            if (symRemarks == null || symRemarks.Count == 0)
            {
                return;
            }

            int startIndex = 0;
            for (int i = 0; i < symRemarks.Count; ++i)
            {
                if (symRemarks[i].Text.ToUpper().Contains("BIOMT1"))
                {
                    startIndex = i;
                    break;
                }
            }

            List<Symmetry> symmetries = new List<Symmetry>();

            int id = 1;
            for (int i = startIndex; i < symRemarks.Count; i += 3)
            {
                if (!symRemarks[i].Text.ToUpper().Contains("BIOMT"))
                {
                    break;
                }

                var v1 = ParseSymmetryRemark(symRemarks[i].Text);
                var v2 = ParseSymmetryRemark(symRemarks[i + 1].Text);
                var v3 = ParseSymmetryRemark(symRemarks[i + 2].Text);

                // Matrix is transposed since it is considered as column-major in Unity
                // while row-major in PDB/Caver
                symmetries.Add(new Symmetry(id.ToString(), new Mat4(v1, v2, v3, Vec4.zero).transpose));
                ++id;
            }

            structure.Symmetries = symmetries;

            // Local function to "deduplicate" parsing
            Vec4 ParseSymmetryRemark(string row)
            {
                return new Vec4(
                    float.Parse(row.SubstringSafe(13, 10).Trim(), System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(row.SubstringSafe(23, 10).Trim(), System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(row.SubstringSafe(33, 10).Trim(), System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(row.SubstringSafe(43).Trim(), System.Globalization.CultureInfo.InvariantCulture));
            }
        }

        protected virtual string EstimateChainIdentifier(int resSeqNum)
        {
            string identifier;

            if (resSeqNum < lastResidueSequenceNumber)
            {
                identifier = StructureUtils.GenerateChainIdentifier(structure.GetChains());
            }
            else
            {
                identifier = lastChainIdentifier;
            }

            return identifier;
        }
    }
}

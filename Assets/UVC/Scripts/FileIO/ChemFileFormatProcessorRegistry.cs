﻿namespace UVC.FileIO
{
    public static class ChemFileFormatProcessorRegistry
    {
        /// <summary>
        /// Returns all file format processors registered to the system
        /// 
        /// To avoid potential concurrency problems, new instance of
        /// processor is returned on every call
        /// </summary>
        /// <returns>Array of file format processors</returns>
        public static IChemFileFormatProcessor[] GetAllProcessors()
        {
            return new IChemFileFormatProcessor[]
            {
                new PDBProcessor()
            };
        }

        /// <summary>
        /// Returns correct processor for the given file
        /// </summary>
        /// <param name="filePath">Local path to the file</param>
        /// <returns>File processor suitable for this file type or null if none exists</returns>
        public static IChemFileFormatProcessor GetProcessor(string localFilePath)
        {
            var processors = GetAllProcessors();

            for (int i = 0; i < processors.Length; ++i)
            {
                if (processors[i].Accepts(localFilePath))
                {
                    return processors[i];
                }
            }

            return null;
        }
    }
}

﻿namespace UVC.FileIO
{
    /// <summary>
    /// Possible file types (used mainly for loaders and parsers)
    /// </summary>
    public enum FileType
    {
        Binary,
        Text
    }

    /// <summary>
    /// Useful functions related to File I/O
    /// </summary>
    public static class FileUtils
    {
        public static string ConvertFromBinaryToText(byte[] inputBytes)
        {
            return System.Text.Encoding.UTF8.GetString(inputBytes);
        }

        public static byte[] ConvertFromTextToBinary(string inputText)
        {
            return System.Text.Encoding.UTF8.GetBytes(inputText);
        }

        /// <summary>
        /// Returns extension of the given file
        /// </summary>
        /// <param name="path">Path to the file</param>
        /// <returns>extension (without the period ".")</returns>
        public static string GetFileExtension(string path)
        {
            var ext = System.IO.Path.GetExtension(path);

            // Path.GetExtension() method includes the period as a part
            // of the extension so it is necessary to get rid of it
            if (ext.Length > 0)
            {
                ext = ext.Substring(1);
            }

            return ext;
        }

        /// <summary>
        /// Returns name of the given file
        /// </summary>
        /// <param name="path">Path to the file</param>
        /// <returns>filename</returns>
        public static string GetFileName(string path)
        {
            return System.IO.Path.GetFileName(path);
        }
    }
}

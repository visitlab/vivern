﻿using System;

namespace UVC.FileIO
{
    public interface IFileLoaderRequest : IFileRequest<IFileLoaderRequest>
    {
        IFileLoader Loader { get; }

        /// <summary>
        /// This event allows to add functions to be called after chunk loading is finished
        /// 
        /// The callback might be executed on different thread than the caller's thread!
        /// </summary>
        event Action<IFileLoaderRequest, byte[]> ChunkLoadingFinished;

        /// <summary>
        /// This function will trigger ChunkLoadingFinishedCallback every time one chunk is loaded into memory
        /// When there are no more chunks to load, RequestFinishedCallback will be fired
        /// </summary>
        void StartLoadingInChunks();
    }
}

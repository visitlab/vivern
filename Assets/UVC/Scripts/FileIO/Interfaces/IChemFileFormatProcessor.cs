﻿using System.Collections.Generic;
using UVC.Structures;

namespace UVC.FileIO
{
    public interface IChemFileFormatProcessor
    {
        /// <summary>
        /// Parses given input data and stores the intermediate result into internal memory
        /// 
        /// If you want to start new parsing, it is necessary to call ResetInternalData()
        /// after retrieving the current data via GetParsedStructures()
        /// </summary>
        /// <param name="inputData">Data to parse</param>
        /// <returns>True if no problems were encountered during parsing, false otherwise</returns>
        bool ParseAppend(byte[] inputData);

        /// <summary>
        /// Returns parsed structures
        /// 
        /// By calling this method, you inform the processor that the current parsing is finished,
        /// i.e., the processor might run some postprocessing functionality in the first execution
        /// of this method since last call to ResetInternalData() or after object creation.
        /// </summary>
        /// <returns>List of parsed structures</returns>
        List<IStructure> GetParsedStructures();

        /// <summary>
        /// Resets internal data of parser to allow its reuse for new parsing
        /// </summary>
        void ResetInternalData();

        bool Accepts(string localFilePath);

        FileType GetDesiredFileType();
    }
}

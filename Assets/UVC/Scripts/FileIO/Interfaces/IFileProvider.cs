﻿namespace UVC.FileIO
{
    public interface IFileProvider
    {
        /// <summary>
        /// Path which was provided as an input to the file provider (e.g., http://abc.def/davidIsCool.png)
        /// </summary>
        string GetFileOriginalPath();

        /// <summary>
        /// Local path on which the desired file can be found
        /// If the file is not yet available locally, it is downloaded during the first method execution
        /// In all subsequent calls, cached path (without downloading) is returned
        /// 
        /// This method causes a blocking call
        /// </summary>
        string GetFileLocalPath();
    }
}

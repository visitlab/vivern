﻿using UVC.Structures;

namespace UVC.FileIO
{
    public interface IChemFileParserRequest : IFileRequest<IChemFileParserRequest>
    {
        IFileLoaderRequest LoaderRequest { get; }

        IChemFileFormatProcessor Processor { get; }

        void StartParsing();

        /// <summary>
        /// Method used to retrieve final parsed data
        /// </summary>
        /// <returns>Final parsed data or null if Status != FileRequestStatus.Success</returns>
        IStructuresContainer GetParsedData();
    }
}

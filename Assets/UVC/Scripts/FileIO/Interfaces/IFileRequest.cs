﻿using System;

namespace UVC.FileIO
{
    /// <summary>
    /// This enum contains the possible values for status of the file request
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public enum FileRequestStatus
    {
        Success = 0,
        WaitingForStart,
        Processing,
        Failed
    }

    /// <summary>
    /// Common interface for file request enabling to query the request status and abort it
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public interface IFileRequest
    {
        /// <summary>
        /// Contains information about current status of the request
        /// </summary>
        FileRequestStatus Status { get; }

        /// <summary>
        /// Allows to abort the request execution
        /// </summary>
        void Abort();
    }

    /// <summary>
    /// Extended request interface allowing to attach a function 
    /// to be called on certain occasions
    /// </summary>
    /// <remarks>Author: David Kuťák, 433409@mail.muni.cz</remarks>
    public interface IFileRequest<TSender> : IFileRequest
    {
        /// <summary>
        /// Allows to add a function to be called every time the request fully finishes its work
        /// Parameter will be this request. Check IFileRequest.Status to verify the state of the request.
        /// 
        /// The callback might be executed on different thread than the caller's thread!
        /// </summary>
        event Action<TSender> RequestFinished;
    }
}

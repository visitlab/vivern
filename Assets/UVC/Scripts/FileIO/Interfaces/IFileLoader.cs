﻿namespace UVC.FileIO
{
    public interface IFileLoader
    {
        IFileProvider FileProvider { get; }

        /// <summary>
        /// Returns one chunk of data loaded from file
        /// 
        /// This method causes a blocking call
        /// First call might take a significant amount of time due to the overhead of file initialization
        /// </summary>
        byte[] LoadChunk();

        bool IsFileReadingFinished();

        FileType GetLoaderType();
    }
}

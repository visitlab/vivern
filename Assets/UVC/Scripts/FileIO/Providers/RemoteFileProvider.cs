﻿namespace UVC.FileIO
{
    public class RemoteFileProvider : IFileProvider
    {
        protected virtual string Uri { get; set; }

        protected virtual string LocalFileName { get; set; }

        protected virtual string Username { get; set; }

        protected virtual string Password { get; set; }

        protected virtual bool FileIsReady { get; set; }

        public RemoteFileProvider(string uri, string localFileName,
            string username = null, string password = null)
        {
            Uri = uri;
            LocalFileName = localFileName;
            Username = username;
            Password = password;

            FileIsReady = false;
        }

        public virtual string GetFileOriginalPath()
        {
            return Uri;
        }

        public virtual string GetFileLocalPath()
        {
            if (!FileIsReady)
            {
                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    if (Username != null)
                    {
                        wc.Credentials = new System.Net.NetworkCredential(Username, Password);
                    }
                    wc.DownloadFile(Uri, LocalFileName);
                    FileIsReady = true;
                }
            }

            return LocalFileName;
        }
    }
}

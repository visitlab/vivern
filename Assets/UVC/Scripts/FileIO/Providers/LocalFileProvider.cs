﻿namespace UVC.FileIO
{
    public class LocalFileProvider : IFileProvider
    {
        protected virtual string Path { get; set; }

        public LocalFileProvider(string path)
        {
            Path = path;
        }

        public virtual string GetFileOriginalPath()
        {
            return Path;
        }

        public virtual string GetFileLocalPath()
        {
            return Path;
        }
    }
}

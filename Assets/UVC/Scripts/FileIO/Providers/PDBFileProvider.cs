﻿namespace UVC.FileIO
{
    public class PDBFileProvider : RemoteFileProvider
    {
        public PDBFileProvider(string pdbId)
            : base("http://files.rcsb.org/download/" + pdbId + ".pdb", pdbId + ".pdb")
        { }
    }
}

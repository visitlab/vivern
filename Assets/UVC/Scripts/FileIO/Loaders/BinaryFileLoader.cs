﻿using System.Linq;

namespace UVC.FileIO
{
    public class BinaryFileLoader : IFileLoader
    {
        public virtual IFileProvider FileProvider { get; }

        protected long numOfBytesInOneChunk;

        protected long startBytesPosition;

        protected long endBytesPosition;

        protected System.IO.FileStream file;

        protected bool isFileInitialized = false;

        public BinaryFileLoader(IFileProvider filePathProvider, long numberOfBytesInOneChunk = -1,
            long startPositionInBytes = -1, long endPositionInBytes = -1)
        {
            FileProvider = filePathProvider;
            numOfBytesInOneChunk = numberOfBytesInOneChunk;
            startBytesPosition = startPositionInBytes;
            endBytesPosition = endPositionInBytes;
        }

        protected virtual void InitFile()
        {
            var path = FileProvider.GetFileLocalPath();
            file = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read);

            if (startBytesPosition > 0)
            {
                file.Position = startBytesPosition;
            }

            isFileInitialized = true;
        }

        public virtual FileType GetLoaderType()
        {
            return FileType.Binary;
        }

        public virtual bool IsFileReadingFinished()
        {
            return isFileInitialized &&
                (file == null || file.Position >= file.Length || (endBytesPosition >= 0 && file.Position > endBytesPosition));
        }

        public virtual byte[] LoadChunk()
        {
            if (!isFileInitialized)
            {
                InitFile();
            }

            try
            {
                long upperLimit = endBytesPosition < 0 ? file.Length - 1 : endBytesPosition;
                long numOfBytesToRead = numOfBytesInOneChunk < 0 ?
                    upperLimit - file.Position + 1 :
                    System.Math.Min(numOfBytesInOneChunk, upperLimit - file.Position + 1);

                byte[] chunkBytes = new byte[numOfBytesToRead];
                var bytesRead = file.Read(chunkBytes, 0, System.Convert.ToInt32(numOfBytesToRead));

                if (IsFileReadingFinished())
                {
                    CloseFile();
                }

                // If the number of read bytes was lower than desired, slice
                // the array to the range with valid values
                return chunkBytes.Take(bytesRead).ToArray();
            }
            catch
            {
                CloseFile();
                throw;
            }
        }

        protected virtual void CloseFile()
        {
            file?.Close();
            file = null;
        }
    }
}

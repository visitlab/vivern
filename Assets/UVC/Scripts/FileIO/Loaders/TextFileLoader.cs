﻿using System.Text;

namespace UVC.FileIO
{
    public class TextFileLoader : IFileLoader
    {
        public virtual IFileProvider FileProvider { get; }

        /// <summary>
        /// Which line will be read by a subsequent call to ReadLine()
        /// Numbering starts from zero
        /// </summary>
        protected int currentLineNum;

        /// <summary>
        /// Number of lines in each chunk (e.g., each call to LoadChunk() returns up to this number of lines)
        /// </summary>
        protected int numOfLinesInOneChunk;

        /// <summary>
        /// Line number to be the first one to read 
        /// Numbering starts from zero
        /// </summary>
        protected int startLineNum;

        /// <summary>
        /// Last line to be read
        /// Numbering starts from zero
        /// </summary>
        protected int endLineNum;

        protected System.IO.StreamReader file;

        protected bool isFileInitialized = false;

        public TextFileLoader(IFileProvider filePathProvider, int numberOfLinesInOneChunk = -1, int startLineNumber = -1,
            int endLineNumber = -1)
        {
            FileProvider = filePathProvider;
            numOfLinesInOneChunk = numberOfLinesInOneChunk;
            startLineNum = startLineNumber;
            endLineNum = endLineNumber;
        }

        protected virtual void InitFile()
        {
            var path = FileProvider.GetFileLocalPath();
            file = new System.IO.StreamReader(path);
            currentLineNum = 0;

            // Skip lines until the current line equals to the desired start line
            while (startLineNum > 0 && currentLineNum < startLineNum && file.ReadLine() != null)
            {
                ++currentLineNum;
            }

            if (startLineNum >= 0 && startLineNum != currentLineNum)
            {
                throw new System.IO.EndOfStreamException("Number of lines in file " + path + " is smaller than expected.");
            }

            isFileInitialized = true;
        }

        public virtual FileType GetLoaderType()
        {
            return FileType.Text;
        }

        public virtual bool IsFileReadingFinished()
        {
            return isFileInitialized &&
                (file == null || file.EndOfStream || (endLineNum >= 0 && currentLineNum > endLineNum));
        }

        public virtual byte[] LoadChunk()
        {
            if (!isFileInitialized)
            {
                InitFile();
            }

            try
            {
                StringBuilder chunkLines = new StringBuilder();
                string currentLine;
                int elementsInChunk = 0;

                while (!IsFileReadingFinished() &&
                    (numOfLinesInOneChunk < 0 || elementsInChunk < numOfLinesInOneChunk) &&
                    (currentLine = file.ReadLine()) != null)
                {
                    chunkLines.Append(currentLine + System.Environment.NewLine);
                    ++currentLineNum;
                    ++elementsInChunk;
                }

                if (IsFileReadingFinished())
                {
                    CloseFile();
                }

                return FileUtils.ConvertFromTextToBinary(chunkLines.ToString());
            }
            catch
            {
                CloseFile();
                throw;
            }
        }

        protected virtual void CloseFile()
        {
            file?.Close();
            file = null;
        }
    }
}

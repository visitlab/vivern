﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class TwistingControllerToolCreator : AbstractControllerToolCreator
    {
        [SerializeField]
        private bool primaryActionIsTwist = true;

        [SerializeField]
        private GameObject selectionPrefab;

        [SerializeField]
        private float selectionAreaRadius = 0.085f;

        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            var selObj = Instantiate(selectionPrefab, cth.transform);
            selObj.transform.localPosition = Vector3.zero;
            selObj.transform.localScale = new Vector3(selectionAreaRadius, selectionAreaRadius, selectionAreaRadius);
            selObj.transform.localRotation = Quaternion.identity;
            selObj.SetActive(false);

            var sc = selObj.GetComponent<SphereCollider>();

            return new TwistingControllerTool(CreatedToolCategory, cth.XRController, createdToolName, primaryActionIsTwist, sc);
        }
    }
}

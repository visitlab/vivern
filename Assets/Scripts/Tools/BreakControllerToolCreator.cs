﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class BreakControllerToolCreator : AbstractControllerToolCreator
    {
        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new BreakControllerTool(CreatedToolCategory, cth.XRController, createdToolName);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class MagicScaleLensControllerToolCreator : AbstractControllerToolCreator
    {
        [SerializeField]
        private GameObject selectionPrefab;

        [SerializeField]
        private float selectionAreaRadius = 0.085f;

        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            var selObj = Instantiate(selectionPrefab, cth.transform);
            selObj.transform.localPosition = Vector3.zero;
            selObj.transform.localScale = new Vector3(selectionAreaRadius, selectionAreaRadius, selectionAreaRadius);
            selObj.transform.localRotation = Quaternion.identity;
            selObj.SetActive(false);

            var sc = selObj.GetComponent<SphereCollider>();

            return new MagicScaleLensControllerTool(CreatedToolCategory, cth.XRController, createdToolName, sc);
        }
    }
}

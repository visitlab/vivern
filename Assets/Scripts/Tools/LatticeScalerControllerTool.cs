﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class LatticeScalerControllerTool : AbstractRealtimeControllerTool
    {
        public override string Category { get; }

        public override string ToolModeName => string.Empty;

        private float scalingSpeed;
        private float initDistance = float.MaxValue;
        private float initApuMultiplier;

        public LatticeScalerControllerTool(string category, XRController xrController, 
            string toolName, float scalingSpeed)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
            this.scalingSpeed = scalingSpeed;
        }

        public override void Disable()
        {
            base.Disable();

            initDistance = float.MaxValue;
        }

        public override void StartAction(ControllerToolActionType actionType)
        {
            base.StartAction(actionType);

            if(actionType == ControllerToolActionType.Primary)
            {
                initDistance = Vector3.Distance(XRController.transform.position, GetOtherController().transform.position);
                initApuMultiplier = LatticeManager.Instance.LatticeAngstromPerUnitMultiplier;
            }
        }

        public override void StopAction(ControllerToolActionType actionType)
        {
            base.StopAction(actionType);

            if (actionType == ControllerToolActionType.Primary)
            {
                initDistance = float.MaxValue;
            }
        }

        protected override void UpdateAction(ControllerToolActionType actionType)
        {
            if(actionType == ControllerToolActionType.Primary)
            {
                LatticeManager.Instance.LatticeAngstromPerUnitMultiplier = initApuMultiplier -
                    ((Vector3.Distance(XRController.transform.position, GetOtherController().transform.position) - initDistance)) 
                    * scalingSpeed;

                // To ensure that scaling will be also propagated on proteins / general structures
                UVC.UnityWrappers.StructureGameObjectsManager.Instance.AngstromsPerUnit = LatticeManager.Instance.LatticeAngstromPerUnit;
            }
        }

        private XRController GetOtherController()
        {
            return XRController == VRUT.XRDeviceManager.Instance.LeftController ?
                VRUT.XRDeviceManager.Instance.RightController :
                VRUT.XRDeviceManager.Instance.LeftController;
        }
    }
}

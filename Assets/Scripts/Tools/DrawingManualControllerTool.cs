﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class DrawingManualControllerTool : AbstractRealtimeControllerTool
    {
        public override string Category { get; }

        public override string ToolModeName => "in a scale: " + currentScale.ToString();

        private bool isFiveToThreePrimary;

        private Structures.VisualScale currentScale = Structures.VisualScale.Nucleotide;

        public DrawingManualControllerTool(string category, XRController xrController, string toolName, bool isFiveToThree)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
            isFiveToThreePrimary = isFiveToThree;

            InvokeModeNameChangedEvent(this);
        }

        public override void StartAction(ControllerToolActionType actionType)
        {
            base.StartAction(actionType);

            if(actionType == ControllerToolActionType.Tertiary)
            {
                currentScale = currentScale.NextEnumValue();
                InvokeModeNameChangedEvent(this);
            }
        }

        protected override void UpdateAction(ControllerToolActionType actionType)
        {
            if ((!LatticeManager.Instance?.ActiveLattice?.ControllersInsideLattice.Contains(XRController) ?? true)
                || actionType == ControllerToolActionType.Tertiary)
            {
                return;
            }

            var latticeIdx = LatticeManager.Instance.ActiveLattice.GetElementIdxForLocalPositionConstrained(
                LatticeManager.Instance.ActiveLattice.GetLocalPositionForWorldPosition(XRController.transform.position)
                );

            if (!latticeIdx.HasValue) { return; }

            LatticeManager.Instance.ActiveLattice.SetNucleotideAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z,
                actionType == ControllerToolActionType.Primary ? isFiveToThreePrimary : !isFiveToThreePrimary, currentScale);
        }
    }
}

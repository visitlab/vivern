﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class DrawingAutoAdjustableControllerTool : AbstractRealtimeControllerTool
    {
        public override string Category { get; }

        public override string ToolModeName => string.Empty;

        public DrawingAutoAdjustableControllerTool(string category, XRController xrController, string toolName)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
        }

        protected override void UpdateAction(ControllerToolActionType actionType)
        {
            if ((!LatticeManager.Instance?.ActiveLattice?.ControllersInsideLattice.Contains(XRController) ?? true)
                || actionType != ControllerToolActionType.Primary)
            {
                return;
            }

            var latticeIdx = LatticeManager.Instance.ActiveLattice.GetElementIdxForLocalPositionConstrained(
                LatticeManager.Instance.ActiveLattice.GetLocalPositionForWorldPosition(XRController.transform.position)
                );

            if (!latticeIdx.HasValue) { return; }

            var prevNeighbor = LatticeManager.Instance.ActiveLattice
                .GetLatticeElementAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z - 1);

            var nextNeighbor = LatticeManager.Instance.ActiveLattice
                .GetLatticeElementAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z + 1);

            if (prevNeighbor != null)
            {
                if (prevNeighbor.FiveToThreeNucleotide != null && prevNeighbor.ThreeToFiveNucleotide != null)
                {
                    LatticeManager.Instance.ActiveLattice.SetNucleotidePairAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z,
                        prevNeighbor.FiveToThreeNucleotide.VisualScale);
                }
                else if (prevNeighbor.FiveToThreeNucleotide != null)
                {
                    LatticeManager.Instance.ActiveLattice.SetNucleotideAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z,
                        true, prevNeighbor.FiveToThreeNucleotide.VisualScale);
                }
                else if (prevNeighbor.ThreeToFiveNucleotide != null)
                {
                    LatticeManager.Instance.ActiveLattice.SetNucleotideAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z,
                        false, prevNeighbor.ThreeToFiveNucleotide.VisualScale);
                }
            }
            else if (nextNeighbor != null)
            {
                if (nextNeighbor.FiveToThreeNucleotide != null && nextNeighbor.ThreeToFiveNucleotide != null)
                {
                    LatticeManager.Instance.ActiveLattice.SetNucleotidePairAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z,
                        nextNeighbor.FiveToThreeNucleotide.VisualScale);
                }
                else if (nextNeighbor.FiveToThreeNucleotide != null)
                {
                    LatticeManager.Instance.ActiveLattice.SetNucleotideAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z,
                        true, nextNeighbor.FiveToThreeNucleotide.VisualScale);
                }
                else if (nextNeighbor.ThreeToFiveNucleotide != null)
                {
                    LatticeManager.Instance.ActiveLattice.SetNucleotideAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z,
                        false, nextNeighbor.ThreeToFiveNucleotide.VisualScale);
                }
            }
            else
            {
                LatticeManager.Instance.ActiveLattice.SetNucleotideAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class LatticeScalerControllerToolCreator : AbstractControllerToolCreator
    {
        [SerializeField]
        private float scalingSpeed = 1.0f;

        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new LatticeScalerControllerTool(CreatedToolCategory, cth.XRController, createdToolName, scalingSpeed);
        }
    }
}

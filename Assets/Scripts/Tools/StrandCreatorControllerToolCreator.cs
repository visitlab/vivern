﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class StrandCreatorControllerToolCreator : AbstractControllerToolCreator
    {
        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new StrandCreatorControllerTool(CreatedToolCategory, cth.XRController, createdToolName);
        }
    }
}

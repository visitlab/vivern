﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class VisReprSwitchToolCreator : AbstractControllerToolCreator
    {
        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new VisReprSwitchTool(CreatedToolCategory, cth.XRController, createdToolName);
        }
    }
}

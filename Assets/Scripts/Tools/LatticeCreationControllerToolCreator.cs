﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class LatticeCreationControllerToolCreator : AbstractControllerToolCreator
    {
        [SerializeField]
        private LatticeType defaultLayout;

        [SerializeField]
        private bool createFilledLattice;

        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new LatticeCreationControllerTool(CreatedToolCategory, cth.XRController, createdToolName, defaultLayout, createFilledLattice);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class DrawingManualControllerToolCreator : AbstractControllerToolCreator
    {
        [SerializeField]
        protected bool isFiveToThreeDir = true;

        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new DrawingManualControllerTool(CreatedToolCategory, cth.XRController, createdToolName, isFiveToThreeDir);
        }
    }
}

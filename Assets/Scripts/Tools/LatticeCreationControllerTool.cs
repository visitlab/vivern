﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class LatticeCreationControllerTool : IControllerTool
    {
        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName { get; protected set; }

        public event Action<IControllerTool> ModeNameChanged;

        private LatticeType layoutType;

        private readonly bool createFilledLattice;

        public LatticeCreationControllerTool(string category, XRController xrController, string toolName, LatticeType defaultLayout, bool createFilledLattice)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
            layoutType = defaultLayout;
            this.createFilledLattice = createFilledLattice;

            UpdateModeName();
        }

        public void Disable()
        {
            LatticeManager.Instance?.ResetLatticeCreationData();
        }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            LatticeManager.Instance.SpawnFilledLattices = createFilledLattice;
            LatticeManager.Instance.CurrentLayoutType = layoutType;

            if (actionType == ControllerToolActionType.Primary)
            {
                LatticeManager.Instance?.OnPointCreateButtonPress(XRController);
            }
            else if(actionType == ControllerToolActionType.Secondary)
            {
                LatticeManager.Instance?.ResetLatticeCreationData();
            }
            else if(actionType == ControllerToolActionType.Tertiary)
            {
                layoutType = layoutType == LatticeType.Honeycomb ? LatticeType.Square : LatticeType.Honeycomb;
                LatticeManager.Instance.CurrentLayoutType = layoutType;
                UpdateModeName();
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }

        private void UpdateModeName()
        {
            ToolModeName = layoutType == LatticeType.Honeycomb ? "Honeycomb" : "Square";
            ModeNameChanged?.Invoke(this);
        }
    }
}

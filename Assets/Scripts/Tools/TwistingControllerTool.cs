﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class TwistingControllerTool : AbstractRealtimeControllerTool
    {
        public override string Category { get; }

        public override string ToolModeName => string.Empty;

        private bool isPrimaryActionTwist;

        private SphereCollider selectionAreaSphere;

        // Bool determines the previous twisted state
        private List<System.Tuple<LatticeElement, bool>> changedElements =
            new List<System.Tuple<LatticeElement, bool>>();

        public TwistingControllerTool(string category, XRController xrController, string toolName, bool isPrimaryTwisting, SphereCollider selectionSphere)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
            isPrimaryActionTwist = isPrimaryTwisting;
            selectionAreaSphere = selectionSphere;
        }

        public override void Enable()
        {
            base.Enable();

            selectionAreaSphere.gameObject.SetActive(true);
        }

        public override void Disable()
        {
            base.Disable();

            RevertChangedElements();
            selectionAreaSphere.gameObject.SetActive(false);
        }

        public override void Update()
        {
            base.Update();

            RevertChangedElements();

            LatticeManager.Instance?.ActiveLattice?
                    .ApplyFunctionToElementsInArea(XRController.transform.position, selectionAreaSphere.bounds.extents.x, ProcessLatticeElement);

            if (activeControllerActions.Contains(ControllerToolActionType.Primary) ||
                activeControllerActions.Contains(ControllerToolActionType.Secondary))
            {
                changedElements.Clear();
            }
        }

        protected override void UpdateAction(ControllerToolActionType actionType)
        { }

        protected void ProcessLatticeElement(LatticeElement element)
        {
            bool? prevTwistedState = null;

            if (element.FiveToThreeNucleotide != null)
            {
                prevTwistedState = element.FiveToThreeNucleotide.IsInTwistedState;
                element.FiveToThreeNucleotide.IsInTwistedState = isPrimaryActionTwist;
            }

            if (element.ThreeToFiveNucleotide != null)
            {
                if(prevTwistedState == null)
                {
                    prevTwistedState = element.ThreeToFiveNucleotide.IsInTwistedState;
                }
                element.ThreeToFiveNucleotide.IsInTwistedState = isPrimaryActionTwist;
            }

            if(prevTwistedState.HasValue)
            {
                changedElements.Add(
                    new System.Tuple
                    <LatticeElement, bool>(element, prevTwistedState.Value));
            }
        }

        private void RevertChangedElements()
        {
            for (int i = 0; i < changedElements.Count; ++i)
            {
                if (changedElements[i] != null)
                {
                    if (changedElements[i].Item1.FiveToThreeNucleotide != null)
                    {
                        changedElements[i].Item1.FiveToThreeNucleotide.IsInTwistedState = changedElements[i].Item2;
                    }

                    if (changedElements[i].Item1.ThreeToFiveNucleotide != null)
                    {
                        changedElements[i].Item1.ThreeToFiveNucleotide.IsInTwistedState = changedElements[i].Item2;
                    }
                }
            }

            changedElements.Clear();
        }
    }
}

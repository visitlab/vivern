﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;
using Vivern.Structures;

namespace Vivern
{
    public class StrandTypeMarkerControllerTool : IControllerTool
    {
        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => currentStrandType.ToString();

        public event Action<IControllerTool> ModeNameChanged;

        private StrandType currentStrandType = StrandType.Scaffold;

        public StrandTypeMarkerControllerTool(string category, XRController xrController, string toolName)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
        }

        public void Disable()
        { }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            if(actionType == ControllerToolActionType.Primary)
            {
                var selectedNucleotide = LatticeManager.Instance.ActiveLattice.GetNucleotideSelectedByController(XRController);

                if(selectedNucleotide != null)
                {
                    selectedNucleotide.Strand.Type = currentStrandType;
                }
            }
            else if(actionType == ControllerToolActionType.Tertiary)
            {
                currentStrandType = currentStrandType.NextEnumValue();
                UpdateModeName();
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }

        private void UpdateModeName()
        {
            ModeNameChanged?.Invoke(this);
        }
    }
}

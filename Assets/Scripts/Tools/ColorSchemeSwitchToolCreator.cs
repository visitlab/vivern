﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class ColorSchemeSwitchToolCreator : AbstractControllerToolCreator
    {
        [SerializeField]
        private ColorScheme[] availableSchemes;

        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new ColorSchemeSwitchTool(CreatedToolCategory, cth.XRController, 
                createdToolName, availableSchemes);
        }
    }
}

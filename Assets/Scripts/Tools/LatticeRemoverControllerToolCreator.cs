﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class LatticeRemoverControllerToolCreator : AbstractControllerToolCreator
    {
        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new LatticeRemoverControllerTool(CreatedToolCategory, cth.XRController, createdToolName);
        }
    }
}

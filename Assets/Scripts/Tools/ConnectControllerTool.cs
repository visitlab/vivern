﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class ConnectControllerTool : IControllerTool
    {
        public string Category { get; } 

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => string.Empty;

        public event Action<IControllerTool> ModeNameChanged;

        private Structures.INucleotide first;

        private Structures.INucleotide second;

        public ConnectControllerTool(string category, XRController xrController, string toolName)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
        }

        public void Disable()
        { }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            if (actionType == ControllerToolActionType.Primary)
            {
                if (first == null)
                {
                    first = LatticeManager.Instance.ActiveLattice.GetNucleotideSelectedByController(XRController);
                    second = null;
                }
                else if (second == null)
                {
                    second = LatticeManager.Instance.ActiveLattice.GetNucleotideSelectedByController(XRController);

                    if (second != null)
                    {
                        LatticeManager.Instance.ActiveLattice.ConnectNucleotides(first, second);
                        ClearSelection();
                    }
                }
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }

        protected void ClearSelection()
        {
            first = second = null;
        }
    }
}

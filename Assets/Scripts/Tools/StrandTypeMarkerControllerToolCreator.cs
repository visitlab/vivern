﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class StrandTypeMarkerControllerToolCreator : AbstractControllerToolCreator
    {
        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new StrandTypeMarkerControllerTool(CreatedToolCategory, cth.XRController, createdToolName);
        }
    }
}

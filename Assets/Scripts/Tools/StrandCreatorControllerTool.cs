﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class StrandCreatorControllerTool : IControllerTool
    {
        public enum StrandCreatorMode
        {
            DoubleStrand,
            SingleStrand53,
            SingleStrand35
        }

        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => mode.ToString();

        public event Action<IControllerTool> ModeNameChanged;

        private StrandCreatorMode mode = StrandCreatorMode.DoubleStrand;

        public StrandCreatorControllerTool(string category, XRController xrController, string toolName)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;

            UpdateModeName();
        }

        public void Disable()
        { }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            if (actionType == ControllerToolActionType.Primary)
            {
                if (!LatticeManager.Instance?.ActiveLattice?.ControllersInsideLattice.Contains(XRController) ?? true)
                {
                    return;
                }

                var latticeIdx = LatticeManager.Instance.ActiveLattice.GetElementIdxForLocalPositionConstrained(
                    LatticeManager.Instance.ActiveLattice.GetLocalPositionForWorldPosition(XRController.transform.position)
                    );

                if (!latticeIdx.HasValue) { return; }

                switch (mode)
                {
                    case StrandCreatorMode.DoubleStrand:
                        LatticeManager.Instance.ActiveLattice.CreateDoubleStrandAt(latticeIdx.Value.x, latticeIdx.Value.y);
                        break;
                    case StrandCreatorMode.SingleStrand53:
                        LatticeManager.Instance.ActiveLattice.CreateStrandAt(latticeIdx.Value.x, latticeIdx.Value.y, true);
                        break;
                    case StrandCreatorMode.SingleStrand35:
                        LatticeManager.Instance.ActiveLattice.CreateStrandAt(latticeIdx.Value.x, latticeIdx.Value.y, false);
                        break;
                }
            }
            else if(actionType == ControllerToolActionType.Tertiary)
            {
                mode = mode.NextEnumValue();
                UpdateModeName();
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }

        private void UpdateModeName()
        {
            ModeNameChanged?.Invoke(this);
        }
    }
}

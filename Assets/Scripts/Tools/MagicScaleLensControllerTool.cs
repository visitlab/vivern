﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class MagicScaleLensControllerTool : AbstractRealtimeControllerTool
    {
        public override string Category { get; }

        public override string ToolModeName => currentScale.ToString();

        private Structures.VisualScale currentScale = Structures.VisualScale.Nucleotide;

        private SphereCollider selectionAreaSphere;

        private List<System.Tuple<LatticeElement, Structures.VisualScale>> changedElements =
            new List<System.Tuple<LatticeElement, Structures.VisualScale>>();

        public MagicScaleLensControllerTool(string category, XRController xrController, string toolName, SphereCollider selectionSphere)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
            selectionAreaSphere = selectionSphere;
        }

        public override void Enable()
        {
            base.Enable();

            selectionAreaSphere.gameObject.SetActive(true);
        }

        public override void Disable()
        {
            base.Disable();

            RevertChangedElements();
            selectionAreaSphere.gameObject.SetActive(false);
        }

        public override void StartAction(ControllerToolActionType actionType)
        {
            base.StartAction(actionType);

            if(actionType == ControllerToolActionType.Tertiary)
            {
                currentScale = currentScale.NextEnumValue();
                InvokeModeNameChangedEvent(this);
            }
        }

        public override void Update()
        {
            base.Update();

            RevertChangedElements();

            LatticeManager.Instance?.ActiveLattice?
                    .ApplyFunctionToElementsInArea(XRController.transform.position, selectionAreaSphere.bounds.extents.x, ProcessLatticeElement);

            if (activeControllerActions.Contains(ControllerToolActionType.Primary))
            {
                changedElements.Clear();
            }
        }

        protected override void UpdateAction(ControllerToolActionType actionType)
        { }

        private void RevertChangedElements()
        {
            for (int i = 0; i < changedElements.Count; ++i)
            {
                if (changedElements[i] != null)
                {
                    if (changedElements[i].Item1.FiveToThreeNucleotide != null)
                    {
                        changedElements[i].Item1.FiveToThreeNucleotide.VisualScale = changedElements[i].Item2;
                    }

                    if (changedElements[i].Item1.ThreeToFiveNucleotide != null)
                    {
                        changedElements[i].Item1.ThreeToFiveNucleotide.VisualScale = changedElements[i].Item2;
                    }
                }
            }

            changedElements.Clear();
        }

        protected void ProcessLatticeElement(LatticeElement element)
        {
            Structures.VisualScale? prevScale = null;

            // Precondition: both 5'3' and 3'5' nucleotides always must have the same visual scale

            if (element.FiveToThreeNucleotide != null)
            {
                prevScale = element.FiveToThreeNucleotide.VisualScale;
                element.FiveToThreeNucleotide.VisualScale = currentScale;
            }

            if (element.ThreeToFiveNucleotide != null)
            {
                if (prevScale == null)
                {
                    prevScale = element.ThreeToFiveNucleotide.VisualScale;
                }
                element.ThreeToFiveNucleotide.VisualScale = currentScale;
            }

            if (prevScale.HasValue)
            {
                changedElements.Add(
                    new System.Tuple
                    <LatticeElement, Structures.VisualScale>(element, prevScale.Value));
            }
        }
    }
}

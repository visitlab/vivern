﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class ColorSchemeSwitchTool : IControllerTool
    {
        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => availableSchemes[selectedSchemeIndex].name;

        public event System.Action<IControllerTool> ModeNameChanged;

        private ColorScheme[] availableSchemes;

        private int selectedSchemeIndex = 0;

        public ColorSchemeSwitchTool(string category, XRController xrController, string toolName, ColorScheme[] availableSchemes)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
            this.availableSchemes = availableSchemes;

            UpdateModeName();
        }

        public void Disable()
        { }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            if (actionType == ControllerToolActionType.Primary)
            {
                LatticeManager.Instance?.ActiveLattice?.DNARenderer.SetNewColorScheme(availableSchemes[selectedSchemeIndex]);
            }
            else if (actionType == ControllerToolActionType.Tertiary)
            {
                selectedSchemeIndex = (selectedSchemeIndex + 1) % availableSchemes.Length;
                UpdateModeName();
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }

        private void UpdateModeName()
        {
            ModeNameChanged?.Invoke(this);
        }
    }
}

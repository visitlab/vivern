﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class LatticeRemoverControllerTool : IControllerTool
    {
        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => string.Empty;

        public event Action<IControllerTool> ModeNameChanged;

        public LatticeRemoverControllerTool(string category, XRController xrController, string toolName)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
        }

        public void Disable()
        { }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            if (actionType == ControllerToolActionType.Primary)
            {
                if (LatticeManager.Instance.ActiveLattice != null)
                {
                    LatticeManager.Instance.ActiveLattice.StartDestruction();
                }
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class VisReprSwitchTool : IControllerTool
    {
        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => currentScale.ToString();

        public event Action<IControllerTool> ModeNameChanged;

        private Structures.VisualScale currentScale = Structures.VisualScale.Nucleotide;

        public VisReprSwitchTool(string category, XRController xrController, string toolName)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;

            UpdateModeName();
        }

        public void Disable()
        { }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            if (actionType == ControllerToolActionType.Primary)
            {
                LatticeManager.Instance?.ActiveLattice?.DNARenderer.SetCompleteScale(currentScale);
            }
            else if(actionType == ControllerToolActionType.Tertiary)
            {
                currentScale = currentScale.NextEnumValue();
                UpdateModeName();
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }

        private void UpdateModeName()
        {
            ModeNameChanged?.Invoke(this);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using VRUT;

namespace Vivern
{
    public class EraseControllerTool : IControllerTool
    {
        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => string.Empty;

        public event Action<IControllerTool> ModeNameChanged;

        public EraseControllerTool(string category, XRController xrController, string toolName)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
        }

        public void Disable()
        { }

        public void Enable()
        { }

        public void StartAction(ControllerToolActionType actionType)
        {
            if (actionType == ControllerToolActionType.Primary)
            {
                var selectedNucleotide = LatticeManager.Instance.ActiveLattice.GetNucleotideSelectedByController(XRController);

                if (selectedNucleotide != null)
                {
                    switch (selectedNucleotide.VisualScale)
                    {
                        case Structures.VisualScale.DoubleStrand:
                            LatticeManager.Instance.ActiveLattice.RemoveDoubleStrand(selectedNucleotide);
                            break;
                        case Structures.VisualScale.SingleStrand:
                            LatticeManager.Instance.ActiveLattice.RemoveStrand(selectedNucleotide.Strand);
                            break;
                        default:
                            LatticeManager.Instance.ActiveLattice.RemoveNucleotide(selectedNucleotide);
                            break;
                    }
                }
            }
        }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }
    }
}

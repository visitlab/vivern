﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    public class DrawingAutoAdjustableControllerToolCreator : AbstractControllerToolCreator
    {
        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new DrawingAutoAdjustableControllerTool(CreatedToolCategory, cth.XRController, createdToolName);
        }
    }
}

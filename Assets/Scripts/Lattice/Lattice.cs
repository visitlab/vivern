﻿using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;
using XRIT = UnityEngine.XR.Interaction.Toolkit;

namespace Vivern
{
    /// <summary>
    /// Lattice is a spatial grid in which x and y axes create the front face
    /// while z-axis goes "into" the object. The local axes of the game object
    /// should correspond to the description above, i.e., the object is rotated exactly as
    /// the lattice it represents
    /// </summary>
    public class Lattice : MonoBehaviour
    {
        [SerializeField]
        private GameObject boundingBoxObject;

        [SerializeField]
        private GameObject latticeAngstromSpaceObject;

        [SerializeField]
        private CubeWireframe cubeWireframeObject;

        [SerializeField]
        private DNANanostructureRenderer dnaRenderer;

        [SerializeField]
        private LatticeCellsVisualization cellsVisualization;

        [SerializeField]
        private float boundingBoxShowDistance = 5.0f;

        [SerializeField]
        private int minNumberOfElementsInZ = 7;

        [SerializeField]
        private LatticeLayout latticeLayout;

        [SerializeField]
        private int dnaRendererUpdateFPS = 20;

        [Header("DNA (cell) dimensions")]
        [SerializeField]
        private float dnaDiameterAngstrom = 20; // B-DNA diameter = 20 A

        [SerializeField]
        private float dnaBasePairRiseAngstrom = 3.32f; // B-DNA base-pair rise = 3.32 A

        [SerializeField]
        private float dnaTwistPerBasePairDeg = 34.3f; // B-DNA rotation per base-pair = 34.3 deg

        /// <summary>
        /// Setting LatticeLayout might be done ONLY before any layout-based computations happened
        /// otherwise the lattice might be invalid.
        /// </summary>
        public LatticeLayout LatticeLayout
        {
            get => latticeLayout;
            set => latticeLayout = value;
        }

        public LatticeType Type => latticeLayout.Type;

        public Vector3Int ExtentElements { get; protected set; }

        private Vector3 _extentAngstroms = new Vector3(0.0f, 0.0f, 0.0f);

        /// <summary>
        /// Extent (not axis-aligned) in angstroms
        /// </summary>
        public Vector3 ExtentAngstroms
        {
            get
            {
                return _extentAngstroms;
            }
            set
            {
                if (value == _extentAngstroms) { return; }

                _extentAngstroms = new Vector3(value.x, value.y,
                    Mathf.Max(minNumberOfElementsInZ * dnaBasePairRiseAngstrom, value.z));
                FitToExtent();
            }
        }

        public float WidthAngstroms
        {
            get => ExtentAngstroms.x;
            set => ExtentAngstroms = new Vector3(value, ExtentAngstroms.y, ExtentAngstroms.z);
        }

        public float HeightAngstroms
        {
            get => ExtentAngstroms.y;
            set => ExtentAngstroms = new Vector3(ExtentAngstroms.x, value, ExtentAngstroms.z);
        }

        public float DepthAngstroms
        {
            get => ExtentAngstroms.z;
            set => ExtentAngstroms = new Vector3(ExtentAngstroms.x, ExtentAngstroms.y, value);
        }

        /// <summary>
        /// Extent (not axis-aligned) in world units
        /// </summary>
        public Vector3 ExtentWorldUnits
        {
            get
            {
                return new Vector3(WidthWorldUnits, HeightWorldUnits, DepthWorldUnits);
            }
            set
            {
                WidthWorldUnits = value.x;
                HeightWorldUnits = value.y;
                DepthWorldUnits = value.z;
            }
        }

        public float WidthWorldUnits
        {
            get => WidthAngstroms / AngstromPerWorldUnit;
            set => WidthAngstroms = value * AngstromPerWorldUnit;
        }

        public float HeightWorldUnits
        {
            get => HeightAngstroms / AngstromPerWorldUnit;
            set => HeightAngstroms = value * AngstromPerWorldUnit;
        }

        public float DepthWorldUnits
        {
            get => DepthAngstroms / AngstromPerWorldUnit;
            set => DepthAngstroms = value * AngstromPerWorldUnit;
        }

        public float AngstromPerWorldUnit
        {
            get
            {
                // Preconditon: uniform scaling
                return LatticeManager.Instance.LatticeAngstromPerUnit *
                    // initial scale [for local space it holds that 1 A = 1 Unity unit] / current scale
                    ((1.0f / LatticeManager.Instance.LatticeAngstromPerUnit) / latticeAngstromSpaceObject.transform.localScale.x);
            }
        }

        public float DnaDiameterAngstrom => dnaDiameterAngstrom;

        public float DnaBasePairRiseAngstrom => dnaBasePairRiseAngstrom;

        public bool IsEmpty => ExtentElements.x == 0 || ExtentElements.y == 0 || ExtentElements.z == 0;

        public IDNANanostructure Nanostructure { get; } = new DNANanostructure();

        public List<XRIT.XRController> ControllersInsideLattice { get; protected set; }
            = new List<XRIT.XRController>(2);

        public DNANanostructureRenderer DNARenderer => dnaRenderer;

        // Indexed as x, y, z cells
        protected Dictionary<int, Dictionary<int, Dictionary<int, LatticeElement>>> latticeElements =
            new Dictionary<int, Dictionary<int, Dictionary<int, LatticeElement>>>();

        // The lattice is marked as fully defined when it is considered as independently usable
        private bool isFullyDefined = false;

        private MeshRenderer boundingBoxRenderer;

        private float dnaRendererUpdateDelay;
        private float lastDnaRendererUpdate;

        private Dictionary<XRIT.XRController, INucleotide> selectedNucleotides = new Dictionary<XRIT.XRController, INucleotide>();

        private void Start()
        {
            boundingBoxRenderer = boundingBoxObject.GetComponent<MeshRenderer>();

            dnaRendererUpdateDelay = 1.0f / dnaRendererUpdateFPS;
            lastDnaRendererUpdate = 0.0f;
            dnaRenderer.ParentLattice = this;
        }

        private void Update()
        {
            CheckLODstate();
            UpdateGridVisualization();

            if (Time.time - lastDnaRendererUpdate >= dnaRendererUpdateDelay)
            {
                dnaRenderer.UpdateRenderedData();
                lastDnaRendererUpdate = Time.time;
            }

            CheckForSelection();
        }

        private void OnDestroy()
        {
            LatticeManager.Instance?.RemoveLattice(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!isFullyDefined) { return; }

            var xrc = other.gameObject.GetComponent<XRIT.XRController>();
            if (xrc != null)
            {
                LatticeManager.Instance.ActiveLattice = this;
                OnXRControllerStateChanged(xrc, true);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!isFullyDefined) { return; }

            var xrc = other.gameObject.GetComponent<XRIT.XRController>();
            if (xrc != null)
            {
                if (ControllersInsideLattice.Count == 0 && LatticeManager.Instance.ActiveLattice == this)
                {
                    LatticeManager.Instance.ActiveLattice = null;
                }

                OnXRControllerStateChanged(xrc, false);
            }
        }

        public void OnActiveStart()
        {
            cubeWireframeObject.IsHighlighted = true;
        }

        public void OnActiveStop()
        {
            cubeWireframeObject.IsHighlighted = false;
        }

        private void OnXRControllerStateChanged(XRIT.XRController controller, bool isInsideLattice)
        {
            if (isInsideLattice)
            {
                ControllersInsideLattice.Add(controller);

                if (!selectedNucleotides.ContainsKey(controller))
                {
                    selectedNucleotides[controller] = null;
                }
            }
            else
            {
                ControllersInsideLattice.Remove(controller);
                selectedNucleotides[controller] = null;
            }
        }

        public void StartDestruction()
        {
            // Necessary XRIT cleanup to avoid exceptions
            var inter = GetComponentInChildren<XRIT.XRBaseInteractable>();
            if(inter != null)
            {
                inter.colliders.Clear();
                inter.enabled = false;
            }

            Destroy(gameObject);
        }

        public INucleotide GetNucleotideSelectedByController(XRIT.XRController controller)
        {
            selectedNucleotides.TryGetValue(controller, out INucleotide result);
            return result;
        }

        private void CheckForSelection()
        {
            for (int i = 0; i < ControllersInsideLattice.Count; ++i)
            {
                var controllerLocalPos = GetLocalPositionForWorldPosition(ControllersInsideLattice[i].transform.position);
                var latticeIdx = GetElementIdxForLocalPositionConstrained(controllerLocalPos);

                if (latticeIdx.HasValue)
                {
                    var selectedLatticeElement = GetLatticeElementAt(latticeIdx.Value.x, latticeIdx.Value.y, latticeIdx.Value.z);
                    INucleotide selectedResult = null;

                    if (selectedLatticeElement != null)
                    {
                        selectedResult = dnaRenderer.CheckNucleotideSelection(controllerLocalPos,
                            selectedLatticeElement.FiveToThreeNucleotide, selectedLatticeElement.ThreeToFiveNucleotide);
                    }

                    selectedNucleotides[ControllersInsideLattice[i]] = selectedResult;
                }
                else
                {
                    selectedNucleotides[ControllersInsideLattice[i]] = null;
                }
            }
        }

        protected void CheckLODstate()
        {
            bool shouldBoundBoxBeShown = Vector3.SqrMagnitude(VRUT.XRDeviceManager.Instance.Headset.transform.position -
                boundingBoxRenderer.bounds.ClosestPoint(VRUT.XRDeviceManager.Instance.Headset.transform.position)) >=
                boundingBoxShowDistance * boundingBoxShowDistance;

            if (shouldBoundBoxBeShown && !boundingBoxRenderer.enabled)
            {
                SetLODboundingBoxState(true);
            }
            else if (!shouldBoundBoxBeShown && boundingBoxRenderer.enabled)
            {
                SetLODboundingBoxState(false);
            }
        }

        protected void SetLODboundingBoxState(bool isVisible)
        {
            boundingBoxRenderer.enabled = isVisible;
            dnaRenderer.gameObject.SetActive(!isVisible);
            cellsVisualization.gameObject.SetActive(!isVisible && LatticeManager.Instance.GlobalCellsVisualizationActiveState);
        }

        public void ToggleCellsVisualization()
        {
            if (!boundingBoxRenderer.enabled)
            {
                SetCellsVisualizationState(!cellsVisualization.gameObject.activeSelf);
            }
        }

        public void SetCellsVisualizationState(bool isVisible)
        {
            cellsVisualization.gameObject.SetActive(isVisible);
        }

        public void SetWireframeVisualizationState(bool isVisible)
        {
            cubeWireframeObject.gameObject.SetActive(isVisible);
        }

        protected void UpdateGridVisualization()
        {
            Vector3Int? latticeIdx = null;
            float maxVisibilityCenterDepth = 0.0f;
            Vector2 maxVisibilityXyPos = Vector2.zero;

            if (ControllersInsideLattice.Count > 0)
            {
                var controllerLocalPos = GetLocalPositionForWorldPosition(ControllersInsideLattice[0].transform.position);
                latticeIdx = GetElementIdxForLocalPositionConstrained(controllerLocalPos);
                maxVisibilityCenterDepth = Mathf.Clamp(controllerLocalPos.z, 0.0f, DepthAngstroms);
                maxVisibilityXyPos = new Vector2(controllerLocalPos.x, controllerLocalPos.y);
            }

            cellsVisualization.VisualizationState = isFullyDefined ?
                (ControllersInsideLattice.Count > 0 ? LatticeCellsVisualization.CellsVisualizationState.Normal :
                                                    LatticeCellsVisualization.CellsVisualizationState.Invisible) :
                                                    LatticeCellsVisualization.CellsVisualizationState.FullyFilled;

            cellsVisualization.UpdateVisualization(latticeAngstromSpaceObject.transform.worldToLocalMatrix,
                maxVisibilityCenterDepth, maxVisibilityXyPos, latticeIdx);
        }

        public INucleotide SetNucleotideAt(int x, int y, int z, bool fiveToThreeDir = true,
            VisualScale defaultScale = VisualScale.Nucleotide,
            IStrand strandToBeForceAddedTo = null, StrandEnd? forceAppendToEnd = null)
        {
            var latticeElement = GetOrCreateLatticeElementAt(x, y, z);

            if (fiveToThreeDir && latticeElement.FiveToThreeNucleotide == null)
            {
                return latticeElement.SetFiveToThreeNucleotide(new Vector3Int(x, y, z), defaultScale,
                            latticeLayout.InitialTwist,
                            latticeLayout.InitialTwist + dnaTwistPerBasePairDeg * z,
                            strandToBeForceAddedTo, forceAppendToEnd);
            }
            else if (!fiveToThreeDir && latticeElement.ThreeToFiveNucleotide == null)
            {
                return latticeElement.SetThreeToFiveNucleotide(new Vector3Int(x, y, z), defaultScale,
                            latticeLayout.InitialTwist + GetThreePToFivePShiftInBases() * dnaTwistPerBasePairDeg,
                            latticeLayout.InitialTwist + GetThreePToFivePShiftInBases() * dnaTwistPerBasePairDeg + dnaTwistPerBasePairDeg * z,
                            strandToBeForceAddedTo, forceAppendToEnd);
            }

            return null;
        }

        public void SetNucleotidePairAt(int x, int y, int z, VisualScale defaultScale = VisualScale.Nucleotide)
        {
            SetNucleotideAt(x, y, z, true, defaultScale);
            SetNucleotideAt(x, y, z, false, defaultScale);
        }

        public void CreateStrandAt(int x, int y, bool isFiveToThreeDir, VisualScale defaultScale = VisualScale.SingleStrand)
        {
            for (int z = 0; z < ExtentElements.z; ++z)
            {
                SetNucleotideAt(x, y, z, isFiveToThreeDir, defaultScale);
            }
        }

        public void CreateDoubleStrandAt(int x, int y, VisualScale defaultScale = VisualScale.DoubleStrand)
        {
            CreateStrandAt(x, y, true, defaultScale);
            CreateStrandAt(x, y, false, defaultScale);
        }

        public void RemoveNucleotide(INucleotide nucleotide)
        {
            var latticeElem = nucleotide.LatticeElement;

            if (latticeElem != null)
            {
                latticeElem.RemoveNucleotide(nucleotide);

                if (latticeElem.FiveToThreeNucleotide == null && latticeElem.ThreeToFiveNucleotide == null)
                {
                    RemoveLatticeElement(latticeElem);
                }
            }
            else
            {
                Debug.LogWarning("Nucleotide is not part of lattice element!");
            }
        }

        public void RemoveStrand(IStrand strand)
        {
            var strandNucleotides = strand.GetNucleotides();

            for (int i = 0; i < strandNucleotides.Count; ++i)
            {
                RemoveNucleotide(strandNucleotides[i]);
            }
        }

        /// <summary>
        /// This function removes double strand which is
        /// defined in this case as a continuous sequence of base pairs
        /// having same conditions as given initial nucleotide pair.
        /// In other words, if the given nucleotide pair contains
        /// nucleotides from strand X and Y, this function removes 
        /// as much nucleotides in this row as possible while
        /// every removed pair must belong to X and Y strands.
        /// </summary>
        public void RemoveDoubleStrand(INucleotide initNucleotide)
        {
            var selectedNucleotide = initNucleotide;
            var otherNucleotide = initNucleotide.ComplementaryBase;
            var strandNucleotides = selectedNucleotide?.Strand?.GetNucleotides();

            if (strandNucleotides != null)
            {
                var selectedNuclIdx = strandNucleotides.IndexOf(selectedNucleotide);

                Vector2Int posCond = new Vector2Int(selectedNucleotide.LatticeLocation.x, selectedNucleotide.LatticeLocation.y);
                var otherNuclStrandCond = otherNucleotide?.Strand;

                int remStart = selectedNuclIdx;
                int remEnd = selectedNuclIdx + 1;

                while (remStart >= 0 && NuclFulfillsConds(strandNucleotides[remStart]))
                {
                    --remStart;
                }

                while (remEnd < strandNucleotides.Count && NuclFulfillsConds(strandNucleotides[remEnd]))
                {
                    ++remEnd;
                }

                for (int i = remStart + 1; i < remEnd; ++i)
                {
                    // Since I want to delete both nucleotides if they exist,
                    // I can directly remove the whole lattice element at that position
                    // which will then handle removal of nucleotides
                    RemoveLatticeElement(strandNucleotides[i].LatticeElement);
                }

                bool NuclFulfillsConds(INucleotide nucl)
                {
                    return
                        nucl.LatticeLocation.x == posCond.x &&
                        nucl.LatticeLocation.y == posCond.y &&
                        nucl.Strand == selectedNucleotide.Strand &&
                        ((nucl.ComplementaryBase == null && otherNucleotide == null)
                        || (nucl.ComplementaryBase != null && nucl.ComplementaryBase.Strand == otherNuclStrandCond));
                }
            }
        }

        public void ConnectNucleotides(INucleotide first, INucleotide second)
        {
            UndoRedoManager.Instance.AddNewCommand(new AppCommandConnectNucleotides(first, second));
        }

        public void BreakStrandAtNucleotide(INucleotide nucleotide)
        {
            UndoRedoManager.Instance.AddNewCommand(new AppCommandBreakAtNucleotide(nucleotide));
        }

        public float GetThreePToFivePShiftInBases()
        {
            return 6.5f;
        }

        public void SetLatticeInitialXAxis(Vector3 latticePosition, Vector3 axisVector)
        {
            transform.position = latticePosition;
            transform.rotation = Quaternion.FromToRotation(Vector3.right, axisVector);

            SetScale(LatticeManager.Instance.LatticeAngstromPerUnit);

            WidthWorldUnits = axisVector.magnitude;
        }

        public void SetLatticeInitialYAxis(Vector3 axisVector)
        {
            if (axisVector.magnitude > Vector3.kEpsilon)
            {
                transform.rotation = Quaternion.LookRotation(Vector3.Cross(transform.right, axisVector), axisVector);
            }

            HeightWorldUnits = axisVector.magnitude;
        }

        public void MarkAsFullyDefined(bool spawnAsFilled = false)
        {
            isFullyDefined = true;

            UpdateCubeWireframe();

            if (spawnAsFilled)
            {
                FillLattice();
            }

            SetCellsVisualizationState(LatticeManager.Instance.GlobalCellsVisualizationActiveState);
            SetWireframeVisualizationState(LatticeManager.Instance.GlobalLatticeWireframeVisualizationState);
        }

        public void SetScale(float angstromPerUnit)
        {
            // Local scale of angstrom-space object is changed in such a way that all positions and scales
            // of child objects can be set in angstroms as they will be automatically recomputed
            // to correct scale in Unity units afterwards
            float unPerAng = 1.0f / LatticeManager.Instance.LatticeAngstromPerUnit;
            latticeAngstromSpaceObject.transform.localScale = new Vector3(unPerAng, unPerAng, unPerAng);
        }

        protected void FillLattice()
        {
            for (int x = 0; x < ExtentElements.x; ++x)
            {
                for (int y = 0; y < ExtentElements.y; ++y)
                {
                    CreateDoubleStrandAt(x, y);
                }
            }
        }

        protected void FitToExtent()
        {
            boundingBoxObject.transform.localScale = ExtentAngstroms;

            if (isFullyDefined)
            {
                UpdateCubeWireframe();
            }

            var avgElWh = GetAverageElementWidthHeight();

            int elementsPerX = (int)(ExtentAngstroms.x / avgElWh.x);
            int elementsPerY = (int)(ExtentAngstroms.y / avgElWh.y);
            int elementsPerZ = Mathf.Max(minNumberOfElementsInZ, (int)(ExtentAngstroms.z / dnaBasePairRiseAngstrom));

            // Skip resizing if not necessary
            if (ExtentElements.x == elementsPerX &&
                ExtentElements.y == elementsPerY &&
                ExtentElements.z == elementsPerZ)
            {
                return;
            }

            ResizeLatticeElements(elementsPerX, elementsPerY, elementsPerZ);
            cellsVisualization.ResizeVisualization(elementsPerX, elementsPerY, GetCellDiamater(),
                DnaBasePairRiseAngstrom, latticeLayout.CrossoverPositionStep, DepthAngstroms, GetLocalPositionForElementIdx,
                -dnaBasePairRiseAngstrom * 0.5f);
        }

        public void SetElementsExtent(int xCells, int yCells, int zCells)
        {
            var avgElWh = GetAverageElementWidthHeight();

            ExtentAngstroms = new Vector3(
                Mathf.Ceil(xCells * avgElWh.x),
                Mathf.Ceil(yCells * avgElWh.y),
                Mathf.Ceil(zCells * dnaBasePairRiseAngstrom));
        }

        protected void UpdateCubeWireframe()
        {
            cubeWireframeObject.SetCubeVertices(new Vector3[] {
                Vector3.zero, new Vector3(WidthAngstroms, 0.0f, 0.0f),
                new Vector3(WidthAngstroms, 0.0f, DepthAngstroms), new Vector3(0.0f, 0.0f, DepthAngstroms),
                new Vector3(0.0f, HeightAngstroms, 0.0f), new Vector3(WidthAngstroms, HeightAngstroms, 0.0f),
                new Vector3(WidthAngstroms, HeightAngstroms, DepthAngstroms), new Vector3(0.0f, HeightAngstroms, DepthAngstroms)});
        }

        protected Vector2 GetAverageElementWidthHeight()
        {
            return new Vector2(dnaDiameterAngstrom * latticeLayout.AverageElementWidthMultiplier,
                dnaDiameterAngstrom * latticeLayout.AverageElementHeightMultiplier);
        }

        protected float GetCellDiamater()
        {
            return dnaDiameterAngstrom * latticeLayout.CellElementDiameterMultiplier;
        }

        public Vector3 GetLocalPositionForElementIdx(int x, int y, int z)
        {
            return latticeLayout.GetLocalPositionForElementIdx(x, y, z, dnaDiameterAngstrom, dnaBasePairRiseAngstrom);
        }

        public Vector3Int GetElementIdxForLocalPosition(Vector3 localPosition)
        {
            return latticeLayout.GetElementIdxForLocalPosition(localPosition, dnaDiameterAngstrom, dnaBasePairRiseAngstrom);
        }

        public Vector3Int? GetElementIdxForLocalPositionConstrained(Vector3 localPosition)
        {
            var idx = GetElementIdxForLocalPosition(localPosition);

            if (!IsEmpty && idx.x >= 0 && idx.x < ExtentElements.x &&
                idx.y >= 0 && idx.y < ExtentElements.y &&
                idx.z >= 0 && idx.z < ExtentElements.z)
            {
                return idx;
            }

            return null;
        }

        /// <summary>
        /// Converts world position to local position where 1 unit = 1 angstrom
        /// </summary>
        public Vector3 GetLocalPositionForWorldPosition(Vector3 worldPosition)
        {
            return latticeAngstromSpaceObject.transform.InverseTransformPoint(worldPosition);
        }

        public Vector3 GetWorldPositionForLocalPosition(Vector3 localPosition)
        {
            return latticeAngstromSpaceObject.transform.TransformPoint(localPosition);
        }

        public float GetLocalScaleForWorldScale(float worldScale)
        {
            return worldScale / latticeAngstromSpaceObject.transform.localScale.x;
        }

        public void ApplyFunctionToEveryElement(System.Action<LatticeElement> funcToApply)
        {
            foreach (var xs in latticeElements.Values)
            {
                foreach (var ys in xs.Values)
                {
                    foreach (var zs in ys.Values)
                    {
                        funcToApply(zs);
                    }
                }
            }
        }

        public void ApplyFunctionToElementsInArea(Vector3 areaWorldCenter, float areaWorldRadius, System.Action<LatticeElement> funcToApply)
        {
            Vector3 areaLatticeCenter = GetLocalPositionForWorldPosition(areaWorldCenter);
            float areaLatticeRadius = GetLocalScaleForWorldScale(areaWorldRadius);

            float areaLatticeRadiusSq = areaLatticeRadius * areaLatticeRadius;

            foreach (var xs in latticeElements)
            {
                var xLoc = GetLocalPositionForElementIdx(xs.Key, 0, 0).x;

                if (Mathf.Abs(xLoc - areaLatticeCenter.x) > areaLatticeRadius) { continue; }

                foreach (var ys in xs.Value)
                {
                    var yLoc = GetLocalPositionForElementIdx(xs.Key, ys.Key, 0).y;

                    if (Mathf.Abs(yLoc - areaLatticeCenter.y) > areaLatticeRadius) { continue; }

                    foreach (var zs in ys.Value)
                    {
                        // The first check should quickly discard all elements which are out of range for sure
                        // The second check should ensure that the area is really spherical
                        if (Mathf.Abs(zs.Value.LocalCenterPosition.z - areaLatticeCenter.z) <= areaLatticeRadius &&
                            Vector3.SqrMagnitude(zs.Value.LocalCenterPosition - areaLatticeCenter) <= areaLatticeRadiusSq)
                        {
                            funcToApply(zs.Value);
                        }
                    }
                }
            }
        }

        protected virtual void ResizeLatticeElements(int elPerX, int elPerY, int elPerZ)
        {
            List<Vector3Int> indicesToRemove = new List<Vector3Int>();

            // Find out what elements to remove and call Clear function on every element to be removed
            foreach (var xs in latticeElements)
            {
                foreach (var ys in xs.Value)
                {
                    foreach (var zs in ys.Value)
                    {
                        if (xs.Key >= elPerX || ys.Key >= elPerY || zs.Key >= elPerZ)
                        {
                            indicesToRemove.Add(new Vector3Int(xs.Key, ys.Key, zs.Key));
                            zs.Value.DestroyWithRelatedData();
                        }
                    }
                }
            }

            // Remove respective elements by going "bottom-to-top"
            for (int i = 0; i < indicesToRemove.Count; ++i)
            {
                latticeElements[indicesToRemove[i].x][indicesToRemove[i].y].Remove(indicesToRemove[i].z);

                if (latticeElements[indicesToRemove[i].x][indicesToRemove[i].y].Count == 0)
                {
                    latticeElements[indicesToRemove[i].x].Remove(indicesToRemove[i].y);
                }

                if (latticeElements[indicesToRemove[i].x].Count == 0)
                {
                    latticeElements.Remove(indicesToRemove[i].x);
                }
            }

            ExtentElements = new Vector3Int(elPerX, elPerY, elPerZ);
        }

        protected virtual LatticeElement GetOrCreateLatticeElementAt(int xIdx, int yIdx, int zIdx)
        {
            return GetLatticeElementAt(xIdx, yIdx, zIdx) ?? CreateLatticeElement(xIdx, yIdx, zIdx);
        }

        public virtual LatticeElement GetLatticeElementAt(int xIdx, int yIdx, int zIdx)
        {
            if (latticeElements.TryGetValue(xIdx, out Dictionary<int, Dictionary<int, LatticeElement>> xRecord) &&
                xRecord.TryGetValue(yIdx, out Dictionary<int, LatticeElement> yRecord) &&
                yRecord.TryGetValue(zIdx, out LatticeElement result))
            {
                return result;
            }

            return null;
        }

        protected virtual LatticeElement CreateLatticeElement(int xIdx, int yIdx, int zIdx)
        {
            var newEl = new LatticeElement(this, new Vector3Int(xIdx, yIdx, zIdx), GetLocalPositionForElementIdx(xIdx, yIdx, zIdx));

            if (!latticeElements.TryGetValue(xIdx, out Dictionary<int, Dictionary<int, LatticeElement>> xRecord))
            {
                xRecord = latticeElements[xIdx] = new Dictionary<int, Dictionary<int, LatticeElement>>();
            }

            if (!xRecord.TryGetValue(yIdx, out Dictionary<int, LatticeElement> yRecord))
            {
                yRecord = xRecord[yIdx] = new Dictionary<int, LatticeElement>();
            }

            yRecord.Add(zIdx, newEl);

            return newEl;
        }

        public virtual void RemoveLatticeElement(LatticeElement element)
        {
            RemoveLatticeElement(element.Index.x, element.Index.y, element.Index.z);
        }

        public virtual void RemoveLatticeElement(int xIdx, int yIdx, int zIdx)
        {
            if (latticeElements.TryGetValue(xIdx, out Dictionary<int, Dictionary<int, LatticeElement>> xRecord) &&
                xRecord.TryGetValue(yIdx, out Dictionary<int, LatticeElement> yRecord) &&
                yRecord.TryGetValue(zIdx, out LatticeElement result))
            {
                result.DestroyWithRelatedData();
                yRecord.Remove(zIdx);
            }
        }
    }
}

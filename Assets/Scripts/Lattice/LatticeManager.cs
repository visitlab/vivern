﻿using System.Collections.Generic;
using UnityEngine;
using VRUT;

namespace Vivern
{
    [System.Serializable]
    public class UnityEventLattice : UnityEngine.Events.UnityEvent<Lattice> { }

    public class LatticeManager : MonoBehaviour
    {
        public static LatticeManager Instance { get; protected set; }

        [SerializeField]
        [UnityEngine.Serialization.FormerlySerializedAsAttribute("latticeAngstromPerUnit")]
        private float defaultLatticeAngstromPerUnit;

        [SerializeField]
        private float maxApuMultiplier = 5.0f;

        [SerializeField]
        private float minApuMultiplier = 0.1f;

        [SerializeField]
        private GameObject latticePrefab;

        [SerializeField]
        private LatticeLayout honeycombLatticeLayout;

        [SerializeField]
        private LatticeLayout squareLatticeLayout;

        [Header("Objects to visualize initial lines determing lattice's x and y axes")]

        [SerializeField]
        private GameObject axisLineRendererPrefab;

        [SerializeField]
        private GameObject axisEndPointPrefab;

        [SerializeField]
        private GameObject perpendicularAxesQuadPrefab;

        [SerializeField]
        private GameObject zAxisSpaceCubePrefab;

        public UnityEventLattice LatticeAdded;

        public UnityEventLattice LatticeRemoved;

        public float LatticeAngstromPerUnit => defaultLatticeAngstromPerUnit * LatticeAngstromPerUnitMultiplier;

        private float _latticeAngstromPerUnitMultiplier = 1.0f;
        public float LatticeAngstromPerUnitMultiplier
        {
            get => _latticeAngstromPerUnitMultiplier;
            set
            {
                _latticeAngstromPerUnitMultiplier = Mathf.Clamp(value, minApuMultiplier, maxApuMultiplier);

                for(int i = 0; i < ExistingLattices.Count; ++i)
                {
                    ExistingLattices[i].SetScale(LatticeAngstromPerUnit);
                }
            }
        }

        private Lattice _activeLattice;
        public Lattice ActiveLattice
        {
            get => _activeLattice;
            set
            {
                if (_activeLattice == value) { return; }

                var oldActiveLattice = _activeLattice;
                _activeLattice = value;

                oldActiveLattice?.OnActiveStop();
                _activeLattice?.OnActiveStart();
            }
        }

        public List<Lattice> ExistingLattices { get; } = new List<Lattice>();

        public bool SpawnFilledLattices { get; set; } = false;

        public LatticeType CurrentLayoutType { get; set; } = LatticeType.Honeycomb;

        public bool GlobalCellsVisualizationActiveState { get; protected set; } = true;

        public bool GlobalLatticeWireframeVisualizationState { get; protected set; } = true;

        private Lattice currentlyCreatedLattice = null;
        private UnityEngine.XR.Interaction.Toolkit.XRController currentlyCreatingController;

        private List<GameObject> axesPoints = new List<GameObject>(3); // Points 0 & 1 define x-axis, points 1 & 2 y-axis, points 2 & 3 z-axis
        private List<LineRenderer> axesLineRenderers = new List<LineRenderer>(3);
        private List<GameObject> axesPerpendQuads = new List<GameObject>(2);
        private GameObject zAxisSpaceCube = null;
        

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            ResetLatticeCreationData();
        }

        protected void Update()
        {
            if (currentlyCreatedLattice != null)
            {
                if (axesPoints.Count == 2)
                {
                    currentlyCreatedLattice.SetLatticeInitialYAxis(
                        Vector3.ProjectOnPlane(currentlyCreatingController.transform.position - axesPoints[1].transform.position,
                        axesPerpendQuads[0].transform.forward));
                }
                else if (axesPoints.Count == 3)
                {
                    currentlyCreatedLattice.DepthWorldUnits =
                        Vector3.ProjectOnPlane(currentlyCreatingController.transform.position - axesPoints[2].transform.position,
                        axesPoints[0].transform.position - axesPoints[1].transform.position).magnitude;
                }
            }

            UpdateAxesVisuals();
        }

        protected virtual void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        public void RemoveLattice(Lattice lattice)
        {
            ExistingLattices.Remove(lattice);

            if (ActiveLattice == lattice)
            {
                ActiveLattice = null;
            }

            LatticeRemoved?.Invoke(lattice);
        }

        private void AddLattice(Lattice lattice)
        {
            ExistingLattices.Add(lattice);
            LatticeAdded?.Invoke(lattice);
        }

        public void OnPointCreateButtonPress(UnityEngine.XR.Interaction.Toolkit.XRController invokingController)
        {
            if (currentlyCreatingController != null && invokingController != currentlyCreatingController) { return; }

            currentlyCreatingController = invokingController;

            var newPoint = Instantiate(axisEndPointPrefab, transform);
            newPoint.transform.position = currentlyCreatingController.transform.position;
            axesPoints.Add(newPoint);

            if (axesLineRenderers.Count != axesPoints.Count)
            {
                var newAxisLine = Instantiate(axisLineRendererPrefab, transform);
                axesLineRenderers.Add(newAxisLine.GetComponent<LineRenderer>());
            }

            if (axesPoints.Count == 2)
            {
                var newLattice = Instantiate(latticePrefab);
                currentlyCreatedLattice = newLattice.GetComponent<Lattice>();

                currentlyCreatedLattice.LatticeLayout = CurrentLayoutType == LatticeType.Honeycomb ? honeycombLatticeLayout : squareLatticeLayout;

                // Detect the lattice position which should correspond to its bottom left corner
                int blcPointIndex =
                    XRDeviceManager.Instance.Headset.transform.InverseTransformPoint(axesPoints[0].transform.position).x <
                    XRDeviceManager.Instance.Headset.transform.InverseTransformPoint(axesPoints[1].transform.position).x ?
                    0 : 1;
                int brcPointIndex = 1 - blcPointIndex;

                currentlyCreatedLattice?.
                    SetLatticeInitialXAxis(axesPoints[blcPointIndex].transform.position,
                    axesPoints[brcPointIndex].transform.position - axesPoints[blcPointIndex].transform.position);
                AddLattice(currentlyCreatedLattice);

                var perpendRotation = Quaternion.FromToRotation(Vector3.forward,
                    axesPoints[1].transform.position - axesPoints[0].transform.position);

                for (int i = 0; i < 2; ++i)
                {
                    var newPerpendVisQuad = Instantiate(perpendicularAxesQuadPrefab, transform);
                    axesPerpendQuads.Add(newPerpendVisQuad);

                    newPerpendVisQuad.transform.position = axesPoints[i].transform.position;
                    newPerpendVisQuad.transform.rotation = perpendRotation;
                }
            }
            else if (axesPoints.Count == 3)
            {
                currentlyCreatedLattice.SetLatticeInitialYAxis(
                    Vector3.ProjectOnPlane(axesPoints[2].transform.position - axesPoints[1].transform.position,
                    axesPerpendQuads[0].transform.forward));

                for (int i = 0; i < axesPerpendQuads.Count; ++i)
                {
                    axesPerpendQuads[i].SetActive(false);
                }

                if (zAxisSpaceCubePrefab != null)
                {
                    zAxisSpaceCube = Instantiate(zAxisSpaceCubePrefab, currentlyCreatedLattice.transform);
                    zAxisSpaceCube.transform.localPosition = Vector3.zero;
                    zAxisSpaceCube.transform.localRotation = Quaternion.identity;
                }
            }
            else if (axesPoints.Count >= 4)
            {
                currentlyCreatedLattice.DepthWorldUnits =
                    Vector3.ProjectOnPlane(axesPoints[3].transform.position - axesPoints[2].transform.position,
                    axesPoints[0].transform.position - axesPoints[1].transform.position).magnitude;

                currentlyCreatedLattice.MarkAsFullyDefined(SpawnFilledLattices);
                ActiveLattice = currentlyCreatedLattice;

                if (!currentlyCreatedLattice.IsEmpty)
                {
                    currentlyCreatedLattice = null;
                }
                ResetLatticeCreationData();
            }
        }

        private void UpdateAxesVisuals()
        {
            if (currentlyCreatingController == null) { return; }

            for (int i = 0; i < axesLineRenderers.Count; ++i)
            {
                axesLineRenderers[i].positionCount = 2;
                axesLineRenderers[i].SetPosition(0, axesPoints[i].transform.position);
                axesLineRenderers[i].SetPosition(1,
                    i + 1 >= axesPoints.Count ? currentlyCreatingController.transform.position : axesPoints[i + 1].transform.position);
            }

            if (axesPoints.Count == 3 && zAxisSpaceCube != null)
            {
                zAxisSpaceCube.transform.localScale = currentlyCreatedLattice.ExtentWorldUnits;
            }
        }

        public void ResetLatticeCreationData()
        {
            axesPoints.Clear(x => { Destroy(x); });
            axesPerpendQuads.Clear(x => { Destroy(x); });
            axesLineRenderers.Clear(x => { Destroy(x.gameObject); });

            if (zAxisSpaceCube != null)
            {
                Destroy(zAxisSpaceCube);
            }

            currentlyCreatingController = null;

            if (currentlyCreatedLattice != null)
            {
                currentlyCreatedLattice.StartDestruction();
            }
        }

        public Lattice SpawnLatticeImmediate(Vector3 bottomLeftCornerPosition, Vector3 xAxis, Vector3 yAxis,
            LatticeType layoutType, int cellsInX, int cellsInY, int cellsInZ, string latticeName = null)
        {
            var newLattice = Instantiate(latticePrefab);
            if (latticeName != null)
            {
                newLattice.name = latticeName;
            }

            var newLatticeComp = newLattice.GetComponent<Lattice>();

            newLatticeComp.LatticeLayout = layoutType == LatticeType.Honeycomb ? honeycombLatticeLayout : squareLatticeLayout;
            newLatticeComp.SetLatticeInitialXAxis(bottomLeftCornerPosition, xAxis);
            newLatticeComp.SetLatticeInitialYAxis(yAxis);
            newLatticeComp.SetElementsExtent(cellsInX, cellsInY, cellsInZ);

            AddLattice(newLatticeComp);

            newLatticeComp.MarkAsFullyDefined();
            ActiveLattice = newLatticeComp;

            return newLatticeComp;
        }

        public void SetGlobalCellsVisualizationState(bool newState)
        {
            GlobalCellsVisualizationActiveState = newState;

            for(int i = 0; i < ExistingLattices.Count; ++i)
            {
                ExistingLattices[i].SetCellsVisualizationState(newState);
            }
        }

        public void SetGlobalLatticeWireframeVisualizationState(bool newState)
        {
            GlobalLatticeWireframeVisualizationState = newState;

            for (int i = 0; i < ExistingLattices.Count; ++i)
            {
                ExistingLattices[i].SetWireframeVisualizationState(newState);
            }
        }
    }
}

﻿using UnityEngine;

namespace Vivern
{
    public enum LatticeType
    {
        Honeycomb,
        Square
    }

    public abstract class LatticeLayout : ScriptableObject
    {
        [SerializeField]
        protected float averageElementWidthMultiplier = 1f;

        [SerializeField]
        protected float averageElementHeightMultiplier = 1f;

        [SerializeField]
        protected float cellElementDiameterMultiplier = 1f;

        [SerializeField]
        protected int crossoverPositionStep;

        [SerializeField]
        protected float initialTwist = 90f;

        public abstract LatticeType Type { get; }

        public float AverageElementWidthMultiplier => averageElementWidthMultiplier;

        public float AverageElementHeightMultiplier => averageElementHeightMultiplier;

        public float CellElementDiameterMultiplier => cellElementDiameterMultiplier;

        public float CrossoverPositionStep => crossoverPositionStep;

        /// <summary>
        /// What is the initial twist at each column for 5' to 3' strand
        /// </summary>
        public float InitialTwist => initialTwist;

        public abstract Vector3 GetLocalPositionForElementIdx(int x, int y, int z, float dnaDiameterAngstrom, float basePairRiseAngstrom);

        public abstract Vector3Int GetElementIdxForLocalPosition(Vector3 localPosition, float dnaDiameterAngstrom, float basePairRiseAngstrom);
    }
}

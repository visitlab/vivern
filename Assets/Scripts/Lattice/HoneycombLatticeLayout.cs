﻿using UnityEngine;

namespace Vivern
{
    [CreateAssetMenu(fileName = "HoneycombLatticeLayout", menuName = "Lattice Layouts/Honeycomb Lattice Layout", order = 2)]
    public class HoneycombLatticeLayout : LatticeLayout
    {
        public override LatticeType Type => LatticeType.Honeycomb;

        public override Vector3 GetLocalPositionForElementIdx(int x, int y, int z, float dnaDiameterAngstrom, float basePairRiseAngstrom)
        {
            return new Vector3(x * dnaDiameterAngstrom * AverageElementWidthMultiplier + dnaDiameterAngstrom * 0.5f,
                (y + (y + 1 - x % 2) / 2) * dnaDiameterAngstrom + dnaDiameterAngstrom * (0.5f + (x % 2) * 0.5f),
                z * basePairRiseAngstrom + basePairRiseAngstrom * 0.5f);
        }

        public override Vector3Int GetElementIdxForLocalPosition(Vector3 localPosition, float dnaDiameterAngstrom, float basePairRiseAngstrom)
        {
            int x = (int)(localPosition.x / (dnaDiameterAngstrom * AverageElementWidthMultiplier));

            // If all the elements in the honeycomb layout of given y position where placed in a line
            // they would overlap. Since they are offseted on y axis, this does not happen.
            // When computing x position by dividing position by average element size, 
            // kind of "rectangular blocks" are created and such blocks might be valid position for
            // two cells but the computation above always returns the cell with higher x
            // – its start on x (left side of rectangle) is smaller than x-coordinate of the end (right side of rectangle)
            // of previous cell which sort of "cuts" the previous cells and "steals" its positions for this cell. 
            // It is therefore necessary to compute final coordinates for both x & x-1 cells,
            // then compute "ideal" center position for these cells and compare these results with the input position
            // and decide which cell is the desired one.
            if (x > 0)
            {
                var currentBlockIndex = GetElementIdxForX(x, localPosition, dnaDiameterAngstrom, basePairRiseAngstrom);
                var previousBlockIndex = GetElementIdxForX(x - 1, localPosition, dnaDiameterAngstrom, basePairRiseAngstrom);

                var currentBlockPosition = GetLocalPositionForElementIdx(currentBlockIndex.x, currentBlockIndex.y,
                    currentBlockIndex.z, dnaDiameterAngstrom, basePairRiseAngstrom);

                var previousBlockPosition = GetLocalPositionForElementIdx(previousBlockIndex.x, previousBlockIndex.y,
                    previousBlockIndex.z, dnaDiameterAngstrom, basePairRiseAngstrom);

                float radiusSq = Mathf.Pow(dnaDiameterAngstrom * 0.5f, 2);
                if (Vector3.SqrMagnitude(currentBlockPosition - localPosition) <= radiusSq)
                {
                    return currentBlockIndex;
                }
                else if (Vector3.SqrMagnitude(previousBlockPosition - localPosition) <= radiusSq)
                {
                    return previousBlockIndex;
                }
                else
                {
                    return new Vector3Int(-1, -1, -1);
                }
            }

            return GetElementIdxForX(x, localPosition, dnaDiameterAngstrom, basePairRiseAngstrom);
        }

        private Vector3Int GetElementIdxForX(int x, Vector3 localPosition, float dnaDiameterAngstrom, float basePairRiseAngstrom)
        {
            int continuousY = (int)((localPosition.y - dnaDiameterAngstrom * (x % 2) * 0.5f) / dnaDiameterAngstrom);
            bool isValidField = (continuousY - (1 + x % 2)) % 3 != 0;

            if (!isValidField)
            {
                return new Vector3Int(-1, -1, -1);
            }

            return new Vector3Int(x,
                continuousY - (int)((localPosition.y - dnaDiameterAngstrom * (x % 2) * 0.5f +
                            (x % 2 == 0 ? dnaDiameterAngstrom : 0.0f)) / (3 * dnaDiameterAngstrom)),
                (int)(localPosition.z / basePairRiseAngstrom));
        }
    }
}

﻿using UnityEngine;

namespace Vivern
{
    [CreateAssetMenu(fileName = "SquareLatticeLayout", menuName = "Lattice Layouts/Square Lattice Layout", order = 1)]
    public class SquareLatticeLayout : LatticeLayout
    {
        public override LatticeType Type => LatticeType.Square;

        public override Vector3 GetLocalPositionForElementIdx(int x, int y, int z, float dnaDiameterAngstrom, float basePairRiseAngstrom)
        {
            return new Vector3(
                    x * dnaDiameterAngstrom * AverageElementWidthMultiplier + dnaDiameterAngstrom * 0.5f,
                    y * dnaDiameterAngstrom * AverageElementHeightMultiplier + dnaDiameterAngstrom * 0.5f,
                    z * basePairRiseAngstrom + basePairRiseAngstrom * 0.5f);
        }

        public override Vector3Int GetElementIdxForLocalPosition(Vector3 localPosition, float dnaDiameterAngstrom, float basePairRiseAngstrom)
        {
            return new Vector3Int(
                (int)(localPosition.x / (dnaDiameterAngstrom * AverageElementWidthMultiplier)),
                (int)(localPosition.y / (dnaDiameterAngstrom * AverageElementHeightMultiplier)),
                (int)(localPosition.z / basePairRiseAngstrom));
        }
    }
}

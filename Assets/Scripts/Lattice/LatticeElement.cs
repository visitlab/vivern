﻿using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    public class LatticeElement
    {
        public Vector3Int Index { get; set; }

        public Vector3 LocalCenterPosition { get; set; }

        public INucleotide FiveToThreeNucleotide => nucleotides[fiveToThreeIdx];

        public INucleotide ThreeToFiveNucleotide => nucleotides[threeToFiveIdx];

        private const int fiveToThreeIdx = 0;

        private const int threeToFiveIdx = 1;

        private INucleotide[] nucleotides = new INucleotide[2];

        private Lattice lattice;

        public LatticeElement(Lattice parentLattice, Vector3Int index, Vector3 locPosition)
        {
            lattice = parentLattice;
            Index = index;
            LocalCenterPosition = locPosition;
        }

        public INucleotide SetFiveToThreeNucleotide(Vector3Int latticeLocation, VisualScale defaultScale, float untwistedAngle,
            float twistedAngle, IStrand strandToBeForceAddedTo = null, StrandEnd? forceAppendToEnd = null)
        {
            return SetNucleotide(latticeLocation, defaultScale, untwistedAngle, twistedAngle, fiveToThreeIdx, strandToBeForceAddedTo, forceAppendToEnd);
        }

        public INucleotide SetThreeToFiveNucleotide(Vector3Int latticeLocation, VisualScale defaultScale, float untwistedAngle,
            float twistedAngle, IStrand strandToBeForceAddedTo = null, StrandEnd? forceAppendToEnd = null)
        {
            return SetNucleotide(latticeLocation, defaultScale, untwistedAngle, twistedAngle, threeToFiveIdx, strandToBeForceAddedTo, forceAppendToEnd);
        }

        public void RemoveNucleotide(INucleotide nucleotide)
        {
            // Removing nucleotide from the middle of the strand basically breaks the strand
            // in this use case (! that's the reason why this behavior is not implemented on the Strand class level) 
            // so it is necessary to deal with that.
            if (!nucleotide.IsThreePrime && !nucleotide.IsFivePrime)
            {
                lattice.BreakStrandAtNucleotide(nucleotide.PreviousNucleotide);
            }

            nucleotide.Strand.RemoveNucleotide(nucleotide);

            if (nucleotide == FiveToThreeNucleotide)
            {
                nucleotides[fiveToThreeIdx] = null;
            }
            else if (nucleotide == ThreeToFiveNucleotide)
            {
                nucleotides[threeToFiveIdx] = null;
            }
        }

        public void DestroyWithRelatedData()
        {
            if (FiveToThreeNucleotide != null)
            {
                RemoveNucleotide(FiveToThreeNucleotide);
            }

            if (ThreeToFiveNucleotide != null)
            {
                RemoveNucleotide(ThreeToFiveNucleotide);
            }
        }

        private INucleotide SetNucleotide(Vector3Int latticeLocation, VisualScale defaultScale, float untwistedAngle, float twistedAngle,
            int nucleotideIdx, IStrand strandToBeForceAddedTo, StrandEnd? forceAppendToEnd)
        {
            if (nucleotides[nucleotideIdx] != null)
            {
                nucleotides[nucleotideIdx].LatticeElement = this;
                nucleotides[nucleotideIdx].UntwistedAngle = untwistedAngle;
                nucleotides[nucleotideIdx].TwistedAngle = twistedAngle;

                return nucleotides[nucleotideIdx];
            }

            int opposNucleotideIdx = GetOppositeNucleotideIdx(nucleotideIdx);

            IStrand parentStrand = strandToBeForceAddedTo;
            INucleotide newNucleotide = null;
            NucleobaseType type =
                nucleotides[opposNucleotideIdx] != null ?
                NucleobaseUtils.GetComplementaryBase(nucleotides[opposNucleotideIdx].BaseType) :
                NucleobaseUtils.GetRandomBase();

            var neighborZMinusOne = lattice.GetLatticeElementAt(latticeLocation.x, latticeLocation.y, latticeLocation.z - 1)?.nucleotides[nucleotideIdx];
            var neighborZPlusOne = lattice.GetLatticeElementAt(latticeLocation.x, latticeLocation.y, latticeLocation.z + 1)?.nucleotides[nucleotideIdx];
            IStrand strandToBeAppendedTo = null;
            IStrand strandToAppend = null;
            bool appendToThreePrimeEnd = nucleotideIdx == fiveToThreeIdx;

            // If the neighbors are in the middle of the strand (or in other words, are no at the end)
            // pretend to not know about them since they should not be connected to the newly added nucleotide
            if(neighborZMinusOne != null && (!neighborZMinusOne.IsFivePrime && !neighborZMinusOne.IsThreePrime))
            {
                neighborZMinusOne = null;
            }

            if(neighborZPlusOne != null && (!neighborZPlusOne.IsFivePrime && !neighborZPlusOne.IsThreePrime))
            {
                neighborZPlusOne = null;
            }

            if (parentStrand == null)
            {
                // If there is no nucleotide already neighboring the newly created one ...
                if (neighborZMinusOne == null && neighborZPlusOne == null)
                {
                    parentStrand = new Strand(lattice.Nanostructure);
                    lattice.Nanostructure.AddStrand(parentStrand);
                }
                // ... or if there are both neighbors present ...
                else if (neighborZMinusOne != null && neighborZPlusOne != null)
                {
                    parentStrand = neighborZMinusOne.Strand;

                    strandToBeAppendedTo = (nucleotideIdx == fiveToThreeIdx ? neighborZMinusOne : neighborZPlusOne).Strand;
                    strandToAppend = (nucleotideIdx == fiveToThreeIdx ? neighborZPlusOne : neighborZMinusOne).Strand;
                }
                // ... or if one of the neighbors exists ...
                else if (neighborZMinusOne != null)
                {
                    parentStrand = neighborZMinusOne.Strand;
                }
                else if (neighborZPlusOne != null)
                {
                    parentStrand = neighborZPlusOne.Strand;
                    appendToThreePrimeEnd = nucleotideIdx == threeToFiveIdx;
                }
            }

            newNucleotide = new Nucleotide(type, parentStrand);
            newNucleotide.LatticeElement = this;
            newNucleotide.UntwistedAngle = untwistedAngle;
            newNucleotide.TwistedAngle = twistedAngle;

            newNucleotide.Nanostructure.SetBasePairConnection(newNucleotide, nucleotides[opposNucleotideIdx]);

            newNucleotide.VisualScale = defaultScale;

            if (forceAppendToEnd.HasValue && forceAppendToEnd.Value != StrandEnd.NotEnd)
            {
                if (forceAppendToEnd.Value == StrandEnd.ThreePrime)
                {
                    parentStrand.AddNucleotideThreePrime(newNucleotide);
                }
                else
                {
                    parentStrand.AddNucleotideFivePrime(newNucleotide);
                }
            }
            else
            {
                if (appendToThreePrimeEnd)
                {
                    parentStrand.AddNucleotideThreePrime(newNucleotide);
                }
                else
                {
                    parentStrand.AddNucleotideFivePrime(newNucleotide);
                }
            }

            if (strandToBeAppendedTo != null && strandToAppend != null)
            {
                lattice.Nanostructure.AppendStrands(strandToBeAppendedTo, strandToAppend);
            }

            nucleotides[nucleotideIdx] = newNucleotide;

            return newNucleotide;
        }

        private int GetOppositeNucleotideIdx(int index)
        {
            return index == fiveToThreeIdx ? threeToFiveIdx : fiveToThreeIdx;
        }
    }
}

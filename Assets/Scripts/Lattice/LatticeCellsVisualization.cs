﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivern
{
    public class LatticeCellsVisualization : MonoBehaviour
    {
        public enum CellsVisualizationState
        {
            FullyFilled,
            Invisible,
            Normal
        }

        [SerializeField]
        private GameObject cellElementPrefab;

        private List<List<MeshRenderer>> cellVisualizationRenderers = new List<List<MeshRenderer>>();

        private MaterialPropertyBlock invisibleElementPropBlock;

        private MaterialPropertyBlock fullyVisibleGridElementPropBlock;

        private MaterialPropertyBlock partiallyVisibleGridlessElementPropBlock;

        private bool defaultPropertyBlockValuesInitialized = false;

        public CellsVisualizationState VisualizationState { get; set; } = CellsVisualizationState.FullyFilled;

        public void UpdateVisualization(Matrix4x4 transformationToLatticeSpace,
            float maxVisibilityDepth, Vector2 maxVisiblityXyPlanePos, Vector3Int? selectedIndex)
        {
            if (!defaultPropertyBlockValuesInitialized) { return; }

            SetPropBlockValues(new System.Tuple<string, Matrix4x4>[]
            {
                new System.Tuple<string, Matrix4x4>("TransformationToLatticeSpace", transformationToLatticeSpace),
            }, fullyVisibleGridElementPropBlock, partiallyVisibleGridlessElementPropBlock);

            SetPropBlockValues(new System.Tuple<string, float>[]
            {
                new System.Tuple<string, float>("VisibilityCenterAxisPos", maxVisibilityDepth),
            }, fullyVisibleGridElementPropBlock, partiallyVisibleGridlessElementPropBlock);

            SetPropBlockValues(new System.Tuple<string, Vector2>[]
            {
                new System.Tuple<string, Vector2>("VisibilityXyPlanePos", maxVisiblityXyPlanePos),
            }, fullyVisibleGridElementPropBlock, partiallyVisibleGridlessElementPropBlock);

            MaterialPropertyBlock selectedBlock = null;

            if (VisualizationState == CellsVisualizationState.Invisible)
            {
                selectedBlock = invisibleElementPropBlock;
            }
            else if (VisualizationState == CellsVisualizationState.FullyFilled)
            {
                selectedBlock = fullyVisibleGridElementPropBlock;
            }

            for (int x = 0; x < cellVisualizationRenderers.Count; ++x)
            {
                for (int y = 0; y < cellVisualizationRenderers[x].Count; ++y)
                {
                    var thisBlock = selectedBlock;

                    if (thisBlock == null)
                    {
                        if (selectedIndex.HasValue && selectedIndex.Value.x == x && selectedIndex.Value.y == y)
                        {
                            thisBlock = fullyVisibleGridElementPropBlock;
                        }
                        else
                        {
                            thisBlock = partiallyVisibleGridlessElementPropBlock;
                        }
                    }

                    cellVisualizationRenderers[x][y].SetPropertyBlock(thisBlock);
                }
            }
        }

        public void ResizeVisualization(int elPerX, int elPerY, float diameter,
            float stepSize, float crossoverPositionStep, float maxDepthAngstroms,
            System.Func<int, int, int, Vector3> indexToLocalPosFunc, float localPosZOffset)
        {
            if (!defaultPropertyBlockValuesInitialized)
            {
                InitDefaultPropertyBlockValues();
            }

            SetPropBlockValues(new System.Tuple<string, float>[]
            {
                new System.Tuple<string, float>("StepSize", stepSize),
                new System.Tuple<string, float>("CrossPosStep", crossoverPositionStep),
                new System.Tuple<string, float>("MaxDepth", maxDepthAngstroms),
            }, invisibleElementPropBlock, fullyVisibleGridElementPropBlock, partiallyVisibleGridlessElementPropBlock);

            cellVisualizationRenderers.Resize(elPerX, x => x.Clear(y => Destroy(y.gameObject)), x => new List<MeshRenderer>());

            for (int x = 0; x < cellVisualizationRenderers.Count; ++x)
            {
                cellVisualizationRenderers[x].Resize(elPerY, y => Destroy(y.gameObject));

                for (int y = 0; y < cellVisualizationRenderers[x].Count; ++y)
                {
                    if (cellVisualizationRenderers[x][y] == null)
                    {
                        var newCellVis = Instantiate(cellElementPrefab, transform);
                        var initLocPos = indexToLocalPosFunc(x, y, 0);

                        newCellVis.transform.localPosition = new Vector3(
                            initLocPos.x, initLocPos.y, initLocPos.z + localPosZOffset);

                        cellVisualizationRenderers[x][y] = newCellVis.GetComponent<MeshRenderer>();
                    }

                    cellVisualizationRenderers[x][y].transform.localScale =
                        new Vector3(diameter, diameter, maxDepthAngstroms);
                }
            }
        }

        private void InitDefaultPropertyBlockValues()
        {
            invisibleElementPropBlock = new MaterialPropertyBlock();

            fullyVisibleGridElementPropBlock = new MaterialPropertyBlock();

            partiallyVisibleGridlessElementPropBlock = new MaterialPropertyBlock();

            SetPropBlockValues(new System.Tuple<string, int>[]
            {
                new System.Tuple<string, int>("DrawGrid", 0),
                new System.Tuple<string, int>("DrawFullLattice", 0)
            }, invisibleElementPropBlock, partiallyVisibleGridlessElementPropBlock);

            SetPropBlockValues(new System.Tuple<string, int>[]
            {
                new System.Tuple<string, int>("DrawGrid", 1),
                new System.Tuple<string, int>("DrawFullLattice", 1)
            }, fullyVisibleGridElementPropBlock);

            SetPropBlockValues(new System.Tuple<string, float>[]
            {
                new System.Tuple<string, float>("VisibilityCenterAxisPos", float.MaxValue),
            }, invisibleElementPropBlock);

            SetPropBlockValues(new System.Tuple<string, Vector2>[]
            {
                new System.Tuple<string, Vector2>("VisibilityXyPlanePos", new Vector2(float.MaxValue, float.MaxValue)),
            }, invisibleElementPropBlock);

            defaultPropertyBlockValuesInitialized = true;
        }

        private void SetPropBlockValues(System.Tuple<string, float>[] values, params MaterialPropertyBlock[] blocksToSet)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                for (int j = 0; j < blocksToSet.Length; ++j)
                {
                    blocksToSet[j].SetFloat(values[i].Item1, values[i].Item2);
                }
            }
        }

        private void SetPropBlockValues(System.Tuple<string, int>[] values, params MaterialPropertyBlock[] blocksToSet)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                for (int j = 0; j < blocksToSet.Length; ++j)
                {
                    blocksToSet[j].SetInt(values[i].Item1, values[i].Item2);
                }
            }
        }

        private void SetPropBlockValues(System.Tuple<string, Vector2>[] values, params MaterialPropertyBlock[] blocksToSet)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                for (int j = 0; j < blocksToSet.Length; ++j)
                {
                    blocksToSet[j].SetVector(values[i].Item1, values[i].Item2);
                }
            }
        }

        private void SetPropBlockValues(System.Tuple<string, Matrix4x4>[] values, params MaterialPropertyBlock[] blocksToSet)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                for (int j = 0; j < blocksToSet.Length; ++j)
                {
                    blocksToSet[j].SetMatrix(values[i].Item1, values[i].Item2);
                }
            }
        }
    }
}

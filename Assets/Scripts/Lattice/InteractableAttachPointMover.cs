﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Vivern
{
    public class InteractableAttachPointMover : MonoBehaviour
    {
        public void OnSelectedByInteractor(XRBaseInteractor baseInteractor)
        {
            transform.position = baseInteractor.transform.position;
            transform.rotation = baseInteractor.transform.rotation;
        }
    }
}

﻿using Leguar.TotalJSON;

namespace Vivern.Cadnano
{
    public class CaDNAnoJSONParser
    {
        private string lastJsonData;
        private JSONCompleteStructure lastProcessedStructure;

        public CaDNAnoJSONParser() { }

        public JSONCompleteStructure ParseStructure(string inputJsonData)
        {
            lastProcessedStructure = new JSONCompleteStructure();
            lastJsonData = inputJsonData;

            ParseData();

            // This is to match the vstrands indexes with their num
            lastProcessedStructure.Sort();

            return lastProcessedStructure;
        }

        private void ParseData()
        {
            JSON caDNAnoData = CreateJSONData();
            ParseVStrands(caDNAnoData);
        }

        private JSON CreateJSONData()
        {
            // This is using the Total JSON library
            JSON caDNAnoData = JSON.ParseString(lastJsonData, "caDNAnoJSON");
            caDNAnoData.SetProtected();
            caDNAnoData.DebugInEditor("caDNAnoData");
            return caDNAnoData;
        }

        private void ParseVStrands(JSON caDNAnoData)
        {
            JArray vstrands = caDNAnoData.GetJArray("vstrands");
            foreach (JSON singleStrand in vstrands.AsJSONArray())
            {
                JSONVstrand newVstrand = ParseSingleVStrand(singleStrand);
                lastProcessedStructure.Add(newVstrand);
            }
        }

        private JSONVstrand ParseSingleVStrand(JSON singleStrand)
        {
            int col = singleStrand.GetInt("col");
            int row = singleStrand.GetInt("row");
            int num = singleStrand.GetInt("num");

            JSONScaf newScaf = new JSONScaf();
            JSONStap newStap = new JSONStap();

            JSONVstrand newVstrand = new JSONVstrand(num, row, col, newScaf, newStap);

            ParseScafs(singleStrand, newVstrand, newScaf);
            ParseStaps(singleStrand, newVstrand, newStap);

            return newVstrand;
        }

        private void ParseScafs(JSON singleStrand, JSONVstrand newVstrand, JSONScaf newScaf)
        {
            JArray scafs = singleStrand.GetJArray("scaf");
            for (int i = 0; i < scafs.Length; i++)
            {
                JArray baseNode = scafs.GetJArray(i);
                JSONBaseNode newBaseNode = ParseBaseNode(newVstrand, baseNode, i);
                newScaf.AddBaseNode(newBaseNode);
            }
        }

        private void ParseStaps(JSON singleStrand, JSONVstrand newVstrand, JSONStap newStap)
        {
            JArray staps = singleStrand.GetJArray("stap");
            for (int i = 0; i < staps.Length; i++)
            {
                JArray baseNode = staps.GetJArray(i);
                JSONBaseNode newBaseNode = ParseBaseNode(newVstrand, baseNode, i);
                newStap.AddBaseNode(newBaseNode);
            }
        }

        private JSONBaseNode ParseBaseNode(JSONVstrand newVstrand, JArray baseNode, int currentBase)
        {
            return new JSONBaseNode(newVstrand, currentBase, baseNode.GetInt(0), baseNode.GetInt(1), baseNode.GetInt(2), baseNode.GetInt(3));
        }

        private void PrintJSONCompleteStructure()
        {
            lastProcessedStructure.ToString();
        }
    }
}

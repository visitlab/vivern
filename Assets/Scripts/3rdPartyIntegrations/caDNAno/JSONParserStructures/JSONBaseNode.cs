﻿using UnityEngine;

namespace Vivern.Cadnano
{
    public class JSONBaseNode
    {
        private int actualBase, previousVstrand, previousBase, nextVstrand, nextBase;
        private JSONVstrand vstrand;

        public JSONBaseNode(JSONVstrand vstrand, int actualBase, int previousVstrand, int previousBase, int nextVstrand, int nextBase)
        {
            this.vstrand = vstrand;
            this.actualBase = actualBase;
            PreviousVstrand = previousVstrand;
            PreviousBase = previousBase;
            NextVstrand = nextVstrand;
            NextBase = nextBase;
        }

        public int PreviousVstrand { get => previousVstrand; set => previousVstrand = value; }
        public int PreviousBase { get => previousBase; set => previousBase = value; }
        public int NextVstrand { get => nextVstrand; set => nextVstrand = value; }
        public int NextBase { get => nextBase; set => nextBase = value; }
        public JSONVstrand Vstrand { get => vstrand; set => vstrand = value; }
        public int ActualBase { get => actualBase; set => actualBase = value; }

        public bool IsEnding()
        {
            return (previousVstrand != -1 && previousBase != -1 && nextVstrand == -1 && nextBase == -1);
        }

        public bool IsBegining()
        {
            return (previousVstrand == -1 && previousBase == -1 && nextVstrand != -1 && nextBase != -1);
        }

        public bool IsJump()
        {
            return previousVstrand != -1 && previousVstrand != vstrand.Num;
        }

        public void ToString()
        {
            Debug.Log("New BaseNode");
            Debug.Log("VStrand: " + vstrand.Num);
            Debug.Log("Actual Base: " + actualBase);
            Debug.Log("Previous VStrand: " + previousVstrand);
            Debug.Log("Previous Base: " + previousBase);
            Debug.Log("Next VStrand: " + nextVstrand);
            Debug.Log("Next Base: " + nextBase);
        }
    }
}
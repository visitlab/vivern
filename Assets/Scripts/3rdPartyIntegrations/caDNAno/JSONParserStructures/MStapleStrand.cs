﻿using System.Collections.Generic;

namespace Vivern.Cadnano
{
    public class MStapleStrand
    {
        private List<MStrand> strands;

        public MStapleStrand()
        {
            strands = new List<MStrand>();
        }

        public void AddStrand(MStrand newStrand)
        {
            strands.Add(newStrand);
        }

        public List<MStrand> GetStrands()
        {
            return strands;
        }

        public override string ToString()
        {
            string result = string.Empty;

            foreach (MStrand s in strands)
            {
                result += s.ToString();
            }

            return result;
        }
    }
}

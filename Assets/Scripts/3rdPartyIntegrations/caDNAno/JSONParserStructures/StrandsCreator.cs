﻿using System.Collections.Generic;

namespace Vivern.Cadnano
{
    public class StrandsCreator
    {
        private List<StrandsList> scaffoldStrands;
        private List<StrandsList> stapleStrands;
        private JSONCompleteStructure structure;

        public StrandsCreator() { }

        public StrandsCreator(JSONCompleteStructure structure)
        {
            Create(structure);
        }

        public void Create(JSONCompleteStructure structure)
        {
            scaffoldStrands = new List<StrandsList>();
            stapleStrands = new List<StrandsList>();
            this.structure = structure;
            foreach (JSONVstrand vstrand in structure.Vstrands)
            {
                foreach (JSONBaseNode baseNode in vstrand.Scafs.BaseNodes)
                {
                    if (baseNode.IsBegining())
                    {
                        StrandsList scaffoldStrand = new StrandsList();
                        FindStrandParts(scaffoldStrand, baseNode, true);
                        scaffoldStrands.Add(scaffoldStrand);
                    }
                }
                foreach (JSONBaseNode baseNode in vstrand.Staps.BaseNodes)
                {
                    if (baseNode.IsBegining())
                    {
                        StrandsList stapleStrand = new StrandsList();
                        FindStrandParts(stapleStrand, baseNode, false);
                        stapleStrands.Add(stapleStrand);
                    }
                }
            }
        }

        public List<StrandsList> GetScaffoldStrands()
        {
            return scaffoldStrands;
        }

        public List<StrandsList> GetStapleStrands()
        {
            return stapleStrands;
        }



        private void FindStrandParts(StrandsList strandsList, JSONBaseNode baseNode, bool isScaf)
        {
            int row = baseNode.Vstrand.Row;
            int col = baseNode.Vstrand.Col;
            int init = baseNode.ActualBase;

            bool endingFound = false;
            bool jumpFound = false;

            JSONBaseNode nextBaseNode = baseNode;

            while (!endingFound && !jumpFound)
            {
                endingFound = nextBaseNode.IsEnding();
                if (!endingFound)
                {
                    nextBaseNode = GetNode(nextBaseNode.NextVstrand, nextBaseNode.NextBase, isScaf);
                }
                jumpFound = nextBaseNode.IsJump();
            }

            int ending = nextBaseNode.ActualBase;

            MStrand newStrand = new MStrand(row, col, init, ending);

            strandsList.AddStrand(newStrand);

            if (!endingFound && jumpFound)
            {
                FindStrandParts(strandsList, nextBaseNode, isScaf);
            }

        }

        private JSONBaseNode GetNode(int vstrand, int actualBase, bool isScaf)
        {
            if (isScaf)
            {
                return structure.GetVstrand(vstrand).Scafs.Get(actualBase);
            }
            else
            {
                return structure.GetVstrand(vstrand).Staps.Get(actualBase);
            }

        }

    }
}
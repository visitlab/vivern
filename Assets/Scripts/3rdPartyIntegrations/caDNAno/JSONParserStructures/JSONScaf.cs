﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Cadnano
{
    public class JSONScaf
    {
        private List<JSONBaseNode> baseNodes;

        public JSONScaf()
        {
            baseNodes = new List<JSONBaseNode>();
        }

        public List<JSONBaseNode> BaseNodes { get => baseNodes; set => baseNodes = value; }

        public void AddBaseNode(JSONBaseNode bn)
        {
            baseNodes.Add(bn);
        }

        public JSONBaseNode Get(int i)
        {
            return baseNodes[i];
        }

        public void ToString()
        {
            Debug.Log("*** New Scaf ***");
            foreach (JSONBaseNode bn in baseNodes)
            {
                bn.ToString();
            }
        }
    }
}

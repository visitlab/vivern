﻿using System.Collections.Generic;
using System.Linq;

namespace Vivern.Cadnano
{
    public class JSONCompleteStructure
    {
        private List<JSONVstrand> vstrands;

        public List<JSONVstrand> Vstrands { get => vstrands; set => vstrands = value; }

        public JSONCompleteStructure()
        {
            vstrands = new List<JSONVstrand>();
        }

        public void Add(JSONVstrand v)
        {
            vstrands.Add(v);
        }

        public void ToString()
        {
            foreach (JSONVstrand v in vstrands)
            {
                v.ToString();
            }
        }

        public void Sort()
        {
            vstrands = vstrands.OrderBy(o => o.Num).ToList();
        }

        public JSONVstrand GetVstrand(int index)
        {
            return vstrands.Find(x => x.Num == index);
        }

    }
}

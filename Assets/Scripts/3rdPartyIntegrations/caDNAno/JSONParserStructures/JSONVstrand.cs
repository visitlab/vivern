﻿using UnityEngine;

namespace Vivern.Cadnano
{
    public class JSONVstrand
    {
        private int num, row, col;
        private JSONScaf scafs;
        private JSONStap staps;

        public JSONVstrand(int num, int row, int col, JSONScaf scafs, JSONStap staps)
        {
            this.num = num;
            this.row = row;
            this.col = col;
            this.scafs = scafs;
            this.staps = staps;
        }

        public int Num { get => num; set => num = value; }
        public int Row { get => row; set => row = value; }
        public int Col { get => col; set => col = value; }
        public JSONScaf Scafs { get => scafs; set => scafs = value; }
        public JSONStap Staps { get => staps; set => staps = value; }

        public void ToString()
        {
            Debug.Log("*** VStrand num " + num + " ***");
            //scafs.ToString();
            staps.ToString();
        }
    }
}



﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Cadnano
{
    public class JSONStap
    {
        private List<JSONBaseNode> baseNodes;

        public JSONStap()
        {
            baseNodes = new List<JSONBaseNode>();
        }

        public List<JSONBaseNode> BaseNodes { get => baseNodes; set => baseNodes = value; }

        public void AddBaseNode(JSONBaseNode bn)
        {
            baseNodes.Add(bn);
        }

        public JSONBaseNode Get(int i)
        {
            return baseNodes[i];
        }

        public void ToString()
        {
            Debug.Log("*** New Stap ***");
            foreach (JSONBaseNode bn in baseNodes)
            {
                bn.ToString();
            }
        }
    }
}

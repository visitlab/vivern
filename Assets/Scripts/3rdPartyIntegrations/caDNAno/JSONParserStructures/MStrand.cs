﻿namespace Vivern.Cadnano
{
    public class MStrand
    {
        private int row, col, init, ending;

        public MStrand(int row, int col, int init, int ending)
        {
            this.row = row;
            this.col = col;
            this.init = init;
            this.ending = ending;
        }

        public int Row { get => row; set => row = value; }
        public int Col { get => col; set => col = value; }
        public int Init { get => init; set => init = value; }
        public int Ending { get => ending; set => ending = value; }

        public override string ToString()
        {
            return string.Format("r:{0} c:{1} i:{2} e:{3}", row, col, init, ending);
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Cadnano
{
    public class JSONSpawner : MonoBehaviour
    {
        [SerializeField]
        private string jsonData;

        private void Start()
        {
            var cadnanoLoader = new CaDNAnoLoader();
            cadnanoLoader.ProcessString(jsonData);

            List<StrandsList> scaffoldStrands = cadnanoLoader.StrandsCreator.GetScaffoldStrands();
            List<StrandsList> stapleStrands = cadnanoLoader.StrandsCreator.GetStapleStrands();

            GetComponent<StrandsDrawer>().DrawStrands(scaffoldStrands, Color.grey);
            GetComponent<StrandsDrawer>().DrawStrands(stapleStrands);
        }
    }
}

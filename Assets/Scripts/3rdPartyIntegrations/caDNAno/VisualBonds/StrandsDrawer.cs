﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Cadnano
{

    public class StrandsDrawer : MonoBehaviour
    {

        public GameObject cylinderPrefab;


        public void DrawStrands(List<StrandsList> stapleStrands)
        {
            foreach (StrandsList mss in stapleStrands)
            {
                Color c = new Color(Random.Range(0.3f, 0.8f), Random.Range(0.3f, 0.8f), Random.Range(0.3f, 0.8f));
                foreach (MStrand strand in mss.GetStrands())
                {
                    DrawSingleStrand(strand, c);
                }
            }
        }

        public void DrawStrands(List<StrandsList> strands, Color c)
        {
            foreach (StrandsList mss in strands)
            {
                foreach (MStrand strand in mss.GetStrands())
                {
                    DrawSingleStrand(strand, c);
                }

            }
        }

        private void DrawSingleStrand(MStrand strand, Color c)
        {
            Vector3 init = new Vector3(strand.Row, -strand.Col, strand.Init);
            Vector3 ending = new Vector3(strand.Row, -strand.Col, strand.Ending);
            GameObject visualStrand = Instantiate(cylinderPrefab) as GameObject;
            visualStrand.GetComponent<VisualBond>().Draw(init, ending, c);
        }



    }
}

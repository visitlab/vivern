﻿using UnityEngine;

namespace Vivern.Cadnano
{
    public class VisualBond : MonoBehaviour
    {
        public void Draw(Vector3 initPos, Vector3 endPos, Color c)
        {
            Vector3 scaleAux = transform.localScale;
            scaleAux.y = Vector3.Distance(initPos, endPos) / 2;
            scaleAux.x = 1f;
            scaleAux.z = 1f;
            transform.localScale = scaleAux;

            Vector3 pos = Vector3.Lerp(initPos, endPos, (float)0.5);
            transform.position = pos;
            transform.up = endPos - initPos;

            GetComponent<MeshRenderer>().material.SetColor("_BaseColor", c);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Cadnano
{
    public class CaDNAnoSpawner : MonoBehaviour
    {
        [SerializeField]
        private Vector3 initLatticeLocation = Vector3.zero;

        [SerializeField]
        private Vector3 initLatticeXaxis = Vector3.right;

        [SerializeField]
        private Vector3 initLatticeYaxis = Vector3.up;

        [SerializeField]
        private LatticeType defaultLatticeType = LatticeType.Honeycomb;

        [SerializeField]
        private string defaultJsonDataToSpawn;

        private CaDNAnoLoader cadnanoLoader = new CaDNAnoLoader();

        private void Start()
        {
            if (defaultJsonDataToSpawn.Length > 0)
            {
                SpawnLatticeFromRawJsonData(defaultJsonDataToSpawn);
            }
        }

        public void SpawnLatticeFromRawJsonData(string jsonData, string name = null)
        {
            SpawnLattice(cadnanoLoader.ProcessString(jsonData), name);
        }

        public void SpawnLatticeFromFile(string filePath)
        {
            SpawnLattice(cadnanoLoader
                .ProcessFile(
                new UVC.FileIO.TextFileLoader(
                    new UVC.FileIO.LocalFileProvider(filePath))),
                        UVC.FileIO.FileUtils.GetFileName(filePath));
        }

        private void SpawnLattice(List<StrandRecord> strandRecords, string name = null)
        {
            StartCoroutine(SpawnCadnanoLatticeSequence(strandRecords, name, defaultLatticeType));
        }

        private IEnumerator SpawnCadnanoLatticeSequence(List<StrandRecord> strandRecords, string name = null, LatticeType latticeType = LatticeType.Honeycomb)
        {
            Vector3Int latticeMaxDimensions = GetDimensionsOfRecords(strandRecords, Mathf.Max, Vector3Int.zero) + new Vector3Int(2, 1, 1);
            Vector3Int latticeMinDimensions = GetDimensionsOfRecords(strandRecords, Mathf.Min, new Vector3Int(int.MaxValue, int.MaxValue, int.MaxValue));
            Vector3Int latticeElementsOffset = new Vector3Int(
                latticeMinDimensions.x % 2 == 0 ? latticeMinDimensions.x : (latticeMinDimensions.x - 1),
                latticeMinDimensions.y % 2 == 0 ? latticeMinDimensions.y : (latticeMinDimensions.y - 1),
                latticeMinDimensions.z);

            // This is to ensure that there will not be a large unnecessary empty space around the structure
            // Some offsets here are present to make the code behave correctly with honeycomb lattice & its different cell spacings
            // NOTE It does not :'( behave correctly in a sense of 1:1 comparison with caDNAno
            var latticeDimensions = latticeMaxDimensions - latticeMinDimensions + Vector3Int.one;

            Lattice newLattice = LatticeManager.Instance.SpawnLatticeImmediate(initLatticeLocation, initLatticeXaxis,
                initLatticeYaxis, latticeType, latticeDimensions.x, latticeDimensions.y, latticeDimensions.z, name);

            // Wait two frames to be sure lattice is initialized
            yield return null;
            yield return null;

            for (int i = 0; i < strandRecords.Count; ++i)
            {
                var currStrandFstNucl = strandRecords[i].FivePrimeNucleotide;

                Structures.IStrand strandReference = new Structures.Strand(newLattice.Nanostructure);
                strandReference.Type = strandRecords[i].Type;
                newLattice.Nanostructure.AddStrand(strandReference);

                while (currStrandFstNucl != null)
                {
                    newLattice.SetNucleotideAt(
                        currStrandFstNucl.X - latticeElementsOffset.x,
                        currStrandFstNucl.Y - latticeElementsOffset.y,
                        currStrandFstNucl.Z - latticeElementsOffset.z,
                        currStrandFstNucl.IsFiveToThreeDir, Structures.VisualScale.Nucleotide,
                        strandReference,
                        Structures.StrandEnd.ThreePrime);

                    currStrandFstNucl = currStrandFstNucl.NextNuclInStrand;
                }
            }
        }

        private Vector3Int GetDimensionsOfRecords(List<StrandRecord> strandRecords, System.Func<int, int, int> comparator, Vector3Int initValue)
        {
            Vector3Int result = initValue;

            for (int i = 0; i < strandRecords.Count; ++i)
            {
                var currRecord = strandRecords[i].FivePrimeNucleotide;
                while (currRecord != null)
                {
                    result.x = comparator(result.x, currRecord.X);
                    result.y = comparator(result.y, currRecord.Y);
                    result.z = comparator(result.z, currRecord.Z);

                    currRecord = currRecord.NextNuclInStrand;
                }
            }

            return result;
        }
    }
}

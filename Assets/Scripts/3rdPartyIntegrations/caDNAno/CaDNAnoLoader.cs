﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UVC.FileIO;
using Vivern.Structures;

namespace Vivern.Cadnano
{
    public class LatticeNuclPosRecord
    {
        public int X { get; set; }

        public int Y { get; set; }

        public int Z { get; set; }

        public bool IsFiveToThreeDir { get; set; }

        public LatticeNuclPosRecord PrevNuclInStrand { get; set; } = null;

        public LatticeNuclPosRecord NextNuclInStrand { get; set; } = null;
    }

    public class StrandRecord
    {
        public StrandType Type { get; set; }

        public LatticeNuclPosRecord FivePrimeNucleotide { get; set; }
    }

    public class CaDNAnoLoader
    {
        protected readonly CaDNAnoJSONParser jsonParser;

        public StrandsCreator StrandsCreator { get; }

        public CaDNAnoLoader()
        {
            jsonParser = new CaDNAnoJSONParser();
            StrandsCreator = new StrandsCreator();
        }

        public List<StrandRecord> ProcessString(string jsonData)
        {
            List<StrandRecord> result = new List<StrandRecord>();

            StrandsCreator.Create(jsonParser.ParseStructure(jsonData));

            result.AddRange(
                ProcessStrandsLists(StrandsCreator.GetScaffoldStrands(), StrandType.Scaffold));

            result.AddRange(
                ProcessStrandsLists(StrandsCreator.GetStapleStrands(), StrandType.Staple));

            return result;
        }

        public List<StrandRecord> ProcessFile(IFileLoader fileToLoad)
        {
            StringBuilder sb = new StringBuilder();

            while (!fileToLoad.IsFileReadingFinished())
            {
                sb.Append(FileUtils.ConvertFromBinaryToText(fileToLoad.LoadChunk()));
            }

            return ProcessString(sb.ToString());
        }

        private List<StrandRecord> ProcessStrandsLists(List<StrandsList> strandsList, StrandType strandsType)
        {
            List<StrandRecord> result = new List<StrandRecord>();

            for (int i = 0; i < strandsList.Count; ++i)
            {
                var strandParts = strandsList[i].GetStrands();

                List<LatticeNuclPosRecord> nuclSeqRecords = new List<LatticeNuclPosRecord>();

                for (int j = 0; j < strandParts.Count; ++j)
                {
                    nuclSeqRecords.Add(GetFiveToThreeNuclSequenceForStrand(strandParts[j]));
                }

                // Merge individual parts of staple strand into one long strand
                for (int j = 0; j < nuclSeqRecords.Count - 1; ++j)
                {
                    var currRecord = nuclSeqRecords[j];
                    while (currRecord.NextNuclInStrand != null)
                    {
                        currRecord = currRecord.NextNuclInStrand;
                    }

                    currRecord.NextNuclInStrand = nuclSeqRecords[j + 1];
                    nuclSeqRecords[j + 1].PrevNuclInStrand = currRecord;
                }

                var newStrandRecord = new StrandRecord
                {
                    Type = strandsType,
                    FivePrimeNucleotide = nuclSeqRecords[0]
                };

                result.Add(newStrandRecord);
            }

            return result;
        }

        /// <summary>
        /// Generates sequence of nucleotide records for one "straight strand", i.e.,
        /// strand which occupies only one row & column
        /// </summary>
        /// <returns>Reference to 5' nucleotide; by visiting NextNuclInStrand you can traverse through the strand until you reach 3' end</returns>
        private LatticeNuclPosRecord GetFiveToThreeNuclSequenceForStrand(MStrand strand)
        {
            int minDepth = Mathf.Min(strand.Init, strand.Ending);
            int maxDepth = Mathf.Max(strand.Init, strand.Ending);
            bool strandIsFiveToThree = strand.Init == minDepth;

            List<LatticeNuclPosRecord> records = new List<LatticeNuclPosRecord>();
            for (int i = minDepth; i <= maxDepth; ++i)
            {
                records.Add(
                    new LatticeNuclPosRecord
                    {
                        X = strand.Col,
                        Y = strand.Row,
                        Z = i,
                        IsFiveToThreeDir = strandIsFiveToThree
                    });
            }

            if (strandIsFiveToThree)
            {
                for (int i = 0; i < records.Count - 1; ++i)
                {
                    records[i].NextNuclInStrand = records[i + 1];
                    records[i + 1].PrevNuclInStrand = records[i];
                }
            }
            else
            {
                for (int i = 0; i < records.Count - 1; ++i)
                {
                    records[i + 1].NextNuclInStrand = records[i];
                    records[i].PrevNuclInStrand = records[i + 1];
                }
            }

            return strandIsFiveToThree ? records[0] : records[records.Count - 1];

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        private void Awake()
        {
            if(Instance != null)
            {
                Destroy(Instance.gameObject);
                Debug.LogError("Duplicate game manager! Destroying the existing one.");
            }

            Instance = this;
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        public void Undo()
        {
            UndoRedoManager.Instance.Undo(1);
        }

        public void Redo()
        {
            UndoRedoManager.Instance.Redo(1);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.IO.Compression;

namespace Vivern
{
    public class SceneSaveFileManager : MonoBehaviour
    {
        private class NuclInfo
        {
            public Vector3Int position;
            public bool isFiveToThree;
        }

        [SerializeField]
        private FileIO.FileExporter fileExporter;

        [SerializeField]
        private string tempFolderName = "vivern_svflman_temp";

        public void ImportSceneFile(string filePath)
        {
            string tempPath = Application.temporaryCachePath + "/" + tempFolderName;

            if (Directory.Exists(tempPath))
            {
                Directory.Delete(tempPath, true);
            }

            Directory.CreateDirectory(tempPath);

            List<string> extractedFiles = new List<string>();

            using (ZipStorer zip = ZipStorer.Open(filePath, FileAccess.Read))
            {
                List<ZipStorer.ZipFileEntry> dir = zip.ReadCentralDir();

                foreach (ZipStorer.ZipFileEntry entry in dir)
                {
                    zip.ExtractFile(entry, tempPath + "/" + entry.FilenameInZip);
                    extractedFiles.Add(tempPath + "/" + entry.FilenameInZip);
                }
            }

            for(int i = 0; i < extractedFiles.Count; ++i)
            {
                List<List<NuclInfo>> strandsNucleotides = new List<List<NuclInfo>>();
                List<NuclInfo> currentStrandNucleotides = new List<NuclInfo>();
                Vector3 blCornerPos, xAxis, yAxis;
                LatticeType layoutType = LatticeType.Honeycomb;
                int xCells, yCells, zCells;

                blCornerPos = xAxis = yAxis = Vector3.zero;
                xCells = yCells = zCells = 1;

                //try
                //{
                    using (StreamReader file = new StreamReader(extractedFiles[i]))
                    {
                        blCornerPos = StringToVector3(file.ReadLine());
                        xAxis = StringToVector3(file.ReadLine());
                        yAxis = StringToVector3(file.ReadLine());
                        layoutType = (LatticeType)System.Enum.Parse(typeof(LatticeType), file.ReadLine());

                        Vector3Int dimensions = StringToVector3Int(file.ReadLine());
                        xCells = dimensions.x;
                        yCells = dimensions.y;
                        zCells = dimensions.z;

                        string line;
                        while ((line = file.ReadLine()) != null)
                        {
                            if(line.Length == 0) { continue; }

                            if (line.ToLower() == "s")
                            {
                                if (currentStrandNucleotides.Count > 0)
                                {
                                    strandsNucleotides.Add(currentStrandNucleotides);
                                }

                                currentStrandNucleotides = new List<NuclInfo>();
                            }
                            else
                            {
                            var dir = line.Substring(0, 1);
                            var pos = line.Substring(1);

                            currentStrandNucleotides.Add(
                                new NuclInfo
                                {
                                    isFiveToThree = dir == ">",
                                    position = StringToVector3Int(pos)
                                }
                            );
                            }
                        }

                    }
                /*}
                catch(System.Exception exc)
                {
                    Debug.LogError("Error when reading file! " + exc.Message + " with data " + exc.Data + " in " + exc.Source);
                }*/

                if (currentStrandNucleotides.Count > 0)
                {
                    strandsNucleotides.Add(currentStrandNucleotides);
                }

                StartCoroutine(SpawnLatticeSequence(strandsNucleotides, blCornerPos, 
                    xAxis, yAxis, layoutType, xCells, yCells, zCells));
            }

            Directory.Delete(tempPath, true);
        }

        public void ExportSceneFile()
        {
            string tempPath = Application.temporaryCachePath + "/" + tempFolderName;

            Directory.CreateDirectory(tempPath);

            if(!Directory.Exists(tempPath))
            {
                Debug.LogError("Failure when creating temporary directories!");
                return;
            }

            List<string> latticeFiles = new List<string>();

            foreach(var lattice in LatticeManager.Instance.ExistingLattices)
            {
                string latticeTempFileName = tempPath + "/" + "lattice_" + lattice.gameObject.GetInstanceID() + ".latt";
                string latticeTempFileContent = string.Empty;

                latticeTempFileContent +=
                    lattice.transform.position.ToString() + // bl corner pos
                    System.Environment.NewLine +
                    lattice.transform.right.ToString() + // x-axis
                    System.Environment.NewLine +
                    lattice.transform.up.ToString() + // y-axis
                    System.Environment.NewLine +
                    lattice.LatticeLayout.Type.ToString() + // layout type
                    System.Environment.NewLine +
                    lattice.ExtentElements.ToString() +  // dimensions
                    System.Environment.NewLine;

                var strands = lattice.Nanostructure.GetStrands();
                foreach (var strand in strands)
                {
                    latticeTempFileContent += "s" + 
                        System.Environment.NewLine;

                    var nucleotides = strand.GetNucleotides();

                    foreach(var nucleotide in nucleotides)
                    {
                        latticeTempFileContent +=
                            ((nucleotide.LatticeElement.FiveToThreeNucleotide == nucleotide) ? ">" : "<") +
                            nucleotide.LatticeLocation.ToString() + 
                            System.Environment.NewLine;
                    }
                }

                try
                {
                    File.WriteAllText(latticeTempFileName, latticeTempFileContent);
                    latticeFiles.Add(latticeTempFileName);
                }
                catch (System.Exception exc)
                {
                    Debug.LogError("Issue when saving file: " + exc.Message);
                }
            }

            string tmpZipPath = tempPath + "/" + "tmpFinalFile.zip";

            using (ZipStorer zip = ZipStorer.Create(tmpZipPath, "Vivern Scene File"))
            {
                zip.EncodeUTF8 = true;

                foreach(var latticeFile in latticeFiles)
                {
                    zip.AddFile(ZipStorer.Compression.Deflate,
                        latticeFile, UVC.FileIO.FileUtils.GetFileName(latticeFile), string.Empty);
                }
            }

            var zipBytes = File.ReadAllBytes(tmpZipPath);

            fileExporter.SaveBinaryFile(zipBytes, "vivern");

            Directory.Delete(tempPath, true);
        }

        // Source: http://answers.unity.com/answers/1135010/view.html
        public Vector3 StringToVector3(string sVector)
        {
            // Remove the parentheses
            if (sVector.StartsWith("(") && sVector.EndsWith(")"))
            {
                sVector = sVector.Substring(1, sVector.Length - 2);
            }

            // split the items
            string[] sArray = sVector.Split(',');

            // store as a Vector3
            Vector3 result = new Vector3(
                float.Parse(sArray[0], System.Globalization.CultureInfo.InvariantCulture),
                float.Parse(sArray[1], System.Globalization.CultureInfo.InvariantCulture),
                float.Parse(sArray[2], System.Globalization.CultureInfo.InvariantCulture));

            return result;
        }

        public Vector3Int StringToVector3Int(string sVector)
        {
            var res = StringToVector3(sVector);
            return new Vector3Int((int)res.x, (int)res.y, (int)res.z);
        }

        private IEnumerator SpawnLatticeSequence(List<List<NuclInfo>> strandsNucleotides, 
            Vector3 bottomLeftCornerPosition, Vector3 xAxis, Vector3 yAxis,
            LatticeType layoutType, int cellsInX, int cellsInY, int cellsInZ)
        {
            Lattice newLattice = LatticeManager.Instance.SpawnLatticeImmediate(bottomLeftCornerPosition, xAxis,
              yAxis, layoutType, cellsInX, cellsInY, cellsInZ);

            // Wait two frames to be sure lattice is initialized
            yield return null;
            yield return null;

            for(int s = 0; s < strandsNucleotides.Count; ++s)
            {
                Structures.IStrand strandReference = new Structures.Strand(newLattice.Nanostructure);
                newLattice.Nanostructure.AddStrand(strandReference);

                for(int n = 0; n < strandsNucleotides[s].Count; ++n)
                {
                    newLattice.SetNucleotideAt(
                        strandsNucleotides[s][n].position.x,
                        strandsNucleotides[s][n].position.y,
                        strandsNucleotides[s][n].position.z,
                        strandsNucleotides[s][n].isFiveToThree, Structures.VisualScale.Nucleotide,
                        strandReference,
                        Structures.StrandEnd.ThreePrime);
                }
            }
        }
    }
}

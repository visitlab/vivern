﻿using SFB;
using UnityEngine;

namespace Vivern.FileIO
{
    // TODO This could be done in a much better way by using some file format registry
    // similarly as in UVC or just by better coupling between events and extension strings.
    // Rework at some point when there is time & mood for that
    public class FileImporter : MonoBehaviour
    {
        [SerializeField]
        private VRUT.UnityEventString OnPDBOpened;

        [SerializeField]
        private VRUT.UnityEventString OnCadnanoOpened;

        [SerializeField]
        private VRUT.UnityEventString OnSceneFileLoaded;

        [SerializeField]
        private VRUT.UnityEventString OnFastaOpened;

        private readonly ExtensionFilter[] allowedExtensions = new ExtensionFilter[]
        {
            new ExtensionFilter("Cadnano files", "json"),
            new ExtensionFilter("PDB files", "pdb"),
            new ExtensionFilter("Scene files", "vivern"),
            new ExtensionFilter("FASTA sequence files", "fasta")
        };

        public void OpenDialog()
        {
            var paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", allowedExtensions, true);

            for (int i = 0; i < paths.Length; i++)
            {
                var extension = UVC.FileIO.FileUtils.GetFileExtension(paths[i]);

                if (extension.ToLower() == "pdb")
                {
                    OnPDBOpened?.Invoke(paths[i]);
                }
                else if(extension.ToLower() == "json")
                {
                    OnCadnanoOpened?.Invoke(paths[i]);
                }
                else if(extension.ToLower() == "vivern")
                {
                    OnSceneFileLoaded?.Invoke(paths[i]);
                }
                else if(extension.ToLower() == "fasta")
                {
                    OnFastaOpened?.Invoke(paths[i]);
                }
            }
        }
    }
}

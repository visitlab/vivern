﻿using SFB;
using UnityEngine;

namespace Vivern.FileIO
{
    public class FileExporter : MonoBehaviour
    {
        public void SaveTextFile(string content, string extension = "")
        {
            var path = StandaloneFileBrowser.SaveFilePanel("Save File", "", "", extension);

            try
            {
                System.IO.File.WriteAllText(path, content);
            }
            catch (System.Exception exc)
            {
                Debug.LogError("Issue when saving file: " + exc.Message);
            }
        }

        public void SaveBinaryFile(byte[] content, string extension = "")
        {
            var path = StandaloneFileBrowser.SaveFilePanel("Save File", "", "", extension);

            try
            {
                System.IO.File.WriteAllBytes(path, content);
            }
            catch (System.Exception exc)
            {
                Debug.LogError("Issue when saving file: " + exc.Message);
            }
        }
    }
}

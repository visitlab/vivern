﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Vivern
{
    public class FastaLoader : MonoBehaviour
    {
        [SerializeField]
        private TMPro.TMP_Text loadedSequenceNameText;

        private string sequenceContent;

        public void OnFastaFileLoaded(string filePath)
        {
            string sequenceName = "NONE";
            sequenceContent = string.Empty;

            var lines = File.ReadLines(filePath);

            foreach (var line in lines)
            {
                if(line.StartsWith(">"))
                {
                    sequenceName = line.Substring(1);
                }
                else if(line.StartsWith(";")) { } // Ignore possible comments
                else
                {
                    sequenceContent += line;
                }
            }

            loadedSequenceNameText.text = sequenceName;
        }

        public void SetSequenceOfActiveLatticeScaffold()
        {
            var activeLattice = LatticeManager.Instance.ActiveLattice;
            var strands = activeLattice.Nanostructure.GetStrands();

            for(int i = 0; i < strands.Count; ++i)
            {
                int counter = 0;

                if(strands[i].Type == Structures.StrandType.Scaffold)
                {
                    var fst = strands[i].FivePrimeEnd;

                    while (fst != null)
                    {
                        fst.BaseType = Structures.NucleobaseUtils
                            .IdentifierToNbType(sequenceContent[(counter++) % sequenceContent.Length]);

                        fst = fst.NextNucleotide;
                    }
                }
            }
        }
    }
}

﻿using UnityEngine;

namespace Vivern.FileIO
{
    public class SequenceExporter : MonoBehaviour
    {
        [SerializeField]
        private FileExporter fileExporter;

        public void Export()
        {
            if (LatticeManager.Instance == null || LatticeManager.Instance.ActiveLattice == null) { return; }

            string fileContent = "Identification,Length,Sequence" + System.Environment.NewLine;

            var strands = LatticeManager.Instance.ActiveLattice.Nanostructure.GetStrands();

            for (int i = 0; i < strands.Count; ++i)
            {
                var nucleotides = strands[i].GetNucleotides();

                fileContent += "Strand " + strands[i].Id.ToString() +
                    " (" + strands[i].Identifier.ToString() + "),";
                fileContent += nucleotides.Count + ",";

                for (int j = 0; j < nucleotides.Count; ++j)
                {
                    fileContent += (char)nucleotides[j].BaseType;
                }

                fileContent += System.Environment.NewLine;
            }

            fileExporter.SaveTextFile(fileContent, "csv");
        }
    }
}

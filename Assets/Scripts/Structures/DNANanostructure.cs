﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Structures
{
    public class DNANanostructure : UVC.Structures.Structure, IDNANanostructureStructure
    {
        protected static int structureIdGenerator = 0;

        public event System.Action<IStrand> StrandAdded;

        public event System.Action<IStrand> StrandRemoved;

        public DNANanostructure()
            : base()
        {
            Id = structureIdGenerator++;
            Name = "DNA_nanostructure_" + Id.ToString();

            refreshChainIndicesOnRemoval = false; // If set to true, causes problems when strands are stored in dictionary etc.
        }

        public void AddStrand(IStrand strand)
        {
            AddChain(strand as IStrandChain);
            StrandAdded?.Invoke(strand);
        }

        public void RemoveStrand(IStrand strand)
        {
            RemoveChain(strand as IStrandChain);
            StrandRemoved?.Invoke(strand);
        }

        /// <summary>
        /// Appends second strand to the first one
        /// </summary>
        public void AppendStrands(IStrand firstStrand, IStrand strandToAppend)
        {
            if (firstStrand.Nanostructure != this || strandToAppend.Nanostructure != this)
            {
                Debug.LogError("Cannot append strands from different structure!");
                return;
            }

            var nuclToAppend = strandToAppend.GetNucleotides();
            //var bondsToCopy = strandToAppend.GetBonds(); NOTE Bonds are (and will not) be probably used

            List<INucleotide> complementaryBases = new List<INucleotide>(nuclToAppend.Count);

            for (int i = 0; i < nuclToAppend.Count; ++i)
            {
                complementaryBases.Add(nuclToAppend[i].ComplementaryBase);
            }

            // Make sure that this information is not lost
            if(strandToAppend.Type == StrandType.Scaffold)
            {
                firstStrand.Type = StrandType.Scaffold;
            }

            RemoveStrand(strandToAppend);

            for (int i = 0; i < nuclToAppend.Count; ++i)
            {
                firstStrand.AddNucleotideThreePrime(nuclToAppend[i]);

                SetBasePairConnection(nuclToAppend[i], complementaryBases[i]);
            }

            /*foreach (var bond in bondsToCopy)
            {
                firstStrand.AddBond(bond);
            }*/
        }

        public void SetBasePairConnection(INucleotide first, INucleotide second)
        {
            if (first != null)
            {
                first.ComplementaryBase = second;
            }

            if (second != null)
            {
                second.ComplementaryBase = first;
            }
        }

        public void RemoveBasePairConnection(INucleotide first)
        {
            var oldComplBase = first.ComplementaryBase;

            if (first != null)
            {
                first.ComplementaryBase = null;
            }

            if (oldComplBase != null)
            {
                oldComplBase.ComplementaryBase = null;
            }
        }

        public List<IStrand> GetStrands()
        {
            var result = new List<IStrand>();

            foreach (var ch in chains)
            {
                var strand = ch.Value as IStrand;

                if (strand != null)
                {
                    result.Add(strand);
                }
            }

            return result;
        }
    }
}

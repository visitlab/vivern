﻿using System.Collections.Generic;
using UVC.Structures;

namespace Vivern.Structures
{
    public enum StrandType
    {
        Scaffold,
        Staple,
        Undefined
    }

    public class Strand : AbstractChain<LinkedList<IResidue>>, IStrandChain
    {
        public IDNANanostructure Nanostructure => Structure as IDNANanostructure;

        public StrandType Type { get; set; } = StrandType.Undefined;

        public INucleotide FivePrimeEnd => residues.First?.Value as INucleotide;

        public INucleotide ThreePrimeEnd => residues.Last?.Value as INucleotide;

        public event System.Action<INucleotide> NucleotideAdded;

        public event System.Action<INucleotide> NucleotideRemoved;

        public event System.Action<INucleotide> NucleotideVisualPropertyChanged;

        protected byte dirtyFlagsCounter = 0;

        protected uint dirtyFlags = uint.MaxValue;

        /// <summary>
        /// Nucleotides (stored in residues list) are stored in 5 prime to 3 prime order
        /// </summary>
        protected bool insertToFivePrimeEnd = true;

        public Strand(IDNANanostructure parentStructure)
            : base()
        {
            SetStructureReference(parentStructure as IDNANanostructureStructure);
        }

        public Strand(Strand other, IDNANanostructure parentStructure)
            : base(other, parentStructure as IDNANanostructureStructure)
        { }

        public void AddNucleotideFivePrime(INucleotide nucleotide)
        {
            insertToFivePrimeEnd = true;
            AddNucleotide(nucleotide);
        }

        public void AddNucleotideThreePrime(INucleotide nucleotide)
        {
            insertToFivePrimeEnd = false;
            AddNucleotide(nucleotide);
        }

        protected void AddNucleotide(INucleotide nucleotide)
        {
            var nuclRes = nucleotide as INucleotideResidue;

            INucleotide previousFpEnd = FivePrimeEnd;
            INucleotide previousTpEnd = ThreePrimeEnd;

            AddResidue(nuclRes);

            nuclRes.ListNode =
                residues.Last.Value == nucleotide ? residues.Last : residues.Find(nuclRes);

            CheckChangeOfStrandEnds(previousFpEnd, previousTpEnd);

            SetAllDirtyFlags(true);

            nucleotide.VisualPropertyChanged += OnNucleotideVisualPropertyChangedCallback;
            NucleotideAdded?.Invoke(nucleotide);
        }

        protected override void AddNewResidueToInternalList(IResidue residue)
        {
            if (insertToFivePrimeEnd)
            {
                residues.AddFirst(residue);
            }
            else
            {
                residues.AddLast(residue);
            }
        }

        protected override bool RemoveResidueFromInternalList(IResidue residue)
        {
            var nuclNode = (residue as INucleotideResidue)?.ListNode;

            if (nuclNode != null)
            {
                residues.Remove(nuclNode);
                return true;
            }

            return base.RemoveResidueFromInternalList(residue);
        }

        /// <summary>
        /// This function removes given nucleotide record.
        /// It does simple removal, i.e., nucleotides preceding and following the removed one
        /// will be neighbors (in the underlying data structure) after the operation.
        /// </summary>
        public void RemoveNucleotide(INucleotide nucleotide)
        {
            INucleotide previousFpEnd = FivePrimeEnd;
            INucleotide previousTpEnd = ThreePrimeEnd;

            RemoveResidue(nucleotide as INucleotideResidue);

            Nanostructure.RemoveBasePairConnection(nucleotide);

            nucleotide.VisualPropertyChanged -= OnNucleotideVisualPropertyChangedCallback;
            (nucleotide as INucleotideResidue).ListNode = null;

            CheckChangeOfStrandEnds(previousFpEnd, previousTpEnd);

            SetAllDirtyFlags(true);
            NucleotideRemoved?.Invoke(nucleotide);

            if (residues.Count == 0)
            {
                Nanostructure.RemoveStrand(this);
            }
        }

        public override void RemoveAllResidues()
        {
            var currNucleotide = residues.First;

            while (currNucleotide != null)
            {
                if (currNucleotide.Value is INucleotide)
                {
                    RemoveNucleotide(currNucleotide.Value as INucleotide);
                }
                else
                {
                    RemoveResidue(currNucleotide.Value);
                }

                currNucleotide = currNucleotide.Next;
            }
        }

        protected void CheckChangeOfStrandEnds(INucleotide previousFpEnd, INucleotide previousTpEnd)
        {
            if (previousFpEnd != FivePrimeEnd)
            {
                previousFpEnd?.FireVisualPropertyChanged();
                FivePrimeEnd?.FireVisualPropertyChanged();
            }

            if (previousTpEnd != ThreePrimeEnd)
            {
                previousTpEnd?.FireVisualPropertyChanged();
                ThreePrimeEnd?.FireVisualPropertyChanged();
            }
        }

        protected void OnNucleotideVisualPropertyChangedCallback(INucleotide nucleotide)
        {
            SetAllDirtyFlags(true);
            NucleotideVisualPropertyChanged?.Invoke(nucleotide);
        }

        public List<INucleotide> GetNucleotides()
        {
            var result = new List<INucleotide>(residues.Count);

            var currNucleotide = residues.First;

            while (currNucleotide != null)
            {
                if (currNucleotide.Value is INucleotide)
                {
                    result.Add(currNucleotide.Value as INucleotide);
                }

                currNucleotide = currNucleotide.Next;
            }

            return result;
        }

        public void BreakAtNucleotide(INucleotide newThreePrimeOfExistingStrand)
        {
            if (newThreePrimeOfExistingStrand.Strand != this || GetResiduesCount() < 2) { return; }

            var newStrand = new Strand(Nanostructure);
            Nanostructure.AddStrand(newStrand);

            var existingStrandNucls = GetNucleotides();

            bool shouldMove = false;
            for (int i = 0; i < existingStrandNucls.Count; ++i)
            {
                if (shouldMove)
                {
                    var oldComplementaryBase = existingStrandNucls[i].ComplementaryBase;
                    RemoveNucleotide(existingStrandNucls[i]);

                    Nanostructure.SetBasePairConnection(existingStrandNucls[i], oldComplementaryBase);

                    newStrand.AddNucleotideThreePrime(existingStrandNucls[i]);
                }

                if (!shouldMove && existingStrandNucls[i] == newThreePrimeOfExistingStrand)
                {
                    shouldMove = true;
                }
            }

            if (newStrand.GetResiduesCount() == 0)
            {
                Nanostructure.RemoveStrand(newStrand);
            }
        }

        public override object Clone()
        {
            return new Strand(this, Nanostructure);
        }

        public override IChain Clone(IStructure newStructure)
        {
            return new Strand(this, newStructure as IDNANanostructure);
        }

        public byte AcquireDirtyFlagId()
        {
            if (dirtyFlagsCounter >= sizeof(uint) * 8 - 1)
            {
                throw new System.IndexOutOfRangeException("No more dirty flags available.");
            }

            return dirtyFlagsCounter++;
        }

        public bool CheckDirtyFlag(byte flagId)
        {
            return (dirtyFlags & (uint)(1 << flagId)) != 0;
        }

        public void SetDirtyFlag(byte flagId, bool value)
        {
            if (value)
            {
                dirtyFlags |= (uint)(1 << flagId);
            }
            else
            {
                dirtyFlags &= (uint)~(1 << flagId);
            }
        }

        protected void SetAllDirtyFlags(bool value)
        {
            dirtyFlags = value ? uint.MaxValue : uint.MinValue;
        }
    }
}

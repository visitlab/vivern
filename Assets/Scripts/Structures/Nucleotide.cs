﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Structures
{
    public class Nucleotide : UVC.Structures.Residue, INucleotideResidue
    {
        public override bool IsNucleobase => true;

        public override bool IsSplitToBackboneAndBase => true;

        public IStrand Strand => Chain as IStrand;

        public IDNANanostructure Nanostructure => Structure as IDNANanostructure;

        protected NucleobaseType _baseType;
        public NucleobaseType BaseType
        {
            get
            {
                return _baseType;
            }
            set
            {
                _baseType = value;

                bool callChangeEvent = Identifier != null;
                Identifier = NucleobaseUtils.NbTypeToIdentifier(value);
                
                if(ComplementaryBase != null)
                {
                    var complBase = NucleobaseUtils.GetComplementaryBase(_baseType);

                    if(ComplementaryBase.BaseType != complBase)
                    {
                        ComplementaryBase.BaseType = complBase;
                    }
                }

                if (callChangeEvent)
                {
                    BaseTypeChanged?.Invoke(this);
                    VisualPropertyChanged?.Invoke(this);
                }
            }
        }

        private INucleotide _complementaryBase;

        /// <summary>
        /// Do NOT call setter but use DNANanostructure.(Set)(Remove)BasePairConnection instead.
        /// Setter exists only for abovementioned functions.
        /// </summary>
        public INucleotide ComplementaryBase
        {
            get => _complementaryBase;
            set
            {
                _complementaryBase = value;

                if (value != null)
                {
                    _visualScale = value.VisualScale;
                }

                // In some case, change of complementary bases might influence the visuals
                VisualPropertyChanged?.Invoke(this);
            }
        }

        public LatticeElement LatticeElement { get; set; }

        public Vector3Int LatticeLocation => LatticeElement.Index;

        /// <summary>
        /// Angle to be used when it is desired to show helical twists
        /// </summary>
        public float TwistedAngle { get; set; }

        /// <summary>
        /// Angle to be used when the nucleotides should be shown in "untwisted" mode
        /// </summary>
        public float UntwistedAngle { get; set; }

        /// <summary>
        /// This property will return currently valid angle according to the value of IsInTwistedState
        /// </summary>
        public float CurrentTwistStateAngle => IsInTwistedState ? TwistedAngle : UntwistedAngle;

        private bool _isInTwistedState = true;
        public bool IsInTwistedState
        {
            get => _isInTwistedState;
            set
            {
                if (_isInTwistedState == value) { return; }

                _isInTwistedState = value;
                TwistStateChanged?.Invoke(this);
                VisualPropertyChanged?.Invoke(this);
            }
        }

        private VisualScale _visualScale = VisualScale.Nucleotide;
        public VisualScale VisualScale
        {
            get => _visualScale;
            set
            {
                if (_visualScale == value) { return; }

                _visualScale = value;
                VisualPropertyChanged?.Invoke(this);

                if (ComplementaryBase != null && ComplementaryBase.VisualScale != value)
                {
                    ComplementaryBase.VisualScale = value;
                }
            }
        }

        public bool IsFivePrime => Strand.FivePrimeEnd == this;

        public bool IsThreePrime => Strand.ThreePrimeEnd == this;

        public LinkedListNode<UVC.Structures.IResidue> ListNode { get; set; }

        public INucleotide PreviousNucleotide => (ListNode.Previous?.Value) as INucleotide;

        public INucleotide NextNucleotide => (ListNode.Next?.Value) as INucleotide;

        public event System.Action<INucleotide> BaseTypeChanged;

        public event System.Action<INucleotide> TwistStateChanged;

        public event System.Action<INucleotide> VisualPropertyChanged; // TODO The event might also include what type of property was changed?

        public Nucleotide(NucleobaseType nbType, IStrand parentStrand)
            : base()
        {
            SetChainReference(parentStrand as IStrandChain);
            BaseType = nbType;
        }

        // This method serves for external classes which might change some
        // property of the nucleotide which might influence it visuals
        // but the nucleotide class is not able to notice the change
        public void FireVisualPropertyChanged()
        {
            VisualPropertyChanged?.Invoke(this);
        }
    }
}

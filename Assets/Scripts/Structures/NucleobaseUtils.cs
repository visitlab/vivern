﻿using UnityEngine;

namespace Vivern.Structures
{
    public enum NucleobaseType
    {
        Adenine = 'A',
        Cytosine = 'C',
        Guanine = 'G',
        Thymine = 'T',
        Uracil = 'U',
        Undefined = '*'
    }

    public enum StrandEnd
    {
        ThreePrime,
        FivePrime,
        NotEnd
    }

    public static class NucleobaseUtils
    {
        public static string NbTypeToIdentifier(NucleobaseType type, bool isDeoxyribonucleotide = true)
        {
            return (isDeoxyribonucleotide ? "D" : string.Empty) + (char)type;
        }

        public static NucleobaseType IdentifierToNbType(string identifier)
        {
            return IdentifierToNbType(identifier.Length > 1 ? identifier[1] : identifier[0]);
        }

        public static NucleobaseType IdentifierToNbType(char identifier)
        {
            return (NucleobaseType)identifier;
        }

        public static NucleobaseType GetComplementaryBase(NucleobaseType nbType, bool isDeoxyribonucleotide = true)
        {
            switch (nbType)
            {
                case NucleobaseType.Adenine:
                    return isDeoxyribonucleotide ? NucleobaseType.Thymine : NucleobaseType.Uracil;
                case NucleobaseType.Thymine:
                    return NucleobaseType.Adenine;

                case NucleobaseType.Cytosine:
                    return NucleobaseType.Guanine;
                case NucleobaseType.Guanine:
                    return NucleobaseType.Cytosine;

                case NucleobaseType.Uracil:
                    return NucleobaseType.Adenine;
            }

            return NucleobaseType.Undefined;
        }

        public static NucleobaseType GetRandomBase(bool isDeoxyribonucleotide = true)
        {
            switch (Random.Range(0, 4))
            {
                case 0:
                    return NucleobaseType.Adenine;
                case 1:
                    return NucleobaseType.Cytosine;
                case 2:
                    return NucleobaseType.Guanine;
                default:
                    return isDeoxyribonucleotide ? NucleobaseType.Thymine : NucleobaseType.Uracil;
            }
        }
    }
}

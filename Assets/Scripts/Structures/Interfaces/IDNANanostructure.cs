﻿using System;
using System.Collections.Generic;

namespace Vivern.Structures
{
    public interface IDNANanostructure
    {
        event Action<IStrand> StrandAdded;
        event Action<IStrand> StrandRemoved;

        int Id { get; set; }

        void AddStrand(IStrand strand);
        void RemoveStrand(IStrand strand);
        void AppendStrands(IStrand firstStrand, IStrand strandToAppend);
        List<IStrand> GetStrands();
        void SetBasePairConnection(INucleotide first, INucleotide second);
        void RemoveBasePairConnection(INucleotide first);
    }

    public interface IDNANanostructureStructure : IDNANanostructure, UVC.Structures.IStructure
    { }
}
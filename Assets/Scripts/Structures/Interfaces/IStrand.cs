﻿using System.Collections.Generic;

namespace Vivern.Structures
{
    public interface IStrand
    {
        int Id { get; set; }
        string Identifier { get; set; }
        IDNANanostructure Nanostructure { get; }
        StrandType Type { get; set; }
        INucleotide FivePrimeEnd { get; }
        INucleotide ThreePrimeEnd { get; }

        bool IsEmpty { get; }

        void AddNucleotideFivePrime(INucleotide nucleotide);
        void AddNucleotideThreePrime(INucleotide nucleotide);
        List<INucleotide> GetNucleotides();
        void RemoveNucleotide(INucleotide nucleotide);
        void BreakAtNucleotide(INucleotide newThreePrimeOfExistingStrand);
        byte AcquireDirtyFlagId();
        bool CheckDirtyFlag(byte flagId);
        void SetDirtyFlag(byte flagId, bool value);

        event System.Action<INucleotide> NucleotideAdded;

        event System.Action<INucleotide> NucleotideRemoved;

        event System.Action<INucleotide> NucleotideVisualPropertyChanged;
    }

    public interface IStrandChain : IStrand, UVC.Structures.IChain
    { }
}
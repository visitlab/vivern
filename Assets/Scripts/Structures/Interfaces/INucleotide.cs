﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Structures
{
    public interface INucleotide
    {
        int Id { get; set; }
        string Identifier { get; set; }
        IDNANanostructure Nanostructure { get; }
        IStrand Strand { get; }
        NucleobaseType BaseType { get; set; }
        INucleotide ComplementaryBase { get; set; }
        float CurrentTwistStateAngle { get; }
        bool IsFivePrime { get; }
        bool IsThreePrime { get; }
        bool IsInTwistedState { get; set; }
        LatticeElement LatticeElement { get; set; }
        Vector3Int LatticeLocation { get; }
        INucleotide NextNucleotide { get; }
        INucleotide PreviousNucleotide { get; }
        float TwistedAngle { get; set; }
        float UntwistedAngle { get; set; }
        VisualScale VisualScale { get; set; }

        event Action<INucleotide> BaseTypeChanged;
        event Action<INucleotide> TwistStateChanged;
        event Action<INucleotide> VisualPropertyChanged;

        void FireVisualPropertyChanged();
    }

    public interface INucleotideResidue : INucleotide, UVC.Structures.IResidue
    {
        LinkedListNode<UVC.Structures.IResidue> ListNode { get; set; }
    }
}
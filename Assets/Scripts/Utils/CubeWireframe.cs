﻿using UnityEngine;

namespace Vivern
{
    public class CubeWireframe : MonoBehaviour
    {
        [SerializeField]
        private GameObject cubeEdgeLineRendererPrefab;

        [SerializeField]
        private Space linePositionsSpace = Space.Self;

        [SerializeField]
        private Color defaultColor = Color.cyan;

        [SerializeField]
        private Color highlightedColor = Color.green;

        private bool edgesInitialized = false;

        private LineRenderer[] edges = new LineRenderer[12];

        private bool _isHighlighted = false;
        public bool IsHighlighted
        {
            get => _isHighlighted;
            set
            {
                _isHighlighted = value;
                UpdateEdgeColors();
            }
        }

        /// <param name="verticesPositions">Should be in order: bottom left, right, back right, back left; top left, right, back right, back left</param>
        public void SetCubeVertices(Vector3[] verticesPositions)
        {
            if (verticesPositions.Length != 8)
            {
                Debug.LogError("Invalid input to cube vertices!");
                return;
            }

            InitRenderers();

            for (int i = 0; i < 4; ++i)
            {
                edges[i].SetPosition(0, verticesPositions[i]);
                edges[i].SetPosition(1, verticesPositions[(i + 1) % 4]);

                edges[i + 4].SetPosition(0, verticesPositions[i]);
                edges[i + 4].SetPosition(1, verticesPositions[i + 4]);

                edges[i + 8].SetPosition(0, verticesPositions[i + 4]);
                edges[i + 8].SetPosition(1, verticesPositions[(i + 5) == 8 ? 4 : (i + 5)]);
            }
        }

        protected void InitRenderers()
        {
            if (edgesInitialized) { return; }

            for (int i = 0; i < edges.Length; ++i)
            {
                if (edges[i] == null)
                {
                    var newEdgeGo = Instantiate(cubeEdgeLineRendererPrefab, transform);
                    edges[i] = newEdgeGo.GetComponent<LineRenderer>();
                    edges[i].useWorldSpace = linePositionsSpace == Space.World;
                    edges[i].positionCount = 2;
                }
            }

            UpdateEdgeColors();

            edgesInitialized = true;
        }

        protected void UpdateEdgeColors()
        {
            for (int i = 0; i < edges.Length; ++i)
            {
                edges[i].startColor = edges[i].endColor = IsHighlighted ? highlightedColor : defaultColor;
            }
        }
    }
}


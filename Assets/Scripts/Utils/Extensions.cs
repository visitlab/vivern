﻿using System.Collections.Generic;

namespace Vivern
{
    public static class Extensions
    {
        public static void Resize<T>(this List<T> list, int newSize,
            System.Action<T> removedObjectsCallback = null,
            System.Func<int, T> newValueGeneratorCallback = null)
        {
            if (newSize > list.Count)
            {
                for (int i = list.Count; i < newSize; ++i)
                {
                    list.Add(newValueGeneratorCallback != null ? newValueGeneratorCallback(i) : default(T));
                }
            }
            else if (newSize < list.Count)
            {
                if (removedObjectsCallback != null)
                {
                    for (int i = newSize; i < list.Count; ++i)
                    {
                        removedObjectsCallback(list[i]);
                    }
                }

                list.RemoveRange(newSize, list.Count - newSize);
            }

            list.TrimExcess();
        }

        public static void Clear<T>(this IList<T> list, System.Action<T> removedObjectsCallback)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                removedObjectsCallback(list[i]);
            }

            list.Clear();
        }

        public static void ApplyFuncToEveryElement<T>(this List<List<List<T>>> list, System.Action<T> func)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                ApplyFuncToEveryElement(list[i], func);
            }
        }

        public static void ApplyFuncToEveryElement<T>(this List<List<T>> list, System.Action<T> func)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                ApplyFuncToEveryElement(list[i], func);
            }
        }

        public static void ApplyFuncToEveryElement<T>(this List<T> list, System.Action<T> func)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                func(list[i]);
            }
        }

        // Source: https://stackoverflow.com/a/643438
        public static T NextEnumValue<T>(this T src) where T : struct
        {
            if (!typeof(T).IsEnum) throw new System.ArgumentException(System.String.Format("Argument {0} is not an Enum", typeof(T).FullName));

            T[] Arr = (T[])System.Enum.GetValues(src.GetType());
            int j = System.Array.IndexOf<T>(Arr, src) + 1;
            return (Arr.Length == j) ? Arr[0] : Arr[j];
        }

        public static T PrevEnumValue<T>(this T src) where T : struct
        {
            if (!typeof(T).IsEnum) throw new System.ArgumentException(System.String.Format("Argument {0} is not an Enum", typeof(T).FullName));

            T[] Arr = (T[])System.Enum.GetValues(src.GetType());
            int j = System.Array.IndexOf<T>(Arr, src) - 1;
            return (0 > j) ? Arr[Arr.Length - 1] : Arr[j];
        }
    }
}

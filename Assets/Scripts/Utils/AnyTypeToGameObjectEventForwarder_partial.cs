﻿using UnityEngine;

namespace UVC.UnityWrappers
{
    // Keep in mind that this is a partial class!
    public partial class AnyTypeToGameObjectEventForwarder : MonoBehaviour
    {
        public void OnSourceEventFired(Vivern.Lattice lattice)
        {
            CallbacksToCall?.Invoke(lattice.gameObject);
        }
    }
}


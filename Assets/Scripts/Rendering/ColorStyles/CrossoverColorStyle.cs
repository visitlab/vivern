﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    [CreateAssetMenu(fileName = "CrossoverBasedColorStyle", menuName = "DNA Color Styles / By Crossovers", order = 3)]
    public class CrossoverColorStyle : AbstractDNAColorStyle
    {
        [SerializeField]
        private Color crossoverPositionColor;

        [SerializeField]
        private Color defaultColor;

        public override Color GetColor(INucleotide nucleotide)
        {
            var next = nucleotide.NextNucleotide;
            var prev = nucleotide.PreviousNucleotide;

            if (next != null)
            {
                if(nucleotide.LatticeLocation.x != next.LatticeLocation.x ||
                    nucleotide.LatticeLocation.y != next.LatticeLocation.y)
                {
                    return crossoverPositionColor;
                }
            }

            if (prev != null)
            {
                if (nucleotide.LatticeLocation.x != prev.LatticeLocation.x ||
                   nucleotide.LatticeLocation.y != prev.LatticeLocation.y)
                {
                    return crossoverPositionColor;
                }
            }

            return defaultColor;
        }

        public override Color GetColor(IStrand strand)
        {
            return defaultColor;
        }

        public override Color GetColor(IDNANanostructure structure)
        {
            return defaultColor;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    [CreateAssetMenu(fileName = "ScaffoldStapleColorStyle", menuName = "DNA Color Styles / By Scaffold", order = 2)]
    public class StrandScaffoldColorStyle : AbstractDNAColorStyle
    {
        [SerializeField]
        private Color scaffoldColor;

        [SerializeField]
        private Color stapleColor;

        public override Color GetColor(INucleotide nucleotide)
        {
            return GetColor(nucleotide.Strand);
        }

        public override Color GetColor(IStrand strand)
        {
            return strand.Type == StrandType.Scaffold ? scaffoldColor : stapleColor;
        }

        public override Color GetColor(IDNANanostructure structure)
        {
            return stapleColor;
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    [CreateAssetMenu(fileName = "NucleotideByBaseTypeColorStyle", menuName = "DNA Color Styles / By Nucleobase Type", order = 0)]
    public class NucleotideColorStyle : AbstractDNAColorStyle
    {
        [System.Serializable]
        private class BaseTypeColorPair
        {
            public NucleobaseType baseType;
            public Color color;
        }

        [SerializeField]
        private Color defaultColor;

        [SerializeField]
        private BaseTypeColorPair[] baseTypeToColorPairs;

        [System.NonSerialized]
        private Dictionary<NucleobaseType, Color> baseTypeToColor;

        [System.NonSerialized]
        private bool isInitialized = false;

        public override Color GetColor(INucleotide nucleotide)
        {
            if (!isInitialized)
            {
                Initialize();
                isInitialized = true;
            }

            Color col;
            if (baseTypeToColor.TryGetValue(nucleotide.BaseType, out col))
            {
                return col;
            }

            return defaultColor;
        }

        public override Color GetColor(IStrand strand)
        {
            return defaultColor;
        }

        public override Color GetColor(IDNANanostructure structure)
        {
            return defaultColor;
        }

        private void Initialize()
        {
            baseTypeToColor = new Dictionary<NucleobaseType, Color>();

            for (int i = 0; i < baseTypeToColorPairs.Length; ++i)
            {
                baseTypeToColor.Add(baseTypeToColorPairs[i].baseType, baseTypeToColorPairs[i].color);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern
{
    [CreateAssetMenu(fileName = "Color Scheme", menuName = "DNA Color Scheme", order = 0)]
    public class ColorScheme : ScriptableObject
    {
        [System.Serializable]
        public class VisualScaleColorStylePair
        {
            public Structures.VisualScale scale;
            public AbstractDNAColorStyle color;
        }

        [SerializeField]
        private VisualScaleColorStylePair[] visualScalesToColorStyles;

        public VisualScaleColorStylePair[] VisualScalesToColorStyle => visualScalesToColorStyles;
    }
}

﻿using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    [CreateAssetMenu(fileName = "StrandByIdColorStyle", menuName = "DNA Color Styles / By Strand Id", order = 1)]
    public class StrandColorStyle : AbstractDNAColorStyle
    {
        [SerializeField]
        private Color[] availableColors;

        [SerializeField]
        private Color defaultColor;

        public override Color GetColor(INucleotide nucleotide)
        {
            return GetColor(nucleotide.Strand);
        }

        public override Color GetColor(IStrand strand)
        {
            return availableColors[strand.Id % availableColors.Length];
        }

        public override Color GetColor(IDNANanostructure structure)
        {
            return defaultColor;
        }
    }
}

﻿using UnityEngine;

namespace Vivern
{
    public abstract class AbstractDNAColorStyle : ScriptableObject
    {
        public abstract Color GetColor(Structures.INucleotide nucleotide);

        public abstract Color GetColor(Structures.IStrand strand);

        public abstract Color GetColor(Structures.IDNANanostructure structure);
    }
}

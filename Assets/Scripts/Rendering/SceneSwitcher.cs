﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern
{
    public class SceneSwitcher : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] objectsToHideInPhotoScene;

        [SerializeField]
        private new Camera camera;

        [SerializeField]
        private Color photoSceneBackgroundColor = Color.white;

        public void SetNewSceneState(bool isPhotoScene)
        {
            ToggleSceneObjects(!isPhotoScene);

            if (isPhotoScene)
            {
                camera.clearFlags = CameraClearFlags.Color;
                camera.backgroundColor = photoSceneBackgroundColor;
            }
            else
            {
                camera.clearFlags = CameraClearFlags.Skybox;
            }
        }

        private void ToggleSceneObjects(bool newState)
        {
            for(int i = 0; i < objectsToHideInPhotoScene.Length; ++i)
            {
                objectsToHideInPhotoScene[i].SetActive(newState);
            }
        }
    }
}

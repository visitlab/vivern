﻿namespace Vivern.Structures
{
    public enum VisualScale : byte
    {
        Nucleotide,
        SingleStrand,
        DoubleStrand
    }
}

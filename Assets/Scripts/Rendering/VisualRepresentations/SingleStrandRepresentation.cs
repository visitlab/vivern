﻿using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    public class SingleStrandRepresentation : DNANanostructureRepresentation
    {
        [SerializeField]
        private Material material;

        [SerializeField]
        private int verticesPerRing = 6;

        [SerializeField]
        private int interpolationSubdivisionRings = 2;

        [SerializeField]
        private float singleStrandRadiusAngstrom = 1.5f;

        private Dictionary<IStrand, GameObject> strandsToGameObjects = new Dictionary<IStrand, GameObject>();

        public override INucleotide CheckNucleotideSelection(Vector3 controllerLocalPos, INucleotide first, INucleotide second)
        {
            float nucleotideRadius = singleStrandRadiusAngstrom * 2.0f; // TODO Mult by two is here just to have bigger radius for detection

            if (first != null && Vector3.SqrMagnitude(
                GetNucleotidePosition(first, nucleotideRadius) - controllerLocalPos)
                <= nucleotideRadius * nucleotideRadius)
            {
                return first;
            }
            else if (second != null && Vector3.SqrMagnitude(
                GetNucleotidePosition(second, nucleotideRadius) - controllerLocalPos)
                <= nucleotideRadius * nucleotideRadius)
            {
                return second;
            }

            return null;
        }

        public override void ClearData()
        {
            foreach (var gameObj in strandsToGameObjects.Values)
            {
                Destroy(gameObj);
            }

            strandsToGameObjects.Clear();
        }

        public override void RenderCompleteStructure()
        {
            ClearData();

            var strands = Nanostructure.GetStrands();

            for (int i = 0; i < strands.Count; ++i)
            {
                RenderStrand(strands[i]);
            }
        }

        public override void UpdateData(List<INucleotide> modifiedOrNewlyAddedNucleotides, List<INucleotide> nucleotidesToUnregister,
            List<IStrand> addedStrands, List<IStrand> removedStrands)
        {
            HashSet<IStrand> strandsToUpdate = new HashSet<IStrand>();
            HashSet<IStrand> strandsToRemove = new HashSet<IStrand>();

            for (int i = 0; i < modifiedOrNewlyAddedNucleotides.Count; ++i)
            {
                strandsToUpdate.Add(modifiedOrNewlyAddedNucleotides[i].Strand);
            }

            for (int i = 0; i < nucleotidesToUnregister.Count; ++i)
            {
                if (nucleotidesToUnregister[i].Strand.IsEmpty)
                {
                    strandsToUpdate.Remove(nucleotidesToUnregister[i].Strand);
                    strandsToRemove.Add(nucleotidesToUnregister[i].Strand);
                }
                else
                {
                    strandsToUpdate.Add(nucleotidesToUnregister[i].Strand);
                }
            }

            for (int i = 0; i < removedStrands.Count; ++i)
            {
                strandsToRemove.Add(removedStrands[i]);
            }

            foreach (var strand in strandsToRemove)
            {
                if (strandsToGameObjects.ContainsKey(strand))
                {
                    Destroy(strandsToGameObjects[strand]);
                    strandsToGameObjects.Remove(strand);
                }
            }

            foreach (var strand in strandsToUpdate)
            {
                RenderStrand(strand);
            }
        }

        private void RenderStrand(IStrand strand, bool lerpBeforeHermit = true)
        {
            var nucleotides = strand.GetNucleotides();

            List<List<Vector3>> connectedStrandPartsPositions = new List<List<Vector3>>();
            List<List<Color>> connectedStrandPartsColors = new List<List<Color>>();

            List<Vector3> nucleotidePositions = new List<Vector3>(nucleotides.Count);
            List<Color> nucleotideColors = new List<Color>(nucleotidePositions.Capacity);

            for (int j = 0; j < nucleotides.Count; ++j)
            {
                if (nucleotides[j].VisualScale == VisualScale ||
                    // This check ensures that in 5' to 3' dir, also the first nucleotide with different scale will be included
                    // in the representation which will cause the "connection" to be rendered as well. Wtihout this,
                    // it looked like the strand was disconnected.
                    (nucleotides[j].PreviousNucleotide != null && nucleotides[j].PreviousNucleotide.VisualScale == VisualScale))
                {
                    nucleotidePositions.Add(GetNucleotidePosition(nucleotides[j], singleStrandRadiusAngstrom));
                    nucleotideColors.Add(colorStyle.GetColor(nucleotides[j]));
                }
                else
                {
                    if (nucleotidePositions.Count > 0)
                    {
                        connectedStrandPartsPositions.Add(nucleotidePositions);
                        connectedStrandPartsColors.Add(nucleotideColors);

                        nucleotidePositions = new List<Vector3>(nucleotides.Count - j);
                        nucleotideColors = new List<Color>(nucleotidePositions.Capacity);
                    }
                }
            }

            if (nucleotidePositions.Count > 0)
            {
                connectedStrandPartsPositions.Add(nucleotidePositions);
                connectedStrandPartsColors.Add(nucleotideColors);
            }

            for (int i = 0; i < connectedStrandPartsPositions.Count; ++i)
            {
                var currColors = connectedStrandPartsColors[i];

                if (lerpBeforeHermit)
                {
                    List<Vector3> newPos = new List<Vector3>();
                    List<Color> newColors = new List<Color>();

                    newPos.Add(connectedStrandPartsPositions[i][0]);
                    newColors.Add(currColors[0]);

                    for (int j = 1; j < connectedStrandPartsPositions[i].Count; ++j)
                    {
                        // The idea here is to linearly interpolate primarly crossover locations where there is a huge gap
                        // Constants here are just something which worked nice
                        int tMax = Mathf.Max(1, Mathf.RoundToInt(Vector3.Distance(connectedStrandPartsPositions[i][j - 1], connectedStrandPartsPositions[i][j]) / (1.5f * ParentLattice.DnaBasePairRiseAngstrom)));

                        for (int t = 1; t <= tMax; ++t)
                        {
                            newPos.Add(Vector3.Lerp(connectedStrandPartsPositions[i][j - 1], connectedStrandPartsPositions[i][j], t / (float)tMax));
                            if (currColors.Count == connectedStrandPartsPositions[i].Count)
                            {
                                newColors.Add(Color.Lerp(currColors[j - 1], currColors[j], t / (float)tMax));
                            }
                        }
                    }

                    connectedStrandPartsPositions[i] = newPos;
                    currColors = newColors;
                }

                connectedStrandPartsPositions[i] = UVC.Utils.MathFunctions
                    .HermitInterpolatePoints(connectedStrandPartsPositions[i], interpolationSubdivisionRings, ref currColors, 0.8f);

                connectedStrandPartsColors[i] = currColors;
            }

            CreateBackboneObject(strand.Identifier, connectedStrandPartsPositions, connectedStrandPartsColors, strand);
        }

        private void CreateBackboneObject(string name, List<List<Vector3>> pointsComponents, List<List<Color>> colorsComponents, IStrand strand)
        {
            GameObject existingGo = null;

            if (strandsToGameObjects.TryGetValue(strand, out existingGo))
            {
                if (pointsComponents.Count == 0)
                {
                    existingGo.SetActive(false);
                    return;
                }
                else
                {
                    existingGo.SetActive(true);
                }
            }
            else if (pointsComponents.Count == 0) { return; }

            var dnaStrandGo = existingGo!= null ? existingGo : new GameObject(name);
            MeshFilter mf;

            if (existingGo == null)
            {
                dnaStrandGo.layer = LayerMask.NameToLayer("DNAPart");
                dnaStrandGo.transform.SetParent(transform);
                dnaStrandGo.transform.localRotation = Quaternion.identity;
                dnaStrandGo.transform.localPosition = Vector3.zero;
                dnaStrandGo.transform.localScale = Vector3.one;
                var mr = dnaStrandGo.AddComponent<MeshRenderer>();
                mr.material = material;

                mf = dnaStrandGo.AddComponent<MeshFilter>();
            }
            else
            {
                mf = dnaStrandGo.GetComponent<MeshFilter>();
            }

            CombineInstance[] combine = new CombineInstance[pointsComponents.Count];

            for (int i = 0; i < combine.Length; ++i)
            {
                var points = pointsComponents[i];
                var colors = colorsComponents[i];

                // Make sure that at least something is shown if only one nucleotide exists in this part
                if (points.Count == 1)
                {
                    points.Add(points[0] + new Vector3(0.0f, 0.0f, ParentLattice.DnaBasePairRiseAngstrom * 0.5f));
                    colors.Add(colors[0]);
                }

                combine[i].mesh = UVC.UnityWrappers.MeshGenerator.GenerateBackboneTube(
                    points, colors, singleStrandRadiusAngstrom, verticesPerRing, 2, 0.25f);
            }

            mf.mesh.CombineMeshes(combine, true, false);

            strandsToGameObjects[strand] = dnaStrandGo;
        }
    }
}

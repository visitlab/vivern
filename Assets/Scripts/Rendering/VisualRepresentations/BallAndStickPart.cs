﻿using UnityEngine;

namespace Vivern
{
    public class BallAndStickPart : MonoBehaviour
    {
        [SerializeField]
        private GameObject ballObject;

        [SerializeField]
        private MeshRenderer ballMeshRenderer;

        [SerializeField]
        private GameObject stickObject;

        [SerializeField]
        private MeshRenderer stickMeshRenderer;

        private MaterialPropertyBlock _ballPropertyBlock;
        private MaterialPropertyBlock BallPropertyBlock
            => _ballPropertyBlock == null ? (_ballPropertyBlock = new MaterialPropertyBlock()) : _ballPropertyBlock;

        private MaterialPropertyBlock _stickPropertyBlock;

        private MaterialPropertyBlock StickPropertyBlock
            => _stickPropertyBlock == null ? (_stickPropertyBlock = new MaterialPropertyBlock()) : _stickPropertyBlock;


        public void SetBallState(bool state)
        {
            ballObject.SetActive(state);
        }

        public void SetStickState(bool state)
        {
            stickObject.SetActive(state);
        }

        public void SetBallColor(Color color, int textureArrayIndex)
        {
            BallPropertyBlock.SetColor("_BaseColor", color);
            BallPropertyBlock.SetInt("_ACGTU_TAIndex", textureArrayIndex);
            ballMeshRenderer.SetPropertyBlock(BallPropertyBlock);
        }

        public void SetStickColor(Color color)
        {
            StickPropertyBlock.SetColor("_BaseColor", color);
            stickMeshRenderer.SetPropertyBlock(StickPropertyBlock);
        }

        public void SetBallMaterialFloat(string name, float value)
        {
            BallPropertyBlock.SetFloat(name, value);
            ballMeshRenderer.SetPropertyBlock(BallPropertyBlock);
        }

        public void SetStickMaterialFloat(string name, float value)
        {
            StickPropertyBlock.SetFloat(name, value);
            stickMeshRenderer.SetPropertyBlock(StickPropertyBlock);
        }

        public void SetBallRadius(float radius)
        {
            float diameter = radius * 2.0f;

            ballObject.transform.localScale =
                new Vector3(diameter, diameter, diameter);
        }

        public void SetStickRadius(float radius)
        {
            float diameter = radius * 2.0f;

            stickObject.transform.localScale =
                new Vector3(diameter, 1.0f, diameter);
        }

        public void SetStickEndPointWorld(Vector3 endPoint)
        {
            var localCoordinatesEndPoint = transform.InverseTransformPoint(endPoint);

            stickObject.transform.localPosition = localCoordinatesEndPoint * 0.5f;

            stickObject.transform.localScale = new Vector3(
                stickObject.transform.localScale.x,
                localCoordinatesEndPoint.magnitude * 0.5f,
                stickObject.transform.localScale.z);

            stickObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, transform.TransformDirection(localCoordinatesEndPoint));
        }
    }
}

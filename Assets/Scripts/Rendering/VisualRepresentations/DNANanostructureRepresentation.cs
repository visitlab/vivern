﻿using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    public abstract class DNANanostructureRepresentation : MonoBehaviour
    {
        [SerializeField]
        protected DNANanostructureRenderer parentRenderer;

        [SerializeField]
        protected VisualScale visualScale;

        protected AbstractDNAColorStyle colorStyle;

        public DNANanostructureRenderer ParentRenderer => parentRenderer;

        public Lattice ParentLattice => ParentRenderer.ParentLattice;

        public IDNANanostructure Nanostructure => ParentLattice.Nanostructure;

        public VisualScale VisualScale => visualScale;

        public AbstractDNAColorStyle ColorStyle
        {
            get => colorStyle;
            set => colorStyle = value;
        }

        public abstract void ClearData();

        public abstract void RenderCompleteStructure();

        /// <summary>
        /// Updates data of this representation
        /// </summary>
        /// <param name="modifiedOrNewlyAddedNucleotides">Newly added or modified nucleotides which should be rendered by this representation</param>
        /// <param name="nucleotidesToUnregister">Nucleotides which should be no more rendered in this representation</param>
        /// <param name="addedStrands">Strands which were added after the last call to UpdateData</param>
        /// <param name="removedStrands">Strands which were removed after the last call to UpdateData</param>
        public abstract void UpdateData(List<INucleotide> modifiedOrNewlyAddedNucleotides, List<INucleotide> nucleotidesToUnregister,
            List<IStrand> addedStrands, List<IStrand> removedStrands);

        public abstract INucleotide CheckNucleotideSelection(Vector3 controllerLocalPos, INucleotide first, INucleotide second);

        protected Vector3 GetNucleotidePosition(INucleotide nucleotide, float borderOffset)
        {
            Vector3 latticeCellCenterPosition = nucleotide.LatticeElement.LocalCenterPosition;
            Vector3 twistOffset = (
                Quaternion.Euler(0.0f, 0.0f, nucleotide.CurrentTwistStateAngle) * Vector3.up
                ).normalized *
                (nucleotide.IsInTwistedState ?
                (ParentLattice.DnaDiameterAngstrom * 0.5f - borderOffset) :
                (borderOffset * 2.0f));

            return latticeCellCenterPosition + twistOffset;
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    public class NucleotideRepresentation : DNANanostructureRepresentation
    {
        [SerializeField]
        protected GameObject ballStickPrefab;

        [SerializeField]
        protected float bondTubeRadiusAngstrom = 0.75f;

        [SerializeField]
        protected float nucleotideRadiusAngstrom = 2.25f;

        private bool _renderLabels = true;
        public bool RenderLabels
        {
            get => _renderLabels;
            set
            {
                if (_renderLabels == value) { return; }

                _renderLabels = value;
                UpdateSpawnedNucleotides();
            }
        }

        private Dictionary<INucleotide, GameObject> nucleotidesToGameObjects = new Dictionary<INucleotide, GameObject>();

        public override INucleotide CheckNucleotideSelection(Vector3 controllerLocalPos, INucleotide first, INucleotide second)
        {
            float nucleotideRadius = nucleotideRadiusAngstrom;

            if (first != null && Vector3.SqrMagnitude(
                GetNucleotidePosition(first, nucleotideRadius) - controllerLocalPos)
                <= nucleotideRadius * nucleotideRadius)
            {
                return first;
            }
            else if (second != null && Vector3.SqrMagnitude(
                GetNucleotidePosition(second, nucleotideRadius) - controllerLocalPos)
                <= nucleotideRadius * nucleotideRadius)
            {
                return second;
            }

            return null;
        }

        public override void ClearData()
        {
            foreach (var nuclGo in nucleotidesToGameObjects.Values)
            {
                Destroy(nuclGo);
            }

            nucleotidesToGameObjects.Clear();
        }

        public override void RenderCompleteStructure()
        {
            ClearData();

            var strands = Nanostructure.GetStrands();

            for (int i = 0; i < strands.Count; ++i)
            {
                var nucleotides = strands[i].GetNucleotides();

                for (int j = 0; j < nucleotides.Count; ++j)
                {
                    RenderNucleotide(nucleotides[j]);
                }
            }
        }

        public override void UpdateData(List<INucleotide> modifiedOrNewlyAddedNucleotides, List<INucleotide> nucleotidesToUnregister,
            List<IStrand> addedStrands, List<IStrand> removedStrands)
        {
            for (int i = 0; i < nucleotidesToUnregister.Count; ++i)
            {
                if (nucleotidesToGameObjects.TryGetValue(nucleotidesToUnregister[i], out GameObject nuclGo))
                {
                    Destroy(nuclGo);
                    nucleotidesToGameObjects.Remove(nucleotidesToUnregister[i]);
                }
            }

            for (int i = 0; i < modifiedOrNewlyAddedNucleotides.Count; ++i)
            {
                RenderNucleotide(modifiedOrNewlyAddedNucleotides[i]);
            }
        }

        private void UpdateSpawnedNucleotides()
        {
            foreach (var nucl in nucleotidesToGameObjects.Keys)
            {
                RenderNucleotide(nucl);
            }
        }

        private void RenderNucleotide(INucleotide nucleotide)
        {
            int textureArrayIndex = 4;

            switch (nucleotide.BaseType)
            {
                case NucleobaseType.Adenine:
                    textureArrayIndex = 0;
                    break;
                case NucleobaseType.Cytosine:
                    textureArrayIndex = 1;
                    break;
                case NucleobaseType.Guanine:
                    textureArrayIndex = 2;
                    break;
                case NucleobaseType.Thymine:
                    textureArrayIndex = 3;
                    break;
            }

            nucleotidesToGameObjects.TryGetValue(nucleotide, out GameObject existingGo);

            var nuclObj = existingGo != null ? existingGo : Instantiate(ballStickPrefab, transform);
            nuclObj.name = nucleotide.Identifier;
            nuclObj.transform.localPosition = GetNucleotidePosition(nucleotide, nucleotideRadiusAngstrom);

            var basPart = nuclObj.GetComponent<BallAndStickPart>();
            basPart.SetBallColor(colorStyle.GetColor(nucleotide), textureArrayIndex);

            if (!RenderLabels)
            {
                basPart.SetBallMaterialFloat("_MaskStrength", 0.0f);
            }

            Vector3? stickEndPosition = nucleotide.IsThreePrime ? (Vector3?)null :
                ParentLattice
                .GetWorldPositionForLocalPosition(
                    GetNucleotidePosition(
                        nucleotide.NextNucleotide, nucleotideRadiusAngstrom));

            basPart.SetStickState(stickEndPosition.HasValue);
            basPart.SetBallRadius(nucleotideRadiusAngstrom);
            basPart.SetStickRadius(bondTubeRadiusAngstrom);

            if (stickEndPosition.HasValue)
            {
                basPart.SetStickEndPointWorld(stickEndPosition.Value);
            }

            if (nucleotide.PreviousNucleotide != null && nucleotide.PreviousNucleotide.VisualScale == nucleotide.VisualScale &&
                nucleotidesToGameObjects.TryGetValue(nucleotide.PreviousNucleotide, out GameObject prevNuclObj))
            {
                prevNuclObj.GetComponent<BallAndStickPart>().SetStickEndPointWorld(
                    ParentLattice.GetWorldPositionForLocalPosition(nuclObj.transform.localPosition));
            }

            nucleotidesToGameObjects[nucleotide] = nuclObj;
        }
    }
}

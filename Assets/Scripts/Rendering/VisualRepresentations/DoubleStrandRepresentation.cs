﻿using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    public class DoubleStrandRepresentation : DNANanostructureRepresentation
    {
        [SerializeField]
        private GameObject doubleStrandCylinderPrefab;

        private Dictionary<INucleotide, GameObject> nucleotidesToGameObjects = new Dictionary<INucleotide, GameObject>();

        public override INucleotide CheckNucleotideSelection(Vector3 controllerLocalPos, INucleotide first, INucleotide second)
        {
            return first ?? second;
        }

        public override void ClearData()
        {
            foreach (var nuclGo in nucleotidesToGameObjects.Values)
            {
                Destroy(nuclGo);
            }

            nucleotidesToGameObjects.Clear();
        }

        public override void RenderCompleteStructure()
        {
            ClearData();

            var strands = Nanostructure.GetStrands();

            for (int i = 0; i < strands.Count; ++i)
            {
                var nucleotides = strands[i].GetNucleotides();

                for (int j = 0; j < nucleotides.Count; ++j)
                {
                    RenderNucleotide(nucleotides[j]);
                }
            }
        }

        public override void UpdateData(List<INucleotide> modifiedOrNewlyAddedNucleotides, List<INucleotide> nucleotidesToUnregister,
            List<IStrand> addedStrands, List<IStrand> removedStrands)
        {
            for (int i = 0; i < nucleotidesToUnregister.Count; ++i)
            {
                if (nucleotidesToGameObjects.TryGetValue(nucleotidesToUnregister[i], out GameObject nuclGo))
                {
                    Destroy(nuclGo);
                    nucleotidesToGameObjects.Remove(nucleotidesToUnregister[i]);
                }
            }

            for (int i = 0; i < modifiedOrNewlyAddedNucleotides.Count; ++i)
            {
                RenderNucleotide(modifiedOrNewlyAddedNucleotides[i]);
            }
        }

        private void RenderNucleotide(INucleotide nucleotide)
        {
            // In double strand representation, only one nucleotide is responsible for drawing
            // the game object. The decision is made based on their IDs.
            if (nucleotide.ComplementaryBase == null ||
                nucleotide.Strand.Id > nucleotide.ComplementaryBase.Strand.Id ||
                (nucleotide.Strand.Id == nucleotide.ComplementaryBase.Strand.Id && nucleotide.Id > nucleotide.ComplementaryBase.Id))
            {
                Vector3 latticeCellCenterPosition = nucleotide.LatticeElement.LocalCenterPosition;

                nucleotidesToGameObjects.TryGetValue(nucleotide, out GameObject existingGo);

                var dsObj = existingGo != null ? existingGo : Instantiate(doubleStrandCylinderPrefab, transform);
                dsObj.name = nucleotide.Identifier;

                Color color =
                    nucleotide.ComplementaryBase == null ?
                    colorStyle.GetColor(nucleotide) :
                    Color.Lerp(colorStyle.GetColor(nucleotide),
                               colorStyle.GetColor(nucleotide.ComplementaryBase), 0.5f);

                var block = new MaterialPropertyBlock();
                block.SetColor("_BaseColor", color);
                dsObj.GetComponent<MeshRenderer>().SetPropertyBlock(block);

                dsObj.transform.localPosition = latticeCellCenterPosition;
                dsObj.transform.localScale = new Vector3(
                    ParentLattice.DnaDiameterAngstrom * 0.9f,
                    ParentLattice.DnaDiameterAngstrom * 0.9f,
                    ParentLattice.DnaBasePairRiseAngstrom);

                nucleotidesToGameObjects[nucleotide] = dsObj;
            }
        }
    }
}

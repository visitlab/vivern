﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern.Structures
{
    public class DNANanostructureRenderer : MonoBehaviour
    {
        private enum StructureRecordAction
        {
            Add,
            Remove,
            Update
        }

        [SerializeField]
        private DNANanostructureRepresentation[] representations;

        [SerializeField]
        private ColorScheme colorScheme;

        [SerializeField]
        private int completeScaleChangeNucleotidesPerFrame = 512;

        private Lattice _parentLattice = null;
        public Lattice ParentLattice
        {
            get
            {
                return _parentLattice;
            }
            set
            {
                if (_parentLattice != null)
                {
                    throw new System.InvalidOperationException("It is not allowed to change parent lattice.");
                }

                _parentLattice = value;

                Nanostructure.StrandAdded += OnStrandAddedToStructure;
                Nanostructure.StrandRemoved += OnStrandRemovedFromStructure;
            }
        }

        public IDNANanostructure Nanostructure => ParentLattice.Nanostructure;

        private Dictionary<VisualScale, DNANanostructureRepresentation> scalesToRepresentation =
            new Dictionary<VisualScale, DNANanostructureRepresentation>();

        private Dictionary<INucleotide, StructureRecordAction> changedNucleotides =
            new Dictionary<INucleotide, StructureRecordAction>(1024);

        private Dictionary<IStrand, StructureRecordAction> changedStrands
            = new Dictionary<IStrand, StructureRecordAction>(8);

        private Dictionary<VisualScale, List<INucleotide>> tmpNucleotidesToAddOrModify = new Dictionary<VisualScale, List<INucleotide>>(3);
        private Dictionary<VisualScale, List<INucleotide>> tmpNucleotidesToRemove = new Dictionary<VisualScale, List<INucleotide>>(3);

        private List<IStrand> tmpStrandsToAdd = new List<IStrand>(8);
        private List<IStrand> tmpStrandsToRemove = new List<IStrand>(8);

        private void Awake()
        {
            for (int i = 0; i < representations.Length; ++i)
            {
                scalesToRepresentation.Add(representations[i].VisualScale, representations[i]);

                tmpNucleotidesToAddOrModify.Add(representations[i].VisualScale, new List<INucleotide>(256));
                tmpNucleotidesToRemove.Add(representations[i].VisualScale, new List<INucleotide>(256));
            }

            for(int i = 0; i < colorScheme.VisualScalesToColorStyle.Length; ++i)
            {
                scalesToRepresentation[colorScheme.VisualScalesToColorStyle[i].scale].ColorStyle = 
                    colorScheme.VisualScalesToColorStyle[i].color;
            }
        }

        public void SetNewColorScheme(ColorScheme newScheme)
        {
            if(colorScheme == newScheme) { return; }

            colorScheme = newScheme;

            for (int i = 0; i < colorScheme.VisualScalesToColorStyle.Length; ++i)
            {
                scalesToRepresentation[colorScheme.VisualScalesToColorStyle[i].scale].ColorStyle =
                    colorScheme.VisualScalesToColorStyle[i].color;
            }

            // Inform the renderer that all nucleotides of the structure
            // will need to be re-rendered
            var strands = Nanostructure.GetStrands();
            
            for(int s = 0; s < strands.Count; ++s)
            {
                INucleotide fst = strands[s].FivePrimeEnd;

                while(fst != null)
                {
                    OnNucleotideUpdated(fst);
                    fst = fst.NextNucleotide;
                }
            }
        }

        public void UpdateRenderedData()
        {
            if (changedNucleotides.Count == 0) { return; }

            foreach (var changedNucl in changedNucleotides)
            {
                if (changedNucl.Value == StructureRecordAction.Add || changedNucl.Value == StructureRecordAction.Update)
                {
                    // The nucleotide must stay up-to-date in one scale and be removed from other scales
                    for (int i = 0; i < representations.Length; ++i)
                    {
                        if (changedNucl.Key.VisualScale == representations[i].VisualScale)
                        {
                            tmpNucleotidesToAddOrModify[representations[i].VisualScale].Add(changedNucl.Key);
                        }
                        else
                        {
                            tmpNucleotidesToRemove[representations[i].VisualScale].Add(changedNucl.Key);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < representations.Length; ++i)
                    {
                        tmpNucleotidesToRemove[representations[i].VisualScale].Add(changedNucl.Key);
                    }
                }
            }

            foreach (var changedStrand in changedStrands)
            {
                if (changedStrand.Value == StructureRecordAction.Remove)
                {
                    tmpStrandsToRemove.Add(changedStrand.Key);
                }
                else
                {
                    tmpStrandsToAdd.Add(changedStrand.Key);
                }
            }

            for (int i = 0; i < representations.Length; ++i)
            {
                var nucleotidesToAddOrModify = tmpNucleotidesToAddOrModify[representations[i].VisualScale];
                var nucleotidesToUnregister = tmpNucleotidesToRemove[representations[i].VisualScale];

                representations[i].UpdateData(nucleotidesToAddOrModify, nucleotidesToUnregister,
                    tmpStrandsToAdd, tmpStrandsToRemove);

                nucleotidesToAddOrModify.Clear();
                nucleotidesToUnregister.Clear();
            }

            tmpStrandsToAdd.Clear();
            tmpStrandsToRemove.Clear();

            changedStrands.Clear();
            changedNucleotides.Clear();
        }

        public void SetCompleteScale(VisualScale scale, bool immediate = false)
        {
            if (immediate)
            {
                var strands = Nanostructure.GetStrands();

                for (int i = 0; i < strands.Count; ++i)
                {
                    var nucleotides = strands[i].GetNucleotides();

                    for (int j = 0; j < nucleotides.Count; ++j)
                    {
                        nucleotides[j].VisualScale = scale;
                    }
                }
            }
            else
            {
                StartCoroutine(SetCompleteScaleSequence(scale));
            }
        }

        private IEnumerator SetCompleteScaleSequence(VisualScale scale)
        {
            int nucleotidesChangedSinceLastFrame = 0;
            var strands = Nanostructure.GetStrands();

            for (int i = 0; i < strands.Count; ++i)
            {
                var nucleotides = strands[i].GetNucleotides();

                for (int j = 0; j < nucleotides.Count; ++j)
                {
                    nucleotides[j].VisualScale = scale;
                    ++nucleotidesChangedSinceLastFrame;

                    if (nucleotidesChangedSinceLastFrame >= completeScaleChangeNucleotidesPerFrame)
                    {
                        yield return null;
                        nucleotidesChangedSinceLastFrame = 0;
                    }
                }
            }
        }

        public INucleotide CheckNucleotideSelection(Vector3 controllerLocalPos, INucleotide first, INucleotide second)
        {
            return scalesToRepresentation[(first ?? second).VisualScale].CheckNucleotideSelection(controllerLocalPos, first, second);
        }

        private void OnStrandAddedToStructure(IStrand newStrand)
        {
            newStrand.NucleotideAdded += OnNucleotideAdded;
            newStrand.NucleotideRemoved += OnNucleotideRemoved;
            newStrand.NucleotideVisualPropertyChanged += OnNucleotideUpdated;

            changedStrands[newStrand] = StructureRecordAction.Add;
        }

        private void OnStrandRemovedFromStructure(IStrand strand)
        {
            strand.NucleotideAdded -= OnNucleotideAdded;
            strand.NucleotideRemoved -= OnNucleotideRemoved;
            strand.NucleotideVisualPropertyChanged -= OnNucleotideUpdated;

            changedStrands[strand] = StructureRecordAction.Remove;
        }

        private void OnNucleotideAdded(INucleotide nucleotide)
        {
            changedNucleotides[nucleotide] = StructureRecordAction.Add;
        }

        private void OnNucleotideRemoved(INucleotide nucleotide)
        {
            changedNucleotides[nucleotide] = StructureRecordAction.Remove;
        }

        private void OnNucleotideUpdated(INucleotide nucleotide)
        {
            // If the nucleotide key already exists, it is already marked for addition, removal
            // or update and these actions are already covering (or higher priority) than this one
            if (!changedNucleotides.ContainsKey(nucleotide))
            {
                changedNucleotides[nucleotide] = StructureRecordAction.Update;
            }
        }
    }
}

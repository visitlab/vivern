﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Vivern
{
    /// <summary>
    /// Capacity-limited stack class
    /// </summary>
    public class DropoutStack<T> : ICollection
    {
        public int Capacity { get; protected set; }

        public int Count => items.Count;

        public bool IsSynchronized => false;

        public object SyncRoot => null;

        private LinkedList<T> items = new LinkedList<T>();

        public DropoutStack(int capacity)
        {
            if(capacity <= 0)
            {
                throw new ArgumentException("Cannot create dropout stack with capacity less than one!");
            }

            Capacity = capacity;
        }

        public void Push(T item)
        {
            if(Count >= Capacity)
            {
                items.RemoveFirst();
            }

            items.AddLast(item);
        }

        /// Calling on empty stack causes undefined behavior.
        public T Pop()
        {
            var result = items.Last.Value;
            items.RemoveLast();
            return result;
        }

        /// <summary>
        /// Calling on empty stack causes undefined behavior.
        /// </summary>
        public T Peek()
        {
            return items.Last.Value;
        }

        public void Clear()
        {
            items.Clear();
        }

        public void CopyTo(Array array, int index)
        {
            int counter = 0;
            foreach(var item in items)
            {
                array.SetValue(item, index + counter++);
            }
        }

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }
}

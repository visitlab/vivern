﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern
{
    public interface IAppCommand
    {
        string CommandName { get; }

        void Execute();
        void Revert();
    }
}

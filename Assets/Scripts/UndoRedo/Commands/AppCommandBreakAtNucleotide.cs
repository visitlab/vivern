﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    public class AppCommandBreakAtNucleotide : IAppCommand
    {
        public string CommandName => "Break at nucleotide";

        INucleotide nucleotideToBreakAt;
        INucleotide followingNucleotide;

        public AppCommandBreakAtNucleotide(INucleotide nucleotide)
        {
            nucleotideToBreakAt = nucleotide;
            followingNucleotide = nucleotide.IsThreePrime ? null : nucleotide.NextNucleotide;
        }

        public void Execute()
        {
            nucleotideToBreakAt?.Strand.BreakAtNucleotide(nucleotideToBreakAt);
        }

        public void Revert()
        {
            new AppCommandConnectNucleotides(nucleotideToBreakAt, followingNucleotide).Execute();
        }
    }
}
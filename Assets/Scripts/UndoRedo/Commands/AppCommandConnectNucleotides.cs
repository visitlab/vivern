﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vivern.Structures;

namespace Vivern
{
    public class AppCommandConnectNucleotides : IAppCommand
    {
        public string CommandName => "Connect nucleotides";

        INucleotide first;
        INucleotide second;

        public AppCommandConnectNucleotides(INucleotide first, INucleotide second)
        {
            this.first = first;
            this.second = second;
        }

        public void Execute()
        {
            if (first.Strand == second.Strand)
            {
                Debug.LogWarning("Cannot connect nucleotides from the same strand!");
                return;
            }

            if (first.IsThreePrime && second.IsFivePrime)
            {
                first.Nanostructure.AppendStrands(first.Strand, second.Strand);
            }
            else if (second.IsThreePrime && first.IsFivePrime)
            {
                first.Nanostructure.AppendStrands(second.Strand, first.Strand);
            }
            else
            {
                Debug.LogWarning("Only end nucleotides (and in correct order) can be connected.");
                return;
            }
        }

        public void Revert()
        {
            new AppCommandBreakAtNucleotide(first.IsThreePrime ? first : second).Execute();
        }
    }
}

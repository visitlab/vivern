﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivern
{
    public class UndoRedoManager
    {
        private static readonly object instanceLock = new object();

        private static UndoRedoManager _instance;
        public static UndoRedoManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (instanceLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new UndoRedoManager(8); // Whoa, magic number
                        }
                    }
                }
                return _instance;
            }
        }

        public bool IsUndoStackEmpty => undoCommands.Count == 0;

        public bool IsRedoStackEmpty => redoCommands.Count == 0;

        public string UndoStackTopCommandName => IsUndoStackEmpty ? string.Empty : undoCommands.Peek().CommandName;

        public string RedoStackTopCommandName => IsRedoStackEmpty ? string.Empty : redoCommands.Peek().CommandName;

        private DropoutStack<IAppCommand> undoCommands;
        private DropoutStack<IAppCommand> redoCommands;

        private UndoRedoManager(int undoDepth)
        {
            undoCommands = new DropoutStack<IAppCommand>(undoDepth);
            redoCommands = new DropoutStack<IAppCommand>(undoDepth);
        }

        public void Undo(int depth = 1)
        {
            for (int i = 0; i < depth && undoCommands.Count > 0; ++i)
            {
                var cmdToUndo = undoCommands.Pop();
                cmdToUndo.Revert();
                redoCommands.Push(cmdToUndo);
            }
        }

        public void Redo(int depth = 1)
        {
            for (int i = 0; i < depth && redoCommands.Count > 0; ++i)
            {
                var cmdToRedo = redoCommands.Pop();
                cmdToRedo.Execute();
                undoCommands.Push(cmdToRedo);
            }
        }

        /// <summary>
        /// Adds and executes new command
        /// </summary>
        public void AddNewCommand(IAppCommand command)
        {
            command.Execute();
            undoCommands.Push(command);
            redoCommands.Clear();
        }
    }
}

﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Vivern.EditorScripts
{
    public class TextureArrayCreator : EditorWindow
    {
        private List<Texture2D> inputTextures = new List<Texture2D>();

        [MenuItem("Window/Texture Array Creator")]
        static void ShowWindow()
        {
            var window = (TextureArrayCreator)GetWindow(typeof(TextureArrayCreator));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("Input textures of SAME DIMENSIONS:", EditorStyles.boldLabel);

            for (int i = 0; i < inputTextures.Count; ++i)
            {
                inputTextures[i] = (Texture2D)EditorGUILayout.ObjectField(inputTextures[i], typeof(Texture2D), false);
            }

            if (GUILayout.Button("Add texture"))
            {
                inputTextures.Add(null);
            }

            if (GUILayout.Button("Remove texture"))
            {
                inputTextures.RemoveAt(inputTextures.Count - 1);
            }

            GUILayout.Label("Save texture array to assets:", EditorStyles.boldLabel);

            string path = GUILayout.TextField("Assets/Textures/textureArray.asset");

            if (GUILayout.Button("Save texture array"))
            {
                Texture2DArray array = new Texture2DArray(inputTextures[0].width, inputTextures[0].height, inputTextures.Count,
                    inputTextures[0].format, true, false);

                for (int i = 0; i < inputTextures.Count; i++)
                {
                    array.SetPixels(inputTextures[i].GetPixels(), i);
                }

                array.Apply();

                AssetDatabase.CreateAsset(array, path);
            }
        }
    }
}

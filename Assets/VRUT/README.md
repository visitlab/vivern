# VRUT – [V]irtual [R]eality [U]nity [T]ools  

This is a kind of deprecated toolkit. Please use the newer version [MUVRE](https://gitlab.fi.muni.cz/grp-edive/muvre.git).

VRUT repository contains the basic framework to simplify virtual reality development in Unity.  
It does not contain Unity project but rather a set of useful assets which can be built on top of XR Interaction Toolkit code.  
Unity projects using VRUT should be using one of the latest versions of the Unity to use up-to-date features.
  
# README

**If you want to use VRUT:**

*  it is **HIGHLY RECOMMENDED** to [request invite to VRUT Trello board](https://trello.com/invite/b/jaDW8W9Q/c4413580c59cfccf650bcdc80c4e307e/vrut-virtual-reality-unity-tools) which serves as a bug/feature tracker.

*  add [XR Interaction Toolkit](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@0.9/manual/index.html) package via Package Manager

*  then, add VRUT as a submodule into your repository, i.e., .gitmodules file might contain something like this afterwards:
  
*[submodule "VRUT"]  
path = Assets/VRUT  
url = https://gitlab.fi.muni.cz/xkutak/vrut  
branch = master*  

**If you want to add something to the repository:**

* make sure it is error-free and storage-effective (i.e., do not upload useless big files)

* choose appropriate directory or create a new one with a meaningful name. Use a good name also for the item you are adding.

* ensure that the item can be easily used by others (e.g, prefab or script). If not possible, provide sufficient documentation/how-to guide.

* in case of adding scripts, (try to) use VRUT namespace.
  

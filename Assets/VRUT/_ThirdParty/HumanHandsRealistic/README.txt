Hands found via following github comment: https://github.com/ExtendRealityLtd/VRTK/issues/1078#issuecomment-319341457

Main author of the model: SuperDasil @ Blendswap.com (https://www.blendswap.com/blend/16177)

License: CC-BY

﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    [RequireComponent(typeof(XRController))]
    public class XRControllerInputManager : MonoBehaviour
    {
        [SerializeField]
        private XRController controller;

        [SerializeField]
        private XRInputSettings inputSettings;

        private void Awake()
        {
            if (!controller) controller = GetComponent<XRController>();
        }

        private void Update()
        {
            HandleButtonEvents();
            HandleAxisEvents();
            HandleAxis2DEvents();
        }

        private void HandleButtonEvents()
        {
            foreach (XRButtonHandler handler in inputSettings.buttonHandlers)
            {
                handler.HandleState(controller);
            }
        }

        private void HandleAxisEvents()
        {
            foreach (XRAxisHandler handler in inputSettings.axisHandlers)
            {
                handler.HandleState(controller);
            }
        }

        private void HandleAxis2DEvents()
        {
            foreach (XRAxis2DHandler handler in inputSettings.axis2DHandlers)
            {
                handler.HandleState(controller);
            }
        }
    }
}

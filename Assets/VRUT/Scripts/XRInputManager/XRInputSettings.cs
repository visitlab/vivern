﻿using System.Collections.Generic;
using UnityEngine;

namespace VRUT
{
    [CreateAssetMenu(fileName = "NewInputSettings", menuName = "XR/VRUT/InputSettings")]
    public class XRInputSettings : ScriptableObject
    {
        public List<XRButtonHandler> buttonHandlers = new List<XRButtonHandler>();
        public List<XRAxisHandler> axisHandlers = new List<XRAxisHandler>();
        public List<XRAxis2DHandler> axis2DHandlers = new List<XRAxis2DHandler>();
    }
}

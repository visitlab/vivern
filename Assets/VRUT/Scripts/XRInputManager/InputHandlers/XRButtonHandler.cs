﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    [CreateAssetMenu(fileName = "NewButtonHandler", menuName = "XR/VRUT/InputHandlers/ButtonHandler")]
    public class XRButtonHandler : XRInputHandler
    {
        public InputHelpers.Button button = InputHelpers.Button.None;

        public delegate void StateChange(XRController controller);
        public event StateChange OnButtonDown;
        public event StateChange OnButtonUp;
        public bool IsPressed { get; private set; } = false;

        public override void HandleState(XRController controller)
        {
            if (controller.inputDevice.IsPressed(button, out bool pressed, controller.axisToPressThreshold))
            {
                if (IsPressed != pressed)
                {
                    IsPressed = pressed;

                    if (pressed)
                    {
                        OnButtonDown?.Invoke(controller);
                    }
                    else
                    {
                        OnButtonUp?.Invoke(controller);
                    }
                }
            }
        }
    }
}

﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    public abstract class XRInputHandler : ScriptableObject
    {
        public abstract void HandleState(XRController controller);
    }
}

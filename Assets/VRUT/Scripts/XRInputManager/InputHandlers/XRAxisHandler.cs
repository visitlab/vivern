﻿using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    [CreateAssetMenu(fileName = "NewAxisHandler", menuName = "XR/VRUT/InputHandlers/AxisHandler")]
    public class XRAxisHandler : XRInputHandler, ISerializationCallbackReceiver
    {
        public enum Axis
        {
            None,
            Trigger,
            Grip
        }

        public delegate void ValueChange(XRController controller, float value);
        public event ValueChange OnValueChange;
        public delegate void StateChange(XRController controller);
        public bool IsClicked;
        public event StateChange OnClicked;
        public event StateChange OnClickReleased;
        public bool IsPressed;
        public event StateChange OnPressed;
        public event StateChange OnPressReleased;

        public Axis axis = Axis.None;

        private InputFeatureUsage<float> inputFeature;
        private float previousValue = .0f;

        public void OnBeforeSerialize() { }

        public void OnAfterDeserialize()
        {
            inputFeature = new InputFeatureUsage<float>(axis.ToString());
        }

        public override void HandleState(XRController controller)
        {
            float value = GetValue(controller);

            if (value != previousValue)
            {
                OnValueChange?.Invoke(controller, value);
                if (value == 1)
                {
                    IsClicked = true;
                    OnClicked?.Invoke(controller);
                }
                else if (previousValue == 1)
                {
                    IsClicked = false;
                    OnClickReleased?.Invoke(controller);
                }
                else if (previousValue == 0)
                {
                    IsPressed = true;
                    OnPressed?.Invoke(controller);
                }
                else if (value == 0)
                {
                    IsPressed = false;
                    OnPressReleased?.Invoke(controller);
                }
                previousValue = value;
            }
        }

        public float GetValue(XRController controller)
        {
            if (controller.inputDevice.TryGetFeatureValue(inputFeature, out float value))
            {
                return value;
            }
            return .0f;
        }
    }
}

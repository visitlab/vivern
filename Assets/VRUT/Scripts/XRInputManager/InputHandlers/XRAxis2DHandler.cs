﻿using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    [CreateAssetMenu(fileName = "NewAxis2DHandler", menuName = "XR/VRUT/InputHandlers/Axis2DHandler")]
    public class XRAxis2DHandler : XRInputHandler, ISerializationCallbackReceiver
    {
        public enum Axis2D
        {
            None,
            Primary2DAxis,
            Secondary2DAxis
        }

        public delegate void ValueChange(XRController controller, Vector2 value);
        public event ValueChange OnValueChange;
        public delegate void StateChange(XRController controller);
        public bool IsUsed;
        public event StateChange OnStartUsing;
        public event StateChange OnStopUsing;

        public Axis2D axis = Axis2D.None;

        private InputFeatureUsage<Vector2> inputFeature;
        private Vector2 previousValue = Vector2.zero;

        public void OnBeforeSerialize() { }

        public void OnAfterDeserialize()
        {
            inputFeature = new InputFeatureUsage<Vector2>(axis.ToString());
        }

        public override void HandleState(XRController controller)
        {
            Vector2 value = GetValue(controller);

            if (value != previousValue)
            {
                // Do not trigger the OnValueChange event on start using and on stop using
                if (previousValue == Vector2.zero)
                {
                    IsUsed = true;
                    OnStartUsing?.Invoke(controller);
                }
                else if (value == Vector2.zero)
                {
                    IsUsed = false;
                    OnStopUsing?.Invoke(controller);
                }
                else
                {
                    OnValueChange?.Invoke(controller, value);
                }
                previousValue = value;
            }
        }

        public Vector2 GetValue(XRController controller)
        {
            if (controller.inputDevice.TryGetFeatureValue(inputFeature, out Vector2 value))
            {
                return value;
            }
            return Vector2.zero;
        }
    }
}

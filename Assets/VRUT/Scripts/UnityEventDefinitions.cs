﻿using UnityEngine;
using UnityEngine.Events;

namespace VRUT
{
    [System.Serializable]
    public class UnityEventKeyCode : UnityEvent<KeyCode> { }

    [System.Serializable]
    public class UnityEventString : UnityEvent<string> { }

    [System.Serializable]
    public class UnityEventXRController : UnityEvent<UnityEngine.XR.Interaction.Toolkit.XRController> { }

    [System.Serializable]
    public class UnityEventFloat : UnityEvent<float> { }

    [System.Serializable]
    public class UnityEventBool : UnityEvent<bool> { }

    [System.Serializable]
    public class UnityEventVector3 : UnityEvent<Vector3> { }

    [System.Serializable]
    public class UnityEventPhysButInteractor : UnityEvent<PhysicalButtonInteractor> { }
}

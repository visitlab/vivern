﻿using UnityEngine;

namespace VRUT
{
    public static class KeyCodeToStringConverter
    {
        public static string Convert(KeyCode keyCode)
        {
            if (keyCode >= KeyCode.Alpha0 && keyCode <= KeyCode.Alpha9)
            {
                return (keyCode - KeyCode.Alpha0).ToString();
            }
            else if (keyCode >= KeyCode.Keypad0 && keyCode <= KeyCode.Keypad9)
            {
                return (keyCode - KeyCode.Keypad0).ToString();
            }
            else if (keyCode == KeyCode.Return)
            {
                return "Enter";
            }

            return keyCode.ToString();
        }
    }
}

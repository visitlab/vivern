﻿using TMPro;
using UnityEngine;

namespace VRUT
{
    public class PhysicalKeyboard : MonoBehaviour
    {
        [SerializeField]
        private TMP_InputField inputField;

        [SerializeField]
        private UnityEventString EnterPressed;

        private void Start()
        {
            var keys = gameObject.GetComponentsInChildren<PhysicalKeyboardButton>();
            for (int i = 0; i < keys.Length; ++i)
            {
                keys[i].KeyPressed.AddListener(OnKeyPressed);
            }
        }

        private void OnKeyPressed(KeyCode keycode)
        {
            if (keycode == KeyCode.Backspace)
            {
                inputField.text = inputField.text.Remove(inputField.text.Length - 1);
            }
            else if (keycode == KeyCode.Space)
            {
                inputField.text += " ";
            }
            else if (keycode == KeyCode.Return)
            {
                EnterPressed?.Invoke(inputField.text.Trim());
                inputField.text = string.Empty;
            }
            else
            {
                inputField.text += KeyCodeToStringConverter.Convert(keycode);
            }
        }
    }
}

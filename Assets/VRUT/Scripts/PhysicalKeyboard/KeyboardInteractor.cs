﻿using UnityEngine;

namespace VRUT
{
    [RequireComponent(typeof(Collider))]
    [System.Obsolete("Keyboard interactor was replaced by more general PhysicalButtonInteractor " +
        " and will be removed in a near future.")]
    public class KeyboardInteractor : PhysicalButtonInteractor
    {
        protected override void Start()
        {
            base.Start();
        }
    }
}

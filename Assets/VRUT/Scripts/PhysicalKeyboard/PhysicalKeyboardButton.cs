﻿using UnityEngine;

namespace VRUT
{
    [ExecuteAlways]
    public class PhysicalKeyboardButton : PhysicalButton
    {
        [SerializeField]
        private KeyCode button;

        public UnityEventKeyCode KeyPressed;

        protected override void Start()
        {
            base.Start();

            textToChange.text = KeyCodeToStringConverter.Convert(button);
        }

        protected override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
        }

        protected override void OnTriggerExit(Collider other)
        {
            base.OnTriggerExit(other);
        }

        public override void OnFirstTouchStart(PhysicalButtonInteractor pbi)
        {
            base.OnFirstTouchStart(pbi);
            KeyPressed?.Invoke(button);
        }

        public override void OnLastTouchEnd()
        {
            base.OnLastTouchEnd();
        }
    }
}

﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    [RequireComponent(typeof(XRInteractorLineVisual))]
    public class XRLineVisualButtonStateController : MonoBehaviour
    {
        [SerializeField]
        private XRButtonHandler stateButton;

        private XRInteractorLineVisual xrInterLineVisual;

        private void Awake()
        {
            xrInterLineVisual = GetComponent<XRInteractorLineVisual>();
            xrInterLineVisual.enabled = false;
        }

        private void OnEnable()
        {
            stateButton.OnButtonDown += EnableVisual;
            stateButton.OnButtonUp += DisableVisual;
        }

        private void OnDisable()
        {
            stateButton.OnButtonDown -= EnableVisual;
            stateButton.OnButtonUp -= DisableVisual;
        }

        private void EnableVisual(XRController controller)
        {
            xrInterLineVisual.enabled = true;
        }

        private void DisableVisual(XRController controller)
        {
            xrInterLineVisual.enabled = false;
        }
    }
}

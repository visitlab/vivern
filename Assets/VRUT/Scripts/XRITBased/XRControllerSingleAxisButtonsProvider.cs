﻿using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    public class XRControllerSingleAxisButtonsProvider : MonoBehaviour
    {
        [System.Serializable]
        protected class SingleAxisFloatButtonElement
        {
            public string axisName;

            public float pressedThreshold;

            public UnityEventXRController onButtonUp;

            public UnityEventXRController onButtonDown;

            public UnityEventFloat onAxisValueUpdated;

            public bool IsPressed { get; set; } = false;
        }

        [System.Serializable]
        protected class SingleAxisBoolButtonElement
        {
            public string axisName;

            public UnityEventXRController onButtonUp;

            public UnityEventXRController onButtonDown;

            public UnityEventBool onAxisValueUpdated;

            public bool IsPressed { get; set; } = false;
        }

        [SerializeField]
        private SingleAxisFloatButtonElement[] floatButtonsActions;

        [SerializeField]
        private SingleAxisBoolButtonElement[] boolButtonsActions;

        [SerializeField]
        private XRController overrideController;

        public XRController XRController { get; protected set; }

        private void Start()
        {
            XRController = overrideController ?? GetComponentInParent<XRController>();
        }

        private void Update()
        {
            if (!XRController.inputDevice.isValid) { return; }

            for (int i = 0; i < floatButtonsActions.Length; ++i)
            {
                float value = 0.0f;
                if (XRController.inputDevice.TryGetFeatureValue(new InputFeatureUsage<float>(floatButtonsActions[i].axisName), out value))
                {
                    floatButtonsActions[i].onAxisValueUpdated.Invoke(value);

                    if (value >= floatButtonsActions[i].pressedThreshold && !floatButtonsActions[i].IsPressed)
                    {
                        floatButtonsActions[i].IsPressed = true;
                        floatButtonsActions[i].onButtonDown.Invoke(XRController);
                    }
                    else if (value < floatButtonsActions[i].pressedThreshold && floatButtonsActions[i].IsPressed)
                    {
                        floatButtonsActions[i].IsPressed = false;
                        floatButtonsActions[i].onButtonUp.Invoke(XRController);
                    }
                }
            }

            for (int i = 0; i < boolButtonsActions.Length; ++i)
            {
                bool value = false;
                if (XRController.inputDevice.TryGetFeatureValue(new InputFeatureUsage<bool>(boolButtonsActions[i].axisName), out value))
                {
                    boolButtonsActions[i].onAxisValueUpdated.Invoke(value);

                    if (value && !boolButtonsActions[i].IsPressed)
                    {
                        boolButtonsActions[i].IsPressed = true;
                        boolButtonsActions[i].onButtonDown.Invoke(XRController);
                    }
                    else if (!value && boolButtonsActions[i].IsPressed)
                    {
                        boolButtonsActions[i].IsPressed = false;
                        boolButtonsActions[i].onButtonUp.Invoke(XRController);
                    }
                }
            }
        }

        public bool IsSingleAxisFloatButtonPressed(string buttonName, float pressedThreshold = 0.1f)
        {
            float value = 0.0f;
            return XRController.inputDevice.TryGetFeatureValue(new InputFeatureUsage<float>(buttonName), out value) && value >= pressedThreshold;
        }

        public bool IsSingleAxisBoolButtonPressed(string buttonName)
        {
            bool value = false;
            return XRController.inputDevice.TryGetFeatureValue(new InputFeatureUsage<bool>(buttonName), out value) && value;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    public class XRDeviceManager : MonoBehaviour
    {
        public static XRDeviceManager Instance { get; protected set; }

        [SerializeField]
        private XRRig rig;

        [SerializeField]
        private Camera headset;

        [SerializeField]
        private XRController leftController;

        [SerializeField]
        private XRController rightController;

        public XRRig Rig => rig;

        public Camera Headset => headset;

        public XRController LeftController => leftController;

        public XRController RightController => rightController;

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        protected virtual void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }
    }
}

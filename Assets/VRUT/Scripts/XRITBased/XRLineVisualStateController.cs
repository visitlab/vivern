﻿using System;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    [Obsolete("Use XRInputManager and XRLineVisualButtonStateController instead.")]
    [RequireComponent(typeof(XRController), typeof(XRInteractorLineVisual))]
    public class XRLineVisualStateController : MonoBehaviour
    {
        [SerializeField]
        private string stateButtonName = "Trigger";

        private XRController xrController;
        private XRInteractorLineVisual xrInterLineVisual;

        private void Start()
        {
            xrController = GetComponent<XRController>();
            xrInterLineVisual = GetComponent<XRInteractorLineVisual>();
        }

        private void Update()
        {
            float value = 0.0f;
            if (xrController.inputDevice.isValid &&
                xrController.inputDevice.TryGetFeatureValue(new InputFeatureUsage<float>(stateButtonName), out value) &&
                value >= xrController.axisToPressThreshold)
            {
                xrInterLineVisual.enabled = true;
            }
            else
            {
                xrInterLineVisual.enabled = false;
            }
        }
    }
}

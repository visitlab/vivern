﻿using TMPro;
using UnityEngine;

namespace VRUT
{
    public class PhysicalButton : MonoBehaviour
    {
        [SerializeField]
        protected string overrideLabelText = "";

        [SerializeField]
        protected TMP_Text textToChange;

        [SerializeField]
        protected GameObject visualRepresentation;

        [SerializeField]
        protected MeshRenderer backgroundObject;

        [SerializeField]
        protected MeshRenderer foregroundObject;

        [SerializeField]
        protected Vector3 pressDepth = new Vector3(0.0f, 0.1f, 0.0f);

        public UnityEngine.Events.UnityEvent Pressed;

        public UnityEventPhysButInteractor PressedByInteractor;

        protected bool isPressed = false;

        protected virtual void Start()
        {
            if (overrideLabelText.Length > 0)
            {
                textToChange.text = overrideLabelText;
            }
        }

        protected virtual void OnDisable()
        {
            if (isPressed)
            {
                OnLastTouchEnd();
            }
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            var pbi = other.GetComponent<PhysicalButtonInteractor>();
            if (pbi != null)
            {
                OnFirstTouchStart(pbi);
            }
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<PhysicalButtonInteractor>() != null && isPressed)
            {
                OnLastTouchEnd();
            }
        }

        public virtual void SetLabel(string text)
        {
            textToChange.text = text;
        }

        public virtual void SetBackgroundColor(Color color)
        {
            backgroundObject.material.SetColor("_BaseColor", color);
        }

        public virtual void SetForegroundColor(Color color)
        {
            foregroundObject.material.SetColor("_BaseColor", color);
        }

        public virtual void OnFirstTouchStart(PhysicalButtonInteractor pbi)
        {
            Pressed?.Invoke();
            PressedByInteractor?.Invoke(pbi);
            visualRepresentation.transform.Translate(-pressDepth.x, -pressDepth.y, -pressDepth.z, Space.Self);
            isPressed = true;
        }

        public virtual void OnLastTouchEnd()
        {
            visualRepresentation.transform.Translate(pressDepth.x, pressDepth.y, pressDepth.z, Space.Self);
            isPressed = false;
        }
    }
}

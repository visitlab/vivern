﻿using UnityEngine;

namespace VRUT
{
    [RequireComponent(typeof(Rigidbody))]
    public class PhysicalButtonInteractor : MonoBehaviour
    {
        protected virtual void Start()
        {
            var colliders = GetComponentsInChildren<Collider>();

            bool isAnyTrigger = false;
            for (int i = 0; i < colliders.Length; ++i)
            {
                isAnyTrigger |= colliders[i].isTrigger;
            }

            if (!isAnyTrigger)
            {
                Debug.Log("The interactor must contain a trigger colider!");
            }
        }
    }
}

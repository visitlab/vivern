﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace VRUT
{
    public class PhysicalCheckbox : PhysicalButton
    {
        public UnityEventBool ValueChanged;

        [SerializeField]
        protected TMP_Text checkmarkTextField;

        [SerializeField]
        protected string falseText = string.Empty;

        [SerializeField]
        protected string trueText = "X";

        [SerializeField]
        protected bool initialValue = false;

        protected bool _currentValue;
        public bool CurrentValue
        {
            get => _currentValue;
            protected set
            {
                if(_currentValue == value) { return; }

                SetNewValue(value);
            }
        }

        protected override void Start()
        {
            base.Start();

            SetNewValue(initialValue);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
        }

        protected override void OnTriggerExit(Collider other)
        {
            base.OnTriggerExit(other);
        }

        private void SetNewValue(bool newValue)
        {
            _currentValue = newValue;
            checkmarkTextField.text = _currentValue ? trueText : falseText;
            ValueChanged?.Invoke(_currentValue);
        }

        public override void OnFirstTouchStart(PhysicalButtonInteractor pbi)
        {
            base.OnFirstTouchStart(pbi);
            CurrentValue = !CurrentValue;
        }
    }
}

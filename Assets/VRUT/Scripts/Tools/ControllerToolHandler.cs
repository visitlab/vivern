﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRUT
{
    public class ControllerToolHandler : VRUT.PhysicalButtonInteractor
    {
        [SerializeField]
        private TMPro.TMP_Text activeToolNameTooltip;

        [SerializeField]
        private UnityEngine.XR.Interaction.Toolkit.XRController xrController;

        [SerializeField]
        private bool inputActionsEnabledOnlyIfNoTool = true;

        public UnityEngine.XR.Interaction.Toolkit.XRController XRController => xrController;

        private IControllerTool _currentlyActiveTool;
        public IControllerTool CurrentlyActiveTool
        {
            get => _currentlyActiveTool;
            set
            {
                if(_currentlyActiveTool != value)
                {
                    if (_currentlyActiveTool != null)
                    {
                        _currentlyActiveTool.Disable();
                        _currentlyActiveTool.ModeNameChanged -= OnToolModeNameChanged;
                    }

                    if (value != null)
                    {
                        value.Enable();
                        value.ModeNameChanged += OnToolModeNameChanged;
                    }
                }

                _currentlyActiveTool = value;

                if(inputActionsEnabledOnlyIfNoTool)
                {
                    xrController.enableInputActions = _currentlyActiveTool == null;
                }

                UpdateActiveToolNameTooltip();
            }
        }

        protected override void Start()
        {
            base.Start();
            UpdateActiveToolNameTooltip();
        }

        protected virtual void Update()
        {
            CurrentlyActiveTool?.Update();
        }

        // Six below On*Button* functions exist just to make these callbacks visible in Unity Editor
        // since the version with enum is not working :(
        public void OnPrimaryButtonPressed()
        {
            OnButtonPressed(ControllerToolActionType.Primary);
        }

        public void OnSecondaryButtonPressed()
        {
            OnButtonPressed(ControllerToolActionType.Secondary);
        }

        public void OnTertiaryButtonPressed()
        {
            OnButtonPressed(ControllerToolActionType.Tertiary);
        }

        public void OnPrimaryButtonReleased()
        {
            OnButtonReleased(ControllerToolActionType.Primary);
        }

        public void OnSecondaryButtonReleased()
        {
            OnButtonReleased(ControllerToolActionType.Secondary);
        }

        public void OnTertiaryButtonReleased()
        {
            OnButtonReleased(ControllerToolActionType.Tertiary);
        }

        public void OnButtonPressed(ControllerToolActionType actionType)
        {
            CurrentlyActiveTool?.StartAction(actionType);
        }

        public void OnButtonReleased(ControllerToolActionType actionType)
        {
            CurrentlyActiveTool?.StopAction(actionType);
        }

        private void OnToolModeNameChanged(IControllerTool tool)
        {
            UpdateActiveToolNameTooltip();
        }

        private void UpdateActiveToolNameTooltip()
        {
            activeToolNameTooltip.text = CurrentlyActiveTool != null ?
                (CurrentlyActiveTool.ToolName + " " + CurrentlyActiveTool.ToolModeName) :
                string.Empty;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRUT
{
    public class TeleportControllerToolCreator : AbstractControllerToolCreator
    {
        [SerializeField]
        private GameObject leftControllerTeleport;

        [SerializeField]
        private GameObject rightControllerTeleport;

        protected override IControllerTool CreateNewTool(ControllerToolHandler cth)
        {
            return new TeleportControllerTool(CreatedToolCategory, cth.XRController, createdToolName,
                cth.XRController == VRUT.XRDeviceManager.Instance.LeftController ? leftControllerTeleport : rightControllerTeleport
                );
        }
    }
}

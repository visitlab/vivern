﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VRUT
{
    public class ControllerToolsMenu : MonoBehaviour
    {
        [System.Serializable]
        public class ToolCategoryToColor
        {
            public string category;
            public Color color;
        }

        [SerializeField]
        private GameObject menuButtonPrefab;

        [SerializeField]
        private int menuColumns = 3;

        [SerializeField]
        private bool addNoToolButton = true;

        [SerializeField]
        private Vector2 menuStep = new Vector2(0.11f, 0.06f);

        [SerializeField]
        private Vector3 positionOffset = new Vector3(0.11f, 0.0f, 0.05f);

        [SerializeField]
        private Vector3 rotationOffset = new Vector3(-20.0f, 180.0f, 0.0f);

        [SerializeField]
        private AbstractControllerToolCreator[] availableTools;

        [SerializeField]
        private ToolCategoryToColor[] toolCategoriesToColor;

        private Dictionary<ControllerToolHandler, AbstractControllerToolCreator> cthToToolCreators =
            new Dictionary<ControllerToolHandler, AbstractControllerToolCreator>();

        private void Start()
        {
            SpawnButtons();
            gameObject.SetActive(false);
        }

        public void ToggleState(UnityEngine.XR.Interaction.Toolkit.XRController callingController)
        {
            gameObject.SetActive(!gameObject.activeSelf);

            if (gameObject.activeSelf)
            {
                transform.SetParent(callingController.transform);

                transform.localPosition = positionOffset;
                transform.localRotation = Quaternion.Euler(rotationOffset);

                transform.SetParent(null);
            }
        }

        private void SpawnButtons()
        {
            int row = 0, col = 0;

            for (int i = 0; i < availableTools.Length; ++i)
            {
                var newButton = Instantiate(menuButtonPrefab, transform);
                newButton.transform.localPosition = new Vector3(col * menuStep.x, row * menuStep.y, 0.0f);

                var newButtonComp = newButton.GetComponent<VRUT.PhysicalButton>();
                newButtonComp.SetBackgroundColor(toolCategoriesToColor.First(x => x.category == availableTools[i].CreatedToolCategory).color);
                newButtonComp.SetLabel(availableTools[i].CreatedToolName);
                int idx = i; // HACK Intentional
                newButtonComp.PressedByInteractor.AddListener((VRUT.PhysicalButtonInteractor pbi) => { ActivateTool(availableTools[idx], pbi); });

                ++col;

                if (col >= menuColumns)
                {
                    col = 0;
                    ++row;
                }
            }

            if (addNoToolButton)
            {
                var newButton = Instantiate(menuButtonPrefab, transform);
                newButton.transform.localPosition = new Vector3(col * menuStep.x, row * menuStep.y, 0.0f);

                var newButtonComp = newButton.GetComponent<VRUT.PhysicalButton>();
                newButtonComp.SetLabel("No tool");
                newButtonComp.PressedByInteractor.AddListener((VRUT.PhysicalButtonInteractor pbi) => { ActivateTool(null, pbi); });
            }
        }

        private void ActivateTool(AbstractControllerToolCreator selectedToolCreator, VRUT.PhysicalButtonInteractor pbi)
        {
            var cth = pbi as ControllerToolHandler;

            if(cth != null)
            {
                cth.CurrentlyActiveTool = selectedToolCreator?.CreateTool(cth);
            }
        }
    }
}

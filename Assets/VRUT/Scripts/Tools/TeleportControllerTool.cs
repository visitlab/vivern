﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    public class TeleportControllerTool : IControllerTool
    {
        public string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public string ToolModeName => string.Empty;

        public event Action<IControllerTool> ModeNameChanged;

        private GameObject controlledTeleportProvider;

        public TeleportControllerTool(string category, XRController xrController, string toolName,
            GameObject teleporterToControl)
        {
            Category = category;
            XRController = xrController;
            ToolName = toolName;
            controlledTeleportProvider = teleporterToControl;
        }

        public void Disable()
        {
            controlledTeleportProvider.SetActive(false);
        }

        public void Enable()
        {
            controlledTeleportProvider.SetActive(true);
        }

        public void StartAction(ControllerToolActionType actionType)
        { }

        public void StopAction(ControllerToolActionType actionType)
        { }

        public void Update()
        { }
    }
}

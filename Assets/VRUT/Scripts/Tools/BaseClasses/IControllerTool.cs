﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRUT
{   
    public enum ControllerToolActionType
    {
        Primary,
        Secondary,
        Tertiary
    }

    public interface IControllerTool
    {
        string Category { get; }

        UnityEngine.XR.Interaction.Toolkit.XRController XRController { get; }

        string ToolName { get; }

        string ToolModeName { get; }

        event System.Action<IControllerTool> ModeNameChanged;

        void Enable();

        void Update();

        void Disable();

        void StartAction(ControllerToolActionType actionType);

        void StopAction(ControllerToolActionType actionType);
    }
}

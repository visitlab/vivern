﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRUT
{
    public abstract class AbstractControllerToolCreator : MonoBehaviour
    {
        [SerializeField]
        protected string createdToolName = "Tool";

        [SerializeField]
        protected string createdToolCategory = "None";

        public string CreatedToolName => createdToolName;

        public string CreatedToolCategory => createdToolCategory;

        protected Dictionary<ControllerToolHandler, IControllerTool> cthToTool =
             new Dictionary<ControllerToolHandler, IControllerTool>();

        /// <summary>
        /// Creates a new instance of a tool for the given controller
        /// or retrieves already existing instance belonging to this controller
        /// </summary>
        public virtual IControllerTool CreateTool(ControllerToolHandler cth)
        {
            if(cthToTool.TryGetValue(cth, out IControllerTool existingTool))
            {
                return existingTool;
            }

            var newTool = CreateNewTool(cth);
            cthToTool.Add(cth, newTool);
            return newTool;
        }

        protected abstract IControllerTool CreateNewTool(ControllerToolHandler cth);
    }
}

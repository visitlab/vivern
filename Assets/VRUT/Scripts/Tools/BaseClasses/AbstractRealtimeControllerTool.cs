﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    public abstract class AbstractRealtimeControllerTool : IControllerTool
    {
        public abstract string Category { get; }

        public XRController XRController { get; protected set; }

        public string ToolName { get; protected set; }

        public abstract string ToolModeName { get; }

        public event Action<IControllerTool> ModeNameChanged;

        protected HashSet<ControllerToolActionType> activeControllerActions =
            new HashSet<ControllerToolActionType>();

        public virtual void Disable()
        {
            activeControllerActions.Clear();
        }

        public virtual void Enable()
        { }

        public virtual void StartAction(ControllerToolActionType actionType)
        {
            activeControllerActions.Add(actionType);
        }

        public virtual void StopAction(ControllerToolActionType actionType)
        {
            activeControllerActions.Remove(actionType);
        }

        public virtual void Update()
        {
            foreach(var action in activeControllerActions)
            {
                UpdateAction(action);
            }
        }

        // Called only when the appropriate action is active, i.e., button is presed for example
        protected abstract void UpdateAction(ControllerToolActionType actionType);

        protected void InvokeModeNameChangedEvent(IControllerTool invoker)
        {
            ModeNameChanged?.Invoke(invoker);
        }
    }
}

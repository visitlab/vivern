﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRUT
{
    public class SwipeDetector : MonoBehaviour
    {
        [SerializeField]
        private XRAxis2DHandler primary2DAxis;

        [SerializeField]
        [Range(1f, 40)]
        private float minSwipeVelocity = 15f;

        public UnityEvent OnSwipeRight = new UnityEvent();
        public UnityEvent OnSwipeLeft = new UnityEvent();
        public UnityEvent OnSwipeUp = new UnityEvent();
        public UnityEvent OnSwipeDown = new UnityEvent();

        private bool initialized = false;
        private Vector2 lastPosition;
        private float lastTime;
        private bool swiped;

        private void OnEnable()
        {
            primary2DAxis.OnValueChange += Primary2DAxis_OnValueChange;
            primary2DAxis.OnStartUsing += Primary2DAxisTouched_OnButtonDown;
        }

        private void OnDisable()
        {
            primary2DAxis.OnValueChange -= Primary2DAxis_OnValueChange;
            primary2DAxis.OnStartUsing -= Primary2DAxisTouched_OnButtonDown;
        }

        private void Primary2DAxisTouched_OnButtonDown(XRController controller)
        {
            swiped = false;
            initialized = false;
        }

        private void Primary2DAxis_OnValueChange(XRController controller, Vector2 value)
        {
            if (!swiped)
            {
                if (!initialized)
                {
                    lastPosition = value;
                    lastTime = Time.time;
                    initialized = true;
                }
                else
                {
                    swiped = CheckSwipe(lastPosition, lastTime, value, Time.time);
                    lastPosition = value;
                    lastTime = Time.time;
                }
            }
        }

        private bool CheckSwipe(Vector2 firstPosition, float firstTime, Vector2 secondPosition, float secondTime)
        {
            Vector2 deltaPosition = secondPosition - firstPosition;
            float timeDelta = secondTime - firstTime;
            float velocity = deltaPosition.magnitude / timeDelta;
            if (velocity > minSwipeVelocity)
            {
                // Most significant
                if (Math.Abs(deltaPosition.x) > Math.Abs(deltaPosition.y))
                {
                    // X delta is increasing -> going up in x axis
                    if (deltaPosition.x > 0)
                    {
                        OnSwipeRight?.Invoke();
                    }
                    // X delta is decreasing -> going down in x axis
                    else
                    {
                        OnSwipeLeft?.Invoke();
                    }
                }
                else
                {
                    if (deltaPosition.y > 0)
                    {
                        OnSwipeUp?.Invoke();
                    }
                    else
                    {
                        OnSwipeDown?.Invoke();
                    }
                }
                return true;
            }
            return false;
        }
    }
}

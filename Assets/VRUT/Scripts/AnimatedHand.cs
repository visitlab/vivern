﻿using System;
using UnityEngine;

namespace VRUT
{
    [RequireComponent(typeof(Animator))]
    public class AnimatedHand : MonoBehaviour
    {
        private Animator animator;
        private float grabAnimProgress = 0.0f;

        [SerializeField]
        private string defaultAnimStateName = "GrabAnimation";

        [SerializeField]
        private string interactionShakeAnimStateName = "InteractionShake";

        private void Awake()
        {
            animator = GetComponent<Animator>();
            animator.speed = 0;
        }

        private void Update()
        {
            animator.Play(defaultAnimStateName, 0, grabAnimProgress);
            // Modulus works with floats in C# so the code below 
            // is a simple way to get the decimal part, i.e., [0.0, 1.0), of the number
            animator.Play(interactionShakeAnimStateName, 1, Time.time % 1);
        }

        public void SetGrabProgress(float value)
        {
            // Value below set to 0.999f instead of 1.0f because
            // the animation returned to default state with value of 1.0
            grabAnimProgress = Mathf.Clamp(value, 0.0f, 0.999f);
        }

        public void SetInteractionShakeState(bool isEnabled)
        {
            animator.SetLayerWeight(1, Convert.ToInt32(isEnabled));
        }
    }
}
